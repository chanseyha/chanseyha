-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.24-log - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.2.0.4961
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table sas_master.1_group_totals
CREATE TABLE IF NOT EXISTS `1_group_totals` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `location_id` int(11) NOT NULL DEFAULT '0',
  `location_group_id` int(11) NOT NULL DEFAULT '0',
  `transaction_detail_id` int(11) DEFAULT NULL,
  `total_qty` decimal(15,3) DEFAULT '0.000',
  `total_order` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`product_id`,`location_id`,`location_group_id`,`lots_number`,`expired_date`),
  KEY `index_keys` (`product_id`,`location_id`,`location_group_id`,`lots_number`,`expired_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.1_group_totals: ~0 rows (approximately)
/*!40000 ALTER TABLE `1_group_totals` DISABLE KEYS */;
/*!40000 ALTER TABLE `1_group_totals` ENABLE KEYS */;


-- Dumping structure for table sas_master.1_group_total_details
CREATE TABLE IF NOT EXISTS `1_group_total_details` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `location_group_id` int(11) NOT NULL DEFAULT '0',
  `transaction_detail_id` int(11) DEFAULT NULL,
  `total_cycle` decimal(15,3) DEFAULT '0.000',
  `total_so` decimal(15,3) DEFAULT '0.000',
  `total_so_free` decimal(15,3) DEFAULT '0.000',
  `total_pos` decimal(15,3) DEFAULT '0.000',
  `total_pos_free` decimal(15,3) DEFAULT '0.000',
  `total_pb` decimal(15,3) DEFAULT '0.000',
  `total_pbc` decimal(15,3) DEFAULT '0.000',
  `total_cm` decimal(15,3) DEFAULT '0.000',
  `total_cm_free` decimal(15,3) DEFAULT '0.000',
  `total_to_in` decimal(15,3) DEFAULT '0.000',
  `total_to_out` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_order` decimal(15,3) DEFAULT '0.000',
  `date` date NOT NULL,
  PRIMARY KEY (`product_id`,`location_group_id`,`date`),
  KEY `index_key` (`product_id`,`location_group_id`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.1_group_total_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `1_group_total_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `1_group_total_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.1_inventories
CREATE TABLE IF NOT EXISTS `1_inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `consignment_id` int(11) DEFAULT NULL,
  `consignment_return_id` int(11) DEFAULT NULL,
  `vendor_consignment_id` int(11) DEFAULT NULL,
  `vendor_consignment_return_id` int(11) DEFAULT NULL,
  `cycle_product_id` int(11) DEFAULT NULL,
  `cycle_product_detail_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `point_of_sales_id` int(11) DEFAULT NULL,
  `credit_memo_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `purchase_return_id` int(11) DEFAULT NULL,
  `transfer_order_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `location_group_id` int(11) NOT NULL,
  `qty` decimal(15,3) NOT NULL,
  `unit_cost` decimal(18,9) DEFAULT '0.000000000',
  `unit_price` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `date_expired` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` bigint(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `lots_number` (`lots_number`),
  KEY `qty` (`qty`),
  KEY `location_group_id` (`location_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.1_inventories: ~0 rows (approximately)
/*!40000 ALTER TABLE `1_inventories` DISABLE KEYS */;
/*!40000 ALTER TABLE `1_inventories` ENABLE KEYS */;


-- Dumping structure for table sas_master.1_inventory_totals
CREATE TABLE IF NOT EXISTS `1_inventory_totals` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `total_qty` decimal(15,3) DEFAULT '0.000',
  `total_order` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`product_id`,`location_id`,`lots_number`,`expired_date`),
  KEY `index_keys` (`product_id`,`location_id`,`lots_number`,`expired_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.1_inventory_totals: ~0 rows (approximately)
/*!40000 ALTER TABLE `1_inventory_totals` DISABLE KEYS */;
/*!40000 ALTER TABLE `1_inventory_totals` ENABLE KEYS */;


-- Dumping structure for table sas_master.1_inventory_total_details
CREATE TABLE IF NOT EXISTS `1_inventory_total_details` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `total_cycle` decimal(15,3) DEFAULT '0.000',
  `total_so` decimal(15,3) DEFAULT '0.000',
  `total_pos` decimal(15,3) DEFAULT '0.000',
  `total_pb` decimal(15,3) DEFAULT '0.000',
  `total_pbc` decimal(15,3) DEFAULT '0.000',
  `total_cm` decimal(15,3) DEFAULT '0.000',
  `total_to_in` decimal(15,3) DEFAULT '0.000',
  `total_to_out` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_order` decimal(15,3) DEFAULT '0.000',
  `date` date NOT NULL,
  PRIMARY KEY (`product_id`,`location_id`,`lots_number`,`expired_date`,`date`),
  KEY `index_keys` (`product_id`,`location_id`,`lots_number`,`expired_date`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.1_inventory_total_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `1_inventory_total_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `1_inventory_total_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `account_type_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;


-- Dumping structure for table sas_master.account_closing_dates
CREATE TABLE IF NOT EXISTS `account_closing_dates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.account_closing_dates: ~1 rows (approximately)
/*!40000 ALTER TABLE `account_closing_dates` DISABLE KEYS */;
INSERT INTO `account_closing_dates` (`id`, `date`, `created`, `created_by`) VALUES
	(1, '2017-01-31', '2018-09-10 17:25:14', 1);
/*!40000 ALTER TABLE `account_closing_dates` ENABLE KEYS */;


-- Dumping structure for table sas_master.account_closing_date_histories
CREATE TABLE IF NOT EXISTS `account_closing_date_histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.account_closing_date_histories: ~0 rows (approximately)
/*!40000 ALTER TABLE `account_closing_date_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_closing_date_histories` ENABLE KEYS */;


-- Dumping structure for table sas_master.account_types
CREATE TABLE IF NOT EXISTS `account_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `group_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.account_types: ~24 rows (approximately)
/*!40000 ALTER TABLE `account_types` DISABLE KEYS */;
INSERT INTO `account_types` (`id`, `name`, `chart_account_id`, `ordering`, `group_by`, `sort`, `status`) VALUES
	(1, 'Inventory Asset Account', 3, 1, 'Inventory', 11, 1),
	(2, 'COGS Account', 11, 2, 'Inventory', 12, 1),
	(3, 'Inventory Adjustment Account', 3, 3, 'Inventory', 13, 1),
	(4, 'Scrapped Inventory Account', NULL, 4, NULL, NULL, 0),
	(5, 'Internal Use Account', NULL, 5, NULL, NULL, 0),
	(6, 'Cash Payment', 1, 6, 'Sales', 36, 1),
	(7, 'Accounts Receivable', 2, 7, 'Sales', 31, 1),
	(8, 'Sales Income', 10, 8, 'Sales', 32, 1),
	(9, 'Service Account', 10, 15, 'Sales', 35, 1),
	(10, 'Miscellaneous Account', 100, 16, NULL, NULL, 0),
	(11, 'Sales Discount', 12, 9, 'Sales', 34, 1),
	(12, 'Sales Change', 93, 11, NULL, NULL, 0),
	(13, 'Cash Pay Bill', 1, 12, 'Purchase', 23, 1),
	(14, 'Accounts Payable', 6, 13, 'Purchase', 21, 1),
	(15, 'Purchase Discount', 12, 14, 'Purchase', 22, 1),
	(17, 'Sales Markup', 10, 10, 'Sales', 33, 1),
	(18, 'Landing Costs', NULL, 17, NULL, NULL, 0),
	(19, 'Cash Expense', 1, 18, 'Purchase', 25, 1),
	(20, 'Unearned Revenue', 105, 19, NULL, NULL, 0),
	(21, 'Unearned Discount', 106, 20, NULL, NULL, 0),
	(22, 'Account Receivable (Landed Cost)', 107, 7, NULL, NULL, 0),
	(23, 'Cash Deposit', 1, 21, 'Sales', 38, 1),
	(24, 'Paybill Discount', 13, 18, 'Purchase', 24, 1),
	(25, 'Payment Discount', 14, 6, 'Sales', 37, 1);
/*!40000 ALTER TABLE `account_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.ap_agings
CREATE TABLE IF NOT EXISTS `ap_agings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`vendor_id`),
  KEY `searchs` (`date`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.ap_agings: ~0 rows (approximately)
/*!40000 ALTER TABLE `ap_agings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ap_agings` ENABLE KEYS */;


-- Dumping structure for table sas_master.ap_aging_details
CREATE TABLE IF NOT EXISTS `ap_aging_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ap_aging_id` bigint(20) DEFAULT NULL,
  `general_ledger_id` bigint(20) DEFAULT NULL,
  `vendor_id` bigint(20) DEFAULT NULL,
  `amount_due` decimal(15,3) DEFAULT NULL,
  `paid` decimal(15,3) DEFAULT NULL,
  `discount` decimal(15,3) DEFAULT NULL,
  `balance` decimal(15,3) DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `filters` (`ap_aging_id`,`general_ledger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.ap_aging_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `ap_aging_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `ap_aging_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.ar_agings
CREATE TABLE IF NOT EXISTS `ar_agings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `cgroup_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `egroup_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`cgroup_id`,`customer_id`,`egroup_id`,`employee_id`),
  KEY `searchs` (`date`,`reference`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.ar_agings: ~0 rows (approximately)
/*!40000 ALTER TABLE `ar_agings` DISABLE KEYS */;
/*!40000 ALTER TABLE `ar_agings` ENABLE KEYS */;


-- Dumping structure for table sas_master.ar_aging_details
CREATE TABLE IF NOT EXISTS `ar_aging_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ar_aging_id` bigint(20) DEFAULT NULL,
  `general_ledger_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `amount_due` decimal(15,3) DEFAULT NULL,
  `paid` decimal(15,3) DEFAULT NULL,
  `discount` decimal(15,3) DEFAULT NULL,
  `balance` decimal(15,3) DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `filters` (`ar_aging_id`,`general_ledger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.ar_aging_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `ar_aging_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `ar_aging_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.branches
CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_type_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_other` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `address_other` text COLLATE utf8_unicode_ci,
  `currency_center_id` int(11) DEFAULT NULL,
  `pos_currency_id` int(11) DEFAULT NULL,
  `work_start` time DEFAULT NULL,
  `work_end` time DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `act` tinyint(4) DEFAULT '1' COMMENT '1: Edit; 2:Update',
  `is_head` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `is_head` (`is_head`),
  KEY `searchs` (`name`,`is_active`,`company_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.branches: ~1 rows (approximately)
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` (`id`, `sys_code`, `company_id`, `branch_type_id`, `name`, `name_other`, `telephone`, `email_address`, `fax_number`, `long`, `lat`, `country_id`, `province_id`, `district_id`, `commune_id`, `village_id`, `address`, `address_other`, `currency_center_id`, `pos_currency_id`, `work_start`, `work_end`, `created`, `created_by`, `modified`, `modified_by`, `act`, `is_head`, `is_active`) VALUES
	(1, 'f526125d83312d9f29ec3b905bbb2060', 1, 1, 'Head Office', 'Head Office', '023998877', '', '', '', '', 36, NULL, NULL, NULL, NULL, 'Phnom Penh', 'Phnom Penh', 1, NULL, '08:00:00', '17:00:00', '2018-07-18 14:34:25', 1, '2018-07-18 14:34:25', NULL, 1, 1, 1);
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;


-- Dumping structure for table sas_master.branch_currencies
CREATE TABLE IF NOT EXISTS `branch_currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `rate_to_sell` decimal(15,9) DEFAULT '0.000000000',
  `rate_to_change` decimal(15,9) DEFAULT '0.000000000',
  `rate_purchase` decimal(15,9) DEFAULT '0.000000000',
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_pos_default` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_searchs` (`branch_id`,`currency_center_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.branch_currencies: ~0 rows (approximately)
/*!40000 ALTER TABLE `branch_currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `branch_currencies` ENABLE KEYS */;


-- Dumping structure for table sas_master.branch_types
CREATE TABLE IF NOT EXISTS `branch_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `search` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.branch_types: ~1 rows (approximately)
/*!40000 ALTER TABLE `branch_types` DISABLE KEYS */;
INSERT INTO `branch_types` (`id`, `sys_code`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '108de0138a3c639a32e153a442560811', 'Branch', '2016-11-04 13:49:31', 1, '2016-11-04 13:49:31', NULL, 1);
/*!40000 ALTER TABLE `branch_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.brands
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.brands: ~0 rows (approximately)
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;


-- Dumping structure for table sas_master.budget_pls
CREATE TABLE IF NOT EXISTS `budget_pls` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `year` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `year` (`year`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.budget_pls: ~0 rows (approximately)
/*!40000 ALTER TABLE `budget_pls` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_pls` ENABLE KEYS */;


-- Dumping structure for table sas_master.budget_pl_details
CREATE TABLE IF NOT EXISTS `budget_pl_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `budget_pl_id` bigint(20) DEFAULT NULL,
  `chart_account_id` bigint(20) DEFAULT NULL,
  `m1` decimal(15,3) DEFAULT NULL,
  `m2` decimal(15,3) DEFAULT NULL,
  `m3` decimal(15,3) DEFAULT NULL,
  `m4` decimal(15,3) DEFAULT NULL,
  `m5` decimal(15,3) DEFAULT NULL,
  `m6` decimal(15,3) DEFAULT NULL,
  `m7` decimal(15,3) DEFAULT NULL,
  `m8` decimal(15,3) DEFAULT NULL,
  `m9` decimal(15,3) DEFAULT NULL,
  `m10` decimal(15,3) DEFAULT NULL,
  `m11` decimal(15,3) DEFAULT NULL,
  `m12` decimal(15,3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `budget_pl_id` (`budget_pl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.budget_pl_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `budget_pl_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_pl_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.cache_datas
CREATE TABLE IF NOT EXISTS `cache_datas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.cache_datas: ~1 rows (approximately)
/*!40000 ALTER TABLE `cache_datas` DISABLE KEYS */;
INSERT INTO `cache_datas` (`id`, `type`, `modified`) VALUES
	(1, 'Products', '2018-09-27 17:30:32');
/*!40000 ALTER TABLE `cache_datas` ENABLE KEYS */;


-- Dumping structure for table sas_master.cgroups
CREATE TABLE IF NOT EXISTS `cgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `user_apply` tinyint(4) DEFAULT '0' COMMENT '0: All; 1: Customize',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.cgroups: ~1 rows (approximately)
/*!40000 ALTER TABLE `cgroups` DISABLE KEYS */;
INSERT INTO `cgroups` (`id`, `sys_code`, `name`, `description`, `created`, `created_by`, `modified`, `modified_by`, `user_apply`, `is_active`) VALUES
	(1, '0ea479a9bcbb44100ca1ceb6939cce11', 'General', NULL, '2017-07-21 15:36:55', 1, '2017-07-21 15:36:55', NULL, 0, 1);
/*!40000 ALTER TABLE `cgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.cgroup_companies
CREATE TABLE IF NOT EXISTS `cgroup_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cgroup_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cgroup_id` (`cgroup_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci CHECKSUM=1;

-- Dumping data for table sas_master.cgroup_companies: ~1 rows (approximately)
/*!40000 ALTER TABLE `cgroup_companies` DISABLE KEYS */;
INSERT INTO `cgroup_companies` (`id`, `cgroup_id`, `company_id`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `cgroup_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.cgroup_price_types
CREATE TABLE IF NOT EXISTS `cgroup_price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cgroup_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cgroup_id` (`cgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.cgroup_price_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `cgroup_price_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `cgroup_price_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.chart_accounts
CREATE TABLE IF NOT EXISTS `chart_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `chart_account_type_id` int(11) DEFAULT NULL,
  `chart_account_group_id` int(11) DEFAULT NULL,
  `account_codes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manual` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `chart_account_type_id` (`chart_account_type_id`),
  KEY `chart_account_group_id` (`chart_account_group_id`),
  KEY `account_codes` (`account_codes`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.chart_accounts: ~14 rows (approximately)
/*!40000 ALTER TABLE `chart_accounts` DISABLE KEYS */;
INSERT INTO `chart_accounts` (`id`, `sys_code`, `parent_id`, `chart_account_type_id`, `chart_account_group_id`, `account_codes`, `account_description`, `manual`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '7b64804932248b9fb6e2204ce529277a', NULL, 1, 1, '1110', 'Cash and Bank', '', '2018-10-01 17:15:06', 1, '2018-10-01 17:15:06', NULL, 1),
	(2, '8b099a9178adf434c30e03669a3a5cd3', NULL, 2, 15, '1120', 'Accounts Receivable', '', '2018-10-01 17:16:37', 1, '2018-10-01 17:16:37', NULL, 1),
	(3, 'f2c44ae6134189100332b6de03fa60c5', NULL, 3, 19, '1130', 'Inventory', '', '2018-10-01 17:17:52', 1, '2018-10-01 17:17:52', NULL, 1),
	(4, '0f763aebd49984986a38d6b386cc1734', NULL, 4, 4, '1210', 'Furniture and Fixtures', '', '2018-10-01 17:18:15', 1, '2018-10-01 17:18:15', NULL, 1),
	(5, 'f1a2a1c230ef3cb9435d6de7f36d681a', NULL, 5, 16, '1310', 'Other assets', '', '2018-10-01 17:18:39', 1, '2018-10-01 17:18:39', NULL, 1),
	(6, '92e750b72a911530d8c66b3f01ef5b38', NULL, 6, 15, '2120', 'Accounts Payable', '', '2018-10-01 17:18:57', 1, '2018-10-01 17:18:57', NULL, 1),
	(7, '3410ddf985b9d2cba1552f497e761136', NULL, 8, 14, '2130', 'Liability', '', '2018-10-01 17:19:18', 1, '2018-10-01 17:19:18', NULL, 1),
	(8, '0f7337e02db73f7e49243db61f8a0a5a', NULL, 9, 13, '2210', 'Long Term Liabilities', '', '2018-10-01 17:19:57', 1, '2018-10-01 17:19:57', NULL, 1),
	(9, '0c7ddbf26070bc6fd3fd00ab82d62a0d', NULL, 10, 20, '3000', 'Equity', '', '2018-10-01 17:20:21', 1, '2018-10-01 17:20:21', NULL, 1),
	(10, '1afbdeaf9f05a7ca7c233fe5666416de', NULL, 11, 10, '4000', 'Income', '', '2018-10-01 17:20:43', 1, '2018-10-01 17:20:43', NULL, 1),
	(11, '4a21c41ad3c8809aea522a9987d029ee', NULL, 12, 8, '5000', 'Cost of Goods Sold', '', '2018-10-01 17:21:04', 1, '2018-10-01 17:21:04', NULL, 1),
	(12, '1502ee93aac654686d26286bb4c5a217', NULL, 13, 7, '6000', 'Expenses', '', '2018-10-01 17:21:38', 1, '2018-10-01 17:21:38', NULL, 1),
	(13, '077ea4fddfece09e3f6dfcaddf7d9b5c', NULL, 14, 6, '4900', 'Other Income', '', '2018-10-01 17:21:58', 1, '2018-10-01 17:21:58', NULL, 1),
	(14, '2253a751c53479972de474c7a493e48f', NULL, 15, 5, '6900', 'Other Expense', '', '2018-10-01 17:22:17', 1, '2018-10-01 17:22:17', NULL, 1);
/*!40000 ALTER TABLE `chart_accounts` ENABLE KEYS */;


-- Dumping structure for table sas_master.chart_account_companies
CREATE TABLE IF NOT EXISTS `chart_account_companies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chart_account_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chart_account_id` (`chart_account_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.chart_account_companies: ~14 rows (approximately)
/*!40000 ALTER TABLE `chart_account_companies` DISABLE KEYS */;
INSERT INTO `chart_account_companies` (`id`, `chart_account_id`, `company_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1),
	(6, 6, 1),
	(7, 7, 1),
	(8, 8, 1),
	(9, 9, 1),
	(10, 10, 1),
	(11, 11, 1),
	(12, 12, 1),
	(13, 13, 1),
	(14, 14, 1);
/*!40000 ALTER TABLE `chart_account_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.chart_account_groups
CREATE TABLE IF NOT EXISTS `chart_account_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_account_type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_depreciation` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `chart_account_type_id` (`chart_account_type_id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.chart_account_groups: ~26 rows (approximately)
/*!40000 ALTER TABLE `chart_account_groups` DISABLE KEYS */;
INSERT INTO `chart_account_groups` (`id`, `sys_code`, `chart_account_type_id`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_depreciation`, `is_active`) VALUES
	(1, 'd83c64331ee0f0d0a8389ba9db087912', 1, 'Cash at Bank', '2014-03-13 19:47:08', 57, '2014-03-13 19:51:56', 57, 0, 1),
	(2, '5128403e8f27e8aef6cf9eb80714aca0', 2, 'Account Receivable', '2014-03-13 19:47:24', 57, '2014-03-13 19:47:24', NULL, 0, 1),
	(3, 'f5254a45340539f1b2b18fd08d909100', 3, 'Other current assets', '2014-03-13 19:47:46', 57, '2014-03-13 19:47:46', NULL, 0, 1),
	(4, 'f7df4ab216e608ce850e34df881feef4', 4, 'Fixed assets', '2014-03-13 19:48:02', 57, '2014-03-13 19:48:02', NULL, 0, 1),
	(5, '08976552a9d8ef839afac6ef7f7f5a44', 15, 'Other expenses', '2014-03-13 19:48:16', 57, '2014-03-13 19:48:16', NULL, 0, 1),
	(6, '3c610818f0d9bd75dd167e1eda626189', 14, 'Other income', '2014-03-13 19:48:29', 57, '2014-03-13 19:48:29', NULL, 0, 1),
	(7, 'e81846649f4fed3d1e7dd860f4820c34', 13, 'Operation Expenses', '2014-03-13 19:48:40', 57, '2014-10-17 10:03:58', 1, 0, 1),
	(8, 'f70b30370202da798e95ddfc51db34f9', 12, 'Cost of Goods Sold', '2014-03-13 19:48:59', 57, '2014-03-13 19:48:59', NULL, 0, 1),
	(9, '0524c850d6c47861bcc80b0bcb95e4fa', 12, 'Cost of Sale', '2014-03-13 19:49:10', 57, '2014-03-13 19:49:10', NULL, 0, 1),
	(10, '20f40a3b780b37a71f3312d0a2f8d006', 11, 'Income', '2014-03-13 19:49:26', 57, '2014-10-17 14:12:30', 1, 0, 1),
	(11, '6f022ae2a88ef6a86d3cb57d3017bb05', 10, 'Share Capital', '2014-03-13 19:49:41', 57, '2014-03-13 19:49:41', NULL, 0, 1),
	(12, '84b93bf951b2d68544d6b0a719af34eb', 10, 'Retained Earning', '2014-03-13 19:49:56', 57, '2014-03-13 19:49:56', NULL, 0, 1),
	(13, '06db2af8e0f6cb408dfb77a2e55ef419', 9, 'Long-term liabilities', '2014-03-13 19:50:19', 57, '2014-03-13 19:50:19', NULL, 0, 1),
	(14, '39977e2fc750241c804994397cf8b76c', 8, 'Other Current Liabilities', '2014-03-13 19:50:38', 57, '2014-10-17 13:55:45', 1, 0, 1),
	(15, '7b1da30decc220313554a3e0b5ab09a0', 6, 'Account payables', '2014-03-13 19:50:53', 57, '2014-03-13 19:50:53', NULL, 0, 1),
	(16, 'c1dfa9cd93afa0abd860326855797e29', 5, 'Other asset', '2014-03-13 19:51:10', 57, '2014-03-13 19:51:10', NULL, 0, 1),
	(17, 'e398d469bbc2f368e1e7a0e2c21b7fb8', 1, 'Cash on hand', '2014-03-13 19:51:39', 57, '2014-03-13 19:51:39', NULL, 0, 1),
	(18, '31e441193d85126ab089c7bca84c1d89', 8, 'Current liaiblities', '2014-03-15 17:39:43', 57, '2014-03-15 17:39:43', NULL, 0, 1),
	(19, '1f78fc6491f921fcc418a9268640b187', 3, 'Inventory', '2014-10-17 08:48:52', 1, '2014-10-17 08:48:52', NULL, 0, 1),
	(20, '6cc66793cc2d806bcad05ec909b0c453', 10, 'Equity', '2014-10-17 09:49:21', 1, '2014-10-17 09:49:21', NULL, 0, 1),
	(21, '7f5ff8802e6e258e6446ea2c6b59fe20', 11, 'Sale Discounts', '2014-10-17 09:57:35', 1, '2014-10-17 09:57:35', NULL, 0, 1),
	(22, '0968b70433446f9cc072e49209ce010c', 13, 'Utilities Expense', '2014-10-17 13:32:56', 1, '2014-10-17 13:32:56', NULL, 0, 1),
	(23, 'd70f12336f882f383a969d363395f132', 13, 'Repairs Expense', '2014-10-17 13:38:54', 1, '2014-10-17 13:38:54', NULL, 0, 1),
	(24, 'be758512053a57750430e7885acc2889', 13, 'Purchase Discounts', '2014-10-17 13:44:00', 1, '2014-10-17 13:44:00', NULL, 0, 1),
	(25, 'd36a4e1bf11b920eb1e5d701b712680d', 2, 'Notes Receivable', '2014-10-17 13:50:52', 1, '2014-10-17 13:50:52', NULL, 0, 1),
	(26, '3ee9d414fc1b1230fc0c330890d801d1', 3, 'Prepaid Expense', '2015-05-04 16:56:24', 1, '2015-05-04 16:56:24', NULL, 0, 1);
/*!40000 ALTER TABLE `chart_account_groups` ENABLE KEYS */;


-- Dumping structure for table sas_master.chart_account_types
CREATE TABLE IF NOT EXISTS `chart_account_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.chart_account_types: ~15 rows (approximately)
/*!40000 ALTER TABLE `chart_account_types` DISABLE KEYS */;
INSERT INTO `chart_account_types` (`id`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, 'Cash and Bank', '2011-12-29 13:48:14', 1, NULL, NULL, 1),
	(2, 'Accounts Receivable', '2011-12-29 13:48:33', 1, NULL, NULL, 1),
	(3, 'Other Current Asset', '2011-12-29 13:48:51', 1, NULL, NULL, 1),
	(4, 'Fixed Asset', '2011-12-29 13:49:00', 1, NULL, NULL, 1),
	(5, 'Other Asset', '2011-12-29 13:49:19', 1, NULL, NULL, 1),
	(6, 'Accounts Payable', '2011-12-29 13:49:25', 1, NULL, NULL, 1),
	(7, 'Credit Card', '2011-12-29 13:49:32', 1, NULL, NULL, 1),
	(8, 'Other Current Liability', '2011-12-29 13:49:41', 1, NULL, NULL, 1),
	(9, 'Long Term Liability', '2011-12-29 13:49:51', 1, NULL, NULL, 1),
	(10, 'Equity', '2011-12-29 13:49:55', 1, NULL, NULL, 1),
	(11, 'Income', '2011-12-29 13:49:59', 1, NULL, NULL, 1),
	(12, 'Cost of Goods Sold', '2011-12-29 13:50:07', 1, NULL, NULL, 1),
	(13, 'Expense', '2011-12-29 13:50:16', 1, NULL, NULL, 1),
	(14, 'Other Income', '2011-12-29 13:50:21', 1, NULL, NULL, 1),
	(15, 'Other Expense', '2011-12-29 13:50:26', 1, NULL, NULL, 1);
/*!40000 ALTER TABLE `chart_account_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.classes
CREATE TABLE IF NOT EXISTS `classes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `ordering` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.classes: ~1 rows (approximately)
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` (`id`, `sys_code`, `parent_id`, `name`, `description`, `ordering`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, 'd5487e87fa6c71af1c9758a4737bdfec', NULL, 'Class', '', NULL, '2017-07-21 11:23:24', 1, '2017-07-21 11:23:24', NULL, 1);
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;


-- Dumping structure for table sas_master.class_companies
CREATE TABLE IF NOT EXISTS `class_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.class_companies: ~1 rows (approximately)
/*!40000 ALTER TABLE `class_companies` DISABLE KEYS */;
INSERT INTO `class_companies` (`id`, `class_id`, `company_id`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `class_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.codes
CREATE TABLE IF NOT EXISTS `codes` (
  `iphash` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created` int(11) DEFAULT NULL,
  PRIMARY KEY (`iphash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `codes` ENABLE KEYS */;


-- Dumping structure for table sas_master.colors
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `duplicate` (`name`),
  KEY `name` (`name`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.colors: ~0 rows (approximately)
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;


-- Dumping structure for table sas_master.communes
CREATE TABLE IF NOT EXISTS `communes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`),
  KEY `district_id` (`district_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.communes: ~157 rows (approximately)
/*!40000 ALTER TABLE `communes` DISABLE KEYS */;
INSERT INTO `communes` (`id`, `sys_code`, `district_id`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(2, '0bfa766e36fe3ae852dee48f24ebe10e', 3, 'TONLE BASSAK', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(3, '706cce60d604d3d53dd2006f0670ae5c', 3, 'BOEUNG KENG KANG I', '2014-12-22 14:57:34', 1, '2015-03-14 12:33:09', 2, 1),
	(4, 'ae8937c954b110918868158b561f2708', 3, 'BOEUNG KENG KANG II', '2014-12-22 14:57:34', 1, '2015-03-14 12:33:17', 2, 1),
	(5, 'af028cebd630f31f1b8a7e22094da8e2', 3, 'BOEUNG KENG KANG III', '2014-12-22 14:57:34', 1, '2015-03-14 12:33:32', 2, 1),
	(6, 'b4758e17b43a0229b04c96d4cb356ebf', 3, 'BOEUNG TRABEK', '2014-12-22 14:57:34', 1, '2015-03-14 12:34:09', 2, 1),
	(7, '0a1d8e1a91652fd7e877727fa7d663af', 3, 'TUMNUP TUEK', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(8, 'edab8f24a5c0a95d9090693e54c6717d', 3, 'PHSAR DOEUM THKOV', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(9, 'ad993440cdb41f3bbaaa77df8f73e8f8', 3, 'TOUL SVAY PREY I', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(10, 'b6cddb44cb91e8004f6a7e3f1b6b45b1', 3, 'TOUL SVAY PREY II', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(11, 'b3751b59b0fdf61b08573f97525bfaae', 3, 'TOUL TUM POUNG I', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(12, '63d9e3731850ef57d95a1c35ca8803b4', 3, 'TOUL TUM POUNG II', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(13, '1cf36bab67da44ccb0408987ad757177', 3, 'OLYMPIC', '2014-12-22 14:57:34', 1, '2014-12-22 14:57:34', NULL, 1),
	(14, '6b3bdb285c8596e9e57d0342f407e8fa', 6, 'DANGKAO', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(15, '548f1409cc539c5ca6e5411f3c6c9774', 6, 'TRAPANG KRASANG', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(16, '6a5081dcd5ed1309bacb3fcc3859e5f1', 6, 'KORKROKA', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(17, '90df5d23a1dc00b402d0f3ab5a5ff1b4', 6, 'PHLEUNG CHHESROTES', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(18, '789ca042dc460d362c38d391d9811df5', 13, 'CHAOM CHAO', '2014-12-22 15:01:26', 1, '2014-12-25 09:04:24', 1, 2),
	(19, '854d0228fcd8f22330537ee262df6c19', 6, 'PORNG TUEK', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(20, 'ad0f4b7e9fe564aef864eb55b7287626', 6, 'PREY VENG', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(21, 'ecfd0347edf1065a7c59d3b7d972b0c4', 6, 'SAMRONG', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(22, 'f7c8c041896faf866cb902fbbaf5e994', 6, 'PREY SAR', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(23, '50f6fa9f488fcd0909a5b49318d20980', 6, 'KRAING THNONG', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(24, '2ffd8d48aa9a4c78480b01cda88572d2', 6, 'KRAING PONGRO', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(25, 'ac2b498971289236ff0c2942c8019ab3', 6, 'PRATASLANG', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(26, '78671df564b947ad6c529efacf1473f2', 6, 'SAC SAMPEOU', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(27, '1054c5e89b84213ae87d666ac8c9b25e', 6, 'CHHEUNG EK', '2014-12-22 15:01:26', 1, '2014-12-22 15:01:26', NULL, 1),
	(28, '1395712015884d424bb3338debcbd1b7', 7, 'SRAAS CHAK', '2014-12-22 15:03:45', 1, '2014-12-23 08:41:20', 1, 1),
	(29, '6fdd4f8f0b50233034b39cf6717a7981', 7, 'WAT PHNOM', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(30, '6813d90afc4d1f963281e945a2724b41', 7, 'PHSAR CHAS', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(31, 'b83b82f805b18e3ef6bb88c5f13e806f', 7, 'PHSAR KANDAL I', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(32, '35053fa087cda8f6c12149224831a432', 7, 'PHSAR KANDAL II', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(33, '596c586ca24462ca2172aeaa0eea7e19', 7, 'CHEY CHOMNAS', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(34, '9cf17d9ef3f7976ec0c17394f64e91f3', 7, 'CHAKTOMUK', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(35, 'a193da28f7bdcf558b1658ff1ff38e86', 7, 'PHSAR THMEY I', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(36, '31e154d43cb147b1e9040ff17815f77b', 7, 'PHSAR THMEY II', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(37, '04e001abac16c4a16c1a5e2341047034', 7, 'PHSAR THMEY III', '2014-12-22 15:03:45', 1, '2014-12-22 15:03:45', NULL, 1),
	(38, 'cf0a7457c501382ab871d18c5e471573', 7, 'BOEUNG RAING', '2014-12-22 15:03:45', 1, '2015-03-14 12:33:50', 2, 1),
	(39, '8476d84032c6cc7400d24ecac66f0c38', 8, 'BOEUNG TUMPUN', '2014-12-22 15:05:26', 1, '2015-03-14 12:34:17', 2, 1),
	(40, '1d5354718d9e6a6be59613440550a169', 8, 'STUNG MEANCHEY', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(41, '567d2c2a370be7b4049ddbfc411756d1', 8, 'CHAK ANGRE KROM', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(42, 'ab96220fa79a5cc9f1c791328ef23a84', 8, 'CHAK ANGRE LEUR', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(43, 'dda88a3e1c9f0ce88c15c099022bcdf1', 8, 'CHBAR AMPOV I', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(44, 'fd6da8642ee0e7fcc4a6b9dab78210dc', 8, 'CHBAR AMPOV II', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(45, 'b3251fd609dbd419dd761f483a13c989', 8, 'NIROTH', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 2),
	(46, 'd45bcc01d945e9d1b4113f4050662898', 8, 'PREK PRA', '2014-12-22 15:05:26', 1, '2014-12-22 15:05:26', NULL, 1),
	(47, 'aff71f22da5f2d33279a7cb9f6d10369', 9, 'MONOROM', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(48, '18d064754fd3b2b87a28bec7e51514be', 9, 'MITTAPHEAP', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(49, 'a25a3935819b7ef3df2a482470d8e616', 9, 'VEAL VONG', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(50, '779a71f9bc19bac6f82253cc17ec1811', 9, 'ORUSSEY I', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(51, 'a41caac158bf278d4acdd0e2853684ba', 9, 'ORUSSEY II', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(52, '64042161d869dcec8fee7ecf9be233d2', 9, 'ORUSSEY III', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(53, 'b69a7cfdffbf4b184c35783359ce43f0', 9, 'ORUSSEY IV', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(54, '0108efde10a5807eff2284ff2a90e16e', 9, 'ORUSSEY V', '2014-12-22 15:06:45', 1, '2014-12-22 15:06:45', NULL, 1),
	(55, 'e582744a940b9f11ca41573e1d4d1f15', 11, 'PHNOM PENH THMEY', '2014-12-22 15:07:51', 1, '2014-12-22 15:07:51', NULL, 1),
	(56, '139d0dee63e2ee3b767534214cd3ccfb', 11, 'TUEK THLA', '2014-12-22 15:07:51', 1, '2014-12-22 15:07:51', NULL, 1),
	(57, '46cfae998492bf975bee31a4280321a8', 11, 'KHMOUNH', '2014-12-22 15:07:51', 1, '2014-12-22 15:07:51', NULL, 1),
	(58, '931861fad92796bc6d5fdcf6206d3607', 12, 'BOEUNG KAK I', '2014-12-22 15:09:41', 1, '2015-03-14 12:32:53', 2, 1),
	(59, 'aba5dab101d7bedca8efcee02f1e5be5', 12, 'BOEUNG KAK II', '2014-12-22 15:09:41', 1, '2015-03-14 12:33:01', 2, 1),
	(60, '144aa674075587be5b93a3d76eec42e7', 12, 'PHSAR DEPO I', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(61, '0f95b116975a3f4811abf05ffdc15ef6', 12, 'PHSAR DEPO II', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(62, '51caa040cdae627694c9ae479f5ab5b1', 12, 'PHSAR DEPO III', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(63, '61486bc116fd9ac2ec80c0e3e58fb43e', 12, 'TUEK LAAK I', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(64, 'ca16c3eba07a9cdc8d49915afbda8a6e', 12, 'TUEK LAAK II', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(65, '166954d5b691f480b5dd901ed547107c', 12, 'TUEK LAAK III', '2014-12-22 15:09:41', 1, '2014-12-22 15:09:41', NULL, 1),
	(66, '4d8d04f9052fe8b8f077bb2db56d0929', 12, 'PHSAR DOEUM KOR', '2014-12-22 15:09:41', 1, '2015-03-07 17:01:44', 2, 1),
	(67, '598ff5736d16fb410ae565293510c869', 12, 'BOEUNG SALANG', '2014-12-22 15:09:41', 1, '2015-03-14 12:34:02', 2, 1),
	(68, 'ce862af8cd219d50a5d552b3029d2df1', 10, 'SVAY PAK', '2014-12-22 15:14:06', 1, '2014-12-22 15:14:06', NULL, 1),
	(69, '7b9cb826f2688fae624504a0612985bc', 10, 'KILO 6', '2014-12-22 15:14:06', 1, '2014-12-22 15:14:06', NULL, 1),
	(70, '9fe3ebc84931f66599d8c11e1ec55925', 15, 'BANTEAY NEANG', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(71, 'b12a2c228069d2bbd73246233e184e72', 15, 'BAT TRANG', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(72, '4f39a95a58d7887951dea388d28c9f94', 15, 'CHAMNAOM', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(73, 'a6508fd272d5801abefff4174639b1a3', 15, 'KOUK BALANG', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(74, '1ac18ee7c95b9b4e09c5fe6c7e8ca753', 15, 'KOY MAENG', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(75, '648306766aac788c2a03757ff92ed48c', 15, 'O PRASAT', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(76, '5ce261c28a181a132c73e3d1b832a57d', 15, 'PHNOM TOUCH', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(77, '62cd3571ab2431578f2d1666288908a1', 15, 'ROHAT TUEK', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(78, '38396e6bb347b52d63ff66021ef8fdc8', 15, 'RUSSEI KRAOK', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(79, 'ade158be1ada80eae439e1deaed37440', 15, 'SAMBUOR', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(80, '530577d9bbb4a129e248c1e64e44d9c8', 15, 'SRAAS REANG', '2014-12-22 15:17:21', 1, '2014-12-23 08:41:39', 1, 1),
	(81, 'fa319d6e31c547e4c97a01a712815a49', 15, 'TA LAM', '2014-12-22 15:17:21', 1, '2014-12-22 15:17:21', NULL, 1),
	(82, '787db5bc5df38ac531321da88baefe6f', 16, 'NAM TAU', '2014-12-22 15:19:23', 1, '2014-12-22 15:19:23', NULL, 1),
	(83, 'e3a4e093e1da2d894c10dff983b0693a', 16, 'PAOY CHAR', '2014-12-22 15:19:23', 1, '2014-12-22 15:19:23', NULL, 1),
	(84, 'acadb8c8b0834188b355963d603fb497', 16, 'PHNOM DEY', '2014-12-22 15:19:23', 1, '2014-12-22 15:19:23', NULL, 1),
	(85, 'b8a30610224a52202e38b868aac49ec2', 16, 'PONLEY', '2014-12-22 15:19:23', 1, '2014-12-22 15:19:23', NULL, 1),
	(86, 'e9e41aaedbcbdd9f7fe6c456bcd2f768', 16, 'SPEAN SRENG ROUK', '2014-12-22 15:19:23', 1, '2014-12-22 15:19:23', NULL, 1),
	(87, 'c4c8ef06a91bfba24e14cf056cbdb39c', 16, 'SRAAS CHIK', '2014-12-22 15:19:23', 1, '2014-12-23 08:41:29', 1, 1),
	(88, '4824d9ee8790803a978245644d4d0958', 17, 'CHHNUOR', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(89, 'e94dde2264d424d7b286e85c31f8e220', 17, 'CHOB', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(90, '7553873edf3ca51e49a36574325740a5', 17, 'PHNOM LIEB', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(91, '17cc57e0ef6c5952537fb45a072115f8', 17, 'PRASAT CHAR', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(92, '5294da4bedeef12aec7f0765b9946208', 17, 'PREAH NET', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(93, '1225d1ebda85d9cddb61068b235cf43f', 17, 'ROHAL ROHAL', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(94, '6c3ec98a5145bdc6a244c47eac5cd919', 17, 'TEAN KAM', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(95, 'dcd3b0d7a5b17bbc5f7b098f76edd3cf', 17, 'TUEK CHOUR SMACH', '2014-12-22 15:21:21', 1, '2014-12-22 15:21:21', NULL, 1),
	(96, '9490a6ee3a8e1a913acf4bdcf873330f', 18, 'CHANGHA', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(97, '8293190427907c4e2a2a5a9fdedc5d7e', 18, 'KOUB', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(98, '1b194726f4e6c4470756570eda1cdb3c', 18, 'KUTTASAT', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(99, 'dbb1d425d64ca15a92b7736325dc2501', 18, 'NIMITT', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(100, 'f66f445ba62d6ce96e6c09692e1d4835', 18, 'O BEI CHOAN', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(101, 'fed5a2ee7fc1c5e2dabbc6a395d90d64', 18, 'PAOY PAET', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(102, '95c035ea6534f7733126a244be9176a4', 18, 'SAMRAONG', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(103, 'b4d43c3bf219cb7b7eb40189bd499a4b', 18, 'SOENGH', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(104, 'c75087a0f15fb8e6fca006ca22c9189b', 18, 'SOUPHI', '2014-12-22 15:24:00', 1, '2014-12-22 15:24:00', NULL, 1),
	(105, 'fc8af04c1c1b6a39843b377587026e1f', 19, 'BOS SBOV', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(106, '2b8c3792707bab00ccadb20964a7abde', 19, 'KAMPONG SVAY', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(107, '0f3f7f1e79f47825d3c8b3ab4c01c215', 19, 'KOH PORNG SATV', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(108, 'c326eec0b3455f66dbaca31cbafed281', 19, 'MKAK', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(109, 'b65aa43218920087d1a46f78c017b131', 19, 'O AMBEL', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(110, '75eaaa178c5e89e37fe83192d72c508d', 19, 'PHNIET', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(111, 'b15c4e06cd7a68bcbfa265759ea32d25', 19, 'PREAH PONLEA', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(112, '448da17f2936247060a3d9e28979048c', 19, 'TUEK THLA', '2014-12-22 15:29:31', 1, '2014-12-22 15:29:31', NULL, 1),
	(113, '195356a26b71a7a344db7cb267feb02d', 21, 'PHKOAM', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(114, '2ec7cb990832d0547293a8f51efcc7b4', 21, 'ROLUOS', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(115, 'b3acb616868f8c9f36c8c93461701646', 21, 'SARONGK', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(116, '14780fa9e3554426abe18f0c72cdf5a7', 21, 'SLA KRAM', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(117, '69ec4cc9ab9e8f0711cb81eb861386c3', 21, 'SVAY CHEK', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(118, '7edb5aac554c0b72ba11e22bf05f6dc4', 21, 'TA BAEN', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(119, 'baa86f7138aae59238fbb1e350ce96cd', 21, 'TA PHOU', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(120, 'ec4c7ae29a7fc8b0cda0e917067f939b', 21, 'TREAS', '2014-12-22 16:15:52', 1, '2014-12-22 16:15:52', NULL, 1),
	(121, 'e94f369197741909643fa6d271e91842', 20, 'BANTEAY CHMAR', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(122, 'e11aab33312452e8cf4ebeff020184d2', 20, 'KOK KAKTHEN', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(123, 'd0c545db4bf909279dafbac64f5c1e22', 20, 'KOK ROMIET', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(124, '561d851bf577179be4aa5c0603e8c098', 20, 'KUMRU', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(125, '821c631ba1ba5c5231951db59116b2b9', 20, 'PHNOM THMEY', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(126, 'c8c3a5a804bb9c3ebe1c9581adb095de', 20, 'THMAR POUK', '2014-12-22 16:17:14', 1, '2014-12-22 16:17:14', NULL, 1),
	(127, '9670f1b2d0715417d1fc1128a0a7fc7b', 22, 'BOEUNG BENG', '2014-12-22 16:18:10', 1, '2015-03-14 12:32:45', 2, 1),
	(128, 'bb9b5e5fc46730747381465bbc6e67f7', 22, 'MALAI', '2014-12-22 16:18:10', 1, '2014-12-22 16:18:10', NULL, 1),
	(129, '2c4f7be52ba3137e1d6f18d8d979c049', 22, 'O SRALAU', '2014-12-22 16:18:10', 1, '2014-12-22 16:18:10', NULL, 1),
	(130, '597d9bc56677cf5764f61f7e2f2ac55f', 22, 'TA KONG', '2014-12-22 16:18:10', 1, '2014-12-22 16:18:10', NULL, 1),
	(131, '827be6cdc985df7377de9d8825388687', 22, 'TOUL PONGRO', '2014-12-22 16:18:10', 1, '2014-12-22 16:18:10', NULL, 1),
	(132, '6bc8fdec5c3f5384536b50e63e6bbfb8', 4, 'NIROTH', '2014-12-23 08:53:24', 1, '2014-12-23 08:53:24', NULL, 1),
	(133, 'a7ca8a1a432bbc91330769156894592c', 13, 'CHOAM CHOA', '2014-12-24 13:35:03', 1, '2014-12-24 13:35:03', NULL, 1),
	(134, '66cbdbb8e2fe201250d464e04d9a3d81', 89, 'PREK SOMROUNG', '2015-01-08 17:49:05', 2, '2015-01-08 17:49:05', NULL, 1),
	(135, '45c40f13284843db5a2ba908a5f87199', 10, 'TOUL SANGKE', '2015-01-08 17:53:06', 2, '2015-01-08 17:53:06', NULL, 1),
	(136, '6d36634a4ede900590b3b26f7409d7af', 13, 'KAKAB', '2015-01-08 17:57:59', 2, '2015-01-08 17:57:59', NULL, 1),
	(137, '03d074372da12bf62b947c68000ed4e5', 25, 'SVAY PAO', '2015-01-09 10:55:48', 2, '2015-01-09 10:55:48', NULL, 1),
	(138, 'd94e3d73c559e3e5e8109a99fa1082d4', 40, 'BOEUNG KOK', '2015-01-10 13:41:55', 2, '2015-03-14 12:33:42', 2, 1),
	(139, 'a3c8e20ec97c8918a2dc949daca6c1c4', 163, 'SVAY DANGKUM', '2015-01-10 16:44:28', 2, '2015-01-10 16:44:28', NULL, 1),
	(140, '867d11f247da39f6500a45ce6df57d88', 47, 'PONLAI', '2015-01-15 11:54:59', 2, '2015-01-15 11:54:59', NULL, 1),
	(141, '90b9f927a35fe2c22c37ecb4c4c577b9', 49, 'KAMPONG CHHNANG', '2015-01-15 11:56:36', 2, '2015-01-15 11:56:36', NULL, 1),
	(142, 'ba3b4bff020a440bc9bd923da6eb8f80', 52, 'PONGRO', '2015-01-15 11:57:11', 2, '2015-01-15 11:57:11', NULL, 1),
	(143, '54b7bff5678cc2afefe48bcfebeddda5', 52, 'ROLEA BIER', '2015-01-15 11:57:38', 2, '2015-01-15 11:57:38', NULL, 1),
	(144, 'e9aa0581a6ec8a20d27f852271061fcc', 45, 'PREK POR', '2015-01-15 12:14:39', 2, '2015-01-15 12:14:39', NULL, 1),
	(145, 'ce51a59cbb8f16e778e6433dc504da26', 45, 'PREK DAMBOK', '2015-01-15 12:15:16', 2, '2015-01-15 12:15:16', NULL, 1),
	(146, '361e763af930b96a87886167249237ce', 59, 'VEANG CHAS', '2015-01-15 12:18:26', 2, '2015-01-15 12:18:26', NULL, 1),
	(147, 'c0f3f0369af16d5de6607ce64fbd7564', 80, 'KOR KI', '2015-01-19 09:57:16', 2, '2015-01-19 09:57:16', NULL, 1),
	(148, '75be1a066c8d9136d88d1b3725acf5a3', 51, 'ORRUSEY', '2015-01-19 09:58:06', 2, '2015-01-19 09:58:06', NULL, 1),
	(149, '300d144193955927771c3a8404ccefe5', 85, 'ROR KAKORNG', '2015-01-19 10:01:43', 2, '2015-01-19 10:01:43', NULL, 1),
	(150, 'd7ba325c7ed9a8182adfff6f9b164071', 87, 'KAMPONG LEUNG', '2015-01-19 10:03:31', 2, '2015-01-19 10:03:31', NULL, 1),
	(151, 'd431d7876c9102a5cd6c3e9ea9c9615b', 87, 'PREK SVAY', '2015-01-19 10:16:36', 2, '2015-01-19 10:16:36', NULL, 2),
	(152, '9dae705ac5d8d84b562f86fe91d11ef0', 140, 'PREK SVAY', '2015-01-19 10:17:41', 2, '2015-01-19 10:17:41', NULL, 1),
	(153, 'c0c785a1d10ff2a12c3b835aa396900e', 178, 'SVAY RIENG', '2015-01-19 10:19:24', 2, '2015-01-19 10:19:24', NULL, 1),
	(154, '6dad8f792fbe0f040f4b26cbf42e50ab', 183, 'TUM LAOB', '2015-01-19 10:30:53', 2, '2015-01-19 10:30:53', NULL, 1),
	(155, 'a2a60f89dd2f8e6af6599a6621ff97e7', 86, 'ANG SNOUL', '2015-01-19 10:31:53', 2, '2015-01-19 10:31:53', NULL, 1),
	(156, '6a17ed6b98b57f02a4705a0df3942b86', 86, 'BEK CHAAN', '2015-01-19 10:33:01', 2, '2015-01-19 10:33:01', NULL, 1),
	(157, 'be7ef7a151ab88ad2f68077625f2bcbd', 78, 'KAMPOT', '2015-01-19 10:33:24', 2, '2015-01-19 10:33:24', NULL, 1),
	(158, '3de414996eae76f061c6ad46c95b613f', 200, 'KAMPONG SPEU', '2015-01-24 15:35:15', 2, '2015-01-24 15:35:15', NULL, 1),
	(159, '3ffb0d387ae755dc3f16d35bcc7b112b', 188, 'TRAM KNOR', '2015-01-24 15:36:00', 2, '2015-01-24 15:36:00', NULL, 1),
	(160, 'fab5508d9c794095e45ef54c9607b988', 83, 'PREK TUMLOAP', '2015-01-26 11:34:03', 2, '2015-01-26 11:34:03', NULL, 1),
	(161, 'ccc10bc1762e4e0bbd24affd26d796c2', 173, 'PREY ANGKUNGH', '2015-01-26 11:35:24', 2, '2015-01-26 11:35:24', NULL, 1),
	(162, 'f88b1a2eef138f3e3faa95c43ea26ca1', 81, 'PREK TAMAB', '2015-01-26 11:44:15', 2, '2015-01-26 11:51:39', 2, 1),
	(163, 'c0b678b533909be0e8e3ef131da37173', 10, 'CHRANG CHAMRESH', '2015-01-29 11:13:53', 2, '2015-01-29 11:13:53', NULL, 1),
	(164, 'a671e036b458e168e018155615814dcb', 201, 'PREAH SIHANOUK', '2015-01-30 17:05:53', 2, '2015-01-30 17:05:53', NULL, 1),
	(165, '708f266bcb9879c358e574f9618dac4c', 94, 'KOH KONG', '2015-02-09 11:02:45', 2, '2015-02-09 11:02:45', NULL, 1),
	(166, 'd50ea411285029e9f5126688fa21ae65', 52, 'ANDOUNG SNAY', '2015-02-24 10:10:17', 2, '2015-02-24 10:10:17', NULL, 1),
	(167, '30312daf059605de037363a98cb9ae91', 59, 'VONG CHAAS', '2015-02-24 10:12:25', 2, '2015-02-24 10:12:25', NULL, 1),
	(168, 'cbf14d233f2a5117738ce2d254bb75e8', 85, 'PREK ANCHANH', '2015-02-24 10:13:09', 2, '2015-02-24 10:13:09', NULL, 1),
	(169, 'bb53e29a6918bb8eaeccbd0f6d0e6083', 141, 'ORKA', '2015-02-24 10:21:04', 2, '2015-02-24 10:21:04', NULL, 2),
	(170, '6e656438d86ca0e7b74eedeb11ec5596', 141, 'ROKA', '2015-02-24 10:21:43', 2, '2015-02-24 10:21:43', NULL, 1),
	(171, 'abf9bbc1dcee1cfad0b52110a1a3f2f4', 138, 'CHI PHOEH', '2015-02-24 10:23:15', 2, '2015-02-24 10:23:15', NULL, 1),
	(172, 'de7af5345cd2340755bbb93d750b49d6', 202, 'SANGKAT MONOROM', '2015-03-23 14:56:10', 2, '2015-03-23 14:56:10', NULL, 1);
/*!40000 ALTER TABLE `communes` ENABLE KEYS */;


-- Dumping structure for table sas_master.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_other` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `vat_number` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat_calculate` tinyint(4) unsigned DEFAULT NULL COMMENT '1: Before Discount, Make Up; 2: After Discount, Make Up',
  `website` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classes` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `currency_center_id` (`currency_center_id`),
  KEY `searchs` (`name`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.companies: ~1 rows (approximately)
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `sys_code`, `name`, `name_other`, `currency_center_id`, `vat_number`, `vat_calculate`, `website`, `photo`, `classes`, `description`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, 'a576c53a36adda240771eb51306c2f85', 'UDAYA Technology', 'UDAYA Technology', 1, '', 2, '', '6cc37ab08f0a9284c1f0848d46176543.jpg', 'a:1:{i:1;a:4:{i:1;s:1:"1";i:2;s:1:"1";i:3;s:1:"1";i:4;s:1:"1";}}', '', '2018-07-18 14:34:25', 1, '2018-07-18 14:34:25', NULL, 1);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.company_categories
CREATE TABLE IF NOT EXISTS `company_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  KEY `other_name` (`other_name`(255)),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.company_categories: ~21 rows (approximately)
/*!40000 ALTER TABLE `company_categories` DISABLE KEYS */;
INSERT INTO `company_categories` (`id`, `sys_code`, `name`, `other_name`, `created`, `is_active`) VALUES
	(1, '6dc85d00efe67b7a5e071a64af1dc633', 'Arts, crafts, and collectibles', NULL, '2017-01-26 16:28:21', 1),
	(2, '04c13dd1c13b02c188ddf7ee90246913', 'Baby', NULL, '2017-01-26 16:28:21', 1),
	(3, 'e38ecd6e1e7a8bdb102f785fa6492f58', 'Beauty and fragrances', NULL, '2017-01-26 16:28:21', 1),
	(4, 'b0dabedd849626b7b7f4491cb28a593e', 'Books and magazines', NULL, '2017-01-26 16:28:21', 1),
	(5, '76a36ab302f74a113e90748941371049', 'Business to business', NULL, '2017-01-26 16:28:21', 1),
	(6, '1bd8204a5ddb1196490a01b7ae11bc5f', 'Clothing, accessories, and shoes ', NULL, '2017-01-26 16:28:21', 1),
	(7, '07a0a620f401776293e9928be32b25fc', 'Computers, accessories, and services', NULL, '2017-01-26 16:28:21', 1),
	(8, '041aa86cc576b4b19a1b1fe81f64a25d', 'Education', NULL, '2017-01-26 16:28:21', 1),
	(9, 'c11905b0bf5826c9d19bebd04bb38e32', 'Electronics and telecom', NULL, '2017-01-26 16:28:21', 1),
	(10, 'f5c930fe7de24681bad21e8151406bfd', 'Entertainment and media', NULL, '2017-01-26 16:28:21', 1),
	(11, '20d9ee466b981e69391f32057eb19b32', 'Financial services and products', NULL, '2017-01-26 16:28:21', 1),
	(12, '8ee8b9787c08e7086a5f5c153d11be24', 'Food retail and service', NULL, '2017-01-26 16:28:21', 1),
	(13, '3c883792631e90df8dfdc666d55845b6', 'Gifts and flowers', NULL, '2017-01-26 16:28:21', 1),
	(14, '304f94deb6ce17e21da59e1a9312f54a', 'Health and personal care', NULL, '2017-01-26 16:28:21', 1),
	(15, 'be381fe9fe4af42f444f77fd9bc1803e', 'Home and garden', NULL, '2017-01-26 16:28:21', 1),
	(16, 'da7191330b9777075f2b240921312d63', 'Pets and animals', NULL, '2017-01-26 16:28:21', 1),
	(17, '5599f3642d4c7a882cd324016d09dcbe', 'Services - other', NULL, '2017-01-26 16:28:21', 1),
	(18, '11cbcc024c05dbbf3e0992fd830e6d89', 'Sports and outdoors', NULL, '2017-01-26 16:28:21', 1),
	(19, '7da3459c73669497df2338da4f21891a', 'Toys and hobbies ', NULL, '2017-01-26 16:28:21', 1),
	(20, '28d196a4afe4752e453a6e4620dd366a', 'Vehicle sales', NULL, '2017-01-26 16:28:21', 1),
	(21, 'a6697970730c8db47813b47eba0ad2f0', 'Vehicle service and accessories', NULL, '2017-01-26 16:28:21', 1);
/*!40000 ALTER TABLE `company_categories` ENABLE KEYS */;


-- Dumping structure for table sas_master.company_with_categories
CREATE TABLE IF NOT EXISTS `company_with_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `company_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `searchs` (`company_id`,`company_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.company_with_categories: ~1 rows (approximately)
/*!40000 ALTER TABLE `company_with_categories` DISABLE KEYS */;
INSERT INTO `company_with_categories` (`id`, `company_id`, `company_category_id`) VALUES
	(1, 1, 4);
/*!40000 ALTER TABLE `company_with_categories` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignments
CREATE TABLE IF NOT EXISTS `consignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_contact_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_group_to_id` int(11) DEFAULT NULL,
  `sales_rep_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued; 2: Fullfiled',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filters` (`date`,`code`,`status`),
  KEY `filter2` (`customer_id`,`location_group_id`,`location_group_to_id`,`sales_rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignments: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignments` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_details
CREATE TABLE IF NOT EXISTS `consignment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignment_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` decimal(15,2) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `consignment_id` (`consignment_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_receives
CREATE TABLE IF NOT EXISTS `consignment_receives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignment_id` int(11) DEFAULT NULL,
  `consignment_detail_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `total_qty` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consignment_id` (`consignment_id`),
  KEY `filters` (`location_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_returns
CREATE TABLE IF NOT EXISTS `consignment_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `consignment_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_contact_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_group_to_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued; 2: Fullfiled',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filters` (`date`,`code`,`status`),
  KEY `filter2` (`customer_id`,`location_group_id`,`location_group_to_id`,`consignment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_returns: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_returns` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_return_details
CREATE TABLE IF NOT EXISTS `consignment_return_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignment_return_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` decimal(15,2) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `consignment_return_id` (`consignment_return_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_return_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_return_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_return_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_return_receives
CREATE TABLE IF NOT EXISTS `consignment_return_receives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignment_return_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `total_qty` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `consignment_return_id` (`consignment_return_id`),
  KEY `filters` (`location_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_return_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_return_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_return_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.consignment_term_conditions
CREATE TABLE IF NOT EXISTS `consignment_term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `consignment_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_search` (`consignment_id`,`term_condition_type_id`,`term_condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.consignment_term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `consignment_term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `consignment_term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.countries: ~232 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `name`, `is_active`) VALUES
	(1, 'Afghanistan', 1),
	(2, 'Albania', 1),
	(3, 'Algeria', 1),
	(4, 'American Samoa', 1),
	(5, 'Andorra', 1),
	(6, 'Angola', 1),
	(7, 'Anguilla', 1),
	(8, 'Antarctica', 1),
	(9, 'Antigua & barbuda', 1),
	(10, 'Argentina', 1),
	(11, 'Armenia', 1),
	(12, 'Aruba', 1),
	(13, 'Australia', 1),
	(14, 'Austria', 1),
	(15, 'Azerbaijan', 1),
	(16, 'Bahamas', 1),
	(17, 'Bahrain', 1),
	(18, 'Bangladesh', 1),
	(19, 'Barbados', 1),
	(20, 'Belarus', 1),
	(21, 'Belgium', 1),
	(22, 'Belize', 1),
	(23, 'Benin', 1),
	(24, 'Bermuda', 1),
	(25, 'Bhutan', 1),
	(26, 'Bolivia', 1),
	(27, 'Bosnia herzegovina', 1),
	(28, 'Botswana', 1),
	(29, 'Bouvet Island', 1),
	(30, 'Brazil', 1),
	(31, 'Brunei Darussalam', 1),
	(32, 'Bulgaria', 1),
	(33, 'Burkinafaso', 1),
	(34, 'Burma', 1),
	(35, 'Burundi', 1),
	(36, 'Cambodia', 1),
	(37, 'Cameroon', 1),
	(38, 'Canada', 1),
	(39, 'Cape Verde', 1),
	(40, 'Cayman Islands', 1),
	(41, 'Central african rep', 1),
	(42, 'Chad', 1),
	(43, 'Chile', 1),
	(44, 'China', 1),
	(45, 'Christmas Island', 1),
	(46, 'Colombia', 1),
	(47, 'Comoros', 1),
	(48, 'Congo', 1),
	(49, 'Cook Islands', 1),
	(50, 'Costa Rica', 1),
	(51, 'Cote D\'Ivoire', 1),
	(52, 'Croatia', 1),
	(53, 'Cuba', 1),
	(54, 'Cyprus', 1),
	(55, 'Czech Republic', 1),
	(56, 'Demrepcongo', 1),
	(57, 'Denmark', 1),
	(58, 'Djibouti', 1),
	(59, 'Dominica', 1),
	(60, 'East Timor', 1),
	(61, 'Ecuador', 1),
	(62, 'Egypt', 1),
	(63, 'El Salvador', 1),
	(64, 'Equatorial Guinea', 1),
	(65, 'Eritrea', 1),
	(66, 'Estonia', 1),
	(67, 'Ethiopia', 1),
	(68, 'Faroe Islands', 1),
	(69, 'Fiji', 1),
	(70, 'Finland', 1),
	(71, 'France', 1),
	(72, 'France, Metropolitan', 1),
	(73, 'French Guiana', 1),
	(74, 'French Polynesia', 1),
	(75, 'Gabon', 1),
	(76, 'Gambia', 1),
	(77, 'Georgia', 1),
	(78, 'Germany', 1),
	(79, 'Ghana', 1),
	(80, 'Gibraltar', 1),
	(81, 'Greece', 1),
	(82, 'Greenland', 1),
	(83, 'Grenada', 1),
	(84, 'Grenadines', 1),
	(85, 'Guadeloupe', 1),
	(86, 'Guam', 1),
	(87, 'Guatemala', 1),
	(88, 'Guinea', 1),
	(89, 'Guinea-bissau', 1),
	(90, 'Guyana', 1),
	(91, 'Haiti', 1),
	(92, 'Honduras', 1),
	(93, 'Hong Kong', 1),
	(94, 'Hungary', 1),
	(95, 'Iceland', 1),
	(96, 'India', 1),
	(97, 'Indonesia', 1),
	(98, 'Iran', 1),
	(99, 'Iraq', 1),
	(100, 'Ireland', 1),
	(101, 'Israel', 1),
	(102, 'Italy', 1),
	(103, 'Ivory Coast', 1),
	(104, 'Jamaica', 1),
	(105, 'Japan', 1),
	(106, 'Jordan', 1),
	(107, 'Kazakhstan', 1),
	(108, 'Kenya', 1),
	(109, 'Kiribati', 1),
	(110, 'Kuwait', 1),
	(111, 'Kyrgyzstan', 1),
	(112, 'Laos', 1),
	(113, 'Latvia', 1),
	(114, 'Lebanon', 1),
	(115, 'Lesotho', 1),
	(116, 'Liberia', 1),
	(117, 'Libya', 1),
	(118, 'Liechtenstein', 1),
	(119, 'Lithuania', 1),
	(120, 'Luxembourg', 1),
	(121, 'Macadonia', 1),
	(122, 'Macau', 1),
	(123, 'Madagascar', 1),
	(124, 'Malawi', 1),
	(125, 'Malaysia', 1),
	(126, 'Maldives', 1),
	(127, 'Mali', 1),
	(128, 'Malta', 1),
	(129, 'Marshall Islands', 1),
	(130, 'Martinique', 1),
	(131, 'Mauritania', 1),
	(132, 'Mauritius', 1),
	(133, 'Mayotte', 1),
	(134, 'Mexico', 1),
	(135, 'Micronesia', 1),
	(136, 'Moldova', 1),
	(137, 'Monaco', 1),
	(138, 'Mongolia', 1),
	(139, 'Montserrat', 1),
	(140, 'Morocco', 1),
	(141, 'Mozambique', 1),
	(142, 'Myanmar', 1),
	(143, 'Namibia', 1),
	(144, 'Nauru', 1),
	(145, 'Nepal', 1),
	(146, 'Neth Antilles', 1),
	(147, 'Netherlands', 1),
	(148, 'New Caledonia', 1),
	(149, 'New Zealand', 1),
	(150, 'Nicaragua', 1),
	(151, 'Niger', 1),
	(152, 'Nigeria', 1),
	(153, 'Niue', 1),
	(154, 'Norfolk Island', 1),
	(155, 'North Korea', 1),
	(156, 'Norway', 1),
	(157, 'Oman', 1),
	(158, 'Pakistan', 1),
	(159, 'Palau', 1),
	(160, 'Panama', 1),
	(161, 'Papua Newguinea', 1),
	(162, 'Paraguay', 1),
	(163, 'Peru', 1),
	(164, 'Philippines', 1),
	(165, 'Pitcairn', 1),
	(166, 'Poland', 1),
	(167, 'Portugal', 1),
	(168, 'Puerto Rico', 1),
	(169, 'Qatar', 1),
	(170, 'Rawanda', 1),
	(171, 'Râ”œÂ®publique dâ”œÂ®mocratique du Congo', 1),
	(172, 'Reunion', 1),
	(173, 'Romania', 1),
	(174, 'Russian Federation', 1),
	(175, 'Saint Kitts and Nevis', 1),
	(176, 'Saint Lucia', 1),
	(177, 'Samoa', 1),
	(178, 'San Marino', 1),
	(179, 'Sao Tome', 1),
	(180, 'Saudi Arabia', 1),
	(181, 'Senegal', 1),
	(182, 'Serbia', 1),
	(183, 'Seychelles', 1),
	(184, 'Sierra Leone', 1),
	(185, 'Singapore', 1),
	(186, 'Slovakia', 1),
	(187, 'Slovenia', 1),
	(188, 'Solomon Islands', 1),
	(189, 'Somalia', 1),
	(190, 'South Africa', 1),
	(191, 'South Korea', 1),
	(192, 'Spain', 1),
	(193, 'Sri Lanka', 1),
	(194, 'St. Helena', 1),
	(195, 'Stkitts Nevis', 1),
	(196, 'Sudan', 1),
	(197, 'Suriname', 1),
	(198, 'Swaziland', 1),
	(199, 'Sweden', 1),
	(200, 'Switzerland', 1),
	(201, 'Syria', 1),
	(202, 'Taiwan', 1),
	(203, 'Tajikistan', 1),
	(204, 'Tanzania', 1),
	(205, 'Thailand', 1),
	(206, 'Togo', 1),
	(207, 'Tokelau', 1),
	(208, 'Tonga', 1),
	(209, 'Trinidad & Tobago', 1),
	(210, 'Tunisia', 1),
	(211, 'Turkey', 1),
	(212, 'Turkmenistan', 1),
	(213, 'Tuvala', 1),
	(214, 'Uganda', 1),
	(215, 'Ukraine', 1),
	(216, 'United Arab Emerates', 1),
	(217, 'United Kingdom', 1),
	(218, 'United States', 1),
	(219, 'Uruguay', 1),
	(220, 'Ussr', 1),
	(221, 'Uzbekistan', 1),
	(222, 'Vanuatu', 1),
	(223, 'Venezuela', 1),
	(224, 'Viet Nam', 1),
	(225, 'Virgin Islands (British)', 1),
	(226, 'Virgin Islands (U.S.)', 1),
	(227, 'Western Sahara', 1),
	(228, 'Yemen', 1),
	(229, 'Yugoslavia', 1),
	(230, 'Zaire', 1),
	(231, 'Zambia', 1),
	(232, 'Zimbabwe', 1);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memos
CREATE TABLE IF NOT EXISTS `credit_memos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `invoice_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `sales_rep_id` int(11) DEFAULT NULL,
  `ar_id` int(11) DEFAULT NULL,
  `cm_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reason_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `total_amount_invoice` decimal(15,3) DEFAULT NULL,
  `balance` decimal(15,3) DEFAULT NULL,
  `discount` decimal(15,3) DEFAULT NULL,
  `discount_percent` decimal(6,3) DEFAULT NULL,
  `mark_up` decimal(15,3) DEFAULT NULL,
  `vat_chart_account_id` int(11) DEFAULT NULL,
  `total_vat` decimal(15,3) DEFAULT NULL,
  `vat_percent` decimal(5,3) DEFAULT NULL,
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_filter_second` (`sales_order_id`,`ar_id`,`reason_id`,`price_type_id`),
  KEY `key_search` (`invoice_code`,`invoice_date`,`cm_code`,`balance`,`order_date`,`created_by`,`status`),
  KEY `key_filter` (`location_group_id`,`location_id`,`company_id`,`customer_id`,`currency_center_id`,`vat_chart_account_id`,`vat_setting_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memos: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memos` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memos` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memo_details
CREATE TABLE IF NOT EXISTS `credit_memo_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `credit_memo_id` int(11) unsigned DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `credit_memo_id` (`credit_memo_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memo_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memo_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memo_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memo_miscs
CREATE TABLE IF NOT EXISTS `credit_memo_miscs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `credit_memo_id` int(11) unsigned DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(10) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `credit_memo_id` (`credit_memo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memo_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memo_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memo_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memo_receipts
CREATE TABLE IF NOT EXISTS `credit_memo_receipts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_memo_id` int(11) unsigned DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `receipt_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_us` decimal(15,3) DEFAULT '0.000',
  `amount_other` decimal(15,3) DEFAULT '0.000',
  `total_amount` decimal(15,3) DEFAULT '0.000',
  `balance` decimal(15,3) DEFAULT '0.000',
  `balance_other` decimal(15,3) DEFAULT '0.000',
  `change` decimal(15,3) DEFAULT NULL,
  `pay_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_void` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `credit_memo_id` (`credit_memo_id`),
  KEY `receipt_code` (`receipt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memo_receipts: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memo_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memo_receipts` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memo_services
CREATE TABLE IF NOT EXISTS `credit_memo_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `credit_memo_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `discount_id` int(10) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `start` date DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: General; 2: Monthly',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `credit_memo_id` (`credit_memo_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memo_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memo_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memo_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.credit_memo_with_sales
CREATE TABLE IF NOT EXISTS `credit_memo_with_sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_memo_id` int(10) unsigned DEFAULT NULL,
  `sales_order_id` int(10) unsigned DEFAULT NULL,
  `total_price` decimal(15,3) DEFAULT '0.000',
  `status` tinyint(4) DEFAULT NULL,
  `apply_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `credit_memo_id` (`credit_memo_id`),
  KEY `sales_order_id` (`sales_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.credit_memo_with_sales: ~0 rows (approximately)
/*!40000 ALTER TABLE `credit_memo_with_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_memo_with_sales` ENABLE KEYS */;


-- Dumping structure for table sas_master.crontab_inv_adjs
CREATE TABLE IF NOT EXISTS `crontab_inv_adjs` (
  `cycle_product_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  UNIQUE KEY `cycle_product_id` (`cycle_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.crontab_inv_adjs: ~0 rows (approximately)
/*!40000 ALTER TABLE `crontab_inv_adjs` DISABLE KEYS */;
/*!40000 ALTER TABLE `crontab_inv_adjs` ENABLE KEYS */;


-- Dumping structure for table sas_master.currency_centers
CREATE TABLE IF NOT EXISTS `currency_centers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `symbol` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_searchs` (`name`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.currency_centers: ~13 rows (approximately)
/*!40000 ALTER TABLE `currency_centers` DISABLE KEYS */;
INSERT INTO `currency_centers` (`id`, `sys_code`, `name`, `symbol`, `photo`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, 'c3691f6f110a04bc03f49ff1b6be11c2', 'USD - US Dollar', '$', NULL, '2015-12-16 13:43:33', 1, '2015-12-16 13:43:33', NULL, 1),
	(2, '50dbcae7b49c7ba293200a3df51b23eb', 'KHR - Cambodia Riel', '៛', NULL, '2015-12-16 13:44:48', 1, '2015-12-16 13:44:48', NULL, 1),
	(3, '5fed42699043f9baba7e518941dafa90', 'THB - Thai Baht', '฿', NULL, '2015-12-16 13:45:56', 1, '2015-12-16 13:45:56', NULL, 1),
	(4, '74305edde4cd955d64ab68229714e719', 'VND - Viet Nam Dong', '₫', NULL, '2015-12-16 13:47:36', 1, '2015-12-16 13:47:36', NULL, 1),
	(5, 'abea1ec3126d23d3eda701ecc0a1dcd7', 'MYR - Malaysia Ringgit', 'RM', NULL, '2015-12-16 13:48:12', 1, '2015-12-16 13:48:12', NULL, 1),
	(6, 'bb5cfa9f1bfad3cb7a7a749870181e6e', 'LAK - Laos Kip', '₭', NULL, '2015-12-16 13:49:50', 1, '2015-12-16 13:51:55', NULL, 1),
	(7, '1571d082a6c3362c83785d28a56856f8', 'SGD - Singapore Dollar', '$', NULL, '2015-12-16 13:51:00', 1, '2015-12-16 13:52:42', NULL, 1),
	(8, '9b50c6bfb5aec95e1d10ed7b6a5398a3', 'PHP - Philippines Peso', '₱', NULL, '2015-12-16 13:51:40', 1, '2015-12-16 13:51:40', NULL, 1),
	(9, '2926eeaaaeaa0873afcee0f6480e56b4', 'IDR - Indonesia Rupiah', 'Rp', NULL, '2015-12-16 13:53:30', 1, '2015-12-16 13:53:30', NULL, 1),
	(10, '983db8739b2fdf6ebffbff1b750a8e8c', 'BND - Brunei Darussalam Dollar', '$', NULL, '2015-12-16 13:54:40', 1, '2015-12-16 13:54:40', NULL, 1),
	(11, '5e41b3512e59058fc1d631133f3df8f8', 'CNY - China Yuan Renminbi', '¥', NULL, '2015-12-17 15:11:01', 1, '2015-12-17 15:11:01', NULL, 1),
	(12, '0b81d08dc7383cef91a806761cca7f72', 'JPY - Japan Yen', '¥', NULL, '2015-12-17 15:13:56', 1, '2015-12-17 15:13:56', NULL, 1),
	(13, '18db72eada1a4375c5a916de6d7ecb5c', 'KRW - Korea (South) Won', '₩', NULL, '2015-12-17 15:15:31', 1, '2015-12-17 15:15:31', NULL, 1);
/*!40000 ALTER TABLE `currency_centers` ENABLE KEYS */;


-- Dumping structure for table sas_master.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_other` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT '1',
  `payment_every` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `limit_balance` double DEFAULT NULL,
  `limit_total_invoice` int(11) DEFAULT NULL,
  `discount` decimal(7,3) DEFAULT '0.000' COMMENT 'Percentage (%)',
  `vat` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `total_sales` int(11) DEFAULT NULL,
  `last_invoice_cm` int(11) DEFAULT NULL,
  `last_invoice_cm_date` date DEFAULT NULL COMMENT 'Invoice Has CM or Not',
  `last_invoice_cm_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_invoice` int(11) DEFAULT NULL COMMENT 'Invoice Not Has CM',
  `last_invoice_date` date DEFAULT NULL,
  `last_invoice_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_cm` int(11) DEFAULT NULL,
  `last_cm` int(11) DEFAULT NULL,
  `last_cm_date` date DEFAULT NULL,
  `last_cm_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_order` int(11) DEFAULT NULL,
  `last_order` int(11) DEFAULT NULL,
  `last_order_date` date DEFAULT NULL,
  `last_order_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_quote` int(11) DEFAULT NULL,
  `last_quote` int(11) DEFAULT NULL,
  `last_quote_date` date DEFAULT NULL,
  `last_quote_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Country, 2: Over Sea',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `is_active` (`is_active`),
  KEY `searchs` (`customer_code`,`name`,`name_kh`,`main_number`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.customers: ~1 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `sys_code`, `house_no`, `street_id`, `province_id`, `district_id`, `commune_id`, `village_id`, `address`, `address_other`, `payment_term_id`, `payment_every`, `customer_code`, `name`, `name_kh`, `sex`, `photo`, `main_number`, `mobile_number`, `other_number`, `email`, `fax`, `limit_balance`, `limit_total_invoice`, `discount`, `vat`, `note`, `total_sales`, `last_invoice_cm`, `last_invoice_cm_date`, `last_invoice_cm_code`, `last_invoice`, `last_invoice_date`, `last_invoice_code`, `total_cm`, `last_cm`, `last_cm_date`, `last_cm_code`, `total_order`, `last_order`, `last_order_date`, `last_order_code`, `total_quote`, `last_quote`, `last_quote_date`, `last_quote_code`, `created`, `created_by`, `modified`, `modified_by`, `type`, `is_active`) VALUES
	(1, '48195eefe4cb8d414f0fc05e52f2696c', '', NULL, NULL, NULL, NULL, NULL, '', NULL, 1, '0', 'General', 'General', 'General', NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-02-17 14:38:38', 1, '2015-12-29 17:10:59', NULL, 1, 1);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;


-- Dumping structure for table sas_master.customer_cgroups
CREATE TABLE IF NOT EXISTS `customer_cgroups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) DEFAULT NULL,
  `cgroup_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `cgroup_id` (`cgroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.customer_cgroups: ~1 rows (approximately)
/*!40000 ALTER TABLE `customer_cgroups` DISABLE KEYS */;
INSERT INTO `customer_cgroups` (`id`, `customer_id`, `cgroup_id`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `customer_cgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.customer_companies
CREATE TABLE IF NOT EXISTS `customer_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.customer_companies: ~1 rows (approximately)
/*!40000 ALTER TABLE `customer_companies` DISABLE KEYS */;
INSERT INTO `customer_companies` (`id`, `customer_id`, `company_id`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `customer_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.customer_contacts
CREATE TABLE IF NOT EXISTS `customer_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `title` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_telephone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modules` (`company_id`,`customer_id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`contact_name`,`is_active`,`contact_telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.customer_contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_contacts` ENABLE KEYS */;


-- Dumping structure for table sas_master.cycle_products
CREATE TABLE IF NOT EXISTS `cycle_products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `location_id` (`location_group_id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.cycle_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `cycle_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `cycle_products` ENABLE KEYS */;


-- Dumping structure for table sas_master.cycle_product_details
CREATE TABLE IF NOT EXISTS `cycle_product_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cycle_product_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `lots_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `current_qty` int(11) DEFAULT '0',
  `new_qty` int(11) DEFAULT '0',
  `qty_difference` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cycle_product_id` (`cycle_product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.cycle_product_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `cycle_product_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `cycle_product_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.dashboard_payable
CREATE TABLE IF NOT EXISTS `dashboard_payable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `amount` decimal(20,9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filter` (`purchase_order_id`,`chart_account_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.dashboard_payable: 0 rows
/*!40000 ALTER TABLE `dashboard_payable` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_payable` ENABLE KEYS */;


-- Dumping structure for table sas_master.dashboard_profit_loss
CREATE TABLE IF NOT EXISTS `dashboard_profit_loss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `debit` decimal(20,9) DEFAULT NULL,
  `credit` decimal(20,9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `chart_account_id` (`chart_account_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.dashboard_profit_loss: 0 rows
/*!40000 ALTER TABLE `dashboard_profit_loss` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_profit_loss` ENABLE KEYS */;


-- Dumping structure for table sas_master.dashboard_receivable
CREATE TABLE IF NOT EXISTS `dashboard_receivable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `amount` decimal(20,9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filter` (`sales_order_id`,`chart_account_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.dashboard_receivable: 0 rows
/*!40000 ALTER TABLE `dashboard_receivable` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_receivable` ENABLE KEYS */;


-- Dumping structure for table sas_master.deliveries
CREATE TABLE IF NOT EXISTS `deliveries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `branch_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `ship_to` text COLLATE utf8_unicode_ci,
  `customer_contact_id` int(11) DEFAULT NULL,
  `delivery_by` int(11) DEFAULT NULL COMMENT 'Employee',
  `long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_receive` tinyint(4) DEFAULT NULL COMMENT '0: Not Yet; 1: Received',
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `picked` datetime DEFAULT NULL,
  `picked_by` int(10) DEFAULT NULL,
  `delivered` datetime DEFAULT NULL,
  `delivered_by` int(10) DEFAULT NULL,
  `closed` datetime DEFAULT NULL,
  `closed_by` int(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0: Void; 1: Issue; 2: Picked; 3: Delivered; 4: Completed',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `search` (`date`,`code`,`status`),
  KEY `delivery_by` (`delivery_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.deliveries: ~0 rows (approximately)
/*!40000 ALTER TABLE `deliveries` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliveries` ENABLE KEYS */;


-- Dumping structure for table sas_master.delivery_details
CREATE TABLE IF NOT EXISTS `delivery_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_order_detail_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL COMMENT 'Total By Invoice',
  `total_pick` int(11) DEFAULT NULL COMMENT 'Total Pick',
  `aisle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bay` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `delivery_id` (`delivery_id`),
  KEY `location_id` (`location_id`),
  KEY `product_id` (`product_id`),
  KEY `sales_order_detail_id` (`sales_order_detail_id`),
  KEY `locationInfo` (`aisle`,`position`,`bay`,`level`,`bin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.delivery_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `delivery_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `delivery_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.discounts
CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(10) DEFAULT NULL,
  `income_chart_account_id` int(10) DEFAULT NULL,
  `expense_chart_account_id` int(10) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percent` decimal(5,3) DEFAULT NULL,
  `amount` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `company_id` (`company_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.discounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;


-- Dumping structure for table sas_master.discount_by_cards
CREATE TABLE IF NOT EXISTS `discount_by_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` decimal(7,3) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.discount_by_cards: ~0 rows (approximately)
/*!40000 ALTER TABLE `discount_by_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount_by_cards` ENABLE KEYS */;


-- Dumping structure for table sas_master.districts
CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`),
  KEY `province_id` (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.districts: ~157 rows (approximately)
/*!40000 ALTER TABLE `districts` DISABLE KEYS */;
INSERT INTO `districts` (`id`, `sys_code`, `province_id`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(3, '308706047a8b7c78623813e9fc2351ff', 26, 'CHAMKARMON', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(4, 'f2aa763acf42fa8496e3c2c563dd7fe7', 26, 'CHBAR AMPOV', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(5, 'fe4da99e594b211dfe412ca103633133', 26, 'CHROY CHANGVA', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(6, '604cc04a7366dc7ce06513624bef23e6', 26, 'DANGKAO', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(7, 'ea1d9ede0eb5d94c0c3c75a8bbe1eb37', 26, 'DAUN PENH', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(8, '7dea419bf5bfd956dae331f53426dcb4', 26, 'MEANCHEY', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(9, '1e7287f8f58cba8871b1d60f9bf52f4f', 26, '7 MAKARA', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(10, 'd689ca2e0dc2db06d64e5148f1df294d', 26, 'RUSSEY KEO', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(11, '5cff26afeb16f14da0f4ce2bf2c24d7a', 26, 'SEN SOK', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(12, '5fd3e3e3540c13f0e8658e06a9c15d02', 26, 'TOUL KORK', '2014-12-03 11:40:16', 1, '2014-12-22 14:15:20', 1, 1),
	(13, '68f38b9f6a64ee0c5061c5e0cce5d5ce', 26, 'PORSENCHEY', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(14, '7ead0754421e743e862c9850946afe2d', 26, 'PREK PNOV', '2014-12-03 11:40:16', 1, '2014-12-03 11:40:16', NULL, 1),
	(15, 'f8a043dc9ddbe5ccbc671c79a10a98c5', 27, 'MONGKOL BOREAY', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(16, '8329e3f84a80a370d4d6dd2861736d59', 27, 'PHNOM SROK', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(17, '9db2601442e26fdae6e872ce9ccad5fe', 27, 'PREAH NET PREAH', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(18, '82de72321908f72d47173a52b07d3deb', 27, 'O CHROV', '2014-12-22 13:18:42', 1, '2014-12-22 15:22:21', 1, 1),
	(19, 'a179081598432dc42e6e2e2d274d1236', 27, 'SEREI SAOPHOAN', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(20, '538d4784bbb2931efaf6c2da4a7ab9b6', 27, 'THMAR POUK', '2014-12-22 13:18:42', 1, '2014-12-22 14:16:01', 1, 1),
	(21, '00a11e1fc151b4e6a5f052e9abdd710e', 27, 'SVAY CHEK', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(22, 'ccc8f8cd772c67d1b6e8afc31dc91777', 27, 'MALAI', '2014-12-22 13:18:42', 1, '2014-12-22 13:18:42', NULL, 1),
	(23, 'dbf75a33036e461a0efc0e44bcf77a04', 28, 'BANAN', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(24, '7af62e472cfbaa8699c71505aa777e3e', 28, 'THMAR KOUL', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(25, '668ec33f8f985613d9714721f626a262', 28, 'BATTAMBANG', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(26, '116ddca51d786f363b25e395f8721fac', 28, 'BAVEL', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(27, '65293ef3b86dd72c95919b77b080da85', 28, 'EK PHNOM', '2014-12-22 13:20:05', 1, '2014-12-22 14:16:34', 1, 1),
	(28, 'd0bc3b562a44492c0b24424c50603625', 28, 'MOUNG RUSSEI', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(29, '6268b62e252d3158443bdc6ea70a9a2a', 28, 'RATANAK MONDUL', '2014-12-22 13:20:05', 1, '2014-12-22 13:20:05', NULL, 1),
	(30, '047e04d4eba23c085e16b9f3726f4dbb', 28, 'SANGKE', '2014-12-22 13:22:54', 1, '2014-12-22 14:14:16', 1, 1),
	(31, '6e2f81d508c596bbb1870623f5be5fb0', 28, 'SAMLOUT', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(32, '16b8cf69a023d4218edd8465163bff4e', 28, 'SAMPOV LOUN', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(33, '3b01ed59a18d666ee1bdbc8d2387f9b6', 28, 'PHNOM PROEK', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(34, '339d6fa1815337341ba7866769471f80', 28, 'KAMRIENG', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(35, '34d93bf124da5fa0b98e6a588e3b5ede', 28, 'KOAS KRALA', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(36, '2404fe6d6c5debe3769516309bf2ccec', 28, 'RUHAKIRI', '2014-12-22 13:22:54', 1, '2014-12-22 13:22:54', NULL, 1),
	(37, '51a73f9739e8f21708382dbdc2802b4f', 29, 'BATHEAY', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(38, '17cae5ada33e9ecc38baa7abe7fb0940', 29, 'CHAMKAR LEU', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(39, '80dc9246fac0077781dea1b21b30e852', 29, 'CHEUNG PREY', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(40, 'b143281f04ad356220529e07e66c95b8', 29, 'KAMPONG CHAM', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(41, 'd5442bc96d3b07c65a3bebfa30d9125f', 29, 'KAMPONG SIEM', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(42, '55854c2bdb675a3ae7d92d34c24be1b9', 29, 'KANG MEAS', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(43, '552b77e67bb7a060ee15b9b991040197', 29, 'KOH SOTIN', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(44, '44e4bc9b04babe418789874970da12a0', 29, 'PREY CHHOR', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(45, 'a180e7e4aaaab179800588fd6b235f3c', 29, 'SREY SANTHOR', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(46, 'e1c982d723fb385d4504be568f17b225', 29, 'STUNG TRANG', '2014-12-22 13:25:17', 1, '2014-12-22 13:25:17', NULL, 1),
	(47, '2cefe4a97b42fbb0781231a09fb91c20', 30, 'BARIBOUR', '2014-12-22 13:25:36', 1, '2014-12-22 13:25:36', NULL, 1),
	(48, 'bb4044cb8a0eecbaec5202b0812adc83', 29, 'CHOL KIRI', '2014-12-22 13:27:51', 1, '2014-12-22 13:27:51', NULL, 1),
	(49, '1fda6f2d3765505a1d4a7412697eae22', 30, 'KAMPONG CHHNANG', '2014-12-22 13:27:51', 1, '2015-01-15 11:56:09', 2, 1),
	(50, 'ec46a2935a39cf8b0b01be5f247a4395', 29, 'KAMPONG LENG', '2014-12-22 13:27:51', 1, '2014-12-22 13:27:51', NULL, 1),
	(51, 'fa771c3b4f21a98561e347a1ab1e10dd', 30, 'KAMPONG TRALACH', '2014-12-22 13:27:51', 1, '2015-01-19 09:59:48', 2, 1),
	(52, '7a431de0963cb6bd97e79270d840c43a', 30, 'ROLEA BIER', '2014-12-22 13:27:51', 1, '2015-01-15 11:50:10', 2, 1),
	(53, '3404a7082331fe1935f654df030d8f20', 29, 'SAMAKI MEANCHEY', '2014-12-22 13:27:51', 1, '2014-12-22 13:27:51', NULL, 1),
	(54, 'bccd3e08903f3fa1a5648ccd3099a94f', 29, 'TUEK PHOS', '2014-12-22 13:27:51', 1, '2014-12-22 13:27:51', NULL, 1),
	(55, '86bbbd647ec23513805410677d4c9b51', 31, 'BASET', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(56, '5faee10ff4fa5c4944e8d32e6280a3b7', 31, 'CHBAR MON', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(57, '3a6e8070f6d2755c0536134636ce2b23', 31, 'KONG PISEI', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(58, 'fd3e3cc506aa787797b87f8b5181f89c', 31, 'AURAL', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(59, 'f7c56a11dc26246eead76270f249a728', 31, 'UDONG', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(60, '4108f95d2a759340f2ed892829b7c40b', 31, 'PHNOM SRUOCH', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(61, '0353d254829d9662f20917fbe4be0685', 31, 'SAMRAONG TONG', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(62, '2cf3c6584c301d6e9fb93d43697971be', 31, 'THPONG', '2014-12-22 13:29:23', 1, '2014-12-22 13:29:23', NULL, 1),
	(63, '90d2cdfb3118e2510f3de792346b98be', 32, 'BARAY', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(64, '875d5aca4b0d9c09565ed35a46c261ec', 32, 'KAMPONG SVAY', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(65, '768342bde9c68daf3468430cfa0e7f78', 32, 'STUNG SEN', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(66, 'b1728eba16134a413390441d35fff9d7', 32, 'PRASAT BALANG', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(67, 'b7f51837908e46f19cc65882b4e4a943', 32, 'PRASAT SAMBOUR', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(68, '0ac71b9ece4afc53042eade96b98ec7e', 32, 'SANDAN', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(69, '0647eeb32740e636903632be4e28bbde', 32, 'SANTUK', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(70, '7358e1b780ef0280953ac2d40b9fe870', 32, 'STOUNG', '2014-12-22 13:31:05', 1, '2014-12-22 13:31:05', NULL, 1),
	(71, '783c676ae725bd7649bf8d1d03705cf8', 33, 'ANGKOR CHEY', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(72, '1ebf9e6eb0d1112c487555ff72e5b68b', 33, 'BANTEAY MEAS', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(73, 'a2593859b6ba5fb6f3f912448fa3a3cf', 33, 'CHHUK', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(74, '5999e446b8e25d186f8c044c59f520fd', 33, 'CHUM KIRI', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(75, 'b475dc4daa54dc8b17125d854ce460aa', 33, 'DANG TONG', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(76, 'e19b4fc4e7ababdcb6146f5b74f9ec82', 33, 'KAMPONG TRACH', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(77, '8377c267a6df7638096868d902a11784', 33, 'TUEK CHHOU', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(78, '65c0f4f746c3e5419475ffd8b888db2a', 33, 'KAMPOT', '2014-12-22 13:33:09', 1, '2014-12-22 13:33:09', NULL, 1),
	(79, '90db0f94803b453f40d433426f525650', 34, 'KANDAL STUNG', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(80, '2d88a6b3b0ae028e3c84017bd313e02b', 34, 'KIEN SVAY', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(81, 'a7dc3512723b60625032600f54639120', 34, 'KHSACH KANDAL', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(82, '4ce782c79c1294bb1211171fcf31076d', 34, 'KOH THOM', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(83, '6fd543663d5a18b6b8e40de860039dac', 34, 'LEUK DEK', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(84, '0e84b042a4fc8507d60fc1f998054525', 34, 'LAVEAR EM', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(85, 'cdce47f75eaeec7a55eb4888db5db106', 34, 'MUKH KOMPHOOL', '2014-12-22 13:35:22', 1, '2015-01-10 14:24:48', 2, 1),
	(86, '063a54f2152f5139cb3d7f7d9ceb5f88', 34, 'ANG SNOUL', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(87, '73ecf456184b2d27bf886c669eb11d75', 34, 'PONHEA LEU', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(88, '95766d0fb087881b7b4b2a83f82a85af', 34, 'S\'ANG', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(89, '70b132a0253b01801a38b89c148365ca', 34, 'TA KHMAO', '2014-12-22 13:35:22', 1, '2014-12-22 13:35:22', NULL, 1),
	(90, '2ec862462ba700fb126f58989a1fcce7', 35, 'KEP', '2014-12-22 13:36:17', 1, '2014-12-22 13:36:17', NULL, 1),
	(91, '42c3f93c35d5a546356d0f5b68135a2a', 35, 'DAMNAK CHANG\' AEUR', '2014-12-22 13:36:17', 1, '2014-12-22 13:36:17', NULL, 1),
	(92, '62d9af8707748bb5214a06032b1268df', 50, 'BOTUM SAKOR', '2014-12-22 13:38:22', 1, '2014-12-22 13:38:22', NULL, 1),
	(93, '2ddc14e200bb2b529b641bb79ea6ad52', 50, 'KIRI SAKOR', '2014-12-22 13:38:22', 1, '2014-12-22 13:38:22', NULL, 1),
	(94, '90d8c5d5b54bfbcb36eb6932ebf1d374', 50, 'KOH KONG', '2014-12-22 13:38:23', 1, '2014-12-22 13:38:23', NULL, 1),
	(95, '4a506f2c7b5e012076c61ffa5adcda66', 50, 'KRONG KAMARAKPUMIN', '2014-12-22 13:38:23', 1, '2014-12-22 13:38:23', NULL, 1),
	(96, '52d81dd252a6bf3c8f5af373d337acab', 50, 'MONDOL SEIMA', '2014-12-22 13:38:23', 1, '2014-12-22 13:38:23', NULL, 1),
	(97, 'f743571de0b2addb4c4235118185dc7f', 50, 'SRAE AMBEL', '2014-12-22 13:38:23', 1, '2014-12-22 13:38:23', NULL, 1),
	(98, '6f38ee6ce74789c40979582a03ead126', 50, 'THMAR BANG', '2014-12-22 13:38:23', 1, '2014-12-22 13:38:23', NULL, 1),
	(99, 'db1a30e43ebcc46c1b35be203f7b3006', 36, 'CHHLOUNG', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(100, 'd12dff943c698907c51007e55ad64254', 36, 'KRATIE', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(101, 'cc95135c138c49d181db60c034ca2801', 36, 'PREK PRASAB', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(102, '516539ce22d8678935e65c3a79f9de26', 36, 'SAMBOUR', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(103, '45eca94587bd24eb9b72bb2e38c8ac9f', 36, 'SNUOL', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(104, '395f6986baa37dedc0c6844a03116088', 36, 'CHET BOREI', '2014-12-22 13:41:29', 1, '2014-12-22 13:41:29', NULL, 1),
	(105, 'b97f56d8a53b83d7f1cb434becf4b24d', 37, 'KEO SEIMA', '2014-12-22 13:43:16', 1, '2014-12-22 13:43:16', NULL, 1),
	(106, '679c3c2bfb3759f49bcc75d9ca7d28b1', 37, 'KOH NHEAK', '2014-12-22 13:43:16', 1, '2014-12-22 13:43:16', NULL, 1),
	(107, '96a0b758989fdbc426fca4376b65b06a', 37, 'O REANG', '2014-12-22 13:43:16', 1, '2014-12-22 13:43:16', NULL, 1),
	(108, 'fdce2f474192153725d14928816df787', 37, 'PICH CHANDA', '2014-12-22 13:43:16', 1, '2014-12-22 13:43:16', NULL, 1),
	(109, '60efe349a65cf8511ab828117bec2ffb', 37, 'SENMONOROM', '2014-12-22 13:43:16', 1, '2014-12-22 13:43:16', NULL, 1),
	(110, 'f641e1f06cb6c7e60d840a805442e083', 38, 'ANLONG VENG', '2014-12-22 13:44:22', 1, '2014-12-22 13:44:22', NULL, 1),
	(111, 'a3ee0c93c6e836ee3b1e702e06fb1539', 38, 'BANTEAY AMPIL', '2014-12-22 13:44:22', 1, '2014-12-22 13:44:22', NULL, 1),
	(112, '016b0c3d8658cc62f1039e2ff42bff7e', 38, 'CHONG KAL', '2014-12-22 13:44:22', 1, '2014-12-22 13:44:22', NULL, 1),
	(113, '38dee0756001d7deded428d765208570', 38, 'SAMRAONG', '2014-12-22 13:44:22', 1, '2014-12-22 13:44:22', NULL, 1),
	(114, '237657942823ae8014983a036ac678c5', 38, 'TRAPEANG PRASAT', '2014-12-22 13:44:22', 1, '2014-12-22 13:44:22', NULL, 1),
	(115, 'b15992c96bce4503213b13abba686e3a', 39, 'PAILIN', '2014-12-22 13:45:04', 1, '2014-12-22 13:45:04', NULL, 1),
	(116, 'cc19c6da7fccc696b9ca40ef1a9b1802', 39, 'SALA KRAOU', '2014-12-22 13:45:04', 1, '2014-12-22 13:45:04', NULL, 1),
	(117, 'd45a119eb035836ecc8f0d94b84e8b6e', 40, 'MITTAPHEAP', '2014-12-22 13:46:00', 1, '2014-12-22 13:46:00', NULL, 1),
	(118, '76aa0e3d26eb0db88aa7673c2789d5ed', 40, 'PREY NOB', '2014-12-22 13:46:00', 1, '2014-12-22 13:46:00', NULL, 1),
	(119, '1cd22a69a8c8daa8477976054c251735', 40, 'STUNG HAV', '2014-12-22 13:46:00', 1, '2014-12-22 13:46:00', NULL, 1),
	(120, '3859e88c5d7f89d4a19dbaa77d38a3ee', 40, 'KAMPONG SEILA', '2014-12-22 13:46:00', 1, '2014-12-22 13:46:00', NULL, 1),
	(121, '01eed7e604532ce16de4c2ff6094b6a5', 41, 'CHEY SEN', '2014-12-22 13:46:59', 1, '2014-12-22 13:46:59', NULL, 1),
	(122, '852cfdee6187bdc8dbcac7d9e3eb1498', 41, 'CHHEB', '2014-12-22 13:46:59', 1, '2014-12-22 13:46:59', NULL, 1),
	(123, '57c11c87df882bef9588bd63b8ad5ef4', 41, 'CHAM KHSANT', '2014-12-22 13:46:59', 1, '2014-12-22 13:46:59', NULL, 1),
	(124, 'b6a332c4a7466439036d461c02f0c2a4', 41, 'KULEN', '2014-12-22 13:48:17', 1, '2014-12-22 13:48:17', NULL, 1),
	(125, '55da8233ed4273a5a4854a65578cf06c', 41, 'ROVIENG', '2014-12-22 13:48:17', 1, '2014-12-22 13:48:17', NULL, 1),
	(126, 'c563cedb34de2bbe5e4ea6e11065cc5f', 41, 'SANGKOM THMEY', '2014-12-22 13:48:17', 1, '2014-12-22 13:48:17', NULL, 1),
	(127, '7ec9a9156aa54dbb94e391a0488b1720', 41, 'TBENG MEANCHEY', '2014-12-22 13:48:17', 1, '2014-12-22 13:48:17', NULL, 1),
	(128, '7ee8e0f6bd2070e385f2284d8edfa003', 42, 'BAKAN', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(129, '81571e3877c1d31196dcb248236dc440', 42, 'KANDIENG', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(130, '4d354546ba7f28af86d345fb2ca14459', 42, 'KRAKOR', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(131, 'f40e42fadade6065550ee944547b56af', 42, 'PHNOM PRAVANH', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(132, '88e7cd7819cbb9588c09f4830e393412', 42, 'SAMPOV MEAS', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(133, '9072a97b7e366211ae4c72d42163c459', 42, 'VEAL VENG', '2014-12-22 13:49:20', 1, '2014-12-22 13:49:20', NULL, 1),
	(134, 'df4ac84620db75f0cb7661d7eb46fde4', 43, 'BA PHNOM', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(135, '0f9c915a7111383c5d87acc9559ad9d5', 43, 'KAMCHAY MEAR', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(136, '5d362af836eedb3094d6054e3fb5b4cf', 43, 'KAMPONG TRABEK', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(137, 'c42aebdbbf984a6d5a860171fa24aead', 43, 'KANHCHRIECH', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(138, 'fbe5d78c8a779d3d436b3c3fa7843b00', 43, 'ME SANG', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(139, '0e8506af3adc2ad6ac709db92e9dc26f', 43, 'PEAM CHOR', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(140, '1b6c13afa808f72a6b328fb35bbefb51', 43, 'PEAM ROR', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(141, '385e95f0912c0633ac461b0caa4fedb2', 43, 'PEA RANG', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(142, 'a46c6b82f4779b6f2327893de600784f', 43, 'PREAH SDACH', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(143, '6663a15654c30596b13db2bd1628133b', 43, 'PREY VENG', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(144, '89ceb59e2b089226c36a516b8963d057', 43, 'KAMPONG LEAV', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(145, '4cd175ee8c8b35835fbf1d0868749e11', 43, 'SITHOR KANDAL', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(146, 'e4acf3c3b29e9718a2bc36b82e32845f', 43, 'SVAY ANTOR', '2014-12-22 13:52:00', 1, '2014-12-22 13:52:00', NULL, 1),
	(147, 'af83dd3a8d049a53702e386c3cf39264', 44, 'ANDOUNG MEAS', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(148, '3bb2bb20c603808b883e52a8ff0f0200', 44, 'BANLUNG', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(149, 'fbf76ee19925f5990c21124c139d48ce', 44, 'BAR KAEV', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(150, '554efaf5c66c3050642e9a81795b30c9', 44, 'KOUN MOM', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(151, 'c0967d6c6b0102ffc73447763dd74eb6', 44, 'LUMPHAT', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(152, '71144a9787c61a20d201c506c2966205', 44, 'O CHUM', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(153, '7cf44e174c7530ba4bac0c6f2810163a', 44, 'O YA DA', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(154, '4729fdb0b3175a8c534dd96101fff51e', 44, 'TA VEANG', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(155, '30f33cd22982819a0960b918cd968b57', 44, 'VEUN SAI', '2014-12-22 13:53:52', 1, '2014-12-22 13:53:52', NULL, 1),
	(156, 'a11a99be84b13766d263a56200a7e7fc', 45, 'ANGKOR CHUM', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(157, '05fbcde5cc923a4eeccec964b9445121', 45, 'ANGKOR THOM', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(158, 'b94f444c37b216281dad8e8b809f630d', 45, 'BANTEAY SREI', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(159, '0d97ba6d7b26d943b6d061a8918d47a1', 45, 'CHI KRENG', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(160, '1a7baf6acd8a72d5722fb180fe9cde72', 45, 'KRALANH', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(161, '949a17e4721b85ef623ac46e51805786', 45, 'PUOK', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(162, '89ed0baee5c9eeb0fe11bdb7a9cf74ce', 45, 'PRASAT BAKONG', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(163, 'a8f6a55695b4543aca37464dd29b3211', 45, 'SIEM REAP', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(164, 'a5b0f719346b20509cf218ac7c00e588', 45, 'SOUT NIKOM', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(165, '0ff0451b26371323dee3d3ea041583ae', 45, 'SREI SNAM', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(166, '59b8f3269e7d850112a83512164f1615', 45, 'SVAY LEU', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(167, '2c6fcb38a03570ab231cf874e95c18dc', 45, 'VARIN', '2014-12-22 13:56:02', 1, '2014-12-22 13:56:02', NULL, 1),
	(168, 'edd8a3d8745d90f91ff0df6cfb545fc3', 46, 'SESAN', '2014-12-22 13:57:16', 1, '2014-12-22 13:57:16', NULL, 1),
	(169, '11001bbcb586ab9bde1658dd88277388', 46, 'SIEM BOUK', '2014-12-22 13:57:16', 1, '2014-12-22 13:57:16', NULL, 1),
	(170, 'cfe773da4fc01b4cbec7006a5680904b', 46, 'SIEM PANG', '2014-12-22 13:57:16', 1, '2014-12-22 13:57:16', NULL, 1),
	(171, '41acb8b0330836f809412e5162cdcfdf', 46, 'STUNG TRENG', '2014-12-22 13:57:16', 1, '2014-12-22 13:57:16', NULL, 1),
	(172, '5398ba9423e0e98df6989aae8640deff', 46, 'THALA BARIVAT', '2014-12-22 13:57:16', 1, '2014-12-22 13:57:16', NULL, 1),
	(173, 'a5a5f8af6d5ae7f684f82a7164c81e0c', 47, 'CHANTREA', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(174, '44c0c6ba7dfb0fff84a0edf464c538f0', 47, 'KAMPONG ROU', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(175, '8deb3e353ae7a109edd30dc2877664f8', 47, 'ROMDOUL', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(176, '9d2148cc22c01d513e9711972aa12ea0', 47, 'ROMEAS HAEK', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(177, 'ad3df56f98465fdd3ecaf8e73e2b40b7', 47, 'SVAY CHROM', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(178, 'f6e864527a83a93d7a3bf14e1efec985', 47, 'SVAY RIENG', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(179, '55954cc690e8c18a375a1bda7c963d9c', 47, 'SVAY TEAP', '2014-12-22 13:58:55', 1, '2014-12-22 13:58:55', NULL, 1),
	(180, '372bd084d6c46c118ca2968c4326726b', 48, 'ANGKOR BOREI', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(181, '8714127b3d06e6b8b16c5a6b194a5a2e', 48, 'BATI', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(182, 'daef8674f5e5b07d4c92e9e4747c7f60', 48, 'BOREI CHOLSAR', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(183, '15205b05b2b86c7d68299bf366e77c81', 48, 'KIRIVONG', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(184, 'f329888501b0f2cf463ce83374f34bd2', 48, 'KOH ANDET', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(185, 'c5c61437969dd32c531378ed767cd0db', 48, 'PREY KABBAS', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(186, '932b85ef072632e5947cd80897f62daa', 48, 'SAMRAONG', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(187, '58bb66e0a9104aff97c93619ad68300e', 48, 'DAUN KEO', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(188, 'b166ca4b71a325c6a5ff74c21aacfa12', 48, 'TRAM KAK', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(189, '81e023fbde91c164b664b990ee7859b0', 48, 'TREANG', '2014-12-22 14:02:24', 1, '2014-12-22 14:02:24', NULL, 1),
	(190, '9c1cd044ffe1503f162b5f61fc37c964', 49, 'DAMBAE', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(191, '12150a778c503e4f5863246dc1cef438', 49, 'KROCH CHHMAR', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(192, 'ab2fe97b8a8162a7cf80983dc6cdc3fb', 49, 'MEMOT', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(193, '3cacc123bcda97292373f3967ac8e132', 49, 'O REANG OV', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(194, 'de98a17be950d693ddd37a011d578ad1', 49, 'PONHEA KRAEK', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(195, '057e5313943d2b55ee318e586743e3f3', 49, 'TBONG KHMUM', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(196, '2ce99815c4fee6abc8757fe08bad9aad', 49, 'SUONG', '2014-12-22 14:04:18', 1, '2014-12-22 14:04:18', NULL, 1),
	(197, '2f7fa7114f9e7ab9517fed7be436653d', 43, 'SVAY POL', '2015-01-15 12:30:24', 2, '2015-01-15 12:30:24', NULL, 1),
	(198, '67d92dff16581bbd34599994c1166a85', 47, 'KRAL KOR', '2015-01-15 12:36:45', 2, '2015-01-15 12:36:45', NULL, 1),
	(199, 'e132113158d110ad0444ad1ec4ecac72', 47, 'BAVET', '2015-01-15 12:43:56', 2, '2015-01-15 12:43:56', NULL, 1),
	(200, 'd0b9de0cec2f0fd6d8a3b587a8ca41ef', 31, 'KAMPONG SPEU', '2015-01-24 15:34:50', 2, '2015-01-24 15:34:50', NULL, 1),
	(201, 'cf298644ad3db8db65acd3d0a476147a', 40, 'PREAH SIHANOUK', '2015-01-30 17:05:26', 2, '2015-01-30 17:05:26', NULL, 1),
	(202, '21dfac69cff6ade152b9b58ea0ab958f', 26, 'KHAN 7 MAKARA', '2015-03-23 14:54:45', 2, '2015-03-23 14:54:45', NULL, 1);
/*!40000 ALTER TABLE `districts` ENABLE KEYS */;


-- Dumping structure for table sas_master.egroups
CREATE TABLE IF NOT EXISTS `egroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.egroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `egroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `egroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.egroup_companies
CREATE TABLE IF NOT EXISTS `egroup_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `egroup_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `egroup_id` (`egroup_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.egroup_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `egroup_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `egroup_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.employees
CREATE TABLE IF NOT EXISTS `employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `start_working_date` date DEFAULT NULL,
  `termination_date` date DEFAULT NULL,
  `personal_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salary` decimal(15,3) DEFAULT NULL,
  `work_for_vendor_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_show_in_sales` tinyint(4) DEFAULT '0' COMMENT '1: Sale Rep; 2: Delivery; 3: Collector;',
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `employee_code` (`employee_code`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.employees: ~0 rows (approximately)
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;


-- Dumping structure for table sas_master.employee_companies
CREATE TABLE IF NOT EXISTS `employee_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.employee_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.employee_egroups
CREATE TABLE IF NOT EXISTS `employee_egroups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) DEFAULT NULL,
  `egroup_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `egroup_id` (`egroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.employee_egroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_egroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_egroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.exchange_rates
CREATE TABLE IF NOT EXISTS `exchange_rates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `rate_to_sell` decimal(15,9) DEFAULT '0.000000000',
  `rate_to_change` decimal(15,9) DEFAULT '0.000000000',
  `rate_purchase` decimal(15,9) DEFAULT '0.000000000',
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_searchs` (`currency_center_id`),
  KEY `company_id` (`branch_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.exchange_rates: ~0 rows (approximately)
/*!40000 ALTER TABLE `exchange_rates` DISABLE KEYS */;
/*!40000 ALTER TABLE `exchange_rates` ENABLE KEYS */;


-- Dumping structure for table sas_master.expenses
CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued',
  PRIMARY KEY (`id`),
  KEY `filters` (`date`,`reference`),
  KEY `searchs` (`vendor_id`,`customer_id`,`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.expenses: ~0 rows (approximately)
/*!40000 ALTER TABLE `expenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `expenses` ENABLE KEYS */;


-- Dumping structure for table sas_master.expense_details
CREATE TABLE IF NOT EXISTS `expense_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expense_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `expense_id` (`expense_id`),
  KEY `chart_account_id` (`chart_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.expense_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `expense_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `expense_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_pgroup_shares
CREATE TABLE IF NOT EXISTS `e_pgroup_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `e_product_category_id` int(11) DEFAULT NULL,
  `pgroup_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pgroup_id` (`pgroup_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_pgroup_shares: ~0 rows (approximately)
/*!40000 ALTER TABLE `e_pgroup_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_pgroup_shares` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_product_categories
CREATE TABLE IF NOT EXISTS `e_product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_product_categories: ~22 rows (approximately)
/*!40000 ALTER TABLE `e_product_categories` DISABLE KEYS */;
INSERT INTO `e_product_categories` (`id`, `sys_code`, `name`, `created`, `modified`, `is_active`) VALUES
	(1, '6bea011c3e7c45e433a7b7149ee349b6', 'Tech Accessories', '2017-02-14 13:15:13', '2017-02-14 13:15:16', 1),
	(2, '7aaaffc1934017a5469cd619767fa59a', 'Women\'s Fashion', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(3, 'f4e6d78dc5707f74e9751e7fd7ff9e4e', 'Jewelry & Watches', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(4, 'e8e6acf421301fbc8fef8db9b4f11a19', 'Cosmetics', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(5, 'ec3791d09d9f7096d485010cfb7b78c0', 'Home Decor', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(6, 'f13dd6c5845037ae0be7a6f4ab9ccdc2', 'Men\'s Fashion', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(7, '833eb321c634b8a1ba40065572ccd8c7', 'Auto Parts', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(8, 'd2bafab3f239b86836ab4e4fcde34d0c', 'Kids & Baby', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(9, 'cb053cc5037caa1c3082bd54f6f7a135', 'Mobile Phones', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(10, 'a89a1fdedbdabf60159684575e3f7918', 'Shoes & Bags', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(11, '7ab981410919ee27222011b20dde206e', 'Sports', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(12, '286f688f8bd98c0cd9ffa9bf71732164', 'Consumer Electronics', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(13, 'c44cc14aac67c95e044813177a8ec22d', 'Human Hair', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(14, '393a7a7fc1c39bab0aa0533f73d29fe5', 'Tablets', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(15, '57a50075891af34d9db0fcc3fd593c5c', 'Beverages', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(16, '136eed1f3e4a01aa1d8d87f703de5547', 'Beer', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(17, 'de84cc0b95b473fe4427399cd7a61441', 'Healthy Food', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(18, '8f991b45eb9a89415b0462bb2c334b2b', 'Vegetable', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(19, '0133849d8922fe3604587063d38928fc', 'Beds, mattresses', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(20, '50f576f41b2b9a18f724b35690a40e4a', 'Mirrors', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(21, 'b8698ef4b44b9089c79bbf7f0e045ec4', 'Chairs', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1),
	(22, 'c4bb8c342578584ab1cada843af52eac', 'Office furniture', '2017-02-14 13:15:14', '2017-02-14 13:15:17', 1);
/*!40000 ALTER TABLE `e_product_categories` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_product_detail_shares
CREATE TABLE IF NOT EXISTS `e_product_detail_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `view` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `rate` int(11) DEFAULT NULL,
  `rate_level` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_product_detail_shares: ~0 rows (approximately)
/*!40000 ALTER TABLE `e_product_detail_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_product_detail_shares` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_product_prices
CREATE TABLE IF NOT EXISTS `e_product_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `before_price` decimal(15,3) DEFAULT NULL,
  `sell_price` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uom_id` (`uom_id`),
  KEY `products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_product_prices: ~0 rows (approximately)
/*!40000 ALTER TABLE `e_product_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_product_prices` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_product_price_histories
CREATE TABLE IF NOT EXISTS `e_product_price_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `before_price` decimal(15,3) DEFAULT NULL,
  `sell_price` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uom_id` (`uom_id`),
  KEY `products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_product_price_histories: ~0 rows (approximately)
/*!40000 ALTER TABLE `e_product_price_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_product_price_histories` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_product_shares
CREATE TABLE IF NOT EXISTS `e_product_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `total_view` int(11) DEFAULT NULL,
  `total_order` int(11) DEFAULT NULL,
  `total_rate` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1' COMMENT '1: Active; 2: Disactive',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`product_id`),
  KEY `product_id` (`product_id`),
  KEY `company_id` (`company_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_product_shares: ~0 rows (approximately)
/*!40000 ALTER TABLE `e_product_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_product_shares` ENABLE KEYS */;


-- Dumping structure for table sas_master.e_store_shares
CREATE TABLE IF NOT EXISTS `e_store_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `e_mail` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `long` int(11) DEFAULT NULL,
  `lat` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_share` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.e_store_shares: ~1 rows (approximately)
/*!40000 ALTER TABLE `e_store_shares` DISABLE KEYS */;
INSERT INTO `e_store_shares` (`id`, `company_id`, `sys_code`, `name`, `telephone`, `address`, `e_mail`, `website`, `description`, `long`, `lat`, `created`, `created_by`, `modified`, `modified_by`, `is_share`) VALUES
	(1, 1, 'a576c53a36adda240771eb51306c2f85', 'UDAYA Technology', '023998877', 'Phnom Penh', '', '', '', NULL, NULL, '2018-07-18 14:34:25', 1, '2018-07-18 14:34:25', NULL, 0);
/*!40000 ALTER TABLE `e_store_shares` ENABLE KEYS */;


-- Dumping structure for table sas_master.fixed_assets
CREATE TABLE IF NOT EXISTS `fixed_assets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `vendor_id` bigint(20) DEFAULT NULL,
  `fixed_asset_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty_expires` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asset_account` int(11) DEFAULT NULL,
  `accum_account` int(11) DEFAULT NULL,
  `depr_account` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `cost` decimal(15,3) DEFAULT NULL,
  `cost_remain` decimal(15,3) DEFAULT '0.000',
  `asset_life` int(11) DEFAULT NULL,
  `depr_method` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salvage_value` decimal(15,3) DEFAULT NULL,
  `business_use_percentage` decimal(15,3) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `is_in_used` tinyint(4) DEFAULT '0',
  `is_depre` tinyint(4) DEFAULT '1',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`location_id`,`vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.fixed_assets: ~0 rows (approximately)
/*!40000 ALTER TABLE `fixed_assets` DISABLE KEYS */;
/*!40000 ALTER TABLE `fixed_assets` ENABLE KEYS */;


-- Dumping structure for table sas_master.fixed_asset_amounts
CREATE TABLE IF NOT EXISTS `fixed_asset_amounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fixed_asset_id` int(11) DEFAULT NULL,
  `general_ledger_id` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1: SLM; 2:DBM; 3:DDBM',
  `date_post` date DEFAULT NULL,
  `amount_post` decimal(15,12) DEFAULT '0.000000000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sas_master.fixed_asset_amounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `fixed_asset_amounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `fixed_asset_amounts` ENABLE KEYS */;


-- Dumping structure for table sas_master.general_ledgers
CREATE TABLE IF NOT EXISTS `general_ledgers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_memo_with_sale_id` int(11) DEFAULT NULL,
  `invoice_pbc_with_pbs_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_order_receipt_id` int(11) DEFAULT NULL,
  `credit_memo_id` int(11) DEFAULT NULL,
  `credit_memo_receipt_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `pv_id` int(11) DEFAULT NULL,
  `purchase_return_id` int(11) DEFAULT NULL,
  `purchase_return_receipt_id` int(11) DEFAULT NULL,
  `ar_ap_gl_id` int(11) DEFAULT NULL,
  `cycle_product_id` int(11) DEFAULT NULL,
  `receive_payment_id` int(11) DEFAULT NULL,
  `pay_bill_id` int(11) DEFAULT NULL,
  `ar_aging_id` int(11) DEFAULT NULL,
  `ap_aging_id` int(11) DEFAULT NULL,
  `apply_to_id` int(11) DEFAULT NULL COMMENT 'Purchase Order; Purchase Bill; Quote; Sales Invoice',
  `landing_cost_id` int(11) DEFAULT NULL,
  `landing_cost_receipt_id` int(11) DEFAULT NULL,
  `expense_id` int(11) DEFAULT NULL,
  `other_income_id` int(11) DEFAULT NULL,
  `inventory_physical_id` int(11) DEFAULT NULL,
  `apply_reference` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Purchase Order; Purchase Bill; Quote; Sales Invoice Code',
  `receive_from_id` int(11) DEFAULT NULL COMMENT 'Vendor or Customer Id',
  `receive_from_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Vendor or Customer Name',
  `date` date DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_deposit` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_sys` tinyint(4) DEFAULT '0',
  `is_adj` tinyint(4) DEFAULT '0',
  `is_approve` tinyint(4) DEFAULT '1',
  `is_depreciated` tinyint(4) DEFAULT '0',
  `is_retained_earnings` tinyint(4) DEFAULT '0',
  `deposit_type` tinyint(4) DEFAULT '0' COMMENT '0:Journal; 1: Normal; 2: Purchase Order; 3: Purchase Bill; 4: Quote; 5: Invoice',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_filter` (`sales_order_id`,`sales_order_receipt_id`,`credit_memo_id`,`credit_memo_receipt_id`,`purchase_order_id`,`pv_id`),
  KEY `key_filter_second` (`purchase_return_id`,`purchase_return_receipt_id`,`ar_ap_gl_id`,`cycle_product_id`,`receive_payment_id`,`pay_bill_id`,`ar_aging_id`,`ap_aging_id`),
  KEY `key_filter_third` (`apply_to_id`,`receive_from_id`,`date`,`reference`,`is_approve`,`deposit_type`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.general_ledgers: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_ledgers` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_ledgers` ENABLE KEYS */;


-- Dumping structure for table sas_master.general_ledger_details
CREATE TABLE IF NOT EXISTS `general_ledger_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `general_ledger_id` int(11) DEFAULT NULL,
  `main_gl_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `is_free` tinyint(4) NOT NULL DEFAULT '0',
  `inventory_valuation_id` int(11) DEFAULT NULL,
  `inventory_valuation_is_debit` tinyint(4) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'General Journal',
  `debit` decimal(20,9) DEFAULT '0.000000000',
  `credit` decimal(20,9) DEFAULT '0.000000000',
  `memo` text COLLATE utf8_unicode_ci,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `other_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `is_reconcile` tinyint(4) DEFAULT '0',
  `reconcile_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_filter_second` (`location_group_id`,`location_id`,`product_id`,`service_id`,`inventory_valuation_id`),
  KEY `key_filter_third` (`customer_id`,`vendor_id`,`employee_id`,`other_id`,`class_id`),
  KEY `key_filter` (`general_ledger_id`,`main_gl_id`,`chart_account_id`,`company_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.general_ledger_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `general_ledger_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `general_ledger_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `price` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `sys_code`, `name`, `type`, `price`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '231341411233', 'Admin', NULL, NULL, '2017-02-17 09:31:52', 1, '2018-10-04 21:19:14', 1, 1);
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventories
CREATE TABLE IF NOT EXISTS `inventories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `consignment_id` int(11) DEFAULT NULL,
  `consignment_return_id` int(11) DEFAULT NULL,
  `vendor_consignment_id` int(11) DEFAULT NULL,
  `vendor_consignment_return_id` int(11) DEFAULT NULL,
  `cycle_product_id` int(11) DEFAULT NULL,
  `cycle_product_detail_id` int(11) DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `point_of_sales_id` int(11) DEFAULT NULL,
  `credit_memo_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `purchase_return_id` int(11) DEFAULT NULL,
  `transfer_order_id` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `location_group_id` int(11) NOT NULL,
  `qty` decimal(15,3) NOT NULL,
  `unit_cost` decimal(18,9) DEFAULT '0.000000000',
  `unit_price` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `date_expired` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `location_id` (`location_id`),
  KEY `lots_number` (`lots_number`),
  KEY `qty` (`qty`),
  KEY `location_group_id` (`location_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventories: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_physicals
CREATE TABLE IF NOT EXISTS `inventory_physicals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`location_group_id`),
  KEY `searchs` (`code`,`date`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_physicals: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_physicals` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_physicals` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_physical_crontabs
CREATE TABLE IF NOT EXISTS `inventory_physical_crontabs` (
  `inventory_physical_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  UNIQUE KEY `inventory_physical_id` (`inventory_physical_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_physical_crontabs: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_physical_crontabs` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_physical_crontabs` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_physical_details
CREATE TABLE IF NOT EXISTS `inventory_physical_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_physical_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty_diff` decimal(15,3) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `expired_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_mix_id` (`inventory_physical_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_physical_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_physical_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_physical_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_totals
CREATE TABLE IF NOT EXISTS `inventory_totals` (
  `product_id` double NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `total_qty` decimal(15,3) DEFAULT '0.000',
  `total_cycle` decimal(15,3) DEFAULT '0.000',
  `total_so` decimal(15,3) DEFAULT '0.000',
  `total_so_free` decimal(15,3) DEFAULT '0.000',
  `total_pos` decimal(15,3) DEFAULT '0.000',
  `total_pos_free` decimal(15,3) DEFAULT '0.000',
  `total_pb` decimal(15,3) DEFAULT '0.000',
  `total_pbc` decimal(15,3) DEFAULT '0.000',
  `total_cm` decimal(15,3) DEFAULT '0.000',
  `total_cm_free` decimal(15,3) DEFAULT '0.000',
  `total_to_in` decimal(15,3) DEFAULT '0.000',
  `total_to_out` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_cus_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_in` decimal(15,3) DEFAULT '0.000',
  `total_ven_consign_out` decimal(15,3) DEFAULT '0.000',
  `total_order` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`product_id`,`lots_number`,`expired_date`),
  KEY `product_id` (`product_id`,`lots_number`,`expired_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_totals: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_totals` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_totals` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_total_by_dates
CREATE TABLE IF NOT EXISTS `inventory_total_by_dates` (
  `product_id` double NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `date` date NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `total_amount_sales` decimal(20,9) DEFAULT '0.000000000',
  `total_amount_purchase` decimal(20,9) DEFAULT '0.000000000',
  `total_cycle` decimal(15,3) DEFAULT '0.000',
  `total_so` decimal(15,3) DEFAULT '0.000',
  `total_pos` decimal(15,3) DEFAULT '0.000',
  `total_pb` decimal(15,3) DEFAULT '0.000',
  `total_pbc` decimal(15,3) DEFAULT '0.000',
  `total_cm` decimal(15,3) DEFAULT '0.000',
  `total_ending` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`product_id`,`lots_number`,`expired_date`,`date`,`type`),
  KEY `index_searchs` (`product_id`,`lots_number`,`expired_date`,`date`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_total_by_dates: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_total_by_dates` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_total_by_dates` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_unit_costs
CREATE TABLE IF NOT EXISTS `inventory_unit_costs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `total_qty` decimal(15,3) DEFAULT '0.000',
  `unit_cost` decimal(18,9) DEFAULT '0.000000000' COMMENT 'Main UOM',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_unit_costs: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_unit_costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_unit_costs` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_valuations
CREATE TABLE IF NOT EXISTS `inventory_valuations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `transaction_detail_id` int(11) DEFAULT NULL,
  `cycle_product_id` bigint(20) DEFAULT NULL,
  `sales_order_id` bigint(20) DEFAULT NULL,
  `credit_memo_id` bigint(20) DEFAULT NULL,
  `purchase_order_id` bigint(20) DEFAULT NULL,
  `purchase_order_detail_id` bigint(20) DEFAULT NULL,
  `purchase_return_id` bigint(20) DEFAULT NULL,
  `point_of_sales_id` bigint(20) DEFAULT NULL,
  `inventory_physical_id` bigint(20) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `small_qty` decimal(15,3) DEFAULT NULL,
  `qty` decimal(15,9) DEFAULT NULL COMMENT 'Main QTY',
  `cost` decimal(18,9) DEFAULT NULL COMMENT 'Main Cost Uom',
  `price` decimal(18,9) DEFAULT NULL,
  `on_hand` decimal(20,9) DEFAULT NULL,
  `on_hand_small` decimal(20,9) DEFAULT NULL,
  `avg_cost` decimal(20,9) DEFAULT NULL COMMENT 'Main Cost Uom',
  `asset_value` decimal(20,9) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `date_edited` datetime DEFAULT NULL,
  `is_var_cost` tinyint(4) DEFAULT '0',
  `is_adjust_value` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `searchs` (`date`,`pid`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_valuations: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_valuations` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_valuations` ENABLE KEYS */;


-- Dumping structure for table sas_master.inventory_valuation_cals
CREATE TABLE IF NOT EXISTS `inventory_valuation_cals` (
  `date` date NOT NULL,
  `product_id` int(11) NOT NULL,
  `is_lock` tinyint(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `runing_date` datetime DEFAULT NULL,
  PRIMARY KEY (`date`,`product_id`,`is_lock`),
  KEY `search` (`date`,`product_id`,`is_lock`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.inventory_valuation_cals: ~0 rows (approximately)
/*!40000 ALTER TABLE `inventory_valuation_cals` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_valuation_cals` ENABLE KEYS */;


-- Dumping structure for table sas_master.invoice_pbc_with_pbs
CREATE TABLE IF NOT EXISTS `invoice_pbc_with_pbs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_id` int(10) DEFAULT NULL,
  `purchase_return_id` int(10) DEFAULT NULL,
  `total_cost` decimal(15,3) DEFAULT '0.000',
  `apply_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_By` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`),
  KEY `purchase_return_id` (`purchase_return_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.invoice_pbc_with_pbs: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoice_pbc_with_pbs` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_pbc_with_pbs` ENABLE KEYS */;


-- Dumping structure for table sas_master.landed_cost_types
CREATE TABLE IF NOT EXISTS `landed_cost_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.landed_cost_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `landed_cost_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `landed_cost_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.landing_costs
CREATE TABLE IF NOT EXISTS `landing_costs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `landed_cost_type_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,9) DEFAULT NULL,
  `balance` decimal(15,9) DEFAULT NULL,
  `ap_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '-1: Edied; 0: Void; 1: Open; 2: Closed',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.landing_costs: ~0 rows (approximately)
/*!40000 ALTER TABLE `landing_costs` DISABLE KEYS */;
/*!40000 ALTER TABLE `landing_costs` ENABLE KEYS */;


-- Dumping structure for table sas_master.landing_cost_details
CREATE TABLE IF NOT EXISTS `landing_cost_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `landing_cost_id` int(11) DEFAULT NULL,
  `purchase_order_detail_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` decimal(15,1) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `small_val_uom` int(11) DEFAULT NULL,
  `unit_cost` decimal(18,9) DEFAULT NULL,
  `landed_cost` decimal(18,9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filter` (`landing_cost_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.landing_cost_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `landing_cost_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `landing_cost_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.landing_cost_receipts
CREATE TABLE IF NOT EXISTS `landing_cost_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `landing_cost_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_us` decimal(15,9) DEFAULT '0.000000000',
  `amount_other` decimal(15,9) DEFAULT '0.000000000',
  `total_amount` decimal(15,9) DEFAULT '0.000000000',
  `balance` decimal(15,9) DEFAULT '0.000000000',
  `balance_other` decimal(15,3) DEFAULT '0.000',
  `pay_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_void` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`landing_cost_id`),
  KEY `pv_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.landing_cost_receipts: ~0 rows (approximately)
/*!40000 ALTER TABLE `landing_cost_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `landing_cost_receipts` ENABLE KEYS */;


-- Dumping structure for table sas_master.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aisle` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bay` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_for_sale` tinyint(4) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `location_group_id` (`location_group_id`),
  KEY `key_search` (`is_for_sale`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.locations: ~1 rows (approximately)
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` (`id`, `sys_code`, `location_group_id`, `name`, `color`, `level`, `aisle`, `bay`, `bin`, `position`, `created`, `created_by`, `modified`, `modified_by`, `is_for_sale`, `is_active`) VALUES
	(1, 'd6d08e5324b88fb71579c3bbcfa5cb17', 1, 'Head Office', '', '', '', '', '', '', '2017-08-30 13:35:30', 1, '2017-11-14 14:56:20', 1, 1, 1);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;


-- Dumping structure for table sas_master.location_groups
CREATE TABLE IF NOT EXISTS `location_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_group_type_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `customer_id` int(11) DEFAULT NULL,
  `allow_negative_stock` tinyint(4) DEFAULT '0',
  `stock_tranfer_confirm` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `warehouse_type_id` (`location_group_type_id`),
  KEY `searchs` (`name`,`is_active`,`code`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.location_groups: ~1 rows (approximately)
/*!40000 ALTER TABLE `location_groups` DISABLE KEYS */;
INSERT INTO `location_groups` (`id`, `sys_code`, `location_group_type_id`, `code`, `name`, `description`, `customer_id`, `allow_negative_stock`, `stock_tranfer_confirm`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, 'f3e3fdbfe232d995fd16c8996aa6525e', 2, 'HQ', 'Head Office', '', NULL, 1, 0, '2017-08-30 13:35:13', 1, '2018-09-24 07:47:53', 1, 1);
/*!40000 ALTER TABLE `location_groups` ENABLE KEYS */;


-- Dumping structure for table sas_master.location_group_classese
CREATE TABLE IF NOT EXISTS `location_group_classese` (
  `company_id` int(11) NOT NULL,
  `location_group_id` int(11) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`company_id`,`location_group_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.location_group_classese: ~1 rows (approximately)
/*!40000 ALTER TABLE `location_group_classese` DISABLE KEYS */;
INSERT INTO `location_group_classese` (`company_id`, `location_group_id`, `class_id`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `location_group_classese` ENABLE KEYS */;


-- Dumping structure for table sas_master.location_group_types
CREATE TABLE IF NOT EXISTS `location_group_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `allow_negative_stock` tinyint(4) DEFAULT '0',
  `stock_tranfer_confirm` tinyint(4) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`),
  KEY `name` (`name`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.location_group_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `location_group_types` DISABLE KEYS */;
INSERT INTO `location_group_types` (`id`, `sys_code`, `name`, `description`, `allow_negative_stock`, `stock_tranfer_confirm`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '1fdc275e4fd22798c821a3098d56c88c', 'Consignment', NULL, 0, 0, '2017-08-23 15:01:19', NULL, '2017-08-23 15:01:21', NULL, 1),
	(2, 'f63ecf61a3c39fc41d5ac793784db387', 'Warehouse', '', 1, 0, '2017-08-26 09:19:37', 1, '2017-11-30 15:02:12', 1, 1);
/*!40000 ALTER TABLE `location_group_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.location_settings
CREATE TABLE IF NOT EXISTS `location_settings` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `modules` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `modules` (`modules`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.location_settings: ~5 rows (approximately)
/*!40000 ALTER TABLE `location_settings` DISABLE KEYS */;
INSERT INTO `location_settings` (`id`, `modules`, `location_status`) VALUES
	(1, 'PB', 0),
	(2, 'BR', 0),
	(3, 'POS', 0),
	(4, 'Sales', 0),
	(5, 'CM', 0);
/*!40000 ALTER TABLE `location_settings` ENABLE KEYS */;


-- Dumping structure for table sas_master.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_type_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0: Disabled; 1: Enable',
  `type` tinyint(4) DEFAULT NULL COMMENT '1: Normal; 2: Admin; 3: Specail Price',
  `price` decimal(15,3) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `module_type_id` (`module_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=647 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.modules: ~93 rows (approximately)
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `sys_code`, `module_type_id`, `name`, `ordering`, `status`, `type`, `price`, `description`) VALUES
	(1, '445556c95c9260a883d9e04adb499840', 1, 'Home Page', 1, 1, NULL, NULL, NULL),
	(41, 'bb214d563f8c320ae2dbb41b15d98773', 82, 'General Setting (Set Up)', 1, 1, NULL, NULL, NULL),
	(46, '45cf09887e1432ca681396233e9b5b21', 13, 'Product/Service (View)', 1, 1, NULL, NULL, NULL),
	(47, '7a7ed2611a5b2b889556fad9e656f969', 13, 'Product/Service (Add)', 2, 1, NULL, NULL, NULL),
	(48, 'c5c073ebc647041fd93374ba60b7b7d6', 13, 'Product/Service (Edit)', 3, 1, NULL, NULL, NULL),
	(49, '8e448a25ea7166fe56867254a6ed2d8a', 13, 'Product/Service (Delete)', 4, 1, NULL, NULL, NULL),
	(50, '16b89a3639d857fa2664f91e8f9b2c48', 14, 'Customer (view)', 1, 1, NULL, NULL, NULL),
	(51, 'e0c4663171b08d79a9ddaff48b7d6a6c', 14, 'Customer (add)', 2, 1, NULL, NULL, NULL),
	(52, '335cb3026530566ebe20eb210b391a86', 14, 'Customer (edit)', 3, 1, NULL, NULL, NULL),
	(53, '48d1d88f77c7f2d78d0b8af8f0363a83', 14, 'Customer (delete)', 4, 1, NULL, NULL, NULL),
	(55, '9cc04a5788d3c36585bc7063ad8f1fc5', 15, 'Inventory Adjustment (Add)', 2, 1, NULL, NULL, NULL),
	(72, 'f1c3b74c454655c991fc6593274fb6e2', 22, 'Vendor (View)', 1, 1, NULL, NULL, NULL),
	(73, 'f018d919fac037b70e2d82194e21b293', 22, 'Vendor (Add)', 2, 1, NULL, NULL, NULL),
	(74, 'd1d14b4053bfeb9f2c6ce91f3a83fdeb', 22, 'Vendor (Edit)', 3, 1, NULL, NULL, NULL),
	(75, '756e399f03de3b05b27c74233b321eb3', 22, 'Vendor (Delete)', 4, 1, NULL, NULL, NULL),
	(88, '3b799c5a42d46672cd13bcea7bfc46d6', 15, 'Inventory Adjustment (Edit)', 3, 1, NULL, NULL, NULL),
	(91, 'e67ec8c9750a07ce3fd43dfd3c2c0f89', 21, 'Purchase Bill (View)', 1, 1, NULL, NULL, NULL),
	(92, 'fe67f79dc98be3de9ba99ff69d79b334', 21, 'Purchase Bill (Add)', 2, 1, NULL, NULL, NULL),
	(94, '8aa7d6d84c6c3daf170958486970b793', 21, 'Purchase Bill (Void)', 5, 1, NULL, NULL, NULL),
	(101, 'f623465023239f26da6bfc905a002781', 24, 'Transfer Order (View)', 1, 1, NULL, NULL, NULL),
	(102, 'e325bc7552e5abaed85f8872bfefa373', 24, 'Transfer Order (Add)', 2, 1, NULL, NULL, NULL),
	(103, '23ada5ee5ce947410602555668c14547', 24, 'Transfer Order (Edit)', 3, 1, NULL, NULL, NULL),
	(104, '8af19d2de46f233f99ec9e175ce97fcf', 24, 'Transfer Order (Delete)', 4, 1, NULL, NULL, NULL),
	(105, 'cfe02aac348b9bb0b82e631554e931e1', 25, 'Sales / Invoice (View)', 1, 1, NULL, NULL, NULL),
	(106, 'eff9f25feb1bca74d0995d286dfc4c43', 25, 'Sales / Invoice (Add)', 2, 1, NULL, NULL, NULL),
	(107, '5bb326ba28c4af2d501e99c20377f373', 25, 'Sales / Invoice (Aging)', 4, 1, NULL, NULL, NULL),
	(108, '38aa888ef14ebcce74035ef11c238ae2', 25, 'Sales / Invoice (Void)', 5, 1, NULL, NULL, NULL),
	(117, '532dcd1d801b20841061379f33a13fd6', 28, 'Report (General Ledger)', 801, 0, NULL, NULL, NULL),
	(118, 'a908d4070638348342778dc91a5f1291', 28, 'Report (TB)', 802, 0, NULL, NULL, NULL),
	(119, 'a4a4144989c7ba939171edb143664096', 121, 'Profit & Loss', 1, 1, NULL, NULL, NULL),
	(120, '1d85ff4d03940917ec9a3dc86c55ac07', 28, 'Report (BS)', 804, 0, NULL, NULL, NULL),
	(132, '6e05773bc1b5dfa1b8b350c519936c9d', 28, 'Report (Customer Address List)', 404, 0, NULL, NULL, NULL),
	(133, '77f17cff5bce93124c2d765bbca14778', 120, 'Inventory Adjustment By Item', 5, 1, NULL, NULL, NULL),
	(134, 'ecbff7388d11d7dd61a9cc42dd317f60', 28, 'Report (Product Expiry Date & Aging)', 105, 0, NULL, NULL, NULL),
	(135, 'db8cbb886128721307c4a2f67bfb9d94', 28, 'Report (CF)', 805, 0, NULL, NULL, NULL),
	(136, '1b9d610bf01b8a06bcab94ce3440c235', 13, 'Product/Service (Set Price)', 5, 1, NULL, NULL, NULL),
	(145, '5752e6b019fe5adb513601c56c6d4d8f', 21, 'Purchase Bill (Aging)', 4, 1, NULL, NULL, NULL),
	(147, 'da12b83695709e0665080e66b066763e', 25, 'Sales / Invoice (Add Misc.)', 7, 0, NULL, NULL, NULL),
	(148, 'b159d60b9a5519d640a4bb115c7ec091', 25, 'Sales / Invoice (Add Service)', 6, 1, NULL, NULL, NULL),
	(149, '2c7f878ae58f2f7982f7d522f5fa559b', 34, 'Point Of Sales (Add)', 2, 1, NULL, NULL, NULL),
	(151, 'e99cb6da95b9d68e241c817222cef556', 34, 'Point Of Sales (Print)', 3, 1, NULL, NULL, NULL),
	(152, '1c865532eae95f5d3246fb7e38e0b873', 34, 'Point Of Sales (Void)', 4, 1, NULL, NULL, NULL),
	(153, 'e6be058f1112d7794903120891ef1a5e', 28, 'A/R Aging', 1, 1, NULL, NULL, NULL),
	(154, '0654f2fc142b800983941925bd3dbccc', 118, 'A/P Aging', 1, 1, NULL, NULL, NULL),
	(155, '277b5f4de3197ca295a8188cc323b750', 28, 'Report (Purchase Bill Barcode)', 601, 0, NULL, NULL, NULL),
	(164, '628d9a2ebb22fab48b92686b5f274f21', 25, 'Sales / Invoice (Print Invoice)', 9, 1, NULL, NULL, NULL),
	(166, 'c4b756088674f214dccf99553f400bf2', 122, 'Product Average Cost', 1, 1, NULL, NULL, NULL),
	(167, 'd6c75d1b3076b9fddbdc4a6c362d6808', 122, 'Product Price List', 2, 1, NULL, NULL, NULL),
	(168, '2d43500a6b19c2c96e040d77183f0caa', 28, 'Report (Dormant Customer)', 406, 0, NULL, NULL, NULL),
	(169, 'a0c6afe92ad4388ecccd3f0c805c619e', 117, 'Discount Summary', 9, 1, NULL, NULL, NULL),
	(170, 'd7d317f58407c7c9d9bc4f8df27a4170', 123, 'User Rights', 1, 1, NULL, NULL, NULL),
	(171, '0aaf12e8c50e3e4fe01db042e26bdcb0', 123, 'User Logs', 2, 1, NULL, NULL, NULL),
	(172, '2b73c111ca6b1ee7f42be279c1c2f864', 28, 'Report (Vendor Product List)', 703, 0, NULL, NULL, NULL),
	(173, '8fe5b33b66981c3d7363820375a865de', 28, 'Report (Vendor Address)', 704, 0, NULL, NULL, NULL),
	(174, '45a6b0aadf81ae6dde015483fc4cd841', 28, 'Report (Vendor Address List)', 705, 0, NULL, NULL, NULL),
	(181, 'aade1080aa917af046e2226118cc12bb', 25, 'Sales / Invoice (Add Discount)', 8, 1, NULL, NULL, NULL),
	(182, '0432fa3482d93bc6a15b79d213786fab', 34, 'Point Of Sales (Add Total Discount)', 7, 1, NULL, NULL, NULL),
	(192, 'ea74c2ab72085d430cb3777cf8656954', 117, 'Sales/Invoice', 7, 1, NULL, NULL, NULL),
	(197, '5fddbb538d692e99a24c0fcd488be4ba', 120, 'Global Inventory', 1, 1, NULL, NULL, NULL),
	(198, 'e80de399303c0e1643c85a0f5bd60c97', 119, 'Purchase By Invoice', 1, 1, NULL, NULL, NULL),
	(201, '771ec8c0049a94f49f6249d29314fb54', 21, 'Purchase Bill (Edit)', 3, 1, NULL, NULL, NULL),
	(202, '1fab7f0de4927d85988c16b9bf30dbb0', 40, 'Sales Return (View)', 1, 1, NULL, NULL, NULL),
	(203, '38ad230fed89070d9cca958078fbc7d5', 40, 'Sales Return (Add)', 2, 1, NULL, NULL, NULL),
	(204, 'c1b3f273c63ec12f56d74613fecab9c3', 40, 'Sales Return (Aging)', 4, 1, NULL, NULL, NULL),
	(205, '9a664ef04d6cd2f2f4a54465a0d28793', 41, 'Purchase Return (View)', 1, 1, NULL, NULL, NULL),
	(206, '57d8bbf5610869d92a74ed86c37b7843', 41, 'Purchase Return (Add)', 2, 1, NULL, NULL, NULL),
	(207, '6e16d7b79db40f82c480ad256410b990', 41, 'Purchase Return (Aging)', 4, 1, NULL, NULL, NULL),
	(209, 'da77c3e9fad1895adc2467e5de02e295', 42, 'Receive Payments', 1, 1, NULL, NULL, NULL),
	(210, 'c68e2faf43fe9aa64803bc29a17f0ca9', 43, 'Pay Bills', 1, 1, NULL, NULL, NULL),
	(211, '26b7483d7017661fbf705fca11222ba9', 25, 'Sales / Invoice (Edit)', 3, 1, NULL, NULL, NULL),
	(212, 'd29a88372d5e391ec26fc7c9521add4c', 117, 'Invoice (Sales Return)', 8, 1, NULL, NULL, NULL),
	(213, '807d83654f97baff90413fd811f85b96', 119, 'Invoice (Purchase Return)', 3, 1, NULL, NULL, NULL),
	(214, '1cbef82e3f505b7ae55a5f9c26e80a9f', 28, 'Customer Balance', 2, 1, NULL, NULL, NULL),
	(215, '0aa9a4414ccb85c9eb1b8b21a13396fd', 118, 'Vendor Balance', 2, 1, NULL, NULL, NULL),
	(216, '57190862eb2be9bb98077d77413fdc89', 40, 'Sales Return (Add Service)', 6, 1, NULL, NULL, NULL),
	(217, '7c6348c6f6a32d7c4ce972bc1dfa74cf', 40, 'Sales Return (Add Misc.)', 7, 0, NULL, NULL, NULL),
	(218, '11d83de315490cdbf59081f42189896b', 41, 'Purchase Return (Add Service)', 6, 1, NULL, NULL, NULL),
	(219, '5c98891685f89fbb8b917644e2cdbcbd', 41, 'Purchase Return (Add Misc.)', 7, 0, NULL, NULL, NULL),
	(220, '1e2f4f814b9afa58ddc9c2c326a941fb', 40, 'Sales Return (Void)', 5, 1, NULL, NULL, NULL),
	(221, '8fad0b0185070bb55c1e729876dc5059', 40, 'Sales Return (Edit)', 3, 1, NULL, NULL, NULL),
	(222, '627cc7e4471bef44e2180070c53d154b', 41, 'Purchase Return (Edit)', 3, 1, NULL, NULL, NULL),
	(223, '641d5776012d4d7d4ce01567936391ad', 41, 'Purchase Return (Void)', 5, 1, NULL, NULL, NULL),
	(226, '2d69ad3033f2660d1c1736dd1b893694', 34, 'Point Of Sales (Add service)', 5, 1, NULL, NULL, NULL),
	(228, '9519ca5e3283dc3af4667c43a81c09eb', 28, 'Report (Sales By Item Type POS)', 302, 0, NULL, NULL, NULL),
	(229, 'bf871fd5e108dee04a626946c3b9a866', 28, 'Report (Sales/Invoice By Customer Group)', 310, 0, NULL, NULL, NULL),
	(230, '9b5c853b156c0e4f8f37b1510e79de64', 117, 'Receive Payments Report', 10, 1, NULL, NULL, NULL),
	(231, '4a1c2006d5387676a6f04107cdf56187', 28, 'Report (Receive Payments By Rep)', 316, 0, NULL, NULL, NULL),
	(232, '92a71033ff4069c785110d6b65cd305e', 119, 'Pay Bills Report', 4, 1, NULL, NULL, NULL),
	(237, '331b148ed9c61566047a3addd4957f27', 28, 'Report (Deposit)', 807, 0, NULL, NULL, NULL),
	(238, '444d84cc4e5ec0f8e80b7b007207b9f6', 28, 'Report (Sales By Item POS)', 301, 0, NULL, NULL, NULL),
	(239, '8d44fa8ec64abe76e789a344897c48cb', 117, 'Sales By Item', 1, 1, NULL, NULL, NULL),
	(240, 'ba7334d6fbf279d0804afe63f4044e62', 28, 'Sales By Customer', 2, 1, NULL, NULL, NULL),
	(241, '1622d441d2ca960c294a87ecb1f6b23f', 28, 'Report (Sales By Customer Group)', 306, 0, NULL, NULL, NULL),
	(242, '8ce450fdced3f8d20dbf464155a20751', 117, 'Invoice POS', 6, 1, NULL, NULL, NULL),
	(243, 'f952599dc53b8a6e52b9e66cfd6e0966', 120, 'Transfer Order', 6, 1, NULL, NULL, NULL),
	(244, 'a2bdcfaff10d24f13fbda92fa172887d', 28, 'Report (Sales By Item Type)', 304, 0, NULL, NULL, NULL),
	(245, '26e413d2b690cb5813ec436fc7a5e9a4', 28, 'Report (Customer Address)', 403, 0, NULL, NULL, NULL),
	(246, '73fb48594c453ca84b4d7b731337c549', 15, 'Inventory Adjustment (View)', 1, 1, NULL, NULL, NULL),
	(251, 'e10003b8538846dfc1b795b1dd70ce40', 120, 'Inventory Valuation', 3, 1, NULL, NULL, NULL),
	(253, 'fb057d09f373be7bf64538f419d59a98', 48, 'Purchase Order (View)', 1, 1, NULL, NULL, NULL),
	(254, 'bd105c91c1f72cdd718ff1a68b64a59b', 48, 'Purchase Order (Add)', 2, 1, NULL, NULL, NULL),
	(255, '1b131e1c9ab2cb028ce1fbed29ccae8e', 48, 'Purchase Order (Edit)', 3, 1, NULL, NULL, NULL),
	(256, 'e6c5020715cad32e7c5b0dfbc7e47746', 48, 'Purchase Order (Delete)', 4, 1, NULL, NULL, NULL),
	(257, 'cea42f093c9d5726d6a25c83764d1d39', 120, 'Inventory Activity', 2, 1, NULL, NULL, NULL),
	(258, 'a32115398900ad975a5a7446a0c1be43', 120, 'Inventory Adjustment', 4, 1, NULL, NULL, NULL),
	(259, 'e971856692fcf1944f794aa636d7615c', 40, 'Sales Return (Add Discount)', 8, 1, NULL, NULL, NULL),
	(260, '9b1f0dc8588a60b215e7a8b1e41d81d8', 48, 'Purchase Order (Add Service)', 5, 1, NULL, NULL, NULL),
	(264, 'd0ca3454b83812c2714908e4d908e185', 120, 'Transfer By Item', 7, 1, NULL, NULL, NULL),
	(265, 'c8e81555f80a8ba15d14a60920361c52', 117, 'Total Sales', 5, 1, NULL, NULL, NULL),
	(266, '8dc1aa100067a641d242da1b0c490604', 28, 'Open Invoice', 3, 1, NULL, NULL, NULL),
	(267, '2a733e92f93779660c9b899411893e83', 28, 'Report (Delivery)', 314, 0, NULL, NULL, NULL),
	(268, 'c8b166b9f65479ebf33ee40d61b751c6', 119, 'Purchase By Item', 2, 1, NULL, NULL, NULL),
	(269, 'a3080b328fe393de9305b56fa2a33b49', 28, 'Report (Check)', 806, 0, NULL, NULL, NULL),
	(277, 'a7b528f448ce677b0c555e6986512462', 28, 'Report (A/R) (Employee)', 501, 0, NULL, NULL, NULL),
	(278, 'efe9af78bd317ed3a2a0a62c977c121b', 28, 'Report (Employee Balance)', 502, 0, NULL, NULL, NULL),
	(280, '53f1c7cc73ec18497def5168baf9ad39', 34, 'Point Of Sales (Add Discount for Product)', 9, 1, NULL, NULL, NULL),
	(282, 'e65ba44b1dd3254df924d10df3bdfc01', 53, 'Sales (Set Up)', 1, 1, NULL, NULL, NULL),
	(290, 'c5ea3a6ed65f39bdb9882bf90df7bfaf', 25, 'Sales / Invoice (Edit Price)', 10, 1, NULL, NULL, NULL),
	(291, '82d687976d1e4b51bdde9e67753ab808', 40, 'Sales Return (Edit Price)', 9, 1, NULL, NULL, NULL),
	(296, 'ed3ed5c43fcf120bde713ac8f7242432', 13, 'Product (Set Packet)', 6, 0, NULL, NULL, NULL),
	(306, '53de291bddc9a9c85b9e3e140b420d9b', 14, 'Customer (Export to Excel)', 5, 1, NULL, NULL, NULL),
	(309, 'aebc9e0de731058a911629960a9dbbb3', 22, 'Vendor (Export to Excel)', 5, 1, NULL, NULL, NULL),
	(313, 'b206a4e662937cb2bb8604a1f3a94282', 13, 'Product/Service (Export to Excel)', 7, 1, NULL, NULL, NULL),
	(315, 'dbdbda12f8ca0f722ef0ecf22be73141', 40, 'Sales Return (View By User)', 2, 1, NULL, NULL, NULL),
	(316, '196e337879a0e77c61313f76d554e633', 25, 'Sales / Invoice (View By User)', 2, 1, NULL, NULL, NULL),
	(317, '4b5e9f1fb81b5150ad544cd3fd683ef0', 21, 'Purchase Bill (View By User)', 7, 1, NULL, NULL, NULL),
	(318, '22e7d793f4f7c7e20e34cb909aea31f7', 41, 'Purchase Return (View By User)', 2, 1, NULL, NULL, NULL),
	(320, 'bf29f394b81608795a237825e7113b8d', 24, 'Transfer Order (View By User)', 5, 1, NULL, NULL, NULL),
	(321, 'f706874b2b0847b8dbf451c14ce6bf71', 15, 'Inventory Adjustment (View By User)', 6, 1, NULL, NULL, NULL),
	(322, '9cb2018ee68ecadfcc7c7b21e9ac9263', 15, 'Inventory Adjustment (Delete)', 4, 1, NULL, NULL, NULL),
	(323, 'c552b12fc1a78f879dbc3a2e144569ad', 15, 'Inventory Adjustment (Receive)', 5, 1, NULL, NULL, NULL),
	(328, '119c37731b154c9a98e2ba68ce6d87eb', 34, 'Point Of Sales (Reprint)', 8, 1, NULL, NULL, NULL),
	(329, 'a5cc597212b93235a6c319621d1958d1', 48, 'Purchase Order (Close)', 8, 1, NULL, NULL, NULL),
	(374, '28be47da323c3c56ad2a1aba4890cdd6', 68, 'Quotataion (View)', 1, 1, NULL, NULL, NULL),
	(375, '28111bf9ebdc220e42b21fd43f247247', 68, 'Quotataion (Add)', 2, 1, NULL, NULL, NULL),
	(376, '43e54711271ed7debb884b7a2a3c9940', 68, 'Quotataion (Edit)', 3, 1, NULL, NULL, NULL),
	(377, '78082186dd94c26ee48d3402cbc30cfb', 68, 'Quotataion (Delete)', 4, 1, NULL, NULL, NULL),
	(378, '6f557aa6aeffa6f91759e7117fd04ae2', 68, 'Quotataion (View By User)', 5, 1, NULL, NULL, NULL),
	(379, '6486217377b10e13b2b2b52ec872e87c', 68, 'Quotataion (Open)', 6, 1, NULL, NULL, NULL),
	(380, '42357652e1e56254182a53112968e223', 68, 'Quotataion (Close)', 7, 1, NULL, NULL, NULL),
	(381, '45b645ce08fa71d0dc3c094a9803b840', 69, 'Sales Order (View)', 1, 1, NULL, NULL, NULL),
	(382, '3cfac6ce8719aef291dd3a428dabd1d4', 69, 'Sales Order (Add)', 2, 1, NULL, NULL, NULL),
	(383, '25d6d82988ae6673fc9ac21a4f296f4e', 69, 'Sales Order (Edit)', 3, 1, NULL, NULL, NULL),
	(384, '87715e311e4faddda9e509812d4021cc', 69, 'Sales Order (Delete)', 4, 1, NULL, NULL, NULL),
	(385, 'ece8a9eaa66e625b2ad8cf2b9089027b', 69, 'Sales Order (View By User)', 5, 1, NULL, NULL, NULL),
	(386, 'dd7885fc0b58decddf3898eedf1f2ed9', 69, 'Sales Order (Action)', 6, 1, NULL, NULL, NULL),
	(387, '932a129dd68cb5e5077a8b8447dee70d', 69, 'Sales Order (Close)', 7, 1, NULL, NULL, NULL),
	(394, 'b0ec1be639a6cfcaf93d0f9cba078dd4', 25, 'Sales / Invoice (Approve)', 11, 1, NULL, NULL, NULL),
	(396, '4230b73d815e94e387f48e407f58bcf5', 40, 'Sales Return (Receive Products)', 10, 1, NULL, NULL, NULL),
	(397, '5553e16fbeea0a8e6c60447ed57dfa47', 48, 'Purchase Order (Active)', 9, 1, NULL, NULL, NULL),
	(398, 'c73a6e92eab8a5e66459f8be7a64df0a', 21, 'Purchase Bill (Discount)', 6, 1, NULL, NULL, NULL),
	(399, 'd31a440fd7dfca6bc2b3d68e4920e9d4', 41, 'Purchase Return (Receive)', 8, 0, NULL, NULL, NULL),
	(417, '9f2a28d76ab0c93bc8643e087d1da0e5', 68, 'Quotataion (Add Service)', 2, 1, NULL, NULL, NULL),
	(418, 'a2fef52bb8a323fc21dd2ac3b6c11c57', 68, 'Quotataion (Add Misc)', 2, 0, NULL, NULL, NULL),
	(419, '9a7f54e13bd068de29d1b70a3f457138', 69, 'Sales Order (Add Service)', 2, 1, NULL, NULL, NULL),
	(420, 'da8a66deb0595ef8e1fd0473f89c7fae', 69, 'Sales Order (Add Misc)', 2, 0, NULL, NULL, NULL),
	(421, '6e896845f88d8e514ddbe545e4a430c3', 68, 'Quotataion (Edit Price)', 8, 1, NULL, NULL, NULL),
	(422, '9d4bd9f8f8a936279593eec6b56a82bb', 69, 'Sales Order (Edit Price)', 8, 1, NULL, NULL, NULL),
	(423, 'c906a4eed280cde0a0114a48921dca78', 69, 'Sales Order (Discount)', 9, 1, NULL, NULL, NULL),
	(424, 'de8bf56cc4d35eb574a2f95790fc69fc', 68, 'Quotataion (Discount)', 9, 1, NULL, NULL, NULL),
	(425, '228e14903317c7dd346079d3062c9ac7', 13, 'Product/Service (Set Cost)', 8, 1, NULL, NULL, NULL),
	(426, 'a90f00d3021607467b5c2480f127c870', 48, 'Purchase Order (Edit Unit Cost)', 10, 1, NULL, NULL, NULL),
	(452, 'a38597f7467583a123e72e4d66bb5840', 68, 'Quotataion (Edit Terms & Condition)', 10, 0, NULL, NULL, NULL),
	(453, '7619ae4efa8a53477b278d94d61c6bcc', 68, 'Quotataion (Total Discount)', 11, 1, NULL, NULL, NULL),
	(458, 'a51a256fcb49efdf8bdf081727ecbee8', 69, 'Sales Order (Edit Terms & Condition)', 10, 0, NULL, NULL, NULL),
	(459, 'f23550808d950f00dc11f0fad7b76827', 69, 'Sales Order (Total Discount)', 11, 1, NULL, NULL, NULL),
	(460, 'e30474ce73939080d742ad88e65b78d5', 48, 'Purchase Order (Edit Term & Conditions)', 11, 0, NULL, NULL, NULL),
	(461, '1b6b35a5d960ade27d47e0c9d7b89838', 25, 'Sales / Invoice (Edit Total Discount)', 12, 1, NULL, NULL, NULL),
	(462, '11b5b2e4fbdff17012c86885590a034e', 25, 'Sales / Invoice (Edit Terms & Condition)', 13, 0, NULL, NULL, NULL),
	(465, '4b733ab7598023884c4b782dd8bab8f8', 21, 'Purchase Bill (Add Service)', 8, 1, NULL, NULL, NULL),
	(466, '8111154cbc2a8cc96ae8d6b1ecaafbc7', 21, 'Purchase Bill (Total Discount)', 9, 1, NULL, NULL, NULL),
	(467, '4da82768f4116dde6967c43808698e3e', 68, 'Quotataion (Approve)', 12, 1, NULL, NULL, NULL),
	(468, '17d988a2213cfb37b3d53d8d45d65580', 21, 'Purchase Bill (Close All Items are Service)', 10, 1, NULL, NULL, NULL),
	(483, '6d6a8305ba5494e343e4165a8bb4cbc9', 69, 'Sales Order (Approve)', 12, 1, NULL, NULL, NULL),
	(489, '3860c49895d9e34a535e2e246d189d58', 117, 'Sales Top/Bottom Items', 3, 1, NULL, NULL, NULL),
	(490, 'bf82b7d6327e42b980add7449031a164', 13, 'Product (View Cost)', 9, 1, NULL, NULL, NULL),
	(491, 'ded7fb8bf584d8c71dc2ee0880d4a17b', 117, 'Sales Top/Bottom Customers', 4, 1, NULL, NULL, NULL),
	(492, '3c8a135e7b52f0c7dc19fc8aa6bccb85', 28, 'Report (Sales By Rep)', 307, 0, NULL, NULL, NULL),
	(493, 'e46efda029f1c00ab19a845f0a52aa09', 28, 'Report (Invoices By Rep)', 307, 0, NULL, NULL, NULL),
	(494, '6172d0e32e82106b8cd8e273675b4aa6', 28, 'Statement', 4, 1, NULL, NULL, NULL),
	(495, 'ac655c7a03add1a33efa8e0bba79d68a', 28, 'Report (Statement By Rep)', 307, 0, NULL, NULL, NULL),
	(496, '95603e7c9361dc31c79c14cfaae85b66', 28, 'Report (Customer Balance By Invoice)', 307, 0, NULL, NULL, NULL),
	(497, '204021bd42e3dae2f15f0fa824a651f7', 28, 'Report (Vendor Balance By Invoice)', 307, 0, NULL, NULL, NULL),
	(498, '8ac546ddb7546f2005e63f2014180e85', 28, 'Report (Audi Trail)', 307, 0, NULL, NULL, NULL),
	(499, '395008d9bffc4e9cb29e83951c783bea', 108, 'Inventory Adjustment (Issue)', 7, 1, NULL, NULL, NULL),
	(503, 'e581c07647bcf24d4146a2c3eb1bc8bf', 86, 'Purchase (Set Up)', 1, 1, NULL, NULL, NULL),
	(504, 'cde27205ba95039dfe08cd2357e70f62', 87, 'POS (Set Up)', 1, 1, NULL, NULL, NULL),
	(506, 'fa9bc308a0c9af9025b3ea3fad8085b2', 28, 'Report (Quotation)', 317, 0, NULL, NULL, NULL),
	(507, 'd0cc5ddb91304b8ef7020b9e61f95135', 28, 'Report (Quotation)', 1, 0, NULL, NULL, NULL),
	(508, 'e663ac204133a169674568b309fc7006', 28, 'Report (Sales Order)', 2, 0, NULL, NULL, NULL),
	(567, '4994ebe1c8aa16be437d49a009256195', 13, 'Product/Service (View Activity By Graph)', 9, 1, NULL, NULL, NULL),
	(568, '8dac3a3ab57ea4096f60d0627d8bc963', 13, 'Product/Service (View Activity Sales/Purchase By Graph)', 10, 1, NULL, NULL, NULL),
	(569, '1ca14bdc2fe4e591282ee5182baad491', 28, 'Report (Inventory Customer Consignment)', 102, 0, NULL, NULL, NULL),
	(570, '64e8afbed0e8bf20ff61b7391ad37508', 28, 'Report (Request Stock)', 201, 0, NULL, NULL, NULL),
	(571, 'add357f6553e72ececc6619d097fbaa4', 28, 'Report (Request Stock By Item)', 201, 0, NULL, NULL, NULL),
	(593, 'c8936eb379533b6f884e7b78f4c0ce05', 24, 'Transfer Order (Approval)', 6, 0, NULL, NULL, NULL),
	(595, '568d21b023a10f817e6f237970c99372', 34, 'Point of Sales (Add Customer)', 1, 1, NULL, NULL, NULL),
	(596, '3976ca0022d61940b553409d9fabb729', 34, 'Point of Sales (Add Product)', 1, 1, NULL, NULL, NULL),
	(597, '63a838c0681f9d3c544a2a081fa907c5', 103, 'Inventory (Set Up)', 1, 1, NULL, NULL, NULL),
	(600, 'd126b502af1226eac71b40acf9febd2f', 117, 'POS Shift Control', 11, 1, NULL, NULL, NULL),
	(601, '046b141e317eb0190c7b96537939c442', 117, 'Collect Shift By User', 12, 1, NULL, NULL, NULL),
	(602, '3d45178e9e6f1e1e497c3c998ae93404', 108, 'Total Sales By Graph', 2, 1, NULL, NULL, NULL),
	(607, '499009c02f5117f6faa5b955f8cd3304', 107, 'Cash Expense (View)', 1, 1, NULL, NULL, NULL),
	(608, 'e482d59ab9551de6080f07bc0aa990e0', 107, 'Cash Expense (Add)', 2, 1, NULL, NULL, NULL),
	(609, '585af340926f8467ce7a8b9317bddf4b', 107, 'Cash Expense (Edit)', 3, 1, NULL, NULL, NULL),
	(610, '784f23c6a270120b7aaa1abbedb6a6a9', 107, 'Cash Expense (Delete)', 4, 1, NULL, NULL, NULL),
	(611, '3fc96bb62718af8a0ed3dea44bd2b3b0', 108, 'Expense (Graph)', 3, 1, NULL, NULL, NULL),
	(612, 'cc32cefa79a21b49f7d2fce605d8649f', 108, 'Sales Top 10 Items (Graph)', 4, 1, NULL, NULL, NULL),
	(613, '2b14c7dc4b6781437db93e6ff2932064', 108, 'Profit & Loss (Graph)', 1, 1, NULL, NULL, NULL),
	(614, 'c2fa3f5330b9cd9e485364f64390b6fc', 108, 'Total Receivables', 5, 1, NULL, NULL, NULL),
	(615, 'b9be5810782a40e83ef17beee9d1ff38', 108, 'Total Payables', 6, 1, NULL, NULL, NULL),
	(616, '7e0f07a13e164584be91f57b39727e0f', 109, 'Sales Mixes (View)', 1, 1, NULL, NULL, NULL),
	(617, 'd1183bd28feb4c2d9c1126f977a04978', 109, 'Sales Mixes (Add)', 2, 1, NULL, NULL, NULL),
	(618, 'd5086920fde3dbd970d97b52fadee9a1', 109, 'Sales Mixes (Edit)', 3, 1, NULL, NULL, NULL),
	(619, 'eac2f5e598031f1cccc688f614229b7f', 109, 'Sales Mixes (Delete)', 4, 1, NULL, NULL, NULL),
	(620, '64e223906eb9a793870eddb4b8b63986', 109, 'Sales Mixes (Approval)', 5, 1, NULL, NULL, NULL),
	(621, '640852ba11f8afb366516c4afc2b92ee', 34, 'Point Of Sales (Edit Price)', 9, 1, NULL, NULL, NULL),
	(626, '23d07aaa2e6eeece76b48ea80a2ed37c', 111, 'Accounting (Set Up)', 1, 1, NULL, NULL, NULL),
	(630, '1461874e4ce4142c56e1f159ddd6f85c', 108, 'Product Reorder Level', 8, 1, NULL, NULL, NULL),
	(631, '89e4716edc75e83fafaec5e3b4c53e27', 108, 'Customer Payment Alert', 3, 1, NULL, NULL, NULL),
	(634, '204021bd42e3dae2f15f0fa824a651f7', 118, 'Open Bill', 307, 1, NULL, NULL, NULL),
	(635, '2b73c111ca6b1ee7f42be279c1c2f864', 122, 'Product List', 3, 1, NULL, NULL, NULL),
	(636, '2b73c111ca6b1ee7f42be279c1c2f864', 122, 'Customer List', 4, 1, NULL, NULL, NULL),
	(637, '2b73c111ca6b1ee7f42be279c1c2f864', 122, 'Vendor List', 5, 1, NULL, NULL, NULL),
	(642, '2b73c111ca6b1ee7f42be279c1c2f864', 115, 'Delivery Note (View)', 1, 1, NULL, NULL, NULL),
	(643, '2b73c111ca6b1ee7f42be279c1c2f864', 115, 'Delivery Note (Pick)', 2, 1, NULL, NULL, NULL),
	(644, '2b73c111ca6b1ee7f42be279c1c2f864', 116, 'Purchase Receive (View)', 1, 1, NULL, NULL, NULL),
	(645, '2b73c111ca6b1ee7f42be279c1c2f864', 116, 'Purchase Receive (Pick)', 2, 1, NULL, NULL, NULL),
	(646, '2b73c111ca6b1ee7f42be279c1c2f864', 507, 'System Setting', 1, 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;


-- Dumping structure for table sas_master.module_code_branches
CREATE TABLE IF NOT EXISTS `module_code_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `adj_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tr_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pos_rep_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quote_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inv_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inv_rep_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dn_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receive_pay_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cm_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cm_rep_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `po_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pb_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pb_rep_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `br_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `br_rep_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pay_bill_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cus_consign_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cus_consign_return_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ven_consign_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ven_consign_return_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landed_cost_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `landed_cost_receipt_code` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receive_collect_shift` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `branch_id` (`branch_id`),
  KEY `company_id` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.module_code_branches: ~1 rows (approximately)
/*!40000 ALTER TABLE `module_code_branches` DISABLE KEYS */;
INSERT INTO `module_code_branches` (`id`, `branch_id`, `adj_code`, `request_code`, `to_code`, `tr_code`, `pos_code`, `pos_rep_code`, `quote_code`, `so_code`, `inv_code`, `inv_rep_code`, `dn_code`, `receive_pay_code`, `cm_code`, `cm_rep_code`, `po_code`, `pb_code`, `pb_rep_code`, `br_code`, `br_rep_code`, `pay_bill_code`, `cus_consign_code`, `cus_consign_return_code`, `ven_consign_code`, `ven_consign_return_code`, `landed_cost_code`, `landed_cost_receipt_code`, `receive_collect_shift`) VALUES
	(1, 1, 'ADJ', NULL, 'TO', NULL, 'POS', 'POSR', NULL, NULL, 'INV', 'IRC', 'DN', 'RP', 'SR', 'SRC', 'PO', 'PB', 'PRC', 'PR', 'PRC', 'PBR', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `module_code_branches` ENABLE KEYS */;


-- Dumping structure for table sas_master.module_details
CREATE TABLE IF NOT EXISTS `module_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `controllers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `views` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  KEY `controllers` (`controllers`),
  KEY `views` (`views`)
) ENGINE=InnoDB AUTO_INCREMENT=1851 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.module_details: ~1,038 rows (approximately)
/*!40000 ALTER TABLE `module_details` DISABLE KEYS */;
INSERT INTO `module_details` (`id`, `sys_code`, `module_id`, `controllers`, `views`) VALUES
	(1, '06caee0a978113fe704d082d65e6484d', 1, 'dashboards', 'index'),
	(2, '48d6ae941ee14005c5ee10217dabf6c2', 41, 'users', 'index'),
	(3, 'ede28f8f06a759f5a1b276a7ec8c8aaa', 41, 'users', 'ajax'),
	(4, 'c13adbe32b8492ec31ce55332b900d1c', 41, 'users', 'view'),
	(8, '4887aa87bb6405cc7d19fb310b4c287e', 41, 'groups', 'index'),
	(9, '17b9a5734f273365801515d4d62b0fa8', 41, 'groups', 'ajax'),
	(10, '518f3228f4fea50dbe8bd336dd53b2ab', 41, 'groups', 'view'),
	(14, '0ad7d8a741b244ba75e13cc8e08a31e9', 597, 'locations', 'index'),
	(15, 'd095900a8289c21a5ba87849ad1843ab', 597, 'locations', 'ajax'),
	(16, 'b857ea644c314fd84f5e2359bd708960', 597, 'locations', 'view'),
	(17, 'd743597e6521f0301c836d337bb60b59', 597, 'locations', 'add'),
	(18, 'f0ee917a30c7012e92c8327064bb87d8', 597, 'locations', 'edit'),
	(19, 'b11704b1be83d527d36e9533f7b89c01', 597, 'locations', 'delete'),
	(20, '1c6ba18ee3621c99ec1a574a13d35fd8', 597, 'location_groups', 'index'),
	(21, 'df2e1445fdee0e3fa0ebecfd63463ddb', 597, 'location_groups', 'ajax'),
	(22, '79a37639108f60f968647e8b741c854d', 597, 'location_groups', 'view'),
	(23, '402633e86a2571629688b7d1b3d37d5e', 597, 'location_groups', 'add'),
	(24, '4f0f1a3e4f25d983c8e24692834d6f9c', 597, 'location_groups', 'edit'),
	(25, 'e3d5f46d924ba0cb43dd820bc47e23bf', 597, 'location_groups', 'delete'),
	(26, 'd1b3814c59e24d66b6dd448781a6c55e', 597, 'uoms', 'index'),
	(27, '41d0c448714cc32b4f3cad946ca01239', 597, 'uoms', 'ajax'),
	(28, '7312ba166d929297ba80fb4a38fe5f74', 597, 'uoms', 'view'),
	(29, '0548089643fd1d6bcb5fb7890711fc30', 597, 'uoms', 'add'),
	(30, 'bdbe6c56b7241d7655b3ff8c56782a3b', 597, 'uoms', 'edit'),
	(31, '7c2642478839f0b0d2ea2d029c91085f', 597, 'uoms', 'delete'),
	(32, '0d707634f83895d452dbaa3eec20a2d9', 597, 'uom_conversions', 'index'),
	(33, 'a1045e499a7dcb0a9867bec4e83e23b1', 597, 'uom_conversions', 'ajax'),
	(34, 'f7dd19a5305edbf8b8a76cb4c2050374', 597, 'uom_conversions', 'view'),
	(35, '384217b3df4214483129676828674b61', 597, 'uom_conversions', 'add'),
	(36, 'e1f2cec2eb3f372e0ca2fc8bddc9c2e8', 597, 'uom_conversions', 'edit'),
	(37, '2a9bdfee52254fb818349321d9869b62', 597, 'uom_conversions', 'delete'),
	(38, '3dd66953e33af010fd29216287994d53', 26, 'provinces', 'index'),
	(39, '2ce042bb0375e56c5ffb7992d9168a51', 26, 'provinces', 'ajax'),
	(40, 'b4fea3fbff21b800ea777becabf8d139', 26, 'provinces', 'view'),
	(41, '2f0ebdf61f96461e9822d061dfc88bca', 27, 'provinces', 'add'),
	(42, '71765a57f34e72158c9a878e6a42b7d7', 28, 'provinces', 'edit'),
	(43, '8f0831ea2855c5d3d352b9eac576805f', 29, 'provinces', 'delete'),
	(44, '2c90dfd542634744b561106d79a17ea3', 30, 'districts', 'index'),
	(45, 'c824120e0f0f60db489afc7ea9bc952f', 30, 'districts', 'ajax'),
	(46, '61a828facd1f29cd297da2d60fed448e', 30, 'districts', 'view'),
	(47, 'edb405bedbc2e9255ef38f9685303fba', 31, 'districts', 'add'),
	(48, 'fb976b6ce4248f58c42376f56f2c2f74', 32, 'districts', 'edit'),
	(49, 'd3b4274d7d19a9dcd0a246132de56a58', 33, 'districts', 'delete'),
	(50, 'adfaba260e0596123d860b12d0ed1837', 34, 'communes', 'index'),
	(51, 'b633de36b962531d13e6136feba7807c', 34, 'communes', 'ajax'),
	(52, '4740353ac79f2c2679bb888724d86253', 34, 'communes', 'view'),
	(53, '718d14cca7f272b68b80aefb1f5e0e29', 35, 'communes', 'add'),
	(54, '4a9405a373fa0a89e6565b150acc65bc', 36, 'communes', 'edit'),
	(55, '643b953aa01b399c7117354f4daa1386', 37, 'communes', 'delete'),
	(56, '33cbc677e884442ce04dbfecfc13a83c', 38, 'villages', 'index'),
	(57, 'd273985f55fe9b1b566b2c874d8f4e99', 38, 'villages', 'ajax'),
	(58, '1ea59b9de3299e1ffc07eb778300170c', 38, 'villages', 'view'),
	(59, '81d2c8e308f6302e2a990ebd1c4ab9f8', 39, 'villages', 'add'),
	(60, 'dacfc389a324ef60a098c82c30647061', 40, 'villages', 'edit'),
	(61, '324c83b92b5fde2d0ff749bf7820b06b', 41, 'villages', 'delete'),
	(62, '19070a8294fb56e3c37c71b1799d04b9', 42, 'cgroups', 'index'),
	(63, 'ab22a40b00469d8ae478d2565fb8fdc5', 42, 'cgroups', 'ajax'),
	(64, '790f1ca014e7c5da2ce2887ae83bd268', 42, 'cgroups', 'view'),
	(65, '3c64b0a42ee738ae11cda2fe8ba66992', 43, 'cgroups', 'add'),
	(66, '487c85737af698b80293fa43673ba237', 44, 'cgroups', 'edit'),
	(67, '1540742529b38b752d8cf9cbcdcd8f56', 45, 'cgroups', 'delete'),
	(68, 'ccd93fdfc685edfb27cb23fa4e21c5ed', 46, 'products', 'index'),
	(69, 'd1c402b9a17da6facec3856af7cec9ba', 46, 'products', 'ajax'),
	(70, '738a86f10401471a026a0c92ba3d137e', 46, 'products', 'view'),
	(71, 'dbd8ef23fda9f608a568c48c225ba150', 47, 'products', 'add'),
	(72, '5e9c8be6043d1fa76705ad80f1ca2b17', 48, 'products', 'edit'),
	(73, '4f7e4a9e1ff427cd9e10859341c28845', 49, 'products', 'delete'),
	(74, '6b0d038c393ac1c8901eb3ea5cf6b5c2', 50, 'customers', 'index'),
	(75, '195c6a5a8379fe389818a56649462f4f', 50, 'customers', 'ajax'),
	(76, '3ce9d08add6b77d81b801ac0e5e05548', 50, 'customers', 'view'),
	(77, 'ebf725e1d72f89bd67de2c686e924c88', 51, 'customers', 'add'),
	(78, 'ee1b8352721a3076500c7d38dd6a4d37', 52, 'customers', 'edit'),
	(79, 'e2cb21a0219215152704a5cbbb11ba65', 53, 'customers', 'delete'),
	(98, '13e8b17e1ccea09f3e47d415179f26eb', 597, 'pgroups', 'index'),
	(99, '41c3e3fbfece2a20eb74fb98fdd9e73c', 597, 'pgroups', 'ajax'),
	(100, 'e48bdde822637f8093689f16ff2fd39d', 597, 'pgroups', 'view'),
	(101, '54de8fa1cf316fdec0b89cc75104e56c', 597, 'pgroups', 'add'),
	(102, '2f5ec943bc325c3dd44d9d92a8e03610', 597, 'pgroups', 'edit'),
	(103, '67a4f598748287ec26438807ece879c6', 597, 'pgroups', 'delete'),
	(104, 'ba1edb23e8d6e190df667a4f622175e0', 597, 'pgroups', 'searchProduct'),
	(116, '86c5307db7ce9514821149912971e036', 72, 'vendors', 'index'),
	(117, '893a99b150409ad21e240fb06d1e5cb9', 72, 'vendors', 'ajax'),
	(118, '81669d8266ebbab9970d225daa893ed8', 73, 'vendors', 'add'),
	(119, '17e9890f791c22eb69e4c056f89bd2d4', 74, 'vendors', 'edit'),
	(120, 'a059940caf9e873ee99a6cbf0db63260', 75, 'vendors', 'delete'),
	(123, 'bfb62692d3345f03a6c96fe1ac4f5d83', 626, 'chart_accounts', 'index'),
	(124, '95e7142f49909cf645ad619d41ea09e5', 626, 'chart_accounts', 'ajax'),
	(125, 'cb7d299d4af56e109e945b10c113813d', 626, 'chart_accounts', 'view'),
	(126, '3a6139c67805af1f8f200acdb404a8b0', 626, 'chart_accounts', 'add'),
	(127, '70f597218a6ca0a1b7d28339e68c0896', 626, 'chart_accounts', 'edit'),
	(128, 'ef18d3f034a91a85cb48c9460f9a980c', 626, 'chart_accounts', 'status'),
	(129, '0eac1d1f5f78f6a250c8e5d8a13347c6', 80, 'chart_account_types', 'index'),
	(130, 'd79da3b918ff3e3848293c06c645a9d4', 80, 'chart_account_types', 'ajax'),
	(134, 'c54eda2d91b22b780b8dd741eae7cf82', 83, 'chart_account_types', 'status'),
	(135, 'a962f2ef350a50ace44ce6d02b3aa61c', 84, 'chart_account_groups', 'index'),
	(136, 'd5f2bf3c299dc3a2e491bdbb7d83179b', 84, 'chart_account_groups', 'ajax'),
	(137, '6e18c41d3736eaf3d67cd5529f6f1cfc', 84, 'chart_account_groups', 'view'),
	(138, '12084e895b15d916d956802bca69cb7c', 85, 'chart_account_groups', 'add'),
	(139, '28a20aeab928c2fbf4b901cf82dfd6c4', 86, 'chart_account_groups', 'edit'),
	(140, '431c5859a479f4c4d037e9ea6a74d102', 87, 'chart_account_groups', 'status'),
	(144, '14a5f4bae7153c8954bc96996001be46', 91, 'purchase_orders', 'index'),
	(145, '618d9f449e8eabdc82c7408d3d45192e', 91, 'purchase_orders', 'ajax'),
	(146, 'de8dc40e4d15738ff07015da6f839c40', 91, 'purchase_orders', 'view'),
	(147, '9dd550cbfa4936359fc56c6a3828dbc7', 92, 'purchase_orders', 'add'),
	(149, '2915be8bc2112822276154c8b8f9b13c', 94, 'purchase_orders', 'delete'),
	(151, 'd1943e767534449f9c8b315f76b93a32', 46, 'products', 'searchProduct'),
	(154, '41f96500b5468d88a306481500304c5f', 72, 'vendors', 'view'),
	(158, '20db0a040556c23beea2fbee11c0d79e', 644, 'purchase_receives', 'index'),
	(159, '959fc13ff358c013ede01fe5cb36360f', 644, 'purchase_receives', 'ajax'),
	(160, 'f635f4fd1a6dcd1c84c1bd0a245529e4', 644, 'purchase_receives', 'view'),
	(166, '30c40d38302bb0d45125ad0e6a10a683', 101, 'transfer_orders', 'index'),
	(167, '8fe5aaae73415b189c977d951aa048b6', 101, 'transfer_orders', 'ajax'),
	(168, '3d175f917a6e3ee2804f62995f54f8f7', 101, 'transfer_orders', 'view'),
	(169, '15a42341981dc0e491b177f9833500b0', 102, 'transfer_orders', 'add'),
	(170, '9689dcef28c50801acf02324efebf6ed', 103, 'transfer_orders', 'edit'),
	(171, '780f8729d961f5f83d7e891fe364b34e', 104, 'transfer_orders', 'delete'),
	(172, 'b866459fa3fd5c1786f11f91c3a7fcc2', 105, 'sales_orders', 'index'),
	(173, 'af17cf1153952f98684b3559e2941a6f', 105, 'sales_orders', 'ajax'),
	(174, 'db862280a82da4f6b3f0df1c8a6b0057', 105, 'sales_orders', 'view'),
	(184, '34b1f0763cb219a48bcaa8abec9561f1', 645, 'purchase_receives', 'receive'),
	(185, '954a8ca98b5a880896e8a6aacc41bdd2', 504, 'exchange_rates', 'index'),
	(186, '8f8ac097733746da8503459e6e0cd2fb', 504, 'exchange_rates', 'ajax'),
	(188, 'a335715029ee0935653be9f0cdb375fe', 504, 'exchange_rates', 'add'),
	(193, 'ed0bc8af1ebb76437c9600dfb2b7d2b4', 46, 'products', 'searchProductByCode'),
	(195, 'c4c116e89611c5636826f1b3496a0afa', 645, 'purchase_receives', 'receiveAll'),
	(196, 'e2043f204cbc02f3bb0041051665bcd1', 113, 'general_ledgers', 'index'),
	(197, '3a84a7473d30329d506b43fec266cb9a', 113, 'general_ledgers', 'ajax'),
	(198, '9ce4d0ee41c7c2425c52c7866f3891f0', 113, 'general_ledgers', 'view'),
	(199, '1f076e2ec1828c11cdaa56517c66b87d', 114, 'general_ledgers', 'add'),
	(200, '6d6ad9654fab4153a2534f7ab7929cef', 115, 'general_ledgers', 'edit'),
	(201, 'e8db09edc9f147be5dfe83c18d6e608a', 116, 'general_ledgers', 'delete'),
	(203, 'e0a5fb8e20a8a244b760cf4f4e34bbcc', 118, 'reports', 'trialBalance'),
	(204, '6091c3a1f9613969eeeb8d0c8e797c06', 118, 'reports', 'trialBalanceResult'),
	(207, '38d5a625be89225133b35505e2dda169', 119, 'reports', 'profitLoss'),
	(208, '7756fb333282bee1af747d10a4989dfb', 119, 'reports', 'profitLossResult'),
	(209, 'a52586b6d2d8d2280513b558cc47af2a', 121, 'discounts', 'index'),
	(210, 'dd4c57efc81718b494151871c7eb0324', 121, 'discounts', 'ajax'),
	(211, '84c6a2f0944d2c40b902d12a9d06c1a8', 121, 'discounts', 'view'),
	(212, '949347dba9a2a0cc4cb497a4ca1f4b14', 122, 'discounts', 'add'),
	(213, '20390f77b92daa0597ad6f33621073c7', 123, 'discounts', 'edit'),
	(214, '8af0b5bdece1eab1575c76655d2d9205', 124, 'discounts', 'delete'),
	(215, '9ef5e90adb217a45f97f0936d1bcb2d2', 120, 'reports', 'balanceSheet'),
	(216, 'df12b630f1e7c6a173ae641d5bfd95b4', 120, 'reports', 'balanceSheetResult'),
	(222, 'bda6ff3bd50aa061a6b2c79414840a9d', 125, 'transfer_receives', 'index'),
	(223, '57b0c7601993f59895ed713cb3d27722', 125, 'transfer_receives', 'ajax'),
	(224, '992343543bea8d4ef2fa1559174a002b', 125, 'transfer_receives', 'receive'),
	(225, '7e7e3fbcaf45156dbdae73bd9396dd21', 126, 'transfer_receives', 'receiveDetail'),
	(227, '85f3cc42d319025a22492e3a3494b51f', 41, 'companies', 'index'),
	(228, '32b63a35d08cacce8a8b6a218c2bef2d', 41, 'companies', 'ajax'),
	(229, '11b34471b3b5f5b348f69ffe8445e20b', 41, 'companies', 'view'),
	(230, 'ead068d87606eba11f776ebcc01b1459', 41, 'companies', 'add'),
	(231, '782cb52ec0e698e2e0242aa01d496798', 41, 'companies', 'edit'),
	(232, '308dcd97001a151322d6d2698df94338', 41, 'companies', 'delete'),
	(235, 'bc09022542b44458d444a64ac4907b06', 126, 'transfer_receives', 'action'),
	(238, '5e0cfb67d5ef70661046a89007245b66', 118, 'reports', 'trialBalanceResultByMonth'),
	(239, 'dfcc998b6cf0414ad5527053254c8223', 119, 'reports', 'profitLossResultByMonth'),
	(240, '80e05b1a4a841fa98cfc2f64b24fdf69', 120, 'reports', 'balanceSheetResultByMonth'),
	(241, '34f2803b79144df9df7b0df311883ab5', 126, 'transfer_receives', 'void'),
	(245, 'e623b946755ecb8464141f868cd49746', 41, 'users', 'editProfile'),
	(248, '991edc53f5615e372ce14075b0037f18', 132, 'reports', 'customerAddressList'),
	(249, 'a716d993ff3301b28b33cd3cfee33750', 132, 'reports', 'customerAddressListResult'),
	(250, '9cf407fcfefa295d75b94acec89f1f40', 114, 'general_ledgers', 'checkCompany'),
	(251, '9760cf268da01d1b81522c1a8bf62015', 115, 'general_ledgers', 'checkCompany'),
	(253, 'd9ee8b1b9ac6f1ae010bea6b4dc9719f', 134, 'reports', 'productAging'),
	(254, 'c3fee97aedc62f56c9f85e9d575d6ca9', 134, 'reports', 'productAgingResult'),
	(255, 'd4415d9a902ea1c79a10b160cf7f9194', 133, 'reports', 'customerAddressDetail'),
	(256, '7fb5495cb19d3ea81c99eab7c1255816', 133, 'reports', 'customerAddressDetailResult'),
	(257, 'b327db7e5d252abc35b9a819630bb1be', 135, 'reports', 'cashFlow'),
	(258, '801073c23f9d3b9af898ff2e49280ec5', 135, 'reports', 'cashFlowResult'),
	(259, '00489eafadc9f087debe6c3669c971ba', 135, 'reports', 'cashFlowResultByMonth'),
	(260, 'c2af2d457eef8726b73d2547da2940c8', 43, 'cgroups', 'customer'),
	(261, '1455e2d926ff4fd34b87695947fc0f87', 43, 'cgroups', 'customer_ajax'),
	(262, '0db5e76af427961c0721dcb7bb904f1c', 44, 'cgroups', 'customer_ajax'),
	(263, '0920408ce9595deca360886e9c7ce849', 44, 'cgroups', 'customer'),
	(264, '6b5c4f56b523d39ac3623213c8ca5bcb', 398, 'purchase_orders', 'discount'),
	(267, '24dca0d16b34fa39a78a9c7b87f74b5d', 597, 'pgroups', 'product'),
	(268, '854cb44316508f5eeb5d2bbf856718e6', 597, 'pgroups', 'productAjax'),
	(269, 'f92541b3ca26a2a2f7f4d9aac1a187ef', 1, 'uoms', 'getRelativeUomByProductId'),
	(273, '214efd1665854cd296d2014636c2a997', 137, 'sections', 'view'),
	(274, 'eb141de20d609dd41cecbd07f0f7be63', 137, 'sections', 'index'),
	(275, 'b57760e075190934c0c1b31c354bdb6d', 137, 'sections', 'ajax'),
	(276, '90731d1c0b8b257b7448c80229568c22', 138, 'sections', 'add'),
	(277, 'e3910247caee01a881041a8d6691257a', 139, 'sections', 'edit'),
	(278, '70162a64f440e04d25ff1c3441def8e5', 140, 'sections', 'delete'),
	(279, '732075e7359892c111758ad83a66a318', 141, 'services', 'view'),
	(280, '49c74271a71b4a4621357fab079ce306', 141, 'services', 'index'),
	(281, '4b94e4ed3ea8bfc8ff119ef1925d0876', 141, 'services', 'ajax'),
	(282, '305fd577b6a67eaddc93ded54d1dec1b', 142, 'services', 'add'),
	(283, '1f9982f62a73e06c705adf8320c22467', 143, 'services', 'edit'),
	(284, '4e4a28f42fcbe832bd25a6a407027b50', 144, 'services', 'delete'),
	(286, 'e4c541080735dba212a6092644b954ef', 118, 'reports', 'trialBalanceResultPeriod'),
	(287, 'f57cb595118f6471bd2ea08b42ee14fd', 118, 'reports', 'trialBalanceResultPeriodByMonth'),
	(288, '252ddc56ec02dd49f5ed6baed304e576', 145, 'purchase_orders', 'aging'),
	(289, '9f841ad1f3c1254fb5a34f877588cecc', 91, 'purchase_orders', 'printInvoice'),
	(290, '74b572273f15985f6042be2e3425f40d', 91, 'purchase_orders', 'printReceipt'),
	(299, '66511fd7ef5ce754ccad051d9cdb1c3b', 645, 'purchase_receives', 'printInvoice'),
	(306, 'a63ae2022454c15dd8f621b0ac767a0c', 149, 'point_of_sales', 'add'),
	(311, 'b85e9546fe298e9033d4f3d2d5b785b3', 152, 'point_of_sales', 'void'),
	(312, '5c4130235aec03d2d5ce040a73c20951', 153, 'reports', 'accountReceivable'),
	(313, '50e9e046d2d2177d88847ae0f22426b2', 153, 'reports', 'accountReceivableResult'),
	(314, '61553eb20a87cccf503e3893a8f6c6d9', 154, 'reports', 'accountPayable'),
	(315, '8698a973013338e6bb83e6980f2c492b', 154, 'reports', 'accountPayableResult'),
	(322, 'b9b6ba3c6da282c4f0a1ac050843ae27', 149, 'point_of_sales', 'getProductByCode'),
	(323, 'bebb4ab8896883352aab54dfa69bfe31', 1, 'general_ledgers', 'indexByAging'),
	(324, 'da0e7f20213da03df59872f670ab900b', 1, 'general_ledgers', 'ajaxByAging'),
	(325, '68a84c37d902e714460dfecc0dbbab15', 155, 'reports', 'purchaseOrderBarcode'),
	(326, '7709cc991e8ddc2825ad2ee289a56868', 155, 'reports', 'purchaseOrderBarcodeAjax'),
	(327, '5c8c45279c37627e1c936f16104219d1', 1, 'uoms', 'getRelativeUom'),
	(330, 'b1fe42a919b257f161d85425a1ee686e', 113, 'general_ledgers', 'indexByTb'),
	(331, 'abb3e0fa82b9c4de213400e880009eb7', 113, 'general_ledgers', 'ajaxByTb'),
	(332, 'e47f883dc2e45898b6e09bb91063658d', 151, 'point_of_sales', 'printReceipt'),
	(334, '4c6d4d3f19adcf70dc63440317ce1717', 117, 'reports', 'generalLedger'),
	(335, '8aa6eadd07678592bfebef20d82652b4', 117, 'reports', 'generalLedgerResult'),
	(336, 'dfd39ee968a8070a21bfeee2df588ded', 117, 'reports', 'generalLedgerAjax'),
	(341, '6f5ec0ac49e7d0eee62f38eed1e94112', 156, 'budget_pls', 'index'),
	(342, '1b2b699d2185beab3ad82785339d89ea', 156, 'budget_pls', 'ajax'),
	(343, '62ee7088393e074a57399ea6ae40f2e6', 156, 'budget_pls', 'view'),
	(344, 'd487b29d16d872421efa885d077f9a31', 157, 'budget_pls', 'add'),
	(345, 'd6cd090b965f7b254d2be7ab91a031b4', 158, 'budget_pls', 'edit'),
	(346, '0b8cbb32827e6eb60f228767e644d268', 159, 'budget_pls', 'delete'),
	(347, '5e08304b06693cf39902a2aa1f3dc37f', 160, 'classes', 'index'),
	(348, '08e5fb1055dad138723a153ad998a727', 160, 'classes', 'ajax'),
	(349, 'de5882affe10e0178241cc594205e1a2', 160, 'classes', 'view'),
	(350, '1d9de1bae32a33dfe540e137611e1d92', 161, 'classes', 'add'),
	(351, 'b3dd607d085d6e0d119238924b9b1c75', 162, 'classes', 'edit'),
	(352, 'b5706739141bde33bfe1adc3af83e7a7', 163, 'classes', 'delete'),
	(354, '73e7ef74969aee62027b881fad8f011c', 134, 'reports', 'productAgingAjax'),
	(355, '2533c615aba6ddce2f05ffe2d1a5d413', 165, 'reports', 'productCycle'),
	(356, 'b5bc69e9fb90ec85ccfddc15d246bc36', 165, 'reports', 'productCycleResult'),
	(357, '2d73c32b9d0f7573e2a74c0d245a2453', 165, 'reports', 'productCycleAjax'),
	(358, '4655479e97ca06794fd72d8e66c24d6a', 166, 'reports', 'productAverageCost'),
	(359, '225c6d83743a04dec4e0bcec72c16b2e', 166, 'reports', 'productAverageCostResult'),
	(360, '05588dfd3ef1cc675e1986e0c8a28b7c', 166, 'reports', 'productAverageCostAjax'),
	(361, 'd1ca10597a9b35e830c32d020d068666', 167, 'reports', 'productPrice'),
	(362, '94f0e735c0b58e7a146e11b8d7a641b9', 167, 'reports', 'productPriceResult'),
	(363, 'd2820bb8bed8949f1b601b33a1e28acf', 167, 'reports', 'productPriceAjax'),
	(370, '3a6af53ccba78ff78e687e194f915543', 168, 'reports', 'customerDormant'),
	(371, '90ccbf3b98d8c40d5fa1527ee0f55aa7', 168, 'reports', 'customerDormantResult'),
	(372, 'ebae1a164248324692fcc247ff9762bd', 168, 'reports', 'customerDormantAjax'),
	(373, '1e08d459db0c06634ee2438cc4ebf8d2', 169, 'reports', 'customerDiscount'),
	(374, '821b245232b6a1075724f0d3330fa34a', 169, 'reports', 'customerDiscountResult'),
	(375, '3e0e134b50841ef36e00cb41a5658257', 169, 'reports', 'customerDiscountAjax'),
	(376, '09fa80d2fbb9742827ebb4d56f0eba8d', 170, 'reports', 'userRights'),
	(377, '78d8ecaafa032678eb76a2d2f1f25d74', 170, 'reports', 'userRightsResult'),
	(379, '069c6818586a44e4aadd18baca36ba8f', 171, 'reports', 'userLog'),
	(380, '4873a6f32284770edbd1927e9b6f491d', 171, 'reports', 'userLogResult'),
	(381, 'd7e38777defdeb63cec865074daede44', 171, 'reports', 'userLogAjax'),
	(382, 'ff367ce070b0be70cf02788daf67b465', 172, 'reports', 'vendorProductList'),
	(383, '10aac7f3ffe4ce321b6f02ccf603ec83', 172, 'reports', 'vendorProductListResult'),
	(384, '4c54ead93d3849fa3a45e4ec9ec683db', 172, 'reports', 'vendorProductListAjax'),
	(385, 'd761674f960494b4d07033e4e02692fa', 173, 'reports', 'vendorAddress'),
	(386, 'fd2423c2d676a020f1e6f38b2be13c6a', 173, 'reports', 'vendorAddressResult'),
	(387, 'df24537916246b8faa3a72475e903f70', 173, 'reports', 'vendorAddressAjax'),
	(388, '4f7849e1afd72bf8801c9af3b0de1dc2', 175, 'reports', 'productScrap'),
	(389, '6066622a578de816514236d54f77ba76', 175, 'reports', 'productScrapResult'),
	(390, '71c051ba82dac738bbc2874aab8eb0f5', 175, 'reports', 'productScrapAjax'),
	(391, 'abdfafd724bbc44b320d680b33b492b6', 174, 'reports', 'vendorAddressList'),
	(392, '8b1f34e5756a7643448f3047ba81a277', 174, 'reports', 'vendorAddressListResult'),
	(393, 'd250895ca2ca7e2a6629e4a2f243177b', 626, 'settings', 'ics'),
	(394, '526626447b766eaf7c75b682ccab0290', 1, 'general_ledgers', 'indexById'),
	(395, 'b34dfe05fc221a9b7a9237de00c08085', 1, 'general_ledgers', 'ajaxById'),
	(396, '84bb471fb71b147a75d7465e5b5a918f', 113, 'general_ledgers', 'indexByGroup'),
	(397, 'a6e50c67d9d58f32cdb904892aa9fa60', 113, 'general_ledgers', 'ajaxByGroup'),
	(398, 'b1ea67bb3979b2fe90639cb8f57eec4e', 177, 'general_ledgers', 'writeChecks'),
	(399, '01953e91e340dc40a108f97a6b518330', 177, 'general_ledgers', 'getBalance'),
	(402, '166eaa4821b30fa06586ec605463ff93', 179, 'reports', 'productMove'),
	(403, '32288347f24b8a7363717010d138faca', 179, 'reports', 'productMoveResult'),
	(404, 'd96b4b340a719015f7969847ac1e3e7d', 179, 'reports', 'productMoveAjax'),
	(408, 'e7a092dc90418cce346f43c30835b063', 182, 'point_of_sales', 'changeDiscount'),
	(420, '004ca4ceb4cf1062665b889a365d4b27', 186, 'general_ledgers', 'indexAll'),
	(421, '7460bd70c4b1a93aaa2f0f156855d18c', 186, 'general_ledgers', 'ajaxAll'),
	(422, '9aab380a55804823f67101ee320eb3ad', 186, 'general_ledgers', 'viewAll'),
	(423, '744225b82738f0f6f012f757258907a9', 187, 'general_ledgers', 'addAll'),
	(424, 'c294a41e8865ef6d0ab9900cd23e79e9', 187, 'general_ledgers', 'checkCompany'),
	(425, 'd400557e980724149c256d16738c49d9', 188, 'general_ledgers', 'makeDeposits'),
	(426, '883b5daaf98fdb26b72f5bc369462392', 188, 'general_ledgers', 'checkCompany'),
	(427, 'aab60fdf51c6164f1d1df2116348b131', 189, 'general_ledgers', 'editAll'),
	(428, 'cdedcdbfdaec32b757c3d7b8652c652b', 189, 'general_ledgers', 'checkCompany'),
	(429, '1bd1695fb66bf880de95c2e6b2aab246', 190, 'general_ledgers', 'deleteAll'),
	(437, 'bc0a492cce4e236269d9cb56ba747144', 191, 'reports', 'productInternalUseResult'),
	(438, '22174c752532c50299824afa136fc624', 191, 'reports', 'productInternalUseAjax'),
	(439, '8eebce95e6b7f61dd3b8a63c9ea74d2a', 192, 'reports', 'customerInvoice'),
	(440, 'e3fca5b0a1f29e1ad7b84ad3ef3c8b6c', 192, 'reports', 'customerInvoiceResult'),
	(441, '7a5fa23e1916cd9d5f26d7b58565022f', 192, 'reports', 'customerInvoiceAjax'),
	(442, 'c11c306d78e5db0295e9ec231939cbcf', 193, 'payment_terms', 'index'),
	(443, '0da49cfacf6a9052f5bfccac85e2e4f6', 193, 'payment_terms', 'ajax'),
	(444, '7898726b8d0e4e311424e28dc49d92b9', 193, 'payment_terms', 'view'),
	(445, 'bb65f3b290a97196c7182c9a2371735d', 194, 'payment_terms', 'add'),
	(446, '7912294ad4bad7a0d6113f5b4a5406e9', 195, 'payment_terms', 'edit'),
	(447, 'babd9064eab2ccc46842b0a7d68029c5', 196, 'payment_terms', 'delete'),
	(450, 'd4df301f5883c63f98a37d71b747aeff', 197, 'reports', 'product'),
	(451, '017aa622ba9601235e04a67f1cc83a91', 197, 'reports', 'productResult'),
	(452, '82a6f174b7f3db66a4c63ab4d671dc11', 197, 'reports', 'productAjax'),
	(453, 'b7a1f35bf66877e4105ab451882ba0ab', 198, 'reports', 'purchaseInvoice'),
	(454, '53db580a80686581fcd74179d6c6fb65', 198, 'reports', 'purchaseInvoiceResult'),
	(455, 'c1886a5eb9a8f1092f24850e2d80907f', 198, 'reports', 'purchaseInvoiceAjax'),
	(456, '8633dab684d178aaf9c348d59f4b2da6', 626, 'chart_accounts', 'delete'),
	(457, '0be8805b46f89120bc21b4d8c40c6ca0', 200, 'chart_account_groups', 'delete'),
	(461, '72e93516aa052ab62f7dcd8ad0f12727', 201, 'purchase_orders', 'edit'),
	(462, '1eb0506cdb7e2ea805a380b801c1e923', 202, 'credit_memos', 'index'),
	(463, '344f0563ec8750c54c99f55c439e5217', 202, 'credit_memos', 'ajax'),
	(464, '171023a1444bae91701ae38254efbd8c', 202, 'credit_memos', 'view'),
	(465, '1db2892ffb198e226df543f2f3876258', 202, 'credit_memos', 'printInvoice'),
	(466, 'f33a6ebd6d96b372da7aae9425a8b8f4', 202, 'credit_memos', 'printReceipt'),
	(467, 'b7a3e8d40aeeedbd3a08b2a79aad9ce9', 202, 'credit_memos', 'printReceiptCurrent'),
	(468, '2e8ee9651e35c26530767e83c1b77397', 203, 'credit_memos', 'add'),
	(469, '3b8e2792068a09f533f3e107c27e6984', 203, 'credit_memos', 'orderDetails'),
	(475, '460dea3e570d279c3ef6ed6d209e9915', 202, 'credit_memos', 'searchProduct'),
	(476, 'a3184f1b0c540a2bdc0d84007335c949', 202, 'credit_memos', 'searchProductByCode'),
	(478, 'd47335cf10c0bb0d982f14ce049b5113', 202, 'credit_memos', 'product'),
	(479, '58dbc7329a8791237885bee9b570df2e', 202, 'credit_memos', 'product_ajax'),
	(480, 'da148e34f18a0361794007072c34c76b', 202, 'credit_memos', 'customer'),
	(481, '7c0285373df4c263d6db9692fefe1b74', 202, 'credit_memos', 'customer_ajax'),
	(482, '2aa482514bfdfc51c94f866df93cb89a', 204, 'credit_memos', 'aging'),
	(485, '521be1c0c607ea7e9800a968c39cc31c', 205, 'purchase_returns', 'index'),
	(486, 'e2f33c4b25fc1da0e2afaf63b7844f16', 205, 'purchase_returns', 'ajax'),
	(487, '56b7bdc883e0ab6cda36b182e8d11aab', 205, 'purchase_returns', 'view'),
	(488, '42a709cd03886aa8ab832e98d584e75f', 205, 'purchase_returns', 'printInvoice'),
	(489, 'aabb67b2e033e0377f6fa02d42504c05', 205, 'purchase_returns', 'printReceipt'),
	(490, 'f53716cf77e5103041926396a05d4b82', 205, 'purchase_returns', 'printReceiptCurrent'),
	(491, '8308096ed2a14d9afee295d8614bcd70', 206, 'purchase_returns', 'add'),
	(492, 'ae8b3dd34af3f3209134b3030951bd8c', 206, 'purchase_returns', 'orderDetails'),
	(505, '487ed7480fdcbfa1b94f102745193dc0', 207, 'purchase_returns', 'aging'),
	(508, '010ff59eba55bfd15191af80fac66cd8', 626, 'settings', 'accountClosingDate'),
	(509, '271808e30ff52627297cb2a731ea5e58', 209, 'receive_payments', 'index'),
	(510, '49a9e0b9f183cd3459451ecb5fff941c', 209, 'receive_payments', 'ajax'),
	(511, '81902e20535b4a281e2319c5341a9355', 209, 'receive_payments', 'save'),
	(512, '81da71d8bc2632a6a36519c32fa46288', 210, 'pay_bills', 'index'),
	(513, '7d80def3972ca240b8e3b0a47a351e76', 210, 'pay_bills', 'ajax'),
	(514, 'ae97cff2cbb1e59733f9392d4c796b14', 210, 'pay_bills', 'save'),
	(519, '2f437f94f04155322d372ed0e2b6efca', 212, 'reports', 'customerInvoiceCredit'),
	(520, '7cc58860267ac19da94ec8f459f1d73a', 212, 'reports', 'customerInvoiceCreditResult'),
	(521, 'eb5c0ae6b5c9d0848e49bf79584b386e', 212, 'reports', 'customerInvoiceCreditAjax'),
	(522, 'f9d4a98413ef74d459847ea51931efd5', 213, 'reports', 'purchaseInvoiceCredit'),
	(523, 'c54b361b591a0759f0707705b9bcc563', 213, 'reports', 'purchaseInvoiceCreditResult'),
	(524, 'a4b90ee4b091dd26b437dce8950c3028', 213, 'reports', 'purchaseInvoiceCreditAjax'),
	(525, '92d4768731a3e3a3faf9e166249508d8', 197, 'reports', 'productViewQtyDetail'),
	(526, '838f66637e3387415112fbc04aadb27e', 214, 'reports', 'customerBalance'),
	(527, '337249f8f60dd29a165096316e19c19c', 214, 'reports', 'customerBalanceResult'),
	(528, 'b1ba02287eb3ab16718d42daee7de34e', 214, 'reports', 'customerBalanceAjax'),
	(529, 'adf3ddc48108f0f1ed91cb642ecf8a84', 215, 'reports', 'vendorBalance'),
	(530, '6b4984f9a69427b8ed2e169a59d06fad', 215, 'reports', 'vendorBalanceResult'),
	(531, '5cfeeb2e7321d9cd2025e2f1fbf1ffea', 215, 'reports', 'vendorBalanceAjax'),
	(532, '3143a98c6be6c2b4fbea1926a18a6e05', 216, 'credit_memos', 'service'),
	(533, '01b2286271d3c447641066a51e64baff', 217, 'credit_memos', 'miscellaneous'),
	(534, 'c7bbfa4f91f3d36c22ac96d536cd98ef', 218, 'purchase_returns', 'service'),
	(535, 'bf66c952339a36c937186752533dbd52', 219, 'purchase_returns', 'miscellaneous'),
	(536, '0bbb828c7d2eee5873b4d6ce5f258255', 204, 'credit_memos', 'invoice'),
	(537, 'cc8877b4248c9e46d93404a176363a49', 204, 'credit_memos', 'invoiceAjax'),
	(538, 'a791bc81045b7e089096c5661a46b263', 207, 'purchase_returns', 'invoice'),
	(539, 'a624856511565d8e16757a2378b41e19', 207, 'purchase_returns', 'invoiceAjax'),
	(553, 'c65b98dba6953cfb879a5084a56dd800', 221, 'credit_memos', 'edit'),
	(554, 'f6ff36378d9175dc7efcb8012a460607', 221, 'credit_memos', 'editDetail'),
	(561, '244726552ea80cdff683d2ec4ead8373', 222, 'purchase_returns', 'edit'),
	(562, '48b46d6f6737b152d740ad9fa9150bf7', 222, 'purchase_returns', 'editDetail'),
	(573, 'ff13318fc282678f0860c8273a025a4c', 223, 'purchase_returns', 'void'),
	(574, '2406813275deb51e0910cab1cd4556c1', 399, 'purchase_returns', 'receive'),
	(575, '92805e7ee8cbaac20aea4054105a3d40', 396, 'credit_memos', 'receive'),
	(576, '1b12cbb183476eeefc27c4690af4ff4d', 224, 'ar_agings', 'index'),
	(577, '53fdc48249ab558685f7ef7e81f1d734', 224, 'ar_agings', 'ajax'),
	(578, 'b128f28a6881c41d1a5c58aa44cb18fe', 224, 'ar_agings', 'save'),
	(579, '9232932d6954ffd92ede2d98ac9a7dd3', 225, 'ap_agings', 'index'),
	(580, 'a1114446d371dcf388bd170a87be5678', 225, 'ap_agings', 'ajax'),
	(581, '33dfef46f29f0eb2bc95e2bd6a6f6080', 225, 'ap_agings', 'save'),
	(583, '9b24e68dd7627b716d3d1c6ddc289312', 220, 'credit_memos', 'void'),
	(584, '47c893e5a14b83b7d55cbcbc1dcd2d80', 226, 'point_of_sales', 'service'),
	(586, '87f3c59d60d8d8d4ce09e101c2058dec', 399, 'purchase_returns', 'getProductDeliveryByLocation'),
	(634, '33830d41d98b64e8c00d8b5a9422dc19', 1, 'general_ledgers', 'indexByTbDateRange'),
	(635, '4270e414bc6fdd4cef2c28ae185205cb', 1, 'general_ledgers', 'ajaxByTbDateRange'),
	(636, '6635dc1b1317b165650c33234ff1e44e', 1, 'general_ledgers', 'indexByGroupDateRange'),
	(637, '7ca94c4bbfb7873c2814a4b7737ad3b5', 1, 'general_ledgers', 'ajaxByGroupDateRange'),
	(638, '2d53be6332584194d646d86341c79038', 228, 'reports', 'posByType'),
	(639, 'ef8d98714bd71cc426ca655d7774e731', 228, 'reports', 'posByTypeResult'),
	(640, 'c1e8c1caafff0dc6dfdb980142ba8d9c', 228, 'reports', 'posByTypeDetailResult'),
	(641, 'bd023209d2338a94e249cb1326cd9c95', 204, 'credit_memos', 'applyToInvoice'),
	(642, '216f8639042caa6ed6048f81971a97f3', 207, 'purchase_returns', 'applyToPO'),
	(643, '25134d7ca00df140abd4a2d14762a7c7', 229, 'reports', 'customerInvoiceByRep'),
	(644, '99955379b4f5bff9070404b04ae57b28', 229, 'reports', 'customerInvoiceByRepResult'),
	(645, '5ac7ec11c28b25487c7d40d79f8fce30', 229, 'reports', 'customerInvoiceByRepAjax'),
	(646, '2f793bd181f1cfef090c828245a5b881', 230, 'reports', 'customerReceivePayment'),
	(647, '3ea99090473588ef2067af5cd4a8cd80', 230, 'reports', 'customerReceivePaymentResult'),
	(648, '62b7bd78433c05b6415fee0e17a23876', 230, 'reports', 'customerReceivePaymentAjax'),
	(649, '66f0f929f234c100110d16ddcdc4641e', 231, 'reports', 'customerReceivePaymentByRep'),
	(650, '39df4e6fa543b54539c7f348c6127707', 231, 'reports', 'customerReceivePaymentByRepResult'),
	(651, '7386648e69ee2553b189b88299c9146e', 231, 'reports', 'customerReceivePaymentByRepAjax'),
	(652, 'fdb712c790902abdcdfd2d62d21def7c', 232, 'reports', 'vendorPayBill'),
	(653, '813adfe7cc97ea8163731980a0aca3d5', 232, 'reports', 'vendorPayBillResult'),
	(654, '12ad9094587a5a2103fb7b75f796ffa1', 232, 'reports', 'vendorPayBillAjax'),
	(655, '93261ce6bedefa607d8daa68b87c79ac', 233, 'others', 'index'),
	(656, 'c47f7614b0b8ca2befe84c26244534e2', 233, 'others', 'ajax'),
	(657, 'df6cdc2f407240291f54051cb33963fa', 233, 'others', 'view'),
	(658, '4d0477afebee3def70ad7438c9b9fce4', 234, 'others', 'add'),
	(659, '200609e259d1c2390f42b84945048899', 235, 'others', 'edit'),
	(660, '59303df7f9f66df1a5209a6b3a42d92d', 236, 'others', 'delete'),
	(661, '17cd0366516b6d7dd0efa4fb99871d8f', 237, 'reports', 'depositDetail'),
	(662, 'decbfc3e2a67f6a34117667329987209', 237, 'reports', 'depositDetailResult'),
	(663, 'db748e83642478cb6426171e9012d421', 237, 'reports', 'depositDetailAjax'),
	(664, '4333cd204bc56f2da5dc05e91c056f22', 242, 'reports', 'pos'),
	(665, 'd8089de9fce427b5ee250b1e7cab0026', 242, 'reports', 'posResult'),
	(666, 'f68dc18c817e3f13e92454c30cb1366e', 242, 'reports', 'posAjax'),
	(667, '3dedf79dbd33451b6dbab05aad1872ea', 126, 'transfer_receives', 'receiveAll'),
	(668, 'adaca18bac60d75042037c72eee19a61', 238, 'reports', 'posByItem'),
	(669, '656ca0624f0b3c1aa8ac27c0dd236a78', 238, 'reports', 'posByItemResult'),
	(670, 'd779c2417f03eeb90a9c075943e21837', 239, 'reports', 'salesByItem'),
	(671, '17951f975a94fda27fab92d408c71e4e', 239, 'reports', 'salesByItemResult'),
	(672, '4f36ff08ae5b38db7bfc0ae2093d136b', 240, 'reports', 'salesByCustomer'),
	(673, '5edf1080d6ce7d4bb10926ecb5390d91', 240, 'reports', 'salesByCustomerResult'),
	(674, '05c0942c266b8d9fff4ad70b6da62a62', 241, 'reports', 'salesByRep'),
	(675, 'e6522102e848f3f6787f5fe09799962a', 241, 'reports', 'salesByRepResult'),
	(676, '868ed657c9d1b51f0e4e531b4183e928', 243, 'reports', 'transferOrder'),
	(677, '6e1c199bcbd66d9e4a7251a80438e48b', 243, 'reports', 'transferOrderResult'),
	(678, 'f6d08d1c5e3bf45f98d54f3a93d08116', 243, 'reports', 'transferOrderAjax'),
	(679, 'ed632606b9ab8c3381a09f53224bed51', 243, 'reports', 'printReceipt'),
	(680, '165fa53ba99ba758fa0819cb9c2c3337', 91, 'purchase_orders', 'printInvoiceProduct'),
	(682, 'd1ca278ba421866f6ae974ccd7eda4a4', 238, 'reports', 'posByItemDetailResult'),
	(683, 'a18413d919b0d10390cb12ea92fcc217', 239, 'reports', 'salesByItemDetailResult'),
	(684, 'f337b7bce0bc5faebdc84306ced0629d', 240, 'reports', 'salesByCustomerDetailResult'),
	(685, '4ff663d47c90dcb376c5ca99a2fce573', 241, 'reports', 'salesByRepDetailResult'),
	(686, '1fc01d5a1056031c1f24b16559a47821', 145, 'purchase_orders', 'voidReceipt'),
	(687, '5e84b4b92dc19cd78ed9ae22b3f661c0', 204, 'credit_memos', 'voidReceipt'),
	(688, 'badc46b9241e613619febc5b4ce4fd51', 207, 'purchase_returns', 'voidReceipt'),
	(690, '7382bd6937f881fa273492f8e7c90ed9', 244, 'reports', 'salesByType'),
	(691, '34d3b68b2519f30c2fbc661932cb82a1', 244, 'reports', 'salesByTypeResult'),
	(692, '62fb9b9f1e2cfd3028088484241d1b8c', 244, 'reports', 'salesByTypeDetailResult'),
	(693, '1ae4f35309109668c9e695f4354ea20d', 197, 'reports', 'productByTypeResult'),
	(694, '06829c137c7d52cbbce790a4611eac1d', 197, 'reports', 'productByTypeAjax'),
	(695, '060bffcd13dbb0221bb2e418940b91cb', 245, 'reports', 'customerAddress'),
	(696, '2d8881692a380ed792867f2a4762d1b8', 245, 'reports', 'customerAddressResult'),
	(697, '5f615bdd3ec3b761314d58a8324b03f2', 245, 'reports', 'customerAddressAjax'),
	(698, 'f9010efbe31b11b1d075726cd6448d3c', 114, 'general_ledgers', 'customer'),
	(699, '99348097e64a2528bb00065834c19bbe', 114, 'general_ledgers', 'customerAjax'),
	(700, 'c43c4a3197a8e7dbd81dc8358ea8761d', 114, 'general_ledgers', 'vendor'),
	(701, '91286d0539118dd7bc2a0c2ac0e4fcdd', 114, 'general_ledgers', 'vendorAjax'),
	(702, 'fb8c21acb99e28291437929bb9dc8092', 114, 'general_ledgers', 'other'),
	(703, 'b899feb1bfe95c86bdd7face0cd3d4a8', 114, 'general_ledgers', 'otherAjax'),
	(704, '64ec4058d2a7375d8124c852e0fd9843', 115, 'general_ledgers', 'customer'),
	(705, '4d334bcd80186007e11a788c5f058d03', 115, 'general_ledgers', 'customerAjax'),
	(706, '18716421a7301908066a8f58175d6008', 115, 'general_ledgers', 'vendor'),
	(707, '9437a13847d56198eb4f20822db8354d', 115, 'general_ledgers', 'vendorAjax'),
	(708, '87206d2bc2599eb07598a4023a6548b1', 115, 'general_ledgers', 'other'),
	(709, 'a72572dc028beede491e613815b611fa', 115, 'general_ledgers', 'otherAjax'),
	(710, '609b332b8b3a0dc8532fb2ad7c4ce9a9', 177, 'general_ledgers', 'customer'),
	(711, '2cdc3aa20e4a7458dcde649e3565dcc0', 177, 'general_ledgers', 'customerAjax'),
	(712, '5b7fb581a41e0a61ca130b499984c92b', 177, 'general_ledgers', 'vendor'),
	(713, '20f787cf791c58baad7b7063b4aa07a4', 177, 'general_ledgers', 'vendorAjax'),
	(714, 'b00eed436225439039db9a224563955e', 177, 'general_ledgers', 'other'),
	(715, 'dfb620a16bf48bbc94ab974346a8bea3', 177, 'general_ledgers', 'otherAjax'),
	(716, 'f3af063aface72ccf42ee54ef2503a48', 188, 'general_ledgers', 'customer'),
	(717, '8306fe0a5e0f92a4f48ece0593935fba', 188, 'general_ledgers', 'customerAjax'),
	(718, '2956f00115c11df0b90ecdde68e3a48e', 188, 'general_ledgers', 'vendor'),
	(719, 'ecdd93cbecf8b7ca081ec51ba4fb8efc', 188, 'general_ledgers', 'vendorAjax'),
	(720, '8ca8b0f46882382d92a151fbdc80cc3f', 188, 'general_ledgers', 'other'),
	(721, 'c7c510b393dec82cd1015a52e2de1010', 188, 'general_ledgers', 'otherAjax'),
	(722, 'a55964b624af7042ea22f2418dcc3b19', 187, 'general_ledgers', 'customer'),
	(723, 'a2e1d40281d94a482e77fe92c018f68d', 187, 'general_ledgers', 'customerAjax'),
	(724, 'ff1276eb82ceca920743b0f634655ea1', 187, 'general_ledgers', 'vendor'),
	(725, '309bd2ef9e65c1fd9aa420953d1e7907', 187, 'general_ledgers', 'vendorAjax'),
	(726, 'd25499a17d8bc5518acb9a7faf0ed1c5', 187, 'general_ledgers', 'other'),
	(727, 'cf552758a0bbf2883dcf8fcb7122fae6', 187, 'general_ledgers', 'otherAjax'),
	(728, '4505163b0020485f3fd5816d721269f1', 189, 'general_ledgers', 'customer'),
	(729, '602a15a5bb5474a808952eaf9b07961f', 189, 'general_ledgers', 'customerAjax'),
	(730, 'fc3016fd8e6c1e186a8bef26ebbc7f51', 189, 'general_ledgers', 'vendor'),
	(731, '4fdc4f7f739f5b7a78199ba1dc5e9d4d', 189, 'general_ledgers', 'vendorAjax'),
	(732, 'd4fa85d06aa6862a964c112ecd9a9242', 189, 'general_ledgers', 'other'),
	(733, 'f1fce7eef34dc7a566731227e4f4f359', 189, 'general_ledgers', 'otherAjax'),
	(734, '9e267ffdd976159ad380b3f54039318a', 260, 'purchase_requests', 'service'),
	(736, 'c79823bd6472d278df8a0be4912d3946', 246, 'inv_adjs', 'index'),
	(737, 'bf1f9e82c71f03de30d74ecbd0810090', 246, 'inv_adjs', 'ajax'),
	(738, '5ef57205e31e8015c7b4cfcb50af6e5b', 55, 'inv_adjs', 'save'),
	(739, 'eeb799e2ea2e349302abcfe274533903', 282, 'employees', 'index'),
	(740, '5de951e783665947bd00824db2bbf92b', 282, 'employees', 'ajax'),
	(741, '91f49f9bc9995e6420e2141c9762b04b', 282, 'employees', 'view'),
	(742, 'a24274db2282f92317f0dbcc8710b577', 282, 'employees', 'add'),
	(743, '7cd73e9b061866161301b2f729024560', 282, 'employees', 'edit'),
	(744, 'e65aeee4966bf554a9b5a8b5bfa2c2de', 282, 'employees', 'delete'),
	(745, '29a581a1030b25a6b1d0452e96a8a1bf', 114, 'general_ledgers', 'employee'),
	(746, '91f06c1edd185006309eb331d73deacb', 114, 'general_ledgers', 'employeeAjax'),
	(747, '31b6d5ffeba397568491312ec8c7f7de', 115, 'general_ledgers', 'employee'),
	(748, 'fc723a92a02a406f1609e1e6d5156ad0', 115, 'general_ledgers', 'employeeAjax'),
	(749, '943c9850f427316d954614833a3663fe', 177, 'general_ledgers', 'employee'),
	(750, 'bdb00ac9fb49f3583fd9dc2244ae70d0', 177, 'general_ledgers', 'employeeAjax'),
	(751, '75e22224589d8573b38cba015382861d', 188, 'general_ledgers', 'employee'),
	(752, '5e789553cac4bd4dd327ee80c8e2eb5f', 188, 'general_ledgers', 'employeeAjax'),
	(753, '21f960358610d66bbdf498f43eb08d97', 187, 'general_ledgers', 'employee'),
	(754, '2e44df3c44e37deff047608bd008b6e5', 187, 'general_ledgers', 'employeeAjax'),
	(755, '1722759ca9df23104309bbd1b46b7267', 189, 'general_ledgers', 'employee'),
	(756, 'b831bafa0342256dd3e4a38586cf2b4d', 189, 'general_ledgers', 'employeeAjax'),
	(757, 'ebe16e23be88a422e2778e582df4066f', 251, 'reports', 'inventoryValuation'),
	(758, '14d37205826f8fc2541edd40d8553ca8', 251, 'reports', 'inventoryValuationResult'),
	(759, 'f146ef573d392bd65b034c7ba7870da0', 251, 'reports', 'inventoryValuationDetailResult'),
	(760, 'a9cecd52158913e00bfec5529ad98785', 246, 'inv_adjs', 'uom'),
	(762, '64af61ae3172fdffafa522c4fec89ffe', 238, 'reports', 'posByItemParentResult'),
	(763, '79d8111448a20542bf403101529fce70', 228, 'reports', 'posByTypeParentResult'),
	(764, 'bd2fa657986af6b2c39d48d4d488babc', 239, 'reports', 'salesByItemParentResult'),
	(765, '70e60975de7f14bf47f0744f52951662', 244, 'reports', 'salesByTypeParentResult'),
	(768, '0190acd56233566a5b22828871d1ca2c', 253, 'purchase_requests', 'index'),
	(769, '4d50598a5d7e9083da17285a1343b12a', 253, 'purchase_requests', 'ajax'),
	(770, 'f1e2548bf8fbd950120e981bfd172635', 253, 'purchase_requests', 'view'),
	(771, 'e8648ba1bab685ecaccd59b8e16f2ad5', 254, 'purchase_requests', 'add'),
	(772, 'f1f393485afa371b56ca61186ead2dec', 253, 'purchase_requests', 'product'),
	(773, '3f990e35ab848cb4b4872142c77802c2', 253, 'purchase_requests', 'productAjax'),
	(774, '2ce888748f690b74428c979308be9655', 253, 'purchase_requests', 'vendor'),
	(775, '288859cc36bbb700bd4ba53f4b5b106d', 253, 'purchase_requests', 'vendorAjax'),
	(776, '231919cc05132fab0f8995392158daed', 255, 'purchase_requests', 'edit'),
	(777, '61668a4a4b01b1ff0622d69ed0b01e89', 256, 'purchase_requests', 'delete'),
	(778, 'dcdddc8d00cb40fe2097275562fca472', 253, 'purchase_requests', 'printInvoice'),
	(787, 'ac6d3dde7af47697f2e125809d8893a3', 253, 'purchase_requests', 'purchaseRequestView'),
	(789, 'b15ece91e778f3c3c2ae79bb1f69844b', 246, 'inv_adjs', 'printInvoice'),
	(790, '1c5ae50c573ab944318fc5e44538125a', 257, 'reports', 'inventoryActivity'),
	(791, 'd2b418eadcc34e335c3230782d70ee5f', 257, 'reports', 'inventoryActivityResult'),
	(792, '93fba7d3de99b5c12f16b7358842a762', 258, 'reports', 'inventoryAdjustment'),
	(793, 'b636b121c134aa86ce2eef1ff61d8434', 258, 'reports', 'inventoryAdjustmentResult'),
	(794, 'a0fb1519b004e2a34411507259e3ebc6', 258, 'reports', 'inventoryAdjustmentAjax'),
	(796, '3ca3a51bf7a2a22a55448f29aaab94da', 55, 'inv_adjs', 'add'),
	(797, '7aadde8d72d809b0c5364f16ed40ebf0', 55, 'inv_adjs', 'addDetail'),
	(798, '2c2f70771a7862f153ba3a6ae4dd7619', 88, 'inv_adjs', 'edit'),
	(799, '89350a438e7faa9fe232cea34b265d41', 88, 'inv_adjs', 'editDetail'),
	(800, 'b1751bd1a7095dd4acd113aba7cb6e3a', 322, 'inv_adjs', 'delete'),
	(801, 'e6db2f03e02e11f62aeeeb097f29d13e', 323, 'inv_adjs', 'approve'),
	(802, '246a683621947356d3839cf9e622087f', 88, 'inv_adjs', 'saveEdit'),
	(803, '5d3469de2c88b5bfbaf12af0016684d8', 259, 'credit_memos', 'discount'),
	(804, 'e7de17d5fff2d8a99492ef8df817b4a4', 224, 'ar_agings', 'printInvoice'),
	(805, '082cf319ea05cc2bdd444508ccb87877', 225, 'ap_agings', 'printInvoice'),
	(806, '4bd841ab355efedd33b33e1e2bfb16a6', 263, 'general_ledgers', 'status'),
	(807, '94a09f52c7a1a8fb4d78ab86aa6d9cf9', 113, 'general_ledgers', 'printJournal'),
	(808, 'c0794649dab9c98fc910518ce5c5f842', 113, 'general_ledgers', 'printCheck'),
	(809, '0157da938d6178fdcd60b4117fb671da', 113, 'general_ledgers', 'printDeposit'),
	(821, '22a48974c0006781dca775a354139ca9', 270, 'reconciles', 'index'),
	(822, '9fea4e0639b9a23ba17305ddb34df4b8', 270, 'reconciles', 'ajax'),
	(823, 'c9c4ab974c9ebf12ccfbee86d49b4bd4', 270, 'reconciles', 'save'),
	(824, '2fe38f8d8719fe7e721ec41f7b6da59d', 186, 'general_ledgers', 'saveNote'),
	(825, '5aded88170a373384cdfb2d0cc2ab5b1', 271, 'egroups', 'index'),
	(826, '5b6496cdce4a814bbefdba86d66279d9', 271, 'egroups', 'ajax'),
	(827, '8a9fed8482a5c560a4ba9073aef550ba', 271, 'egroups', 'view'),
	(828, 'ddf2178f8fec3a06b35cfdbc884c04cd', 272, 'egroups', 'add'),
	(829, '1b537357aefd8902ea69bd6b89c024c5', 272, 'egroups', 'employee'),
	(830, 'd66cfd2f50854dc07599f3e4634aecd8', 272, 'egroups', 'employee_ajax'),
	(831, 'cdd46128b6f3cc52ab9b3d0848fb814d', 273, 'egroups', 'edit'),
	(832, '41b3454238663200b18fed7a98b18f70', 273, 'egroups', 'employee'),
	(833, '2c87ef537b3ea8117329fa52e781a6e9', 273, 'egroups', 'employee_ajax'),
	(834, 'a3420c227dce7666fcf357dd7893a66b', 274, 'egroups', 'delete'),
	(836, '27ed3eb6dbd39ca1f209522f6bb1ec35', 267, 'reports', 'deliveryResult'),
	(837, '688f23a2186c69ba2f3f8db03b885c98', 267, 'reports', 'deliveryAjax'),
	(838, '2681480281f053ad41736c3f53c8448f', 265, 'reports', 'invoice'),
	(839, 'be8a0a3a1d080b2f49898c6e9f1000f0', 265, 'reports', 'invoiceResult'),
	(840, '241fddb879288c7a96393f243fe883a3', 265, 'reports', 'invoiceAjax'),
	(841, 'bcc945a94aa750f89a626da58f977f02', 266, 'reports', 'openInvoiceByRep'),
	(842, '7f073473a48a87c70a6205017c28f957', 266, 'reports', 'openInvoiceByRepResult'),
	(843, 'a09ca693d4c0c252d1f4ca114935d76d', 264, 'reports', 'transferByItem'),
	(844, '4d96ccf286e14b665151a4c65ed75484', 264, 'reports', 'transferByItemResult'),
	(845, 'eff00aeaf119319bb0723e31dac711a6', 264, 'reports', 'transferByItemParentResult'),
	(846, 'f128d2ae3baa14a1f78190c869e6fa4e', 264, 'reports', 'transferByItemDetailResult'),
	(847, '838f28a0fbca652b2d6a89225c9e6fdd', 268, 'reports', 'purchaseByItem'),
	(848, '6f6cdd4b55f866c29d80fc2ba2638093', 268, 'reports', 'purchaseByItemResult'),
	(849, '631f8b9e9a72375004fcd825163b25de', 268, 'reports', 'purchaseByItemParentResult'),
	(850, '8f0a7e3633f32acff560cd629bb2d853', 268, 'reports', 'purchaseByItemDetailResult'),
	(851, '4cfcd2275f6f5a798d5a7e655d4dd187', 269, 'reports', 'checkDetail'),
	(852, '91e0a143aebdb4e1be52536556c4be79', 269, 'reports', 'checkDetailResult'),
	(853, '9f54981246b294854aba7f97c98e2f56', 269, 'reports', 'checkDetailAjax'),
	(857, '8239ed4fa1d8fd8c45b99cd23b725f39', 204, 'credit_memos', 'deleteCmWSlae'),
	(858, 'c14be7c41984950a90578c22f1c7cfe3', 204, 'credit_memos', 'deleteSlae'),
	(859, '0f65b2687dbdba5d6c409299c4e3a1a3', 133, 'reports', 'inventoryAdjustmentByItem'),
	(860, '8a5dc4150405037a9a7c6158d0c6b2ab', 133, 'reports', 'inventoryAdjustmentByItemResult'),
	(861, '307eca6e4487307f6fa349572d6a049b', 133, 'reports', 'inventoryAdjustmentByItemParentResult'),
	(862, '6d3799b3fadcf2664e52dcf106ff7dbe', 133, 'reports', 'inventoryAdjustmentByItemDetailResult'),
	(863, 'c3e7b3c096653f4db78a11ebd1a3411a', 276, 'fixed_assets', 'index'),
	(864, '558207588145cb435ab46f606124d342', 276, 'fixed_assets', 'ajax'),
	(865, 'dc8814da87ece55eacd2e251bf6d4d85', 276, 'fixed_assets', 'view'),
	(866, '531d4c81b5e7786c752425bcb803a9ee', 276, 'fixed_assets', 'add'),
	(867, '6606a0496012da77692b3be778d51f62', 276, 'fixed_assets', 'edit'),
	(868, 'dd60d01e2d9f8e944b7f21611535f189', 276, 'fixed_assets', 'delete'),
	(869, 'd85b6ab4ef73f9f1647119311d625011', 276, 'fixed_assets', 'post'),
	(870, '1293eed6a7c546391f457817c520d5b7', 276, 'fixed_assets', 'postDetail'),
	(871, 'dfc15336fc9b195d0e2ae85997d97e56', 276, 'fixed_assets', 'save'),
	(872, 'c02383c49c58866f1d1231662b52732d', 253, 'purchase_requests', 'searchProduct'),
	(873, 'b5c081400eac0f5b0138c73429c872ae', 253, 'purchase_requests', 'searchProductCode'),
	(874, '47e8ec536d73f4e685aae6ccfa65b3d6', 257, 'reports', 'inventoryActivityParentResult'),
	(875, '302fe7d4d173d2abc0aa8d25a7f754ef', 257, 'reports', 'inventoryActivityDetailResult'),
	(876, '415a2edafe379deef4599d51676f9e4d', 257, 'reports', 'inventoryActivityWithGlobalResult'),
	(877, 'e49602a0e7cc87c06ed71d69d27be715', 257, 'reports', 'inventoryActivityWithGlobalDetailResult'),
	(878, '9af0c82f571239509e398dc89ce1b902', 270, 'reports', 'reconcile'),
	(879, 'b6eb1c156ae36f60e28a0e4bf267ffba', 270, 'reports', 'reconcileResult'),
	(880, '34f62ec4906782ad2ad7c322b49e0495', 270, 'reports', 'getStatementEndingDate'),
	(881, 'e611dc58d83d1fefcd996f7d9eccb377', 117, 'reports', 'ledger'),
	(882, '8e406ad961a70c20e5a8aae92fae30b5', 117, 'reports', 'ledgerResult'),
	(883, 'd8798e4ea11ace99b1dfd50cf9d4a3ca', 117, 'reports', 'ledgerAjax'),
	(884, 'e61e168a7084aaa8e53f6f701c902d8e', 224, 'ar_aging_employees', 'index'),
	(885, 'f4d2b81483013eb8b23011b1afad7417', 224, 'ar_aging_employees', 'ajax'),
	(886, 'd6a818dccafec6e2b8e13e7ccdc9b327', 224, 'ar_aging_employees', 'save'),
	(887, '1c7dcfab532c1db7e2cd551ac043b9dd', 224, 'ar_aging_employees', 'printInvoice'),
	(888, '9daa240c056f6abb8baacebf0a311578', 277, 'reports', 'accountReceivableEmployee'),
	(889, '68ff80ab842e05629cc4fb51a0af8b3b', 277, 'reports', 'accountReceivableEmployeeResult'),
	(890, '7355c973638c41bb57463b63b63ad6a0', 278, 'reports', 'employeeBalance'),
	(891, 'b6273b25b59e60436e14d26ce13ba90e', 278, 'reports', 'employeeBalanceResult'),
	(892, '66ca21f72d3af564dfe650f2d778f915', 278, 'reports', 'employeeBalanceAjax'),
	(895, '3ebb3e0c72ac7d835452055d0c84eba6', 46, 'products', 'getSkuUom'),
	(897, 'f4c6f7b87e7d8ffa78e145a42a03f73b', 253, 'purchase_requests', 'searchVendor'),
	(898, 'a26725c44004d76b688ac2ad04b67030', 136, 'products', 'productPrice'),
	(906, '2127b8410a1be313d6841e778c71b041', 399, 'purchase_returns', 'pickProduct'),
	(907, '49200d079d68aa20072542925f05bf52', 399, 'purchase_returns', 'pickProductAjax'),
	(908, '7ca7fef78c4f41effbcbef6aebdba5e1', 399, 'purchase_returns', 'pickProductSave'),
	(909, 'e7e283f2c2dd5307cf35d1ac17782b98', 126, 'transfer_receives', 'addReceiveCrontab'),
	(912, 'ec04a33d57ef28370e948096aac5fba1', 46, 'products', 'checkSkuUomEdit'),
	(913, 'd94d8f91a0f8719e41265b4a2b8dac3a', 46, 'products', 'checkPucEdit'),
	(914, '1e850227fe9f1c3a259ff94ce31a8dc2', 46, 'products', 'checkPuc'),
	(916, 'fd34cbea045feeb6d97bde5d7847f4f6', 46, 'products', 'checkSkuUom'),
	(917, 'b24510fcb1d976e9074bec11f68bd717', 46, 'products', 'checkSkuUomEdit'),
	(918, 'dccb5aab737f2d85a9850047fc1cc0b4', 46, 'products', 'cropPhoto'),
	(919, '522ccf0698d5ef49c0665850e1936232', 46, 'products', 'upload'),
	(920, '1efcebbb9a7fa1b27a0a3648ac9eac36', 46, 'products', 'product'),
	(921, 'e85ca49bd9918eb994007a0750c42d85', 46, 'products', 'productAjax'),
	(922, '9807319bedb43280c98142cfb96414cc', 207, 'purchase_returns', 'applyToInvoice'),
	(923, '4688cdae2634fab185bf4507e3f18ed4', 280, 'point_of_sales', 'productDiscount'),
	(924, '60f570148d2b08ba270bc2e3da2c9171', 207, 'purchase_returns', 'deletePbcWPo'),
	(928, 'a28cbbf01ff92b4f2f26fa571ea008f6', 643, 'deliveries', 'pickProduct'),
	(929, '19b6fd3bbd139ac2e455c85db41af859', 643, 'deliveries', 'pickProductAjax'),
	(930, 'a6b3d35b245de52501b1c92f13a47588', 643, 'deliveries', 'pickProductSave'),
	(933, '23738b5df2e850259d0b7d5681720737', 642, 'deliveries', 'index'),
	(934, 'fc6d9cd7492abe41d631c555348c8deb', 642, 'deliveries', 'ajax'),
	(947, '990db34f19a6f7e87f945fff380745a8', 267, 'reports', 'delivery'),
	(948, '191e927196675e185ed6456a0af9e1cc', 286, 'vgroups', 'index'),
	(949, 'a081c00dd40baa4dfce8e4675cbae5e7', 286, 'vgroups', 'ajax'),
	(950, '779203336e4b5c9a02977b0f17b92a30', 286, 'vgroups', 'view'),
	(951, '4a139ff21f718ec53a810abb26e33c62', 286, 'vgroups', 'vendor'),
	(952, '4d2dcb7fe2d351f0b31bd88da62195cd', 286, 'vgroups', 'vendorAjax'),
	(953, '7377a8a124c4a10fec77b2e6305725c5', 287, 'vgroups', 'add'),
	(954, 'c52c1d2b06d456e9b1b770009dd6af27', 288, 'vgroups', 'edit'),
	(955, '72b94905cb831476545b974e24cb3821', 289, 'vgroups', 'delete'),
	(957, 'a61d8aa62f29d43f021d3e520f3f0d6e', 291, 'credit_memos', 'editUnitPrice'),
	(958, '92ee3bc39aab4264c38bbece77216595', 282, 'employees', 'searchEmployee'),
	(959, '9740d5145c1ab5e3427aa9277d049b97', 282, 'price_types', 'index'),
	(960, '88b24e26e160414ace2c3310af7e6d91', 282, 'price_types', 'ajax'),
	(961, '6f9ac197d8316f190190641146476986', 282, 'price_types', 'add'),
	(962, '91855af756d8b6bc7eab463533a2cefe', 282, 'price_types', 'edit'),
	(963, '5867b189590dd7eb4a4999ec98496649', 282, 'price_types', 'delete'),
	(964, '47b51d40f667e1b7da64ea908c0d7007', 296, 'products', 'setProductPacket'),
	(965, 'aaf303407ca4b739454ec57a81804beb', 597, 'uoms', 'exportExcel'),
	(966, 'a3096e36aedc49347cef0a93c65c1cf4', 597, 'uom_conversions', 'exportExcel'),
	(967, 'bceef41e2823c44f5831310ae0a3fe67', 597, 'locations', 'exportExcel'),
	(968, '24af9ede515418abb3be02084e470237', 282, 'employees', 'exportExcel'),
	(969, 'a3a289d8da173e4692fdd075df76e21d', 302, 'egroups', 'exportExcel'),
	(970, 'e2cc815d7bc4981d5279371eb142114c', 303, 'payment_terms', 'exportExcel'),
	(971, '7ee7fcd1600613a14d0e27c58e497dc4', 304, 'others', 'exportExcel'),
	(972, '5afd71510d80094a476c60f03baebc43', 305, 'classes', 'exportExcel'),
	(974, 'f6c74f72a312d797c9d9cd61b6a7dd75', 307, 'cgroups', 'exportExcel'),
	(975, '14f1a458e51cc7560acbfa68a92bca24', 308, 'vgroups', 'exportExcel'),
	(976, 'd1e28ad3e83d5ac3007339d5d7a84d98', 309, 'vendors', 'exportExcel'),
	(977, '039ba890b4156595c903db0824ea16bd', 310, 'sections', 'exportExcel'),
	(978, 'dcb6fa1e692c60ce9f6417816ce7523d', 311, 'services', 'exportExcel'),
	(979, '816b0013120f15a6df140dbfcc9e7434', 597, 'pgroups', 'exportExcel'),
	(980, 'a1f5412c2c1943c932acb879b1e7f1cc', 313, 'products', 'exportExcel'),
	(981, '3f91b87c83fe14fa84eda99523541dee', 314, 'chart_account_groups', 'exportExcel'),
	(982, 'c5ae34f085c26f0b8269ae65e269c33f', 315, 'credit_memos', 'viewByUser'),
	(984, '78cddbee7922f8e360c107d7d2c1040a', 317, 'purchase_orders', 'viewByUser'),
	(985, 'acfd52346292035176e7a1b816b1fb80', 318, 'purchase_returns', 'viewByUser'),
	(987, 'c8dbec22f8cb7c36de6f6ce6e3577e9a', 320, 'transfer_orders', 'viewByUser'),
	(988, 'da9b2c7438c85d132d2e76c5b4f22e1f', 321, 'inv_adjs', 'viewByUser'),
	(1007, '0c4f9601605deeeb66e2de640b55d355', 328, 'point_of_sales', 'reprintReceiptSm'),
	(1008, 'f5e840cdf76cc27593b21d96a70c5a57', 149, 'point_of_sales', 'viewPosDaily'),
	(1009, '9d3b1a66a420ec7c7880b11336572805', 149, 'point_of_sales', 'customer'),
	(1010, '1a9d9fff39c40aabc125ae45c3bae7d1', 149, 'point_of_sales', 'customerAjax'),
	(1011, '709c32d812bba4388ad2104db946a75b', 149, 'point_of_sales', 'discount'),
	(1012, 'c36d756a738c0fb32ebc03667d31d3a9', 224, 'ar_aging_employees', 'employee'),
	(1013, 'f5c2c895ffc2769b9c253572ec3bc7c4', 224, 'ar_aging_employees', 'employeeAjax'),
	(1019, 'a227d7a724d2cc6618ae2c873f8a1a54', 334, 'shipments', 'index'),
	(1020, 'e92886f56b26044a779dc9c24e518518', 334, 'shipments', 'ajax'),
	(1021, '403c1fc7fa01b05976cea21b6d2f902b', 334, 'shipments', 'view'),
	(1022, '8b52b6b356a437ce54da7430edd06cf4', 335, 'shipments', 'add'),
	(1023, 'f2ee59506014ea774487336f5be4a198', 336, 'shipments', 'edit'),
	(1024, '4a07db22ecc1e90cd6b275db3c82afd3', 337, 'shipments', 'delete'),
	(1025, '2643822129508603e3e00f6c01979a1e', 338, 'streets', 'index'),
	(1026, 'f76cdc4dbb4cb1ac0f2a595d84e515e3', 338, 'streets', 'ajax'),
	(1027, '6522eb9d444a20362034548a58a32c3e', 338, 'streets', 'view'),
	(1028, '07445b90abb1508a3ea7f78efc514a69', 339, 'streets', 'add'),
	(1029, 'ad309328c681c4435a7c115cae60c655', 340, 'streets', 'edit'),
	(1030, '562b8177e6d522bd45a03e318f35f2d1', 341, 'streets', 'delete'),
	(1031, '1ab384bdc5b4cbf8dbb9e243c2c3672a', 342, 'reasons', 'index'),
	(1032, 'ed14a911e7a5803059485278d25625d5', 342, 'reasons', 'ajax'),
	(1033, '9fe40cf856e47fdea41ace42fcf73d4c', 342, 'reasons', 'view'),
	(1034, '89cb8d00580c6882f374b24a57849535', 343, 'reasons', 'add'),
	(1035, 'afb6bab49ba16670744a82315586d533', 344, 'reasons', 'edit'),
	(1036, '921ca7c7dd7e892b6534c333dac9b674', 345, 'reasons', 'delete'),
	(1037, '3a459f05561dbebe657d4e018bd44181', 346, 'places', 'index'),
	(1038, '71d570bc9b22ea2331abd3634cdc869f', 346, 'places', 'ajax'),
	(1039, 'a94ede44b215aa0ad24d285d7c09c84d', 346, 'places', 'view'),
	(1040, '67b0bd83e0ab440c4002e7efbd66c43b', 347, 'places', 'add'),
	(1041, 'c2aece80fe8a9848dde0cb842553dbf2', 348, 'places', 'edit'),
	(1042, 'f14f6c1616dbebc6d6495de3219ba6a9', 349, 'places', 'delete'),
	(1043, '31fc6b8ee860d11c1501adc3499e92c5', 350, 'positions', 'index'),
	(1044, '2956eb59d9c93f0d253fd62db73ad97d', 350, 'positions', 'ajax'),
	(1045, 'd553cca8d8deb18a20155ee75f97014d', 350, 'positions', 'view'),
	(1046, '19b5cc07e61d29cf3e15371aa6176d55', 351, 'positions', 'add'),
	(1047, 'f7232930755f37949c1466f1fdbfbafb', 352, 'positions', 'edit'),
	(1048, 'ec0e1c93e09cac9186cb079abbc15736', 353, 'positions', 'delete'),
	(1079, 'e4b77eb49bc5e79d221d4a1304d9af61', 374, 'quotations', 'index'),
	(1080, '472ce24f68d413f2ba9776faf9a3645a', 374, 'quotations', 'ajax'),
	(1081, '04fbcf6b08dea38e373b5d609ac9e011', 374, 'quotations', 'view'),
	(1082, '3d2572b61f12d146f04fd09ee56833e4', 374, 'quotations', 'product'),
	(1083, '116d43a20a8e25c81903fda7ec4412d9', 374, 'quotations', 'productAjax'),
	(1084, '18ba4d44f553999c8cb22908724be901', 374, 'quotations', 'customer'),
	(1085, '0992f19f780f45727db85d3fd777235e', 374, 'quotations', 'customerAjax'),
	(1086, '3a968c53da867a32cfca2aa421c300f1', 374, 'quotations', 'searchCustomer'),
	(1087, 'f2b5acaee5011867dd92ad31f577c5d2', 374, 'quotations', 'searchProduct'),
	(1088, 'a2621041253471859fd12f08624a94c8', 374, 'quotations', 'printInvoice'),
	(1089, '43989fce5d35c64356e9a6fc2f7d7556', 374, 'quotations', 'searchProductByCode'),
	(1090, '0564aad3f96676c884f7b24fc70dbe62', 375, 'quotations', 'add'),
	(1091, '8eb2dc109884fb7262d75eb25bae5654', 375, 'quotations', 'orderDetails'),
	(1092, '5002dc5694bb3edd08a6e6b078472ae3', 376, 'quotations', 'edit'),
	(1093, '2668cd4b6ddc5c80343403798c75585b', 376, 'quotations', 'editDetails'),
	(1094, 'c8f368926012dd35e4661b48386a54f1', 377, 'quotations', 'delete'),
	(1095, 'e1492e0659190698b482248e069deb8d', 378, 'quotations', 'viewByUser'),
	(1096, 'dea0f13a50c234ee80d46da843bf0946', 379, 'quotations', 'open'),
	(1097, 'bdebab6dec3ceee52d5ab18d1db50b9e', 380, 'quotations', 'close'),
	(1098, '8f0320d4335bddd185340863670d67e7', 381, 'orders', 'index'),
	(1099, '2fa995faa7a0da098c8c690f5c5121e3', 381, 'orders', 'ajax'),
	(1100, 'eb73adb83333571404ab7bc021d86513', 381, 'orders', 'view'),
	(1101, '3a713b842f55ccf062083e1a60a55913', 381, 'orders', 'product'),
	(1102, '3dc7f02309d074069fc68e7052dcb721', 381, 'orders', 'productAjax'),
	(1103, '56dc490d6d03b4247e9daba3a46cfa72', 381, 'orders', 'customer'),
	(1104, 'd5a0d09164e5e5e3305abde6b378f406', 381, 'orders', 'customerAjax'),
	(1105, 'cfc9d53affab8a9418a3c835a69517e2', 381, 'orders', 'searchCustomer'),
	(1106, '4625d9eee76bd1a7c8092f8835208391', 381, 'orders', 'searchProduct'),
	(1107, '08db42d684489a539be346250b427887', 381, 'orders', 'printInvoice'),
	(1108, 'ec0300d6ecf13b84ca7b75d9374a1574', 381, 'orders', 'searchProductByCode'),
	(1109, '10f74253cd736579c44407485134f730', 382, 'orders', 'add'),
	(1110, '4d218e4880ab7a828805e310f122766c', 382, 'orders', 'orderDetails'),
	(1111, 'bca9ca756c9763d1f83e29a9417904df', 383, 'orders', 'edit'),
	(1112, 'f973e382e775ad468fd4724e2c9b6dae', 383, 'orders', 'editDetails'),
	(1113, '0db8b18c41230df919b523e2ea58f8ea', 384, 'orders', 'delete'),
	(1114, '04a73dac4119cb8dc3c3c87b2a9d9230', 385, 'orders', 'viewByUser'),
	(1115, '71549ce2d580c66107d8395f9c788491', 386, 'orders', 'open'),
	(1116, 'b41becc5e9ebae2ba345f0478d7814a2', 387, 'orders', 'close'),
	(1136, '69b72db5ad422d331fb77c631b46270c', 381, 'orders', 'getProductFromQuote'),
	(1137, '9b08267ec50a50ec10eb37495c2f8301', 246, 'inv_adjs', 'searchProductSku'),
	(1138, 'be5bf9c499c04b01bc8be74965774e67', 246, 'inv_adjs', 'searchProductPuc'),
	(1139, '2de07f885fb3646c1f2cd3b29e760d06', 246, 'inv_adjs', 'product'),
	(1140, '724022b94b7aa15fd282e14bdf122fe9', 246, 'inv_adjs', 'productAjax'),
	(1141, '09a9fa90136fc0a4858bdc65c0ed5321', 246, 'inv_adjs', 'getTotalQtyOnHand'),
	(1142, '79ad5ee7e7d9395d62357a526b0c8285', 246, 'inv_adjs', 'searchProduct'),
	(1143, 'c5b7ed2223caa3393d4477a616c4a3b8', 388, 'request_stocks', 'index'),
	(1144, '60fa9b565098a863380e5d295c8e9539', 388, 'request_stocks', 'ajax'),
	(1145, '072c17f6e27d42e5d02ad9f74bd05815', 388, 'request_stocks', 'view'),
	(1146, 'dc7bec7df9a665a854b38e82326e0b59', 388, 'request_stocks', 'searchProductCode'),
	(1147, 'fe3c1dddb9fe96cf9d24f725242c3e50', 388, 'request_stocks', 'searchProduct'),
	(1148, '1414ecf918297b98950612454a2f4ae8', 388, 'request_stocks', 'product'),
	(1149, '5cfa240c013e26df832536ac4c305867', 388, 'request_stocks', 'productAjax'),
	(1150, '97173d8b8c5d3b544585a83252e5020f', 389, 'request_stocks', 'add'),
	(1151, 'bbc203dcdd4ad73fbbca2afe860a625f', 390, 'request_stocks', 'edit'),
	(1152, '9110d02e576336fbfdfb1d155de2d844', 391, 'request_stocks', 'delete'),
	(1153, 'd35c8591ed90e1b8620f5d399b0dc4b2', 392, 'request_stocks', 'printInvoice'),
	(1154, '094744d8af40398612f8988f811585bb', 393, 'request_stocks', 'viewByUser'),
	(1155, '43a6618b540d682e5e5fc20ae878d2ea', 101, 'transfer_orders', 'printInvoice'),
	(1156, 'ffab54c33bf2efafd0f8bea5f2f2648f', 101, 'transfer_orders', 'searchProductCode'),
	(1157, 'f9bf0587b28cf927d2f9a5b6331eb2fa', 101, 'transfer_orders', 'getProductTotalQty'),
	(1158, 'bdc602502f8708a65424e0ca23062e19', 101, 'transfer_orders', 'product'),
	(1159, '0c35a857a2b7684e4cbdc172c21d6735', 101, 'transfer_orders', 'productAjax'),
	(1160, '39fe72dafe7015a0eb250561887314ad', 101, 'transfer_orders', 'requestStock'),
	(1161, 'f5e0b160cb49c6fa776857003317824c', 101, 'transfer_orders', 'requestStockAjax'),
	(1162, 'da02fe86f4f301f73b2495502c0ce402', 101, 'transfer_orders', 'getProductFromRequest'),
	(1163, '11465186d67fa7b5555c9854bc9e76a7', 101, 'transfer_orders', 'searchProduct'),
	(1164, '09887119ae274bb8a781c5eeaa5cc2b7', 101, 'transfer_orders', 'searchRequestStockCode'),
	(1165, '8bfad959a7c3009a3ef4cf8947139d80', 126, 'transfer_receives', 'printInvoice'),
	(1166, '2909c01970a763b467bf334e367edb1e', 125, 'transfer_receives', 'view'),
	(1167, 'c8ddee56d3dfcc6ff5956a0f660dbc70', 105, 'sales_orders', 'searchProductByCode'),
	(1168, '707cb0a41972ed648ef0edf1463ca41b', 105, 'sales_orders', 'searchProduct'),
	(1169, 'b4844768bed6afcef5adcc730a3e4c11', 105, 'sales_orders', 'product'),
	(1170, '23f9590325a4695cf8f0ba82ba62d63b', 105, 'sales_orders', 'product_ajax'),
	(1171, 'bcd9b6e9dec06d7c4055b210bd59b777', 105, 'sales_orders', 'customer'),
	(1172, '43d7dbeea7ee14c53ad6b3d475ebe1ea', 105, 'sales_orders', 'customer_ajax'),
	(1173, 'f462ad6846a51b40e207e3b0930d1b90', 105, 'sales_orders', 'employee'),
	(1174, '9a40a0affde12a90629b96525221bda2', 105, 'sales_orders', 'employeeAjax'),
	(1175, '3e5b3559e61419b81b69bdfd9b7b0d12', 105, 'sales_orders', 'quotation'),
	(1176, '96febf0ee1188185cfa50457bd2ae4f2', 105, 'sales_orders', 'quotationAjax'),
	(1177, '266cd1634708bf224f069e54c44fb2b5', 105, 'sales_orders', 'getProductFromQuote'),
	(1178, '07edc7ca6607762f93c48cfecdf37f69', 105, 'sales_orders', 'searchQuotationCode'),
	(1179, 'd324587b60aaf20dfbdad8585254f444', 105, 'sales_orders', 'searchQuotation'),
	(1180, '57782b4bf7a6185342b4e44a30f15314', 105, 'sales_orders', 'searchOrder'),
	(1181, '3f63d004bdb95780d9bd64fd1ac1d660', 105, 'sales_orders', 'searchOrderCode'),
	(1182, '70059a8f0b675ff5e47748556da90230', 105, 'sales_orders', 'getProductFromOrder'),
	(1183, 'bd9d3f014f619df0fb71b9508f4df6a7', 106, 'sales_orders', 'orderDetails'),
	(1184, 'd0be525966d545390d838c445d7223e9', 211, 'sales_orders', 'edit'),
	(1185, 'b5abbbbcbf78550d959a3e5f01772471', 211, 'sales_orders', 'editDetail'),
	(1186, '0ea239356e56102c1b693b0c6ffccdcc', 107, 'sales_orders', 'aging'),
	(1187, '4e79f90e390b93f2ff9b88c89433a090', 107, 'sales_orders', 'voidReceipt'),
	(1188, '44239834f3cd95fe654eeadfa95b1cde', 107, 'sales_orders', 'printReceipt'),
	(1189, '81cc75b21741f3d7344ead5f34dd136c', 107, 'sales_orders', 'printReceiptCurrent'),
	(1190, 'd15359f47e238a33744f60d1604e11af', 108, 'sales_orders', 'void'),
	(1191, '4fc06f4587adb7696afef22644867f49', 147, 'sales_orders', 'miscellaneous'),
	(1192, '46bf48aca0fc1ce1454de0f41a3c9979', 148, 'sales_orders', 'service'),
	(1193, 'cb4dd094968eca3b14f4391203f1499f', 181, 'sales_orders', 'discount'),
	(1194, '9632cd03b1fecba58259a5d715364c3d', 164, 'sales_orders', 'printInvoice'),
	(1195, 'a88a0531417fa3df96d936d2d4e56b80', 290, 'sales_orders', 'editUnitPrice'),
	(1196, '8d26b06ddc6afff50bd97936456e4581', 316, 'sales_orders', 'viewByUser'),
	(1197, '7a1bf729a96b7c360243faac3449035a', 394, 'sales_orders', 'approveSale'),
	(1198, '89a69dc23c01e25f15d9f70536f5ef6f', 643, 'deliveries', 'pickSlip'),
	(1199, 'd1f9cbb18d25c08304eec09eabf5f734', 643, 'deliveries', 'printInvoicePickSlip'),
	(1200, '0e7315d2c0e95955271e27edeb9ce34c', 643, 'deliveries', 'printInvoicePickSlip'),
	(1201, '585229df4bbc858c334d533694544d76', 202, 'credit_memos', 'salesOrder'),
	(1202, '79561d58ac41794d67d1f61f8473d80a', 202, 'credit_memos', 'salesOrderAjax'),
	(1203, '858c640cae7b4ceebabbbfbd129db36e', 202, 'credit_memos', 'getProductFromSales'),
	(1204, '36b78a1d165aae6e3e0d206999fbf340', 202, 'credit_memos', 'searchSalesInvoice'),
	(1213, '722534b053c347eb450a207ad5972374', 1, 'customers', 'searchCustomer'),
	(1214, '011b9097943648af88164b23d9c82b15', 50, 'customers', 'getVillage'),
	(1215, 'ffc29211275dd7cf5775bdea1abf1516', 50, 'customers', 'searchCustomer'),
	(1216, '233c1ebdce2f0185fb6d6803bb6af3a2', 50, 'customers', 'searchCustomerByCode'),
	(1217, '06b12f51595d43f7a76670b963c5d7b3', 50, 'customers', 'upload'),
	(1218, '8462ecc64202a2dc8b49869d2ac11c91', 50, 'customers', 'cropPhoto'),
	(1219, '8da7e4332dd02ecd014a850e00a0cab9', 50, 'customers', 'vendor'),
	(1220, '8b901f340ad59ce5bc0598130b5068f3', 50, 'customers', 'vendorAjax'),
	(1221, '362d1412a585002c65049febcc199385', 306, 'customers', 'delete'),
	(1222, 'a2a5c8c5b5e24d27069e4021a965acd8', 329, 'purchase_requests', 'close'),
	(1223, 'e7689e6914a1f7f7e9d405e4661a93cd', 397, 'purchase_requests', 'open'),
	(1224, '78cb39dbfa2a7ef49b33bd299bae10e5', 91, 'purchase_orders', 'product'),
	(1225, 'dab42cfb39942193da4ecbe87b57eece', 91, 'purchase_orders', 'productAjax'),
	(1226, '953ab0eed4d2e40d295330d194e1dea8', 91, 'purchase_orders', 'vendor'),
	(1227, '27b5486fdea0e8c9aa0a131dae98101a', 91, 'purchase_orders', 'vendorAjax'),
	(1228, 'fee3325c0ed481b1744efb433703e46f', 91, 'purchase_orders', 'searchProductCode'),
	(1229, '04d3dc647008eac5180bc837ac66096c', 91, 'purchase_orders', 'searchProduct'),
	(1230, 'bea4dbdb2ddeafd0940b347ead7387b3', 91, 'purchase_orders', 'searchVendor'),
	(1231, 'ce6061f1365b9b594b2f36c0414e1ee8', 91, 'purchase_orders', 'purchaseRequest'),
	(1232, 'd60a4284efff0f6cf8a70dbae2ef137e', 91, 'purchase_orders', 'purchaseRequestAjax'),
	(1233, '7fdca9e23c35d14079281ac0e7c6ef48', 91, 'purchase_orders', 'getProductsFromPO'),
	(1234, 'b3ee046cf6fb793744753909ad3125ce', 91, 'purchase_orders', 'printReceiptOne'),
	(1235, 'fd1ded227266a58951f0e778c6c4c6f6', 205, 'purchase_returns', 'searchProduct'),
	(1236, 'ef84b9a6c7620b437d85cb4833fba94e', 205, 'purchase_returns', 'searchProductByCode'),
	(1237, '9fc73714f640a98f5e6ace6e7a0726ef', 205, 'purchase_returns', 'product'),
	(1238, '072527093f1d09db3dd430b4dca70678', 205, 'purchase_returns', 'product_ajax'),
	(1239, '52324b5f36dee976825e7076791fd52e', 205, 'purchase_returns', 'vendor'),
	(1240, '80bd97a9dd93b99c010e4c0eaeb55257', 205, 'purchase_returns', 'vendorAjax'),
	(1241, 'f4b9466464c3286dac066ee006ee9f4c', 72, 'vendors', 'searchVendor'),
	(1242, 'faf80e846fb65ec936756399f80c10d7', 72, 'vendors', 'searchVendorByCode'),
	(1243, '498368edd73229382f40d8c0b7f9a754', 72, 'vendors', 'upload'),
	(1244, '64da62e2a307234956de8c523f6f9cc6', 72, 'vendors', 'cropPhoto'),
	(1245, '2f3d28474784eaee1e0f1e023f8fd389', 72, 'vendors', 'address'),
	(1246, 'ff377e496cf6d0e8d28b0b7a97e2510e', 642, 'deliveries', 'view'),
	(1273, '5b5cabe267b223dfd47f7aa5bb0ef3fb', 282, 'employees', 'upload'),
	(1274, '1317f624354388b1e20108ac30bd3d36', 282, 'employees', 'cropPhoto'),
	(1275, '179ac295515004db8265c9f4ad37113e', 282, 'employees', 'status'),
	(1276, 'f92335eac967a4bdc2b72bf6bb7fe1b6', 381, 'orders', 'quotation'),
	(1277, '202e3b6fcac81e44453a50070a8426ec', 381, 'orders', 'quotationAjax'),
	(1278, '883a086c11a43f0d86383056e22c9ddb', 417, 'quotations', 'service'),
	(1279, '02a5ce55e7fe5d39ad126446f05a82ed', 374, 'quotations', 'serviceCombo'),
	(1280, 'a3643b7342f82716df5ba9e775d469b4', 418, 'quotations', 'miscellaneous'),
	(1281, '501d4717dbfb02ba7801e179c6ec5d7f', 419, 'orders', 'service'),
	(1282, '7c66b0a24feeeffe82371d3a3a2f2db0', 420, 'orders', 'miscellaneous'),
	(1283, '7b733f0d38aab1f6c362576d366fa84a', 105, 'sales_orders', 'order'),
	(1284, 'dd6efe15abff273bd64f697b522650e8', 105, 'sales_orders', 'orderAjax'),
	(1287, '4e8c602fca92ca6b9fdac1ab80456b75', 421, 'quotations', 'editPrice'),
	(1288, '16514b523f5660a28850b04104c443e9', 422, 'orders', 'editPrice'),
	(1289, '3996d26b1ea842a820a2d7d336911178', 423, 'orders', 'discount'),
	(1290, 'db8f49f77902f80b01320cabf7c4fef7', 424, 'quotations', 'discount'),
	(1291, '4bf75a9869093bfc22caa8a81381239d', 425, 'products', 'setCost'),
	(1292, 'd6dbeaa87fb622dde94c541cca1f4935', 426, 'purchase_requests', 'editCost'),
	(1293, '13fe1587ec227d5595cb62510b7f5cea', 1, 'vendors', 'searchVendor'),
	(1294, '023cc134acfb7b663c9dc0b5a5cf1e06', 1, 'cgroups', 'searchCustomer'),
	(1295, '35d7f2bb7aa1bdb2742343d2ba017a12', 1, 'employees', 'searchEmployee'),
	(1296, '29b90ef0847e435df397991c16f017d3', 101, 'transfer_orders', 'printInvoiceConsignment'),
	(1297, '1e39d6634ce21fc8534be5c56c09b7aa', 626, 'vat_settings', 'index'),
	(1298, 'f27b339bd6804635e5cc9f9860e974d4', 626, 'vat_settings', 'ajax'),
	(1299, 'fde502ba2822d4eb44d10f2aa5890b86', 626, 'vat_settings', 'edit'),
	(1300, '52bfc9b45f636434d5555ae4d20238dd', 1, 'products', 'setProductWithCustomer'),
	(1301, 'a5699d779baaf4b9cf597c2e0b73c3d1', 41, 'companies', 'upload'),
	(1302, '72bc51b0e96b82f1a4a6feb0220d0b0a', 41, 'groups', 'add'),
	(1303, 'f5ad8961dc4cd8662a778ea7e242a514', 41, 'groups', 'edit'),
	(1304, '260f0dff13dc4cc4edb46715ae8c7c90', 41, 'groups', 'delete'),
	(1305, '0730ff93a46368973a776a34fbe796a1', 41, 'users', 'add'),
	(1306, '585a5481784f95491d7eee10f9edbb5f', 41, 'users', 'edit'),
	(1307, '307b77f39f514d372fb546cde4df837b', 41, 'users', 'delete'),
	(1308, '9b38d346a6ead6c1a8a2245771dbb2df', 188, 'general_ledgers', 'searchPurchaseOrder'),
	(1309, 'ee669a75cbb6e4fc3855a037ddbf609f', 188, 'general_ledgers', 'searchPurchaseBill'),
	(1310, '661d5f71b28b4d25e134491e2b178651', 188, 'general_ledgers', 'searchQuote'),
	(1311, '44b8ecd67d810d359db376dd061b4825', 188, 'general_ledgers', 'searchSalesInvoice'),
	(1312, '147dac7a1672ff7c4d7c34f589bdfc00', 188, 'general_ledgers', 'purchaseRequest'),
	(1313, 'a705d8ca9e3db3e42e1f93d67440c424', 188, 'general_ledgers', 'purchaseRequestAjax'),
	(1314, '5bd5ace71a793be9099eaa0699eee049', 188, 'general_ledgers', 'purchaseBill'),
	(1315, 'c109879f340072a4a7d1b7a568459dda', 188, 'general_ledgers', 'purchaseBillAjax'),
	(1316, '800443f6ee0d55e2ad06ffb010fd39c2', 188, 'general_ledgers', 'quotation'),
	(1317, 'f419d8b6f49698f177b80eaab945ede7', 188, 'general_ledgers', 'quotationAjax'),
	(1318, 'a94ad82c62804943d691466ff40d4bfd', 188, 'general_ledgers', 'salesInvoice'),
	(1319, '945468dce9bbc18b177b2cf6f37d5416', 188, 'general_ledgers', 'salesInvoiceAjax'),
	(1320, 'c22e731c79957c57a7978e004fed4f7a', 188, 'general_ledgers', 'editMakeDeposit'),
	(1321, '384dc68643d02476c70947b820ed16d5', 205, 'purchase_returns', 'searchVendor'),
	(1322, '6be0216454ba953a72c458ed2daf9b2b', 1, 'users', 'createSysAct'),
	(1323, '403c4ab41163de3637ebf4a348ad62ef', 435, 'term_conditions', 'index'),
	(1324, '2229d0782ac4c65f9cdfc45021b6df5f', 435, 'term_conditions', 'ajax'),
	(1325, '7072bd931dcd5f9341f8c94cc2f3939d', 435, 'term_conditions', 'view'),
	(1326, '47acdbe4851839a6fd31ddd8e9030c41', 436, 'term_conditions', 'add'),
	(1327, '220fa0f8f81f051d4cd883e5b168141f', 437, 'term_conditions', 'edit'),
	(1328, 'ebb47e4382400307bdd0c1e7fb742f9f', 438, 'term_conditions', 'delete'),
	(1329, '92a199c48f5f8a777d3d8db5e6c85612', 439, 'term_condition_types', 'index'),
	(1330, '0abf0fe38ae7f134727328b027b10358', 439, 'term_condition_types', 'ajax'),
	(1331, '0783aa5e4de20b7168bb4b3ad4021220', 439, 'term_condition_types', 'view'),
	(1332, '869e6b365799e8a041e6f6c4975e74ac', 440, 'term_condition_types', 'add'),
	(1333, '3c089a538dc9a94e474c1cd4a8d07f0f', 441, 'term_condition_types', 'edit'),
	(1334, '2f3d7a79fab095f89e9131f0b27c961f', 442, 'term_condition_types', 'delete'),
	(1335, '09c1a2f30c54336896076268e20333e9', 443, 'term_condition_applies', 'index'),
	(1336, 'ff89cc5cca3309bee9fc12323c6434d3', 443, 'term_condition_applies', 'ajax'),
	(1337, '77d70124ab2799e0beb9d3fc86becad6', 443, 'term_condition_applies', 'view'),
	(1338, 'ebd5a4cdc5e3fda80c8a53b5fe51c1c4', 444, 'term_condition_applies', 'add'),
	(1339, 'd12398db12a4efc605eeb3e5ffef8f9a', 445, 'term_condition_applies', 'edit'),
	(1340, 'c522697769566f40f6b49bc3829d08af', 446, 'term_condition_applies', 'delete'),
	(1341, 'fbbc8633f6c72c82c91ac8029ab8eb9e', 282, 'price_types', 'changeOrdering'),
	(1342, '80329d717a2786ec94f0b8cdc79e3ce8', 447, 'customer_contacts', 'index'),
	(1343, '497cbde34dd207c8ffb5e514ed493783', 447, 'customer_contacts', 'ajax'),
	(1344, '29dabdf196f499f78d5020580f40e480', 447, 'customer_contacts', 'view'),
	(1345, '3984aa7c43f4a2836d48bf4788478afb', 448, 'customer_contacts', 'add'),
	(1346, '2e648118b5d2215514274e85c59cb853', 449, 'customer_contacts', 'edit'),
	(1347, 'df480126f317c8522e1428a936b0f4e9', 450, 'customer_contacts', 'delete'),
	(1348, '2ea39bfa6f9b53a07f8a155a14947adb', 451, 'customer_contacts', 'exportExcel'),
	(1349, 'be7be0e5ce8c4e97ba33b730ccedf01b', 501, 'quotations', 'editTermsCondition'),
	(1350, '41524960962a6ea4a1e849e941ebf8d7', 453, 'quotations', 'invoiceDiscount'),
	(1351, '64b7b31cdcbb03334c854bfed62cdba2', 374, 'quotations', 'getCustomerContact'),
	(1352, '5449cf4aa5b9a5590413eee3634d34be', 374, 'quotations', 'searchCustomer'),
	(1358, '699c541eb5829487f3d308992eeb3bbe', 458, 'orders', 'editTermsCondition'),
	(1359, '0b674faa067502feefa0c67c0d121c37', 459, 'orders', 'invoiceDiscount'),
	(1360, 'e8fe3a3b8b90c2e451a0339b828f8d63', 381, 'orders', 'getCustomerContact'),
	(1361, '88a1401139be85936442da18f40d641a', 381, 'orders', 'searchCustomer'),
	(1362, 'fc457a0fdedeb236c98dcfb6d8f36be3', 460, 'purchase_requests', 'editTermsCondition'),
	(1363, '04969001a55124ca9e17cfc0462944a3', 461, 'sales_orders', 'invoiceDiscount'),
	(1364, '830eea22e45392be07c245e7e464d864', 462, 'sales_orders', 'editTermsCondition'),
	(1365, '12512d38fb0e041c89dade87757789e6', 626, 'vat_settings', 'add'),
	(1366, 'c467ffe97d8a7bc4f0e54148109dacba', 626, 'vat_settings', 'delete'),
	(1367, '83b7856166c94158e089bf147a41b2d1', 105, 'sales_orders', 'getCustomerContact'),
	(1368, 'f80e5c77c0ca135caa421c835208d8f5', 105, 'sales_orders', 'customerCondition'),
	(1369, 'ab092c0ddaba6502c0738c85187123c5', 202, 'credit_memos', 'invoiceDiscount'),
	(1370, '5da2058c2cf2f42d694890c4e1e39746', 465, 'purchase_orders', 'service'),
	(1371, '348cc6135c5690f443485d7e325a5b44', 466, 'purchase_orders', 'invoiceDiscount'),
	(1372, 'd74db7829d42ddbf0b4e3e07d6d7c309', 1, 'dashboards', 'share'),
	(1373, '177e5abe17055925f7495bc6fc0f671c', 1, 'dashboards', 'shareSave'),
	(1374, 'fc1c2b5abd85e217b09b579061bbae47', 467, 'quotations', 'approve'),
	(1375, 'ad918b56b8817ea29a34fdd5747d480c', 374, 'quotations', 'saveShareQuote'),
	(1376, '37fc1c0aa8792c636cb13d4d3f3f8444', 468, 'purchase_orders', 'close'),
	(1377, '3925e75acfe33ff947220ba9d2e716fa', 469, 'currency_centers', 'index'),
	(1378, '61c6a00953ff84ca866fb9b06ff4d0b1', 469, 'currency_centers', 'ajax'),
	(1379, '245558e1831c5badc884fecf074f1ba1', 470, 'currency_centers', 'add'),
	(1380, '1b4d26ff4d22bd778480157925994b38', 471, 'currency_centers', 'edit'),
	(1381, '6416a0a2149a762e81f3b3b5593fcbd5', 472, 'currency_centers', 'delete'),
	(1382, 'adc5df4ddb0754f909a213d57d7569b1', 504, 'branch_currencies', 'index'),
	(1383, '2082dfec59fb5e6cdd767fc680d44a16', 504, 'branch_currencies', 'ajax'),
	(1384, '4eef76a52f1a370cb5a103996d007ee1', 504, 'branch_currencies', 'add'),
	(1385, '4e84a249bde313e8b81a7d344e406fce', 504, 'branch_currencies', 'edit'),
	(1386, '8e435bd4c49fde959aca40989cecacab', 504, 'branch_currencies', 'delete'),
	(1387, '17dbc8df132c498a1b5b6fe7481297c9', 504, 'branch_currencies', 'applyPos'),
	(1388, 'b097f8d80c64d8074efa90e861b107b5', 478, 'sales_targets', 'index'),
	(1389, '845d151dbcd079d6ecb401708c207016', 478, 'sales_targets', 'ajax'),
	(1390, '3d08072a4ccae682b83cd9d67fccafc3', 479, 'sales_targets', 'add'),
	(1391, '1dd5bfe2f4ed72d40a2fba201c6f0455', 480, 'sales_targets', 'edit'),
	(1392, '80dac9dc084daf57fd4747bbad541753', 481, 'sales_targets', 'delete'),
	(1393, '5afcb7d76e08c9fd8aa84308d78499d4', 482, 'sales_targets', 'approve'),
	(1394, '12a46e83d88db4cfd4c43d7f52105248', 478, 'sales_targets', 'employee'),
	(1395, 'e21cfdfecf83ce5c8c5b4a538c1360c4', 478, 'sales_targets', 'employeeAjax'),
	(1396, '58ba78d130a94a47accd2eb3b0860d14', 478, 'sales_targets', 'view'),
	(1397, 'ec4e031be40901228864904fe7242142', 483, 'orders', 'approve'),
	(1398, '518d19a483771d02fa89834520a9d45f', 374, 'quotations', 'history'),
	(1399, 'a95378ceb05c499ab90aba33878cad68', 47, 'products', 'cloneProductInfo'),
	(1400, 'fb377bed0da7fa79dcbf3a529865913e', 1, 'reports', 'searchCgroup'),
	(1401, '689bf62cc6705431753dfb8c8391cf4c', 1, 'reports', 'searchCustomer'),
	(1402, '2f9c69a98f3a290da2dd8174683ede88', 1, 'reports', 'searchPgroup'),
	(1403, '66db0ac225639247d08677b5aa557da5', 1, 'reports', 'searchProduct'),
	(1404, 'c2909313805d6ed8a2a60bf57ab95031', 1, 'reports', 'searchEmployee'),
	(1405, '712f1b9d3a526ad1266ab8018cfa8619', 1, 'reports', 'searchEgroup'),
	(1406, '6077b9fdee640b04cab8952f5e4b1722', 1, 'reports', 'searchVendor'),
	(1407, '1620010c8a78dd514718389cfa39f664', 485, 'products', 'viewTotalCostPrice'),
	(1409, 'b0a24be25be160a57323649f8d698ac7', 486, 'products', 'viewChangeCost'),
	(1410, '6162369edf1cc8bb35c0f3ddf70b4b38', 486, 'products', 'resultChangeCost'),
	(1411, '349e06d2d701b19eae96f69df1208265', 487, 'request_stocks', 'viewRequestIssued'),
	(1414, '68b69af111b529f219da2f23c54b9352', 1, 'dashboards', 'userDashboard'),
	(1415, '6955b71c54ae952a26831cb3149d88f8', 489, 'reports', 'salesTopItem'),
	(1416, 'cad2ef557a5350673bf32f0550b3fd6f', 489, 'reports', 'salesTopItemResult'),
	(1417, 'c1f09924ccce7b3174bd0755fa41d5a0', 489, 'reports', 'salesTopItemGraph'),
	(1418, 'ca1582f9314579b04fcbc2404c78a81f', 490, 'products', 'viewCost'),
	(1419, '1b32d9cf3f9c4e6eec54731a920ad430', 491, 'reports', 'salesTopCustomer'),
	(1420, 'ef67ac83e9ab8b0d674b5bbf46804960', 491, 'reports', 'salesTopCustomerResult'),
	(1421, '9fd75fb3446fc33fa00c02b4ccc39e1f', 491, 'reports', 'salesTopCustomerGraph'),
	(1422, 'c8c61b1186a378a87b28f74fdffac6f3', 106, 'sales_orders', 'add'),
	(1423, '0daa6525c2f81bbd0c256665be362af7', 492, 'reports', 'salesBySalesman'),
	(1424, '8a8f12377be13abb9eeb12a5d26d5104', 492, 'reports', 'salesBySalesmanDetailResult'),
	(1425, '98cfa99e0d2cf8d5d9ff79192d95c9e6', 493, 'reports', 'invoiceByRep'),
	(1426, '8f8b38eaf82aec453c606a11370eeadc', 493, 'reports', 'invoiceByRepResult'),
	(1427, 'cdf9f2ee9d0f6dc59c9e41111e6f791b', 493, 'reports', 'invoiceByRepAjax'),
	(1428, '58f9358d2aa85ec993437e5f3d654708', 494, 'reports', 'statement'),
	(1429, '077152a006277611da8b647529b890d2', 494, 'reports', 'statementResult'),
	(1430, 'ca73b61275d7a98156e3f95e55249d5d', 495, 'reports', 'statementByRep'),
	(1431, 'c991d63874f87827cbe4a04769abce3f', 495, 'reports', 'statementByRepResult'),
	(1432, 'a75bdbc6b66f10c8ac740dad5e8fc951', 496, 'reports', 'customerBalanceByInvoice'),
	(1433, '707922a8406c8c4a623b69805772f876', 496, 'reports', 'customerBalanceByInvoiceResult'),
	(1434, '0161063fad6e4d6dc04363ae53bf2167', 496, 'reports', 'customerBalanceByInvoiceAjax'),
	(1435, '683279b37d696f8cf54033b89df1cb7e', 497, 'reports', 'vendorBalanceByInvoice'),
	(1436, 'bf1e083b712886444cefe319409ee2f2', 497, 'reports', 'vendorBalanceByInvoiceResult'),
	(1437, 'ac6a2c4273a779bb1f378e994f6ccdaf', 497, 'reports', 'vendorBalanceByInvoiceAjax'),
	(1438, '04e0994179acb3bdd9ea150de4f1bfb0', 498, 'reports', 'auditTrail'),
	(1439, '3d5767818294d465f39cf9137481384a', 498, 'reports', 'auditTrailResult'),
	(1440, '112d78d47b3d924da5689b6d6080a7ea', 499, 'inv_adjs', 'viewAdjustmentIssued'),
	(1441, '5fb63d152c08faf216068df8b6f98be4', 500, 'transfer_orders', 'viewTransferIssued'),
	(1442, 'ab48f61ce3d9b6bc66e74b2ee0e40092', 501, 'quotations', 'viewQuotationNoApprove'),
	(1443, '5e2137593de2d9a4c45aba1db6cb8df4', 502, 'orders', 'viewOrderNoApprove'),
	(1444, '4e30b54731be9cb0fb1affe58830c91e', 503, 'sales_orders', 'viewInvoiceNoDelivery'),
	(1445, '29617126c7672dd2f24c752a9ee8bcdc', 504, 'credit_memos', 'viewCreditMemoIssued'),
	(1446, '2af2fd3796a431fdd3f27dee0feab237', 505, 'purchase_orders', 'viewPurchaseIssued'),
	(1447, 'c3c06093bfb1f310538c0c824c1dd5c4', 645, 'purchase_receives', 'receiveDetail'),
	(1448, '57280e87071633be5f0e95aeef793d5f', 506, 'reports', 'quotation'),
	(1449, 'b7b0eb4cae46831df6a2c1f3ee360910', 506, 'reports', 'quotationResult'),
	(1450, '399f201ce311709d3c85425d07e4fc76', 506, 'reports', 'quotationAjax'),
	(1451, 'c678f744f4857fafe6dec0e5dbb4ffb6', 507, 'reports', 'customerQuotation'),
	(1452, '2323308edcd640676773db891b115c9d', 507, 'reports', 'customerQuotationResult'),
	(1453, '2d5e3be30aa1e4c41988d21358fa9949', 507, 'reports', 'customerQuotationAjax'),
	(1454, 'b53a30dd16d96a27e88dc6623ebb1053', 508, 'reports', 'customerSaleOrder'),
	(1455, 'fbffdeda07c6200746a2627742af9be1', 508, 'reports', 'customerSaleOrderResult'),
	(1456, 'b0e542e1cbdc681c1d999da84d2c836d', 508, 'reports', 'customerSaleOrderAjax'),
	(1457, '42e17eba3cc1b4cf6439154a3697cf11', 508, 'reports', 'customerSaleOrderProductSummaryResult'),
	(1458, '658ebd84c0d6a3d8757503fc04eae4a9', 508, 'reports', 'customerSaleOrderProductSummaryAjax'),
	(1459, 'edcedfd7200b33d7a220f1b27035beee', 508, 'reports', 'customerSaleOrderProductResult'),
	(1460, '066f47e0162942be566e2c10c3448803', 508, 'reports', 'customerSaleOrderProductAjax'),
	(1461, 'e05c979cc7895e4bc2fa7a0b8fdb850e', 508, 'reports', 'customerQuotationProductSummaryResult'),
	(1462, '7a327ca6912a222111fb40894538b9b0', 508, 'reports', 'customerQuotationProductSummaryAjax'),
	(1463, '09447422b5af2d23a00a0b4a106d2669', 508, 'reports', 'customerQuotationProductResult'),
	(1464, 'f0e32255e24e62e0848985d270d094a6', 508, 'reports', 'customerQuotationProductAjax'),
	(1465, '7445d61277f3d9eef8576e52645aaa1f', 504, 'exchange_rates', 'view'),
	(1466, '22ab10c01591570c0cdc38b01e6c7968', 47, 'products', 'viewProductHistory'),
	(1467, '930d1eed26e5bbeb98cf10003d68ec01', 509, 'products', 'viewProductReorderLevel'),
	(1468, '66a3423b1195e1984705a937d38c0d5a', 509, 'products', 'viewProductReorderLevelAjax'),
	(1469, '7e703f92312a63766665e8ad0ec9fde0', 510, 'products', 'viewProductExpireDate'),
	(1470, 'c0549b30b1347e771eb5cdaca1592ae3', 510, 'products', 'viewProductExpireDateAjax'),
	(1471, 'c6f172aeb1eea8c7e31dcd875aa653c9', 41, 'branches', 'index'),
	(1472, 'e4b70fcaaede914ceb323562d0be0c9b', 41, 'branches', 'ajax'),
	(1473, '1c63279a7dfdc554a987885a3a2500e1', 41, 'branches', 'view'),
	(1474, 'aa3894d10f06f373b9b7ed2c2c2b1dbf', 41, 'branches', 'add'),
	(1475, '043f757218beba8d81bcf2e0b081c914', 41, 'branches', 'edit'),
	(1476, 'f25fd5abb24194375517447e1283f981', 41, 'branches', 'delete'),
	(1477, '58d892335c444be8ec58d18e656ac817', 1, 'users', 'getBranchByCompany'),
	(1478, '0f6f0710f88b8c52c739edc2297670d0', 136, 'products', 'productPriceDetail'),
	(1479, 'c7980f0718ab4ce604e4bfb2079f2fbd', 374, 'quotations', 'productHistory'),
	(1480, '4c679ec9efef92290f329d42475e14a8', 374, 'quotations', 'productHistoryAjax'),
	(1481, '9fc019654db1a12914acfd93ec7d9795', 381, 'orders', 'productHistory'),
	(1482, '7ae1a261fe1117ce1ef1b167daa98bba', 381, 'orders', 'productHistoryAjax'),
	(1483, '693251a4ac3bd260ba94c0d1f0e05110', 105, 'sales_orders', 'productHistory'),
	(1484, '8e753edd93024719632e70b209bb1b9f', 105, 'sales_orders', 'productHistoryAjax'),
	(1485, 'e00c0f708e2de2d87f8942b3da5a7e60', 253, 'purchase_requests', 'productHistory'),
	(1486, '23d1480786ec7c623264e69184497708', 253, 'purchase_requests', 'productHistoryAjax'),
	(1487, '9373a0e3717e8f5b55fe38cb764891ea', 91, 'purchase_orders', 'productHistory'),
	(1488, '04a40eb7eea8522f9820470310d74042', 91, 'purchase_orders', 'productHistoryAjax'),
	(1489, '454575d4ae51ddd5f9721e1d170af55d', 46, 'products', 'printProduct'),
	(1490, '546af125130335d1950757b4dc693ada', 46, 'products', 'printProductByOne'),
	(1491, '16ef60eaf5902b0d27b8858f97bf2e1b', 46, 'products', 'printBarcode'),
	(1492, '128e1ecf23139e3c7579133a534d50f5', 46, 'products', 'printBarcodeByOne'),
	(1493, '5f5a1a01ab6a33d00eaaf8e1d90bf615', 46, 'products', 'printProductByCheck'),
	(1494, 'a1dfe085ac98962229bedea0892e27f1', 515, 'branch_types', 'index'),
	(1495, '7d82a2d701bbb1bf934dd354b51d149e', 515, 'branch_types', 'ajax'),
	(1496, '867fa8583881a127b0739853b29c5471', 515, 'branch_types', 'view'),
	(1497, 'f67d185c5e3b08deccbb36d2f7849b78', 516, 'branch_types', 'add'),
	(1498, '33de88b28278014508bd40edb254402a', 517, 'branch_types', 'edit'),
	(1499, '0fce57900026006c06f83e14aa46a112', 518, 'branch_types', 'delete'),
	(1500, 'f73a422c0ee12f071827e8939351f537', 597, 'location_groups', 'searchCustomer'),
	(1501, 'ff79b5ef27c594c1d83009092556fd62', 597, 'location_groups', 'customer'),
	(1502, '001e1b210bbdb447e362d0187d62237b', 597, 'location_groups', 'customerAjax'),
	(1503, 'd00f505bbd226b281191c4d479839c73', 519, 'consignments', 'index'),
	(1504, '10c3fd4aa7363f709ed1f3d6aa296b29', 519, 'consignments', 'ajax'),
	(1505, 'bb7a469b2e2ab82aae767ef59b702b06', 519, 'consignments', 'view'),
	(1506, 'a6ff716d4a665bb95a89ce81cf9c0987', 520, 'consignments', 'add'),
	(1507, '4d53d7498ab84c8c2b8bcf8a8d139bc8', 521, 'consignments', 'edit'),
	(1508, '8349920a335f185d36b2386b212a4e5f', 522, 'consignments', 'void'),
	(1509, '8a566f41998aeda62708aa1931cecb0c', 523, 'consignments', 'receive'),
	(1510, '9c83bff8b73e77c820f2fc75215eb825', 524, 'consignments', 'viewByUser'),
	(1511, '29c49bab75fc4e53a6a326cb1f5daac9', 525, 'consignments', 'printInvoice'),
	(1512, 'b246bec6da87b3abf36b4638f6f7f685', 519, 'consignments', 'product'),
	(1513, '1b0d1a4e0b2b05f90d6bf6f7dcd26bd0', 519, 'consignments', 'product_ajax'),
	(1514, '37e8f7c48a09b80e24ef296651fbb2be', 519, 'consignments', 'customer'),
	(1515, '1d97af5a67b6d03e12efd2e0b5cb1b6e', 519, 'consignments', 'customer_ajax'),
	(1516, 'a8c4cca055d5509b3150228ecfe3f8be', 519, 'consignments', 'employee'),
	(1517, 'eabe50c98cef11880c842ef4f72c00d4', 519, 'consignments', 'employeeAjax'),
	(1518, '0d37aefa99311c62fc87cbe5a1cff348', 519, 'consignments', 'searchProduct'),
	(1519, 'cc4e9fd48f885abb9dff5cdf89fe379d', 519, 'consignments', 'searchProductByCode'),
	(1520, '5312293e90a113709cca241f7fe936a0', 519, 'consignments', 'getCustomerContact'),
	(1521, '72f8010cc09d32647c52b789c48b4651', 520, 'consignments', 'orderDetails'),
	(1522, '6e9089da1f2d1e03e405be19f9938f30', 521, 'consignments', 'editDetail'),
	(1523, '4d8f1bbd864e1097c6e10d3a12542cd7', 526, 'consignments', 'editTermsCondition'),
	(1524, 'e8dbf55c13571c18bf5bd1922d4e90b1', 519, 'consignments', 'searchCustomer'),
	(1525, 'e7af9d5d6a3d5896fb1d4a24ec6458de', 527, 'consignment_returns', 'index'),
	(1526, '33147257dfe4788e848d1d805dc1bd39', 527, 'consignment_returns', 'ajax'),
	(1527, 'b54b4a78ab02fe42f397cc643ee9a8bd', 527, 'consignment_returns', 'view'),
	(1528, '1c2594da7e2c23dd926feeb5ccf67461', 527, 'consignment_returns', 'customer'),
	(1529, '4982e02ccdded6c8b8df3003adedf1ee', 527, 'consignment_returns', 'customer_ajax'),
	(1530, '5c4ee4eeb936e783dcb8811c7aa5b54e', 527, 'consignment_returns', 'searchCustomer'),
	(1531, 'e0646296e723827cc0e0eae4dee52c5a', 527, 'consignment_returns', 'getCustomerContact'),
	(1532, '43c32383dcb3d79faca8869226f4fa36', 527, 'consignment_returns', 'getConsignmentReturn'),
	(1533, 'eef01cdb1a5f2772cd9a5cf77bb5469b', 528, 'consignment_returns', 'add'),
	(1534, 'e7724593a4cc16a9598786d700ecd20d', 528, 'consignment_returns', 'orderDetails'),
	(1535, 'fed52430d661ff8dfa376d339c43e9d6', 529, 'consignment_returns', 'edit'),
	(1536, 'a1799bc97f16e94636ccb774b7ab4e9f', 529, 'consignment_returns', 'editDetail'),
	(1537, '6ea50e8b9ede3a5e6f7b762e6783e366', 530, 'consignment_returns', 'void'),
	(1538, 'f78e2662cac753c342995090a12e8861', 531, 'consignment_returns', 'printInvoice'),
	(1539, '745c9d9654a0f9611a1104858ded2ad6', 532, 'consignment_returns', 'receive'),
	(1540, '97b347fb56e4e2211ed1e5da460fea06', 533, 'consignment_returns', 'viewByUser'),
	(1541, '6f378bbff014ea62739014f11083512c', 527, 'consignment_returns', 'consignment'),
	(1542, '4065ad03cfdb1d824f3c7e84a7c67ee5', 527, 'consignment_returns', 'consignmentAjax'),
	(1543, 'a11b399f45c83ade0c96917fd2251c3e', 534, 'vendor_consignments', 'index'),
	(1544, '265b5e76d75c8e1ca0f97e1a658dfdd4', 534, 'vendor_consignments', 'ajax'),
	(1545, 'e86c95587d043f47549580b700fe918c', 534, 'vendor_consignments', 'view'),
	(1546, '954fc7e96da34301b8542ee37d16cea8', 534, 'vendor_consignments', 'vendor'),
	(1547, '0acb96712bfc69333841bc018fe54283', 534, 'vendor_consignments', 'vendorAjax'),
	(1548, '37d2019edb7c17888c344127f7d5140f', 534, 'vendor_consignments', 'searchVendor'),
	(1549, '996dbb0ad4b9f2370ec05b99999f4d72', 534, 'vendor_consignments', 'product'),
	(1550, '2aed43d40ed98ebe715719299310bb15', 534, 'vendor_consignments', 'productAjax'),
	(1551, '4025760926f8ada495c8c3bc72871697', 534, 'vendor_consignments', 'searchProduct'),
	(1552, '41eddc3e5c21be74c240a8b3e5f26036', 534, 'vendor_consignments', 'searchProductCode'),
	(1553, '926e310ccdd8c9925573590283014173', 535, 'vendor_consignments', 'add'),
	(1554, '16900a1a58b222b93af28cfd0997296b', 536, 'vendor_consignments', 'edit'),
	(1555, '690cebc21967f4e22b0ffb12072d3569', 537, 'vendor_consignments', 'delete'),
	(1556, 'd6437be87ecbaa1d4bfcf9eebd6761c9', 538, 'vendor_consignments', 'printInvoice'),
	(1557, '30faeeddbb351d287fc1d4eed4760549', 539, 'vendor_consignments', 'receive'),
	(1558, '99a73629fbacfc5315ddbeb6863e3ed4', 540, 'vendor_consignments', 'viewByUser'),
	(1559, 'd180b824642666d20782aad6c7d01057', 541, 'vendor_consignment_returns', 'index'),
	(1560, '0c9ca6616a5221c78c3560cac11cd2d9', 541, 'vendor_consignment_returns', 'ajax'),
	(1561, '4a49b622d3023062aacbfadc4ad903d7', 541, 'vendor_consignment_returns', 'view'),
	(1562, '4c1bf1a0645fa48a6cf52e2f68d66ede', 541, 'vendor_consignment_returns', 'vendor'),
	(1563, '39c022ac286b8d3706e203fa91d89ed6', 541, 'vendor_consignment_returns', 'vendorAjax'),
	(1564, 'ca1051c58ec415134b607347ebde22c1', 541, 'vendor_consignment_returns', 'searchVendor'),
	(1566, '55f9a18c6b6bbe038e268905e6672e04', 541, 'vendor_consignment_returns', 'getVendorConsignmentReturn'),
	(1567, '97c95dc2d71261e1960dd1582b1086ad', 541, 'vendor_consignment_returns', 'vendorConsignmentAjax'),
	(1568, '03b49b1526fd9b62603db6521cc2c24d', 541, 'vendor_consignment_returns', 'vendorConsignment'),
	(1569, 'b376eded5fa71b18d030c6f639f803c9', 542, 'vendor_consignment_returns', 'add'),
	(1570, 'ddcddeeb50bd6d678282090fe42e1267', 542, 'vendor_consignment_returns', 'orderDetails'),
	(1571, 'fb9d20ba4a9866ecd8e154048c60a4ae', 543, 'vendor_consignment_returns', 'edit'),
	(1572, 'aa787bd5700f49267e0510f99b71bd82', 543, 'vendor_consignment_returns', 'editDetail'),
	(1573, 'be79d29ddc06ea8de7f491488c25e252', 544, 'vendor_consignment_returns', 'void'),
	(1574, 'eb1ece86b5cbdba2278304f5dd1f9f27', 545, 'vendor_consignment_returns', 'printInvoice'),
	(1575, 'b30ba62a874023257c4401b999188e45', 546, 'vendor_consignment_returns', 'receive'),
	(1576, '770b16a3f9ac209a3880a52e524fb2b1', 547, 'vendor_consignment_returns', 'viewByUser'),
	(1577, 'd520ec283443b3e20900b48c400f3ff6', 91, 'purchase_orders', 'vendorConsignment'),
	(1578, 'ccc8830e64d7207436de66a4dec1b94f', 91, 'purchase_orders', 'vendorConsignmentAjax'),
	(1579, 'cb552dc7722bc3c3140d0fb90e5bb07c', 91, 'purchase_orders', 'getVendorConsignment'),
	(1580, '091d4e7699fd83bbd98d9c0a5b4da7e7', 548, 'landed_cost_types', 'index'),
	(1581, '40c24aa94e1dbee57378b7ae7df8ae45', 548, 'landed_cost_types', 'ajax'),
	(1582, 'ebeaa189b1a0e48548b40e51d38bf480', 548, 'landed_cost_types', 'view'),
	(1583, '35f988f58c5582356072f6002d34970b', 549, 'landed_cost_types', 'add'),
	(1584, 'd84e24032709e65d266a185d8edeaf79', 550, 'landed_cost_types', 'edit'),
	(1585, '44ea3982a89de3de244338d97316e76c', 551, 'landed_cost_types', 'delete'),
	(1586, '93e9fe99574c31710024a1b739477dec', 552, 'landing_costs', 'index'),
	(1587, 'c81f661b174366e4da8c992fa6979d48', 552, 'landing_costs', 'ajax'),
	(1588, 'e45e16560954bb7470c25a3b2bea3f83', 552, 'landing_costs', 'view'),
	(1589, '69854c8ec5342ba1b9b0c0801bf9ed0d', 552, 'landing_costs', 'vendor'),
	(1590, '4d392b1db23b7a2f3853f57e10e9fee3', 552, 'landing_costs', 'vendorAjax'),
	(1591, 'e4f567ff2644aef95b2faa4d4175dd30', 552, 'landing_costs', 'searchVendor'),
	(1592, '400a11807df531797e4aa52774145c56', 552, 'landing_costs', 'purchaseBill'),
	(1593, '2ab016f9f4b863a97c8330c4683b37a1', 552, 'landing_costs', 'purchaseBillAjax'),
	(1594, '0bfcd68740ee872f97d75a5913abf94b', 553, 'landing_costs', 'add'),
	(1595, '7a6a07920ae8850f6d376be236ee8d64', 553, 'landing_costs', 'orderDetails'),
	(1596, 'a045140a00215a7d23e526b00a0706a5', 554, 'landing_costs', 'edit'),
	(1597, '812cd966847aa4ecc56378edcb89c18e', 554, 'landing_costs', 'editDetail'),
	(1598, '09883eb993a7d95d399d4948b10bf460', 555, 'landing_costs', 'delete'),
	(1599, '01595dd9e708878c6f291637ff804325', 556, 'landing_costs', 'aging'),
	(1600, '87aa0437f630e27755fcb9fdbd584c7b', 557, 'landing_costs', 'viewByUser'),
	(1601, '5f9dd0ec359473e6eac8f04f5d1381b2', 552, 'landing_costs', 'getPurchaseLandingCost'),
	(1602, 'dfc25b7a888cdef38c06296545f6aaad', 556, 'landing_costs', 'voidReceipt'),
	(1603, 'a22607f1e8bdfffbcd7dc801467000c3', 558, 'landing_costs', 'close'),
	(1604, '2c973d6b4329dcc38688486d34b9526d', 559, 'location_group_types', 'index'),
	(1605, '4cddcd62c2d002506e6302f92653d176', 559, 'location_group_types', 'ajax'),
	(1606, '97fc2041d04bda319a0ee756ba066a2b', 559, 'location_group_types', 'view'),
	(1607, 'bb73d1e473b8dfedfec6b33a0251fa21', 560, 'location_group_types', 'add'),
	(1608, '048bb2008646b4e635edaac018cbe860', 561, 'location_group_types', 'edit'),
	(1609, '45d14c568a204462958c161152f281cf', 562, 'location_group_types', 'delete'),
	(1610, '476e432114223c3ca9b233f88904f1bd', 563, 'location_group_types', 'exportExcel'),
	(1611, 'ba6023664df027ff6be3d219d4b10c5c', 597, 'locations', 'status'),
	(1612, 'e3744c827c3bf88499ba7c5e54f1fb4f', 46, 'products', 'printByUomBarcode'),
	(1613, 'e26fd6410ed0151c2579f2da960ca7be', 565, 'e_pgroup_shares', 'index'),
	(1614, '84e7d3abc5fe94eafe85cebf9634670f', 565, 'e_pgroup_shares', 'saveShopShare'),
	(1615, 'd3728a842c5fa2a8e8ad52913e03559f', 565, 'e_pgroup_shares', 'editShopShare'),
	(1616, '233544fdb7cd1905f948c3f22919f0ec', 565, 'e_pgroup_shares', 'savePgroupShare'),
	(1617, '5856910687d444422d202a0b5039b12b', 565, 'e_product_shares', 'index'),
	(1618, '15ecae90646c99922d56d06496c229fe', 565, 'e_product_shares', 'ajax'),
	(1619, 'c005f9c4a5c1297dca3f45e436645de3', 565, 'e_product_shares', 'productPrice'),
	(1620, 'b6d4f5c9e2277e6a9ca7ca814afe7926', 565, 'e_product_shares', 'productPriceDetail'),
	(1621, '163993c1b1c55d7e8db26a47c5f9082e', 566, 'sync_monitors', 'index'),
	(1622, 'aa4ed9faecb8633b85c8b71bd053f0ee', 566, 'sync_monitors', 'checkConnection'),
	(1623, '74c9e204ae815e42af6dc43d2ab89aec', 566, 'sync_monitors', 'saveConfig'),
	(1625, '70e88397432a0311905fe09fa0523f92', 567, 'products', 'viewActivityByGraph'),
	(1626, 'bd09066259afd764ca35601b94bd1d41', 568, 'products', 'viewPurchaseSalesByGraph'),
	(1627, 'c59780229dc6d6f43e8ef1c08e729f78', 569, 'reports', 'inventoryConsignment'),
	(1628, '4064ad25788432193d7929b32437b12f', 569, 'reports', 'inventoryConsignmentParentResult'),
	(1629, '14f3e9821bf9fd4e9cfcdb7092229da1', 569, 'reports', 'inventoryConsignmentResult'),
	(1630, '66c9cea6e4041c8d363fa732360051fc', 569, 'reports', 'inventoryConsignmentWithGlobalResult'),
	(1631, '913e52ed87bc1f91eb7f2cdff272167f', 569, 'reports', 'inventoryConsignmentWithGlobalDetailResult'),
	(1632, 'd0927fbd80022c1df7b467494dd68283', 569, 'reports', 'inventoryConsignmentDetailResult'),
	(1633, 'df1fb0eaed3600c01929b3f8f3385bde', 570, 'reports', 'requestStock'),
	(1634, '58926c65432b805350898e37acec304b', 570, 'reports', 'requestStockResult'),
	(1635, '70c0b4fb066d1630fe3f965f672091bc', 570, 'reports', 'requestStockAjax'),
	(1636, 'ed8d5bd92c8d5cf550e1839b986273d3', 571, 'reports', 'requestByItem'),
	(1637, '1a747dae2e6150e0ac0b2c1a6bb6ff95', 571, 'reports', 'requestByItemParentResult'),
	(1638, 'e502f497179fdd28cbe4180cba531cda', 571, 'reports', 'requestByItemResult'),
	(1639, '99d5a26e3c92dadd8302e0cf5c68d587', 571, 'reports', 'requestByItemDetailResult'),
	(1640, 'bf170418e0548afc282e8f85d84581ab', 572, 'sales_consignments', 'index'),
	(1641, 'a1a36ef5d4262cd7d1ec09628d367d00', 572, 'sales_consignments', 'ajax'),
	(1642, '3be4c82f6734621faddb9b8459c510a5', 572, 'sales_consignments', 'view'),
	(1649, '61f747005c034f1efea470076aab448b', 572, 'sales_consignments', 'customer'),
	(1650, '248998e702c073a4f04d94907a8eb6b4', 572, 'sales_consignments', 'customer_ajax'),
	(1651, '3a980f57591deec1a01aaf2cd3b28c78', 572, 'sales_consignments', 'employee'),
	(1652, '9064f75618f38163da7f4bd6cdbc8590', 572, 'sales_consignments', 'employeeAjax'),
	(1653, 'dfc4cce4a46178743a7ad5c188836458', 573, 'sales_consignments', 'orderDetails'),
	(1654, 'fb6d6d78c5352f6b865b32e8d2799a6c', 580, 'sales_consignments', 'edit'),
	(1655, 'b7dd3ce6ff1c977adac6eb00cb680f9a', 580, 'sales_consignments', 'editDetail'),
	(1656, '60c68c6dc352c30712ab753c3ff1c870', 574, 'sales_consignments', 'aging'),
	(1657, '41be2ef464476aeeca3968730297b163', 574, 'sales_consignments', 'voidReceipt'),
	(1658, '59d7ac3ecf06624f1d7aa216b99b71d4', 574, 'sales_consignments', 'printReceipt'),
	(1659, '05819dff632ecfb2c900487d341cc798', 574, 'sales_consignments', 'printReceiptCurrent'),
	(1660, '4ebaf508e145a4e9aec7536b1c1791a5', 575, 'sales_consignments', 'void'),
	(1661, 'cda0c0e33b6124f8afb86332fc096c5b', 576, 'sales_consignments', 'miscellaneous'),
	(1662, '1c8a48cf647c9b353f137aa59c0795b4', 577, 'sales_consignments', 'service'),
	(1663, '0c460605c9c7a19f25992facb2aa21d2', 579, 'sales_consignments', 'discount'),
	(1664, '8cafbc13984c28df4fd9261b3ce46b71', 578, 'sales_consignments', 'printInvoice'),
	(1665, 'e4fd14ea9a96bda7612831b53d830767', 581, 'sales_consignments', 'editUnitPrice'),
	(1666, '9732e98f347bdf2b354b23bed8f8e682', 582, 'sales_consignments', 'viewByUser'),
	(1667, '49b2927161dc968c1345228c437f9d5d', 583, 'sales_consignments', 'pick'),
	(1668, 'bf3d604871bef974601d0d5ff2ceb198', 584, 'sales_consignments', 'invoiceDiscount'),
	(1669, '8166b672579a13d72c1d9d753282becc', 585, 'sales_consignments', 'editTermsCondition'),
	(1670, 'd7a03f7bee1f98d7618d5d5ed95f0bc0', 572, 'sales_consignments', 'getCustomerContact'),
	(1671, '0e53a8b97877745487e49f6eb1a701f6', 572, 'sales_consignments', 'customerCondition'),
	(1672, '863e7979b4421a924ce8c6bbbcf58ff8', 573, 'sales_consignments', 'add'),
	(1673, '9aebf214d3a101cfc4e26a52621f674c', 572, 'sales_consignments', 'productHistory'),
	(1674, '121b2c4adea9efafe61af6e924f6b39c', 572, 'sales_consignments', 'productHistoryAjax'),
	(1675, '941f5e37a0d07a9f700ba32167c4934a', 586, 'vendor_contacts', 'index'),
	(1676, 'd20627e4ff936461fcc0acf41ca8ef91', 586, 'vendor_contacts', 'ajax'),
	(1677, '5c16f224c51f91d31864fe23cf8e3f70', 586, 'vendor_contacts', 'view'),
	(1678, '488c7677488946c5c79aa6271726e868', 587, 'vendor_contacts', 'add'),
	(1679, '519ca053b0ba870e824bf2bcd70dd049', 588, 'vendor_contacts', 'edit'),
	(1680, '859946091222f1a68ceebcec471eb724', 589, 'vendor_contacts', 'delete'),
	(1681, 'c968c95641e8429da53a1078605f8dcb', 590, 'vendor_contacts', 'exportExcel'),
	(1682, 'e62d9115a01a1b43fc288b7adfe02fdb', 1, 'dashboards', 'getProductCache'),
	(1683, '40a9a2f9c051f2a67b0c5ab2e1a74056', 1, 'users', 'connection'),
	(1684, '72fac7733df34eda5bf8bb97a97749e8', 597, 'locations', 'viewProductLocation'),
	(1685, 'b5935e5325747c769363aa8da0803e70', 597, 'locations', 'viewProductLocationAjax'),
	(1686, '632e9e259f2146aa4a9da918576af0c8', 597, 'location_groups', 'viewProductWarehouse'),
	(1687, '6d7d660366dd43172169d1d1a3aef95c', 597, 'location_groups', 'viewProductWarehouseAjax'),
	(1688, '0db548bb9385e1aa48b04effbf835418', 594, 'warehouse_maps', 'index'),
	(1689, 'c32d3e85f51de8b323840d5aafdb2d8d', 594, 'warehouse_maps', 'viewLocationDetail'),
	(1690, '665a5e21db0657443493cf29bdc943fb', 594, 'warehouse_maps', 'viewProductWarehouse'),
	(1691, '6a49d0fc81e39d807951d2e080776cd0', 594, 'warehouse_maps', 'viewProductWarehouseAjax'),
	(1692, 'ba8c8d24e2085d5ba56181ce878b8e87', 594, 'warehouse_maps', 'viewProductLocation'),
	(1693, 'e764d9417b4aa1d4633220b67ec89a42', 594, 'warehouse_maps', 'viewProductLocationAjax'),
	(1694, '645ecc39a227c28c867def429c9f2388', 593, 'transfer_orders', 'approve'),
	(1695, 'ac3332636116f13e1cb8623cf9edb02e', 595, 'point_of_sales', 'quickAddCustomer'),
	(1696, '59fe0453eb9705288dff72638ed25115', 596, 'point_of_sales', 'quickAddProduct'),
	(1697, '1c630847576d39ff124dd764b0c7df73', 597, 'general_settings', 'index'),
	(1698, '805c24812d705e3ff1bec2b44dabad83', 597, 'general_settings', 'save'),
	(1699, '2b056b7843bb49da3498414dcb2159eb', 205, 'purchase_returns', 'purchaseBill'),
	(1700, '4fca40d7a89b39e3fef1aff6cd32d8cf', 205, 'purchase_returns', 'purchaseBillAjax'),
	(1701, 'c9a3112e1dc296c3037507aafc4f9fa5', 149, 'point_of_sales', 'checkStartShift'),
	(1702, '2b7f019aa40a837ca925a53b7e43760d', 149, 'point_of_sales', 'addShiftRegister'),
	(1703, 'd01ef419606efd43874c2fa95b17c38c', 149, 'point_of_sales', 'endShiftRegister'),
	(1704, '4c256b3779dff785df6d5b5ad2207f94', 149, 'point_of_sales', 'printShift'),
	(1705, 'bbe3ac9f16ee411ea6d167063878926f', 149, 'point_of_sales', 'saveAdjShiftRegister'),
	(1706, 'b1dbc7f53e111b8fee4e52531259fc60', 149, 'point_of_sales', 'checkAdjShiftRegister'),
	(1707, '43d6b8789472678db798609b94fa4f1b', 149, 'point_of_sales', 'getDataAdjShiftRegister'),
	(1708, '707052c78d15c1fe683f2b757f76428d', 598, 'shifts', 'index'),
	(1709, '6883bf9caa8ff7fd406bd8de647533d9', 598, 'shifts', 'ajax'),
	(1710, '356a74ec10cc4e6f8c392f79d8b9458f', 598, 'shifts', 'view'),
	(1711, 'ffa2f1a9b54371fe8abd19f9eff6baef', 599, 'shift_collects', 'index'),
	(1712, '6b4683b720f967805011a82938415159', 599, 'shift_collects', 'ajax'),
	(1713, '14c8c591e557c2dbc45972a8e5802370', 599, 'shift_collects', 'view'),
	(1714, '9c33d1f4929679fee41a55cbd07094b6', 599, 'shift_collects', 'save'),
	(1715, 'e5c8c28d30bdf9e617e1d492d645f322', 599, 'shift_collects', 'printShiftCollect'),
	(1716, '997bf3624489ede7f42c81ab0a1bab55', 600, 'reports', 'posShiftControl'),
	(1717, '4c1b838c24d1980528c11dcce0526e9d', 600, 'reports', 'posShiftControlAjax'),
	(1718, 'e6ca52c9f024800bf4c7db8c535bc974', 600, 'reports', 'posShiftControlResult'),
	(1719, 'd5d29fcbd46d5d634b44c7f2c8e92a32', 601, 'reports', 'posCollectShiftByUser'),
	(1720, '4e94bd321290f6760b15f54e7b48fc98', 601, 'reports', 'posCollectShiftByUserAjax'),
	(1721, '3ae66b7f6a8a1bf1689595a5ded0a8c6', 601, 'reports', 'posCollectShiftByUserResult'),
	(1722, 'a1a42d31b894f19d15ba2099057bc13e', 572, 'sales_consignments', 'consignment'),
	(1723, 'f6d16182aa5be679e0e949424fb31040', 572, 'sales_consignments', 'consignmentAjax'),
	(1724, 'addf755a64634f3040cf51dfde17930a', 572, 'sales_consignments', 'getProductFromConsignment'),
	(1725, 'c2e847ba3e117b9f81f10ddad1bb883d', 602, 'dashboards', 'viewTotalSales'),
	(1726, 'f11949ec632d0876d949c1acd567b520', 597, 'locations', 'printLayout'),
	(1727, 'f30e960c1e07b656c301a2e1ddb320d5', 597, 'general_settings', 'saveRecalculate'),
	(1728, '0c3f0b460d4a98bf969383b0b8a9272c', 603, 'colors', 'index'),
	(1729, '660629cf4ef82e880c26f31002210b16', 603, 'colors', 'ajax'),
	(1730, 'f81454ba4a91d54ab08e4ae3fe4efb81', 604, 'colors', 'add'),
	(1731, 'd0c4f1bddbd8ce8f9b9914409b86cedc', 605, 'colors', 'edit'),
	(1732, '2797f33392fdf6fde718406bb389d4ab', 606, 'colors', 'delete'),
	(1733, '4384c287f9a464b644062f05a1b6d898', 523, 'consignments', 'pickProduct'),
	(1734, '49e2bf0e11c7d8102d3e5cc0922069e4', 523, 'consignments', 'pickProductAjax'),
	(1735, '4c7dc1930f2eb2afe1eae189748cccd2', 523, 'consignments', 'pickProductSave'),
	(1736, '4d9951a7b5a764aa69b3a6bad85e5f2c', 572, 'sales_consignments', 'searchCustomer'),
	(1737, '06d25d38c4f0c4caa4317c86e3af6efc', 149, 'point_of_sales', 'getTotalQtyByLotExp'),
	(1738, '28b4a3e1be02178254d0dd470510e308', 607, 'expenses', 'index'),
	(1739, 'ac02380c55aa19a1f8da3cec8074071b', 607, 'expenses', 'ajax'),
	(1740, '1186ebaf6d883018fefec66a379801a6', 607, 'expenses', 'view'),
	(1741, 'ee63116f45b06f42248ddd1623a83b9f', 608, 'expenses', 'add'),
	(1742, '9d901b11b1f728f9cf84496d982b2b5f', 609, 'expenses', 'edit'),
	(1743, '8e044891bb6be72a317a39b4aaa23ed3', 610, 'expenses', 'delete'),
	(1744, 'ae1a3a6670461ac12f8c49d0eea35635', 607, 'expenses', 'customer'),
	(1745, 'a1aa190e59f724c958f6a132f6a0f9a3', 607, 'expenses', 'customerAjax'),
	(1746, '8ce13540a0d767e24d64d9d5d5d1e39c', 607, 'expenses', 'vendor'),
	(1747, 'a548927a71186df7dd1997d1bbd1b127', 607, 'expenses', 'vendorAjax'),
	(1748, '99b53129d44f66232993c9bcac88d51f', 105, 'sales_orders', 'getProductByExp'),
	(1749, '72fd9cc3aa1f3d9d6a76b3b52f2b6d9a', 205, 'purchase_returns', 'getProductByExp'),
	(1750, '7e7d460f281b412f154f9279c5593787', 61, 'products', 'addPgroup'),
	(1751, 'c17f5ffbc0c0a390d7de3beaa9b20d34', 19, 'products', 'addUom'),
	(1752, 'cc46790cd80983b2b99ff68b752454b2', 47, 'products', 'quickAdd'),
	(1753, 'd0dd81a55bd6f9c8f0bf8d4254a18f48', 194, 'vendors', 'addTerm'),
	(1754, '898ae25d5e1c47842af5e36a36da97df', 287, 'vendors', 'addVgroup'),
	(1755, 'fd040afa99e89cca29c1cc7e21d057e9', 73, 'vendors', 'quickAdd'),
	(1756, '488508f2585933920442f4b55d153509', 43, 'customers', 'addCgroup'),
	(1757, 'aa78861f7b826e2f5c7471565d714cba', 43, 'customers', 'addTerm'),
	(1758, '2ce738f8cfc6c06f8951b93e2895760c', 51, 'customers', 'quickAdd'),
	(1759, 'a3179f095839ac35ca47d7353ec45504', 343, 'credit_memos', 'addReason'),
	(1760, '44e802b8c11a77fe2b5cf684ce6d525a', 138, 'services', 'addSection'),
	(1761, '96787e832e49fae28de17add8e43c9cc', 611, 'dashboards', 'viewExpenseGraph'),
	(1762, '50139d0e045bec16b6811a5c2457dd3e', 612, 'dashboards', 'viewSalesTop10Graph'),
	(1763, 'f5a349a82c65817bcc3080e407455d41', 613, 'dashboards', 'viewProfitLoss'),
	(1764, 'e730a29b67b4b512dbc900642ffa8963', 614, 'dashboards', 'viewReceivable'),
	(1765, '577376b6f00c9908f461efff0d0e2537', 615, 'dashboards', 'viewPayable'),
	(1766, 'a3b2901329a08f1809a829feb88a8652', 596, 'point_of_sales', 'addPgroup'),
	(1767, '1aef061001e95c5b9d68157276028f61', 596, 'point_of_sales', 'addUom'),
	(1768, '2edf8fa88544522425a0576f0b70974e', 596, 'point_of_sales', 'getSkuUom'),
	(1769, '01f8dcec0861595e7b74614ba05dec9c', 595, 'point_of_sales', 'addTerm'),
	(1770, 'f77d152457e4c5788c00a51571d5e495', 595, 'point_of_sales', 'addCgroup'),
	(1771, 'b266b7a73c017e7a614f412eab39697c', 616, 'inventory_physicals', 'index'),
	(1772, 'e089679a0ceca039c7ccd1d22bfe6b8b', 616, 'inventory_physicals', 'ajax'),
	(1773, 'ceb2937b095f4c18e8f3957e46b814f8', 616, 'inventory_physicals', 'view'),
	(1774, '5e883ad14fcf96f949086644b40dc11e', 616, 'inventory_physicals', 'printInvoice'),
	(1775, 'f0b2ad88f5da7b791764fa969f59116f', 617, 'inventory_physicals', 'add'),
	(1776, 'a82384a518e26b4e91baa79ed491e033', 617, 'inventory_physicals', 'addDetail'),
	(1777, '3d1182a5df182de7b7e53f6c49948123', 618, 'inventory_physicals', 'edit'),
	(1778, 'c10d4a149fbeec5ff9c9e0f07c0baea3', 618, 'inventory_physicals', 'editDetail'),
	(1779, 'bba0996a13a78d935d347bd074430a00', 616, 'inventory_physicals', 'uom'),
	(1780, '46b0047b186c735581c3ec6598f982b5', 616, 'inventory_physicals', 'product'),
	(1781, '868f619192505a130116106726fa265b', 616, 'inventory_physicals', 'productAjax'),
	(1782, '2492f2d7c0c4e94d9984992bd4fff23f', 616, 'inventory_physicals', 'searchProduct'),
	(1783, '9801c4f924a93f8c4f65392a1e055611', 619, 'inventory_physicals', 'delete'),
	(1784, '7d780273d18cb13a93ad754d8cc0e324', 616, 'inventory_physicals', 'getTotalQtyOnHand'),
	(1785, '86a5f56b98dae03f84d31703a925bf5e', 617, 'inventory_physicals', 'save'),
	(1786, 'd107f506d015aeea80acd8271148c581', 618, 'inventory_physicals', 'saveEdit'),
	(1787, 'd156c2557fc8ef810ddf071f8d42528b', 620, 'inventory_physicals', 'approve'),
	(1788, '7bd2e99a7ad4c5a8cd338786c69c7293', 616, 'inventory_physicals', 'searchProductPuc'),
	(1789, 'b1b94c2c7e6dd163ffcfb1339f7de7a4', 616, 'inventory_physicals', 'searchProductSku'),
	(1790, '50653c81017bca30bebed36fc7ee6287', 621, 'point_of_sales', 'editPrice'),
	(1792, 'ae2d9531aeb657bd1b02f32df4271ee8', 622, 'brands', 'index'),
	(1793, 'ce32a61cb63fcf4b232f28ad6643f1e4', 622, 'brands', 'ajax'),
	(1794, '2d164fa8b4696bd206ae333713cda2ad', 622, 'brands', 'view'),
	(1795, '98dbabd8a368413d6241f5b2cc01fdf6', 623, 'brands', 'add'),
	(1796, '4894a90c3b0b7a517fd751db3d0bbfc0', 623, 'products', 'addBrand'),
	(1797, 'b1f9c2461e9fde7c1c5547dbece07a0c', 624, 'brands', 'edit'),
	(1798, '50812ab3993a44e433dc06d98b59a991', 624, 'brands', 'delete'),
	(1799, '47a8a3690e85f645d31bc26d1522d13f', 164, 'sales_orders', 'printInvoiceNoHead'),
	(1800, 'ad791fa6805cea500ca8867261eb4580', 46, 'products', 'viewProductInventory'),
	(1801, '4a0b8a383179104f20ef431cd909daf9', 149, 'point_of_sales', 'disByCard'),
	(1802, '7a6ea87b87421273a128369a3e6723ae', 1, 'users', 'checkInventoryPhysical'),
	(1803, '454f3fdd4c12ab87a8b9c0d7d0255249', 280, 'point_of_sales', 'discountByItem'),
	(1804, '496a04d5a6184a3aacbc8555aa8508ed', 626, 'other_incomes', 'index'),
	(1805, '7a0b46ac84284f5a370453c7f6c9aad0', 626, 'other_incomes', 'ajax'),
	(1806, '1183c6aff00a2705ffdd1857519eb6ab', 626, 'other_incomes', 'view'),
	(1807, '3efcd31eb3d1f9569205e7294f89e104', 626, 'other_incomes', 'customer'),
	(1808, '400ed1aed68055c7537db5017d5e02f3', 626, 'other_incomes', 'customerAjax'),
	(1809, '558189fd61456f7e1dc5bddbd709c975', 627, 'other_incomes', 'add'),
	(1810, 'f12e412d89f178b396a9b4921c59384c', 627, 'other_incomes', 'edit'),
	(1811, '716e1cea7d9c8dd6ed5ee636c0e71268', 627, 'other_incomes', 'delete'),
	(1812, '1df76d7a5372aae33dfc446e85222f37', 630, 'dashboards', 'viewReorderLevel'),
	(1813, '0d7a7ceb14091e104d3981ad669ac11d', 630, 'dashboards', 'viewReorderLevelAjax'),
	(1814, 'b57c78d0a1ecde65eb13a1ea12d2c471', 202, 'credit_memos', 'employee'),
	(1815, '3c65ea2ec6cf319004c4eed97584c762', 202, 'credit_memos', 'employeeAjax'),
	(1816, 'f81454ba4a91d54ab08e4ae3fe4efb81', 604, 'products', 'addColor'),
	(1817, 'c0549b30b1347e771eb5cdaca1592ae3', 631, 'dashboards', 'viewCustomerPaymentAlert'),
	(1818, '06caee0a978113fe704d082d65e6484d', 646, 'settings', 'config'),
	(1819, '06caee0a978113fe704d082d65e6484d', 646, 'settings', 'configSetting'),
	(1820, 'e764d9417b4aa1d4633220b67ec89a42', 41, 'settings', 'downloadTemplate'),
	(1821, 'e764d9417b4aa1d4633220b67ec89a42', 41, 'settings', 'import'),
	(1822, 'e764d9417b4aa1d4633220b67ec89a42', 41, 'settings', 'convertImportToDb'),
	(1823, 'd4415d9a902ea1c79a10b160cf7f9194', 634, 'reports', 'openBill'),
	(1824, 'd4415d9a902ea1c79a10b160cf7f9194', 634, 'reports', 'openBillResult'),
	(1825, '06caee0a978113fe704d082d65e6484d', 646, 'settings', 'saveCheckBox'),
	(1826, '06caee0a978113fe704d082d65e6484d', 626, 'settings', 'saveBaseCurrencySetting'),
	(1827, '06caee0a978113fe704d082d65e6484d', 626, 'settings', 'saveDecimalSetting'),
	(1828, '082cf319ea05cc2bdd444508ccb87877', 47, 'products', 'addService'),
	(1829, '082cf319ea05cc2bdd444508ccb87877', 48, 'products', 'editService'),
	(1830, '082cf319ea05cc2bdd444508ccb87877', 46, 'products', 'viewService'),
	(1831, '082cf319ea05cc2bdd444508ccb87877', 49, 'products', 'deleteService'),
	(1832, '082cf319ea05cc2bdd444508ccb87877', 136, 'products', 'saveServicePrice'),
	(1833, '082cf319ea05cc2bdd444508ccb87877', 635, 'reports', 'productList'),
	(1834, '082cf319ea05cc2bdd444508ccb87877', 635, 'reports', 'productListResult'),
	(1835, '082cf319ea05cc2bdd444508ccb87877', 635, 'reports', 'productListAjax'),
	(1836, '082cf319ea05cc2bdd444508ccb87877', 636, 'reports', 'customerList'),
	(1837, '082cf319ea05cc2bdd444508ccb87877', 636, 'reports', 'customerListResult'),
	(1838, '082cf319ea05cc2bdd444508ccb87877', 636, 'reports', 'customerListAjax'),
	(1839, '082cf319ea05cc2bdd444508ccb87877', 637, 'reports', 'vendorList'),
	(1840, '082cf319ea05cc2bdd444508ccb87877', 637, 'reports', 'vendorListResult'),
	(1841, '082cf319ea05cc2bdd444508ccb87877', 637, 'reports', 'vendorListAjax'),
	(1842, '06caee0a978113fe704d082d65e6484d', 1, 'dashboards', 'refreshMenu'),
	(1843, '06caee0a978113fe704d082d65e6484d', 626, 'settings', 'saveLockTransaction'),
	(1844, 'fb377bed0da7fa79dcbf3a529865913e', 1, 'reports', 'searchVgroup'),
	(1845, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'index'),
	(1846, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'ajax'),
	(1847, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'view'),
	(1848, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'add'),
	(1849, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'edit'),
	(1850, 'fb377bed0da7fa79dcbf3a529865913e', 504, 'pos_pay_methods', 'delete');
/*!40000 ALTER TABLE `module_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.module_introduces
CREATE TABLE IF NOT EXISTS `module_introduces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `controllers` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pages` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_en` mediumtext COLLATE utf8_unicode_ci,
  `description_kh` mediumtext COLLATE utf8_unicode_ci,
  `step` int(11) DEFAULT NULL,
  `element` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `searchs` (`controllers`,`pages`),
  KEY `filter` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.module_introduces: ~0 rows (approximately)
/*!40000 ALTER TABLE `module_introduces` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_introduces` ENABLE KEYS */;


-- Dumping structure for table sas_master.module_types
CREATE TABLE IF NOT EXISTS `module_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `group_by` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '0: Disabled; 1: Enable',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.module_types: ~36 rows (approximately)
/*!40000 ALTER TABLE `module_types` DISABLE KEYS */;
INSERT INTO `module_types` (`id`, `sys_code`, `name`, `ordering`, `group_by`, `status`) VALUES
	(1, '8cc5af3f1140170ad7644ce8a76f08f2', 'Home Page', 99, 'Home Page', 0),
	(13, '90cfbeb6b13c24d934d014bf9a2dcc7f', 'Product/Service', 101, 'Inventory', 1),
	(14, 'e09902c95769e7e24595839da3bada8e', 'Customer', 118, 'Sales', 1),
	(15, '24c2565f66e42e5d935005f01ed7fb83', 'Inventory Adjustment', 102, 'Inventory', 1),
	(21, 'ad5bd8caf59b36839c0501e26e005afb', 'Purchase Bill', 106, 'Purchase', 1),
	(22, '9b5762f0bf43cae9d63edffa1d47bd9e', 'Vendor', 111, 'Purchase', 1),
	(24, '79af7b6956c56fd3ab4fba61c881c8e9', 'Tranfer Order', 104, 'Inventory', 1),
	(25, 'f3295fe9125127b25b5ecbbf7f3c8220', 'Sales / Invoice', 114, 'Sales', 1),
	(28, '7ae7637951ef26e933f13913378df83e', 'Customer Balance', 120, 'Report', 1),
	(34, '84f2f52e8d9e21d7875431710248fee4', 'Point Of Sales', 119, 'POS', 1),
	(40, '26f7b013a5e4b002eb734e5d852c65a1', 'Sales Return', 117, 'Sales', 1),
	(41, '39ddf11c69d543a520f76f98fa353232', 'Purchase Return', 109, 'Purchase', 1),
	(42, '1346da6ef9f0d605bcdbdcac3b6fef41', 'Receive Payments', 116, 'Sales', 1),
	(43, 'eb8108897bd6a9ecc24d19b3796891bc', 'Pay Bills', 108, 'Purchase', 1),
	(48, '2c217515fae782d44381bc9cc7deaad9', 'Purchase Order', 105, 'Purchase', 1),
	(53, '3d55a930af07afa8ac7ae8e45a894e3a', 'Sales', 130, 'System Setting', 1),
	(68, 'd393a9d1fe246e5b7addc22e993117e6', 'Quotation', 112, 'Sales', 1),
	(69, '7bac24b73ffb34b49477cfeb08f5075b', 'Sales Order', 113, 'Sales', 1),
	(82, '25eecb3dde16f5048c9ea833469d46d6', 'General Setting', 132, 'System Setting', 1),
	(86, 'a398687363cb032d36cd367e5cf2c9b5', 'Purchase', 129, 'System Setting', 1),
	(87, 'edad8e0e53db30aed2fd1bf4adca28de', 'POS', 131, 'System Setting', 1),
	(103, '9f33de8fbd94cc76991057f32ffb028b', 'Inventory', 128, 'System Setting', 1),
	(107, 'fd79c930fba71e8a32822131dfb0f995', 'Cash Expense', 110, 'Purchase', 1),
	(108, '342485fb342726d2257631da4963b4eb', 'Dashboard', 100, 'Dashboard', 1),
	(109, '81a68724b8104248fe8c1e07aceb4ea0', 'Sales Mixes', 103, 'Inventory', 1),
	(111, 'ef78db65d3ea79f372fe7c2320a925c7', 'Accounting', 133, 'System Setting', 1),
	(115, 'f3295fe9125127b25b5ecbbf7f3c8220', 'Delivery Note', 115, 'Sales', 0),
	(116, 'ad5bd8caf59b36839c0501e26e005afb', 'Purchase Receive', 107, 'Purchase', 0),
	(117, '7ae7637951ef26e933f13913378df83e', 'Sales Transaction', 121, 'Report', 1),
	(118, '7ae7637951ef26e933f13913378df83e', 'Vendor Balance', 122, 'Report', 1),
	(119, '7ae7637951ef26e933f13913378df83e', 'Purchase Transaction', 123, 'Report', 1),
	(120, '7ae7637951ef26e933f13913378df83e', 'Stock/Inventory', 124, 'Report', 1),
	(121, '7ae7637951ef26e933f13913378df83e', 'Financial', 125, 'Report', 1),
	(122, '7ae7637951ef26e933f13913378df83e', 'List', 126, 'Report', 1),
	(123, '7ae7637951ef26e933f13913378df83e', 'User', 127, 'Report', 1),
	(124, '8cc5af3f1140170ad7644ce8a76f08f2', 'System Setting', 98, 'Home Page', 0);
/*!40000 ALTER TABLE `module_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_contact_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `quotation_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `discount` decimal(15,3) DEFAULT NULL,
  `discount_percent` decimal(6,3) DEFAULT NULL,
  `total_vat` decimal(15,3) DEFAULT NULL,
  `vat_percent` decimal(5,3) DEFAULT NULL,
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `total_deposit` decimal(15,3) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `approved` datetime DEFAULT NULL,
  `approved_by` bigint(20) DEFAULT NULL,
  `long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_close` tinyint(4) DEFAULT '0',
  `is_approve` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`quotation_number`,`order_code`,`order_date`,`is_close`,`status`),
  KEY `key_filter` (`company_id`,`customer_id`,`customer_contact_id`,`quotation_id`,`currency_center_id`,`vat_setting_id`,`price_type_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;


-- Dumping structure for table sas_master.order_details
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_cost` decimal(15,3) DEFAULT '0.000',
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.order_miscs
CREATE TABLE IF NOT EXISTS `order_miscs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.order_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.order_services
CREATE TABLE IF NOT EXISTS `order_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.order_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.order_term_conditions
CREATE TABLE IF NOT EXISTS `order_term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_search` (`order_id`,`term_condition_type_id`,`term_condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.order_term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.others
CREATE TABLE IF NOT EXISTS `others` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `other_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `business_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personal_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `other_code` (`other_code`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.others: ~0 rows (approximately)
/*!40000 ALTER TABLE `others` DISABLE KEYS */;
/*!40000 ALTER TABLE `others` ENABLE KEYS */;


-- Dumping structure for table sas_master.other_companies
CREATE TABLE IF NOT EXISTS `other_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `other_id` (`other_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.other_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `other_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.other_incomes
CREATE TABLE IF NOT EXISTS `other_incomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `reference` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued',
  PRIMARY KEY (`id`),
  KEY `filters` (`date`,`reference`),
  KEY `searchs` (`vendor_id`,`customer_id`,`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.other_incomes: ~0 rows (approximately)
/*!40000 ALTER TABLE `other_incomes` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_incomes` ENABLE KEYS */;


-- Dumping structure for table sas_master.other_income_details
CREATE TABLE IF NOT EXISTS `other_income_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `other_income_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `other_income_id` (`other_income_id`),
  KEY `chart_account_id` (`chart_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.other_income_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `other_income_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_income_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.payment_terms
CREATE TABLE IF NOT EXISTS `payment_terms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `net_days` int(11) DEFAULT NULL,
  `discount_percent` decimal(5,3) DEFAULT '0.000',
  `discount_days` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.payment_terms: ~1 rows (approximately)
/*!40000 ALTER TABLE `payment_terms` DISABLE KEYS */;
INSERT INTO `payment_terms` (`id`, `sys_code`, `name`, `net_days`, `discount_percent`, `discount_days`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '9372fb3f5384ecc646eb271109184f32', 'COD', 0, 0.000, NULL, '2016-08-11 15:28:18', 1, '2016-08-11 15:28:22', NULL, 1);
/*!40000 ALTER TABLE `payment_terms` ENABLE KEYS */;


-- Dumping structure for table sas_master.pay_bills
CREATE TABLE IF NOT EXISTS `pay_bills` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`location_id`,`vendor_id`),
  KEY `searchs` (`date`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pay_bills: ~0 rows (approximately)
/*!40000 ALTER TABLE `pay_bills` DISABLE KEYS */;
/*!40000 ALTER TABLE `pay_bills` ENABLE KEYS */;


-- Dumping structure for table sas_master.pay_bill_details
CREATE TABLE IF NOT EXISTS `pay_bill_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pay_bill_id` bigint(20) DEFAULT NULL,
  `purchase_order_id` bigint(20) DEFAULT NULL,
  `amount_due` decimal(15,3) DEFAULT '0.000',
  `paid` decimal(15,3) DEFAULT '0.000',
  `balance` decimal(15,3) DEFAULT '0.000',
  `due_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters` (`pay_bill_id`,`purchase_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pay_bill_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `pay_bill_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `pay_bill_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id_module_id` (`group_id`,`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1153 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.permissions: ~178 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `group_id`, `module_id`) VALUES
	(975, 1, 1),
	(1096, 1, 41),
	(976, 1, 46),
	(977, 1, 47),
	(978, 1, 48),
	(979, 1, 49),
	(986, 1, 50),
	(987, 1, 51),
	(988, 1, 52),
	(989, 1, 53),
	(991, 1, 55),
	(1007, 1, 72),
	(1008, 1, 73),
	(1009, 1, 74),
	(1010, 1, 75),
	(992, 1, 88),
	(997, 1, 91),
	(998, 1, 92),
	(999, 1, 94),
	(1012, 1, 101),
	(1013, 1, 102),
	(1014, 1, 103),
	(1015, 1, 104),
	(1017, 1, 105),
	(1018, 1, 106),
	(1019, 1, 107),
	(1020, 1, 108),
	(1144, 1, 119),
	(1137, 1, 133),
	(980, 1, 136),
	(1000, 1, 145),
	(1021, 1, 148),
	(1034, 1, 149),
	(1035, 1, 151),
	(1036, 1, 152),
	(1029, 1, 153),
	(1130, 1, 154),
	(1022, 1, 164),
	(1145, 1, 166),
	(1146, 1, 167),
	(1119, 1, 169),
	(1150, 1, 170),
	(1151, 1, 171),
	(1023, 1, 181),
	(1037, 1, 182),
	(1120, 1, 192),
	(1138, 1, 197),
	(1133, 1, 198),
	(1001, 1, 201),
	(1044, 1, 202),
	(1045, 1, 203),
	(1046, 1, 204),
	(1054, 1, 205),
	(1055, 1, 206),
	(1056, 1, 207),
	(1061, 1, 209),
	(1062, 1, 210),
	(1024, 1, 211),
	(1121, 1, 212),
	(1134, 1, 213),
	(1030, 1, 214),
	(1131, 1, 215),
	(1047, 1, 216),
	(1057, 1, 218),
	(1048, 1, 220),
	(1049, 1, 221),
	(1058, 1, 222),
	(1059, 1, 223),
	(1038, 1, 226),
	(1122, 1, 230),
	(1135, 1, 232),
	(1123, 1, 239),
	(1031, 1, 240),
	(1124, 1, 242),
	(1139, 1, 243),
	(993, 1, 246),
	(1140, 1, 251),
	(1063, 1, 253),
	(1064, 1, 254),
	(1065, 1, 255),
	(1066, 1, 256),
	(1141, 1, 257),
	(1142, 1, 258),
	(1050, 1, 259),
	(1067, 1, 260),
	(1143, 1, 264),
	(1125, 1, 265),
	(1032, 1, 266),
	(1136, 1, 268),
	(1039, 1, 280),
	(1071, 1, 282),
	(1025, 1, 290),
	(1051, 1, 291),
	(990, 1, 306),
	(1011, 1, 309),
	(981, 1, 313),
	(1052, 1, 315),
	(1026, 1, 316),
	(1002, 1, 317),
	(1060, 1, 318),
	(1016, 1, 320),
	(994, 1, 321),
	(995, 1, 322),
	(996, 1, 323),
	(1040, 1, 328),
	(1068, 1, 329),
	(1072, 1, 374),
	(1073, 1, 375),
	(1074, 1, 376),
	(1075, 1, 377),
	(1076, 1, 378),
	(1077, 1, 379),
	(1078, 1, 380),
	(1084, 1, 381),
	(1085, 1, 382),
	(1086, 1, 383),
	(1087, 1, 384),
	(1088, 1, 385),
	(1089, 1, 386),
	(1090, 1, 387),
	(1027, 1, 394),
	(1053, 1, 396),
	(1069, 1, 397),
	(1003, 1, 398),
	(1079, 1, 417),
	(1091, 1, 419),
	(1080, 1, 421),
	(1092, 1, 422),
	(1093, 1, 423),
	(1081, 1, 424),
	(982, 1, 425),
	(1070, 1, 426),
	(1082, 1, 453),
	(1094, 1, 459),
	(1028, 1, 461),
	(1004, 1, 465),
	(1005, 1, 466),
	(1083, 1, 467),
	(1006, 1, 468),
	(1095, 1, 483),
	(1126, 1, 489),
	(983, 1, 490),
	(1127, 1, 491),
	(1033, 1, 494),
	(1104, 1, 499),
	(1097, 1, 503),
	(1098, 1, 504),
	(984, 1, 567),
	(985, 1, 568),
	(1041, 1, 595),
	(1042, 1, 596),
	(1099, 1, 597),
	(1128, 1, 600),
	(1129, 1, 601),
	(1105, 1, 602),
	(1100, 1, 607),
	(1101, 1, 608),
	(1102, 1, 609),
	(1103, 1, 610),
	(1106, 1, 611),
	(1107, 1, 612),
	(1108, 1, 613),
	(1109, 1, 614),
	(1110, 1, 615),
	(1113, 1, 616),
	(1114, 1, 617),
	(1115, 1, 618),
	(1116, 1, 619),
	(1117, 1, 620),
	(1043, 1, 621),
	(1118, 1, 626),
	(1111, 1, 630),
	(1112, 1, 631),
	(1132, 1, 634),
	(1147, 1, 635),
	(1148, 1, 636),
	(1149, 1, 637),
	(1152, 1, 646);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;


-- Dumping structure for table sas_master.pgroups
CREATE TABLE IF NOT EXISTS `pgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(10) DEFAULT NULL,
  `user_apply` tinyint(4) DEFAULT '0',
  `ics_apply_sub` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `parent_id` (`parent_id`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `pgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `pgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.pgroup_accounts
CREATE TABLE IF NOT EXISTS `pgroup_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pgroup_id` int(11) NOT NULL,
  `account_type_id` int(11) NOT NULL,
  `chart_account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pgroup_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `pgroup_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `pgroup_accounts` ENABLE KEYS */;


-- Dumping structure for table sas_master.pgroup_companies
CREATE TABLE IF NOT EXISTS `pgroup_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pgroup_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pgroup_id` (`pgroup_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pgroup_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `pgroup_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `pgroup_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.places
CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.places: ~0 rows (approximately)
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
/*!40000 ALTER TABLE `places` ENABLE KEYS */;


-- Dumping structure for table sas_master.positions
CREATE TABLE IF NOT EXISTS `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `searchs` (`name`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.positions: ~0 rows (approximately)
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;


-- Dumping structure for table sas_master.pos_pay_methods
CREATE TABLE IF NOT EXISTS `pos_pay_methods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_of_account_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pos_pay_methods: ~1 rows (approximately)
/*!40000 ALTER TABLE `pos_pay_methods` DISABLE KEYS */;
INSERT INTO `pos_pay_methods` (`id`, `sys_code`, `name`, `chart_of_account_id`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, NULL, 'Cash', 1, '2018-10-02 08:25:06', 1, '2018-10-02 08:25:08', NULL, 1);
/*!40000 ALTER TABLE `pos_pay_methods` ENABLE KEYS */;


-- Dumping structure for table sas_master.pos_pick_details
CREATE TABLE IF NOT EXISTS `pos_pick_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_order_detail_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_id` (`sales_order_id`),
  KEY `location_id` (`location_id`),
  KEY `product_id` (`product_id`),
  KEY `sales_order_detail_id` (`sales_order_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pos_pick_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `pos_pick_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_pick_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.pos_price_types
CREATE TABLE IF NOT EXISTS `pos_price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`company_id`,`price_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pos_price_types: ~1 rows (approximately)
/*!40000 ALTER TABLE `pos_price_types` DISABLE KEYS */;
INSERT INTO `pos_price_types` (`id`, `company_id`, `price_type_id`, `created`, `created_by`, `is_active`) VALUES
	(1, 1, 3, '2017-08-23 15:49:07', 1, 1);
/*!40000 ALTER TABLE `pos_price_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.price_types
CREATE TABLE IF NOT EXISTS `price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` int(11) DEFAULT NULL,
  `is_set` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` tinyint(4) DEFAULT NULL,
  `is_ecommerce` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`),
  KEY `is_set` (`is_set`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.price_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `price_types` DISABLE KEYS */;
INSERT INTO `price_types` (`id`, `sys_code`, `name`, `ordering`, `is_set`, `created`, `created_by`, `modified`, `modified_by`, `is_ecommerce`, `is_active`) VALUES
	(1, '1fdc275e4fd22798c821a3098d56c69c', 'E-Commerce', 0, 1, '2017-03-23 20:09:35', 1, '2017-03-25 09:24:32', 1, 1, 1),
	(2, '7d35f435976aedd4f536cb5e43a2b434', 'Wholesale', 1, 1, '2017-07-21 10:53:30', 1, '2017-10-06 13:47:20', 1, 0, 1),
	(3, '56b444f1a5680d214249472b24fbe1b3', 'Retail Price', 2, 1, '2017-07-21 10:53:46', 1, '2017-08-23 15:49:07', 1, 0, 1);
/*!40000 ALTER TABLE `price_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.price_type_companies
CREATE TABLE IF NOT EXISTS `price_type_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`),
  KEY `price_type_id` (`price_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.price_type_companies: ~3 rows (approximately)
/*!40000 ALTER TABLE `price_type_companies` DISABLE KEYS */;
INSERT INTO `price_type_companies` (`id`, `company_id`, `price_type_id`) VALUES
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3);
/*!40000 ALTER TABLE `price_type_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.printers
CREATE TABLE IF NOT EXISTS `printers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `printer_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `silent` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`module_name`,`printer_name`),
  KEY `filters` (`module_name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.printers: ~0 rows (approximately)
/*!40000 ALTER TABLE `printers` DISABLE KEYS */;
/*!40000 ALTER TABLE `printers` ENABLE KEYS */;


-- Dumping structure for table sas_master.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT '1',
  `brand_id` int(11) DEFAULT NULL,
  `photo` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_kh` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spec` text COLLATE utf8_unicode_ci,
  `color_id` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `default_cost` decimal(18,9) NOT NULL DEFAULT '0.000000000',
  `unit_cost` decimal(18,9) NOT NULL DEFAULT '0.000000000',
  `unit_price` decimal(15,5) NOT NULL DEFAULT '0.00000',
  `price_uom_id` int(11) DEFAULT '2',
  `small_val_uom` int(11) NOT NULL DEFAULT '1',
  `width` double DEFAULT NULL,
  `height` double DEFAULT NULL,
  `length` double DEFAULT NULL,
  `size_uom_id` decimal(23,8) DEFAULT NULL COMMENT 'metre',
  `cubic_meter` decimal(23,8) DEFAULT NULL COMMENT 'Square Metre',
  `weight` double DEFAULT NULL,
  `weight_uom_id` decimal(23,8) DEFAULT NULL COMMENT 'yard',
  `reorder_level` double DEFAULT NULL,
  `period_from` date DEFAULT NULL,
  `period_to` date DEFAULT NULL,
  `file_catalog` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Product; 2: Service',
  `is_expired_date` tinyint(4) NOT NULL DEFAULT '0',
  `is_not_for_sale` tinyint(4) NOT NULL DEFAULT '0',
  `is_packet` tinyint(4) NOT NULL DEFAULT '0',
  `is_lots` tinyint(4) NOT NULL DEFAULT '0',
  `is_warranty` tinyint(4) NOT NULL DEFAULT '0',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`code`,`barcode`,`name`,`name_kh`),
  KEY `filters` (`company_id`,`is_active`),
  KEY `type` (`type`,`is_expired_date`,`is_lots`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_branches
CREATE TABLE IF NOT EXISTS `product_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_branches: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_branches` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_branches` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_inventories
CREATE TABLE IF NOT EXISTS `product_inventories` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `location_group_id` int(11) NOT NULL DEFAULT '0',
  `location_id` int(11) NOT NULL DEFAULT '0',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `expired_date` date NOT NULL,
  `total_qty` decimal(15,3) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`location_group_id`,`location_id`,`lots_number`,`expired_date`),
  KEY `products` (`product_id`,`location_group_id`,`location_id`,`lots_number`,`expired_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_inventories: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_inventories` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_inventories` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_inventory_tmp1
CREATE TABLE IF NOT EXISTS `product_inventory_tmp1` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `total_qty` decimal(15,3) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_inventory_tmp1: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_inventory_tmp1` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_inventory_tmp1` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_pgroups
CREATE TABLE IF NOT EXISTS `product_pgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) DEFAULT NULL,
  `pgroup_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id_pgroup_id` (`product_id`,`pgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_pgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_pgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_pgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_photos
CREATE TABLE IF NOT EXISTS `product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_photos: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_photos` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_prices
CREATE TABLE IF NOT EXISTS `product_prices` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `product_id` bigint(20) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `old_unit_cost` decimal(15,3) DEFAULT '0.000',
  `amount_before` decimal(15,3) DEFAULT '0.000',
  `amount` decimal(15,3) DEFAULT '0.000',
  `percent` decimal(6,3) DEFAULT '0.000',
  `add_on` decimal(15,3) DEFAULT '0.000',
  `set_type` tinyint(4) DEFAULT '0' COMMENT '1: Amount, 2: Percent, 3: Add On',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `uom_id` (`uom_id`),
  KEY `price_type_id` (`price_type_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_prices: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_prices` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_price_histories
CREATE TABLE IF NOT EXISTS `product_price_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `price_type_id` int(11) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `unit_cost` decimal(15,3) DEFAULT NULL,
  `old_amount_before` decimal(15,3) DEFAULT NULL,
  `old_amount` decimal(15,3) DEFAULT NULL,
  `old_percent` decimal(6,3) DEFAULT NULL,
  `old_add_on` decimal(15,3) DEFAULT NULL,
  `old_set_type` tinyint(4) DEFAULT NULL,
  `new_amount_before` decimal(15,3) DEFAULT NULL,
  `new_amount` decimal(15,3) DEFAULT NULL,
  `new_percent` decimal(6,3) DEFAULT NULL,
  `new_add_on` decimal(15,3) DEFAULT NULL,
  `new_set_type` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters` (`product_id`,`price_type_id`,`uom_id`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_price_histories: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_price_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_price_histories` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_unit_cost_histories
CREATE TABLE IF NOT EXISTS `product_unit_cost_histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` bigint(20) NOT NULL DEFAULT '0',
  `product_id` bigint(20) DEFAULT NULL,
  `old_cost` decimal(18,9) DEFAULT NULL,
  `new_cost` decimal(18,9) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_order_id_product_id` (`purchase_order_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sas_master.product_unit_cost_histories: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_unit_cost_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_unit_cost_histories` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_with_customers
CREATE TABLE IF NOT EXISTS `product_with_customers` (
  `product_id` bigint(20) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`customer_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_with_customers: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_with_customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_with_customers` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_with_packets
CREATE TABLE IF NOT EXISTS `product_with_packets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `main_product_id` int(11) DEFAULT NULL,
  `packet_product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `main_product_id_packet_product_id` (`main_product_id`,`packet_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_with_packets: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_with_packets` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_with_packets` ENABLE KEYS */;


-- Dumping structure for table sas_master.product_with_skus
CREATE TABLE IF NOT EXISTS `product_with_skus` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) DEFAULT NULL,
  `sku` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uom_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `sku` (`sku`),
  KEY `uom_id` (`uom_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.product_with_skus: ~0 rows (approximately)
/*!40000 ALTER TABLE `product_with_skus` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_with_skus` ENABLE KEYS */;


-- Dumping structure for table sas_master.promotionals
CREATE TABLE IF NOT EXISTS `promotionals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `cgroup_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `approved` datetime DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Disapprove; 0: Void; 1: Request; 2: Approved',
  PRIMARY KEY (`id`),
  KEY `period` (`start`,`end`),
  KEY `status` (`status`),
  KEY `filters` (`date`,`cgroup_id`,`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.promotionals: ~0 rows (approximately)
/*!40000 ALTER TABLE `promotionals` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotionals` ENABLE KEYS */;


-- Dumping structure for table sas_master.promotional_details
CREATE TABLE IF NOT EXISTS `promotional_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotional_id` int(11) DEFAULT NULL,
  `product_request_id` int(11) DEFAULT NULL,
  `qty_request` decimal(15,3) DEFAULT NULL,
  `uom_request` int(11) DEFAULT NULL,
  `product_promo_id` int(11) DEFAULT NULL,
  `qty_promo` decimal(15,3) DEFAULT NULL,
  `uom_promo` int(11) DEFAULT NULL,
  `discount_percent` decimal(7,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promotional_id` (`promotional_id`),
  KEY `products` (`product_request_id`,`product_promo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.promotional_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `promotional_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `promotional_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.provinces
CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abbr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.provinces: ~25 rows (approximately)
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` (`id`, `sys_code`, `name`, `abbr`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(26, 'f1b57977c0471a4ebf078a28a7b79eaf', 'PHNOM PENH', 'PNH', '2014-12-03 11:27:44', 1, '2014-12-03 11:27:44', NULL, 1),
	(27, '2cfaa750990b70acb718e9a7ea28e4be', 'BANTEAY MEANCHEY', 'BMC', '2014-12-03 11:27:58', 1, '2014-12-03 11:27:58', NULL, 1),
	(28, '474c5eaacd210a5d5024f9a47b15da2f', 'BATTAMBANG', 'BDB', '2014-12-03 11:28:09', 1, '2014-12-03 11:28:09', NULL, 1),
	(29, '047fca4a002d628e934b814282c11d2f', 'KAMPONG CHAM', 'KPC', '2014-12-03 11:28:39', 1, '2014-12-03 11:28:39', NULL, 1),
	(30, '3bc73a7b2937898ee2a40d89fd21f939', 'KAMPONG CHHNANG', 'KCH', '2014-12-03 11:28:51', 1, '2014-12-03 11:28:51', NULL, 1),
	(31, '06fa61d3f49302ad9cb59405efd4890c', 'KAMPONG SPEU', 'KPS', '2014-12-03 11:29:08', 1, '2014-12-03 11:29:08', NULL, 1),
	(32, 'faee449b5e53c3553ea6275e848b1c13', 'KAMPONG THOM', 'KPT', '2014-12-03 11:29:19', 1, '2014-12-03 11:29:19', NULL, 1),
	(33, 'bf11ce1a07ee94c363f47b22d9af86af', 'KAMPOT', 'KAP', '2014-12-03 11:29:39', 1, '2014-12-03 11:29:39', NULL, 1),
	(34, '8d0dda52f7b41751d1bc407548504c69', 'KANDAL', 'KND', '2014-12-03 11:29:51', 1, '2014-12-03 11:29:51', NULL, 1),
	(35, 'ba7fe9da8d7baa44eed4dd9456b894e1', 'KEP', 'KEP', '2014-12-03 11:30:02', 1, '2014-12-03 11:30:02', NULL, 1),
	(36, 'f62b6c848a68479c8538f6ae11fb6579', 'KRATIE', 'KRT', '2014-12-03 11:30:12', 1, '2014-12-03 11:30:12', NULL, 1),
	(37, 'cd34420e5e22c5f3ed08fc7c5ed76c1d', 'MONDULKIRI', 'MKR', '2014-12-03 11:30:22', 1, '2014-12-03 11:30:22', NULL, 1),
	(38, '251e199f12ff121c941df156978b54e7', 'ODDAR MEANCHEY', 'OMC', '2014-12-03 11:30:35', 1, '2014-12-03 11:30:35', NULL, 1),
	(39, '87e6d5327a2c2814de4cdca259b89ad8', 'PAILIN', 'PAL', '2014-12-03 11:30:45', 1, '2014-12-03 11:30:45', NULL, 1),
	(40, '69b6a0a2da0bd20859fd864f933c6705', 'PREAH SIHANOUK', 'SHV', '2014-12-03 11:30:55', 1, '2014-12-03 11:30:55', NULL, 1),
	(41, '73f129ac5009cff04d42e1aa01f525b7', 'PREAH VIHEAR', 'PVH', '2014-12-03 11:31:10', 1, '2014-12-03 11:31:10', NULL, 1),
	(42, '42a446e138f4529811e55c4c84571c86', 'PURSAT', 'PUR', '2014-12-03 11:31:23', 1, '2014-12-03 11:31:23', NULL, 1),
	(43, 'a9f666fe8e2e715c7df754ca6447307e', 'PREY VENG', 'PRV', '2014-12-03 11:31:44', 1, '2014-12-03 11:31:44', NULL, 1),
	(44, '081e97d0ddca6298ae77a2075028e12a', 'RATANAKIRI', 'RKR', '2014-12-03 11:31:58', 1, '2014-12-03 11:31:58', NULL, 1),
	(45, '51ad012d8a57bed387a488fcd8fb593d', 'SIEM REAP', 'SMR', '2014-12-03 11:32:11', 1, '2014-12-03 11:32:11', NULL, 1),
	(46, 'a6e784004da8c809d3396e8b1ee26885', 'STUNG TRENG', 'STG', '2014-12-03 11:32:19', 1, '2014-12-03 11:32:19', NULL, 1),
	(47, 'b68265acacd680610e6191c36821a1aa', 'SVAY RIENG', 'SVR', '2014-12-03 11:32:30', 1, '2014-12-03 11:32:30', NULL, 1),
	(48, 'f32b1fea1b7b21939c070ff181ee6a00', 'TAKEO', 'TKE', '2014-12-03 11:32:40', 1, '2014-12-03 11:32:40', NULL, 1),
	(49, '760148e8d94332593ac6d0cde9ad6a1c', 'TBONG KHMUM', 'TBK', '2014-12-03 11:32:51', 1, '2014-12-03 11:32:51', NULL, 1),
	(50, 'a3a4ef3d55b3071c80ccf1f3b62c197c', 'KOH KONG', 'KOK', '2014-12-03 11:32:59', 1, '2014-12-03 11:32:59', NULL, 1);
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_orders
CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_request_id` int(11) DEFAULT NULL,
  `vendor_consignment_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `ap_id` int(11) DEFAULT NULL,
  `po_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `total_amount` decimal(15,9) DEFAULT NULL,
  `total_deposit` decimal(15,9) DEFAULT NULL,
  `discount_amount` decimal(15,9) DEFAULT NULL,
  `discount_percent` decimal(7,3) DEFAULT NULL,
  `balance` decimal(15,9) DEFAULT NULL,
  `vat_chart_account_id` int(11) DEFAULT NULL,
  `total_vat` decimal(15,9) DEFAULT NULL,
  `vat_percent` decimal(7,3) DEFAULT NULL,
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `principal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_deposit_reference` tinyint(4) DEFAULT '0',
  `is_update_cost` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_filter_second` (`purchase_request_id`,`ap_id`),
  KEY `key_search` (`po_code`,`invoice_code`,`invoice_date`,`balance`,`order_date`,`created_by`,`modified_by`,`status`),
  KEY `key_filters` (`company_id`,`branch_id`,`location_group_id`,`location_id`,`vendor_id`,`currency_center_id`,`vat_chart_account_id`,`vat_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_orders` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_order_details
CREATE TABLE IF NOT EXISTS `purchase_order_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_id` int(10) unsigned DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,9) DEFAULT '0.000000000',
  `discount_percent` decimal(7,3) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `max_order` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `default_cost` decimal(15,9) NOT NULL DEFAULT '0.000000000',
  `new_unit_cost` decimal(15,9) NOT NULL DEFAULT '0.000000000',
  `unit_cost` decimal(15,9) DEFAULT '0.000000000',
  `total_cost` decimal(15,9) DEFAULT '0.000000000',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`),
  KEY `discount_id` (`discount_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_order_miscs
CREATE TABLE IF NOT EXISTS `purchase_order_miscs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(10) unsigned DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,9) DEFAULT '0.000000000',
  `discount_percent` decimal(7,3) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(10) DEFAULT NULL,
  `unit_cost` decimal(15,9) DEFAULT '0.000000000',
  `total_cost` decimal(15,9) DEFAULT '0.000000000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`),
  KEY `discount_id` (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_order_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_order_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_order_services
CREATE TABLE IF NOT EXISTS `purchase_order_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `discount_id` int(10) DEFAULT NULL,
  `discount_amount` decimal(15,9) DEFAULT '0.000000000',
  `discount_percent` decimal(7,3) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `unit_cost` decimal(15,9) DEFAULT '0.000000000',
  `total_cost` decimal(15,9) DEFAULT '0.000000000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`),
  KEY `service_id` (`service_id`),
  KEY `discount_id` (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_order_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_order_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_order_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_receives
CREATE TABLE IF NOT EXISTS `purchase_receives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_receive_result_id` int(10) unsigned DEFAULT NULL,
  `purchase_order_id` int(10) unsigned DEFAULT NULL,
  `purchase_order_detail_id` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `received_date` date DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_expired` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_filter` (`purchase_receive_result_id`,`purchase_order_id`,`purchase_order_detail_id`(255),`status`),
  KEY `key_search` (`product_id`,`qty_uom_id`,`received_date`,`created_by`,`modified_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_receive_results
CREATE TABLE IF NOT EXISTS `purchase_receive_results` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_id` int(11) unsigned DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_receive_results: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_receive_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_receive_results` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_requests
CREATE TABLE IF NOT EXISTS `purchase_requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `pr_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `currency_center_id` int(11) NOT NULL,
  `ref_quotation` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `port_of_dischange_id` int(11) DEFAULT NULL,
  `final_place_of_delivery_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,9) NOT NULL DEFAULT '0.000000000',
  `total_deposit` decimal(15,9) DEFAULT '0.000000000',
  `total_vat` decimal(15,9) NOT NULL DEFAULT '0.000000000',
  `vat_percent` decimal(7,3) NOT NULL,
  `vat_setting_id` int(11) NOT NULL,
  `vat_calculate` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_close` tinyint(4) DEFAULT '0',
  `status` tinyint(50) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_filter_second` (`shipment_id`,`port_of_dischange_id`,`final_place_of_delivery_id`),
  KEY `key_search` (`pr_code`,`order_date`,`created_by`,`modified_by`,`is_close`,`status`),
  KEY `filter_first` (`company_id`,`branch_id`,`vendor_id`,`currency_center_id`,`vat_setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_requests: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_requests` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_request_details
CREATE TABLE IF NOT EXISTS `purchase_request_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_request_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `unit_cost` decimal(15,9) DEFAULT '0.000000000',
  `total_cost` decimal(15,9) DEFAULT '0.000000000',
  `note` text COLLATE utf8_unicode_ci,
  `is_close` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purchase_request_id` (`purchase_request_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_request_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_request_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_request_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_request_services
CREATE TABLE IF NOT EXISTS `purchase_request_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `purchase_request_id` bigint(20) DEFAULT NULL,
  `service_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `unit_cost` decimal(15,9) DEFAULT '0.000000000',
  `total_cost` decimal(15,9) DEFAULT '0.000000000',
  `note` text COLLATE utf8_unicode_ci,
  `is_close` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purchase_request_id` (`purchase_request_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_request_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_request_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_request_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_request_term_conditions
CREATE TABLE IF NOT EXISTS `purchase_request_term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_request_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_search` (`purchase_request_id`,`term_condition_type_id`,`term_condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_request_term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_request_term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_request_term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_returns
CREATE TABLE IF NOT EXISTS `purchase_returns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `ap_id` int(11) DEFAULT NULL,
  `pr_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_amount` decimal(15,9) DEFAULT NULL,
  `total_amount_po` decimal(15,9) DEFAULT NULL,
  `balance` decimal(15,9) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `vat_chart_account_id` int(11) DEFAULT NULL,
  `total_vat` decimal(15,9) DEFAULT NULL,
  `vat_percent` decimal(7,3) DEFAULT NULL,
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`pr_code`,`balance`,`order_date`,`created_by`,`modified_by`,`status`),
  KEY `key_filter` (`company_id`,`location_group_id`,`location_id`,`vendor_id`,`currency_center_id`,`ap_id`,`vat_chart_account_id`,`vat_setting_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_returns: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_returns` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_return_details
CREATE TABLE IF NOT EXISTS `purchase_return_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_return_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `unit_price` decimal(15,9) DEFAULT '0.000000000',
  `total_price` decimal(15,9) DEFAULT '0.000000000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_return_id` (`purchase_return_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_return_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_return_miscs
CREATE TABLE IF NOT EXISTS `purchase_return_miscs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(10) DEFAULT NULL,
  `unit_price` decimal(15,9) DEFAULT '0.000000000',
  `total_price` decimal(15,9) DEFAULT '0.000000000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_return_id` (`purchase_return_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_return_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_return_receipts
CREATE TABLE IF NOT EXISTS `purchase_return_receipts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_return_id` int(10) unsigned DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `receipt_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_us` decimal(15,9) DEFAULT '0.000000000',
  `amount_other` decimal(15,9) DEFAULT '0.000000000',
  `total_amount` decimal(15,9) DEFAULT '0.000000000',
  `balance` decimal(15,9) DEFAULT '0.000000000',
  `balance_other` decimal(15,9) DEFAULT '0.000000000',
  `change` decimal(15,9) DEFAULT NULL,
  `pay_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_void` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purchase_return_id` (`purchase_return_id`),
  KEY `receipt_code` (`receipt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_return_receipts: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_receipts` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_return_receives
CREATE TABLE IF NOT EXISTS `purchase_return_receives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int(10) unsigned DEFAULT NULL,
  `purchase_return_detail_id` int(10) unsigned DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_return_id` (`purchase_return_id`),
  KEY `purchase_return_detail_id` (`purchase_return_detail_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_return_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.purchase_return_services
CREATE TABLE IF NOT EXISTS `purchase_return_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_return_id` int(10) unsigned DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `unit_price` decimal(15,9) DEFAULT '0.000000000',
  `total_price` decimal(15,9) DEFAULT '0.000000000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_return_id` (`purchase_return_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.purchase_return_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `purchase_return_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_return_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.pvs
CREATE TABLE IF NOT EXISTS `pvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `purchase_order_id` int(10) unsigned DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `pv_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_us` decimal(15,9) DEFAULT '0.000000000',
  `amount_other` decimal(15,9) DEFAULT '0.000000000',
  `total_amount` decimal(15,9) DEFAULT '0.000000000',
  `discount` decimal(15,9) DEFAULT '0.000000000',
  `discount_other` decimal(15,9) DEFAULT '0.000000000',
  `balance` decimal(15,9) DEFAULT '0.000000000',
  `balance_other` decimal(15,9) DEFAULT '0.000000000',
  `pay_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_void` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `purchase_order_id` (`purchase_order_id`),
  KEY `pv_code` (`pv_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.pvs: ~0 rows (approximately)
/*!40000 ALTER TABLE `pvs` DISABLE KEYS */;
/*!40000 ALTER TABLE `pvs` ENABLE KEYS */;


-- Dumping structure for table sas_master.quotations
CREATE TABLE IF NOT EXISTS `quotations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_contact_id` int(11) DEFAULT NULL,
  `quotation_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `total_vat` decimal(15,3) DEFAULT NULL,
  `vat_percent` decimal(5,3) DEFAULT NULL,
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` tinyint(4) DEFAULT NULL COMMENT '1: Before Discount, Mark Up; 2: After Discount, Mark Up',
  `discount` decimal(15,3) DEFAULT NULL,
  `discount_percent` decimal(6,3) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `total_deposit` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `approved` datetime DEFAULT NULL,
  `approved_by` bigint(20) DEFAULT NULL,
  `share_save_option` tinyint(4) DEFAULT NULL,
  `user_share_id` int(11) DEFAULT NULL,
  `share_option` tinyint(4) DEFAULT NULL COMMENT '1: Only me; 2: Everyone; 3: User customize; 4: Everyone but except user',
  `share_user` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_except_user` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_close` tinyint(4) DEFAULT '0',
  `is_approve` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`quotation_code`,`quotation_date`,`created_by`,`edited_by`,`modified_by`,`approved_by`,`is_close`,`is_approve`,`status`),
  KEY `key_search_share` (`user_share_id`,`share_option`,`share_user`(255),`share_except_user`(255)),
  KEY `key_filter` (`company_id`,`customer_id`,`customer_contact_id`,`currency_center_id`,`price_type_id`,`vat_setting_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.quotations: ~0 rows (approximately)
/*!40000 ALTER TABLE `quotations` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotations` ENABLE KEYS */;


-- Dumping structure for table sas_master.quotation_details
CREATE TABLE IF NOT EXISTS `quotation_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_cost` decimal(15,3) DEFAULT '0.000',
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `quotaion_id` (`quotation_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.quotation_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `quotation_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.quotation_miscs
CREATE TABLE IF NOT EXISTS `quotation_miscs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `quotaion_id` (`quotation_id`),
  KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.quotation_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `quotation_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.quotation_services
CREATE TABLE IF NOT EXISTS `quotation_services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quotation_id` bigint(20) DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  PRIMARY KEY (`id`),
  KEY `quotaion_id` (`quotation_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.quotation_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `quotation_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.quotation_term_conditions
CREATE TABLE IF NOT EXISTS `quotation_term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotation_id_term_condition_type_id_term_condition_id` (`quotation_id`,`term_condition_type_id`,`term_condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.quotation_term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `quotation_term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation_term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.reasons
CREATE TABLE IF NOT EXISTS `reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.reasons: ~1 rows (approximately)
/*!40000 ALTER TABLE `reasons` DISABLE KEYS */;
INSERT INTO `reasons` (`id`, `sys_code`, `name`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '48de4e2976edaf09048dc7b9d48652c5', 'Customer Return', '2017-09-14 17:36:15', 1, '2017-09-14 17:36:15', NULL, 1);
/*!40000 ALTER TABLE `reasons` ENABLE KEYS */;


-- Dumping structure for table sas_master.receive_payments
CREATE TABLE IF NOT EXISTS `receive_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `cgroup_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `deposit_to` int(11) DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `filters` (`company_id`,`branch_id`,`cgroup_id`,`customer_id`),
  KEY `searchs` (`date`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.receive_payments: ~0 rows (approximately)
/*!40000 ALTER TABLE `receive_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `receive_payments` ENABLE KEYS */;


-- Dumping structure for table sas_master.receive_payment_details
CREATE TABLE IF NOT EXISTS `receive_payment_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receive_payment_id` bigint(20) DEFAULT NULL,
  `sales_order_id` bigint(20) DEFAULT NULL,
  `amount_due` decimal(15,3) DEFAULT '0.000',
  `paid` decimal(15,3) DEFAULT '0.000',
  `paid_other` decimal(15,3) DEFAULT '0.000',
  `discount` decimal(15,3) DEFAULT '0.000',
  `discount_other` decimal(15,3) DEFAULT '0.000',
  `balance` decimal(15,3) DEFAULT '0.000',
  `due_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.receive_payment_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `receive_payment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `receive_payment_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.reconciles
CREATE TABLE IF NOT EXISTS `reconciles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `service_charge_gl_id` bigint(20) DEFAULT NULL,
  `interested_earned_gl_id` bigint(20) DEFAULT NULL,
  `diff_gl_id` bigint(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filters` (`chart_account_id`,`service_charge_gl_id`,`interested_earned_gl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.reconciles: ~0 rows (approximately)
/*!40000 ALTER TABLE `reconciles` DISABLE KEYS */;
/*!40000 ALTER TABLE `reconciles` ENABLE KEYS */;


-- Dumping structure for table sas_master.report_sales_by_days
CREATE TABLE IF NOT EXISTS `report_sales_by_days` (
  `date` date NOT NULL,
  `company_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_rep_id` int(11) NOT NULL DEFAULT '0',
  `s_total_amount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `s_total_discount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `s_total_vat` decimal(15,3) NOT NULL DEFAULT '0.000',
  `p_total_amount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `p_total_discount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `p_total_vat` decimal(15,3) NOT NULL DEFAULT '0.000',
  `c_total_amount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `c_total_discount` decimal(15,3) NOT NULL DEFAULT '0.000',
  `c_total_mark_up` decimal(15,3) NOT NULL DEFAULT '0.000',
  `c_total_vat` decimal(15,3) NOT NULL DEFAULT '0.000',
  PRIMARY KEY (`date`,`company_id`,`branch_id`,`customer_id`,`sales_rep_id`),
  KEY `filters` (`date`,`company_id`,`branch_id`,`customer_id`,`sales_rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.report_sales_by_days: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_sales_by_days` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_sales_by_days` ENABLE KEYS */;


-- Dumping structure for table sas_master.report_sales_by_months
CREATE TABLE IF NOT EXISTS `report_sales_by_months` (
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sales_rep_id` int(11) NOT NULL DEFAULT '0',
  `s_total_amount` decimal(15,3) NOT NULL,
  `s_total_discount` decimal(15,3) NOT NULL,
  `s_total_vat` decimal(15,3) NOT NULL,
  `p_total_amount` decimal(15,3) NOT NULL,
  `p_total_discount` decimal(15,3) NOT NULL,
  `p_total_vat` decimal(15,3) NOT NULL,
  `c_total_amount` decimal(15,3) NOT NULL,
  `c_total_discount` decimal(15,3) NOT NULL,
  `c_total_mark_up` decimal(15,3) NOT NULL,
  `c_total_vat` decimal(15,3) NOT NULL,
  PRIMARY KEY (`month`,`year`,`company_id`,`branch_id`,`customer_id`,`sales_rep_id`),
  KEY `filters` (`month`,`year`,`company_id`,`branch_id`,`customer_id`,`sales_rep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.report_sales_by_months: ~0 rows (approximately)
/*!40000 ALTER TABLE `report_sales_by_months` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_sales_by_months` ENABLE KEYS */;


-- Dumping structure for table sas_master.request_stocks
CREATE TABLE IF NOT EXISTS `request_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `from_location_group_id` int(11) DEFAULT NULL,
  `to_location_group_id` int(11) DEFAULT NULL,
  `transfer_order_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_keys` (`from_location_group_id`,`to_location_group_id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.request_stocks: ~0 rows (approximately)
/*!40000 ALTER TABLE `request_stocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_stocks` ENABLE KEYS */;


-- Dumping structure for table sas_master.request_stock_details
CREATE TABLE IF NOT EXISTS `request_stock_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `request_stock_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `request_stock_id` (`request_stock_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.request_stock_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `request_stock_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `request_stock_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_orders
CREATE TABLE IF NOT EXISTS `sales_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_contact_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `ar_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `price_type_id` int(11) DEFAULT NULL,
  `sales_rep_id` int(11) DEFAULT NULL,
  `deliver_id` int(11) DEFAULT NULL,
  `collector_id` int(11) DEFAULT NULL,
  `consignment_id` int(11) DEFAULT NULL,
  `consignment_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `quotation_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `customer_po_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `project` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `total_amount_kh` decimal(15,3) DEFAULT NULL,
  `total_amount_return` decimal(15,3) DEFAULT NULL,
  `total_deposit` decimal(15,3) DEFAULT NULL,
  `balance` decimal(15,3) DEFAULT NULL,
  `discount` decimal(15,3) DEFAULT NULL,
  `discount_percent` decimal(6,3) DEFAULT NULL,
  `vat_chart_account_id` int(11) DEFAULT NULL,
  `total_vat` decimal(15,3) DEFAULT NULL,
  `vat_percent` decimal(5,3) DEFAULT NULL COMMENT 'Percent (%)',
  `vat_setting_id` int(11) DEFAULT NULL,
  `vat_calculate` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `memo` text COLLATE utf8_unicode_ci,
  `shift_id` int(11) DEFAULT NULL,
  `pos_pay_method_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `approved` datetime DEFAULT NULL,
  `approved_by` bigint(20) DEFAULT NULL,
  `long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1:Edit, 1:Issue, 2:Fullfied; 3: Partial',
  `is_deposit_reference` tinyint(4) DEFAULT '0',
  `is_approve` tinyint(4) DEFAULT '1',
  `is_print` tinyint(4) DEFAULT '0',
  `is_reprint` tinyint(4) DEFAULT '0',
  `is_pos` tinyint(4) DEFAULT '0' COMMENT '0: Invoice; 1: POS; 2: Consignment',
  PRIMARY KEY (`id`),
  KEY `key_filter` (`company_id`,`location_group_id`,`location_id`,`customer_id`,`customer_contact_id`,`currency_center_id`,`payment_term_id`,`price_type_id`,`sales_rep_id`,`branch_id`),
  KEY `key_filter_second` (`delivery_id`,`ar_id`,`deliver_id`,`collector_id`,`quotation_id`,`order_id`,`vat_chart_account_id`,`vat_setting_id`,`consignment_id`),
  KEY `key_search` (`quotation_number`,`order_number`,`so_code`,`balance`,`order_date`,`created_by`,`modified_by`,`status`,`is_approve`,`consignment_code`),
  KEY `shift` (`shift_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_orders` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_order_details
CREATE TABLE IF NOT EXISTS `sales_order_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `discount_id` int(11) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_cost` decimal(15,3) DEFAULT '0.000',
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_remianing` int(11) DEFAULT NULL COMMENT 'Total Qty Not Delivery ( Small Qty)',
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_id` (`sales_order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_order_miscs
CREATE TABLE IF NOT EXISTS `sales_order_miscs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sales_order_id` bigint(20) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` double DEFAULT '0',
  `qty_free` double DEFAULT '0',
  `qty_uom_id` int(10) DEFAULT NULL,
  `discount_id` int(10) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_id` (`sales_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_order_miscs: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_order_miscs` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_miscs` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_order_receipts
CREATE TABLE IF NOT EXISTS `sales_order_receipts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `receipt_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount_us` decimal(15,3) DEFAULT '0.000',
  `amount_other` decimal(15,3) DEFAULT '0.000',
  `discount_us` decimal(15,3) DEFAULT '0.000',
  `discount_other` decimal(15,3) DEFAULT '0.000',
  `total_amount` decimal(15,3) DEFAULT '0.000',
  `total_amount_other` decimal(15,3) DEFAULT '0.000',
  `balance` decimal(15,3) DEFAULT '0.000',
  `balance_other` decimal(15,3) DEFAULT '0.000',
  `change` decimal(15,3) DEFAULT NULL,
  `change_other` decimal(15,3) DEFAULT NULL,
  `pay_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_void` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sales_order_id` (`sales_order_id`),
  KEY `receipt_code` (`receipt_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_order_receipts: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_order_receipts` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_receipts` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_order_services
CREATE TABLE IF NOT EXISTS `sales_order_services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(10) DEFAULT NULL,
  `service_id` int(10) DEFAULT NULL,
  `discount_id` int(10) DEFAULT NULL,
  `discount_amount` decimal(15,3) DEFAULT '0.000',
  `discount_percent` decimal(5,3) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_free` int(11) DEFAULT '0',
  `unit_price` decimal(15,3) DEFAULT '0.000',
  `total_price` decimal(15,3) DEFAULT '0.000',
  `start` date DEFAULT '0000-00-00',
  `type` tinyint(4) DEFAULT '1' COMMENT '1: General; 2: Monthly',
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_id` (`sales_order_id`),
  KEY `service_id` (`service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_order_services: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_order_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_services` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_order_term_conditions
CREATE TABLE IF NOT EXISTS `sales_order_term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_order_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quotation_id_term_condition_type_id_term_condition_id` (`sales_order_id`,`term_condition_type_id`,`term_condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_order_term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_order_term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_order_term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.sales_targets
CREATE TABLE IF NOT EXISTS `sales_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `m1` decimal(15,3) DEFAULT NULL,
  `m2` decimal(15,3) DEFAULT NULL,
  `m3` decimal(15,3) DEFAULT NULL,
  `m4` decimal(15,3) DEFAULT NULL,
  `m5` decimal(15,3) DEFAULT NULL,
  `m6` decimal(15,3) DEFAULT NULL,
  `m7` decimal(15,3) DEFAULT NULL,
  `m8` decimal(15,3) DEFAULT NULL,
  `m9` decimal(15,3) DEFAULT NULL,
  `m10` decimal(15,3) DEFAULT NULL,
  `m11` decimal(15,3) DEFAULT NULL,
  `m12` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_by` bigint(20) DEFAULT NULL,
  `is_approve` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`company_id`,`employee_id`,`year`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sales_targets: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_targets` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_targets` ENABLE KEYS */;


-- Dumping structure for table sas_master.sections
CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.sections: ~0 rows (approximately)
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;


-- Dumping structure for table sas_master.section_companies
CREATE TABLE IF NOT EXISTS `section_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.section_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `section_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `section_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.services
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT '1',
  `section_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` double DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uom_id` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: General; 2: Monthly',
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`),
  KEY `filters` (`section_id`,`chart_account_id`),
  KEY `searchs` (`code`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.services: ~0 rows (approximately)
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
/*!40000 ALTER TABLE `services` ENABLE KEYS */;


-- Dumping structure for table sas_master.service_branches
CREATE TABLE IF NOT EXISTS `service_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `searchs` (`service_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.service_branches: ~0 rows (approximately)
/*!40000 ALTER TABLE `service_branches` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_branches` ENABLE KEYS */;


-- Dumping structure for table sas_master.setting_options
CREATE TABLE IF NOT EXISTS `setting_options` (
  `uom_detail_option` tinyint(4) DEFAULT '0' COMMENT '0: Disable; 1: Enable',
  `calculate_cogs` tinyint(4) DEFAULT '1' COMMENT '1: AVG, 2: FIFO, 3:FILO',
  `shift` tinyint(4) DEFAULT '0' COMMENT '0: Disable; 1: Enable',
  `product_cost_decimal` tinyint(4) DEFAULT '2',
  `allow_delivery` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.setting_options: ~1 rows (approximately)
/*!40000 ALTER TABLE `setting_options` DISABLE KEYS */;
INSERT INTO `setting_options` (`uom_detail_option`, `calculate_cogs`, `shift`, `product_cost_decimal`, `allow_delivery`) VALUES
	(0, 1, 0, 2, 0);
/*!40000 ALTER TABLE `setting_options` ENABLE KEYS */;


-- Dumping structure for table sas_master.shifts
CREATE TABLE IF NOT EXISTS `shifts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `shift_collect_id` int(11) DEFAULT NULL,
  `exchange_rate_id` int(11) DEFAULT NULL,
  `shift_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `total_register` decimal(15,3) DEFAULT NULL,
  `total_register_other` decimal(15,3) DEFAULT NULL,
  `total_sales` decimal(15,3) DEFAULT NULL,
  `total_sales_other` decimal(15,3) DEFAULT NULL,
  `total_acture` decimal(15,3) NOT NULL DEFAULT '0.000',
  `total_acture_other` decimal(15,3) NOT NULL DEFAULT '0.000',
  `total_spread` decimal(15,3) NOT NULL DEFAULT '0.000',
  `register_memo` text COLLATE utf8_unicode_ci,
  `close_shift_memo` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: Open; 2: Closed; 3: Collected',
  PRIMARY KEY (`id`),
  KEY `times` (`date_start`,`date_end`),
  KEY `filters` (`created_by`,`status`,`shift_collect_id`),
  KEY `company` (`company_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.shifts: ~0 rows (approximately)
/*!40000 ALTER TABLE `shifts` DISABLE KEYS */;
/*!40000 ALTER TABLE `shifts` ENABLE KEYS */;


-- Dumping structure for table sas_master.shift_adjusts
CREATE TABLE IF NOT EXISTS `shift_adjusts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shift_id` int(11) DEFAULT NULL,
  `total_adj` decimal(15,3) NOT NULL DEFAULT '0.000',
  `total_adj_other` decimal(15,3) NOT NULL DEFAULT '0.000',
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.shift_adjusts: ~0 rows (approximately)
/*!40000 ALTER TABLE `shift_adjusts` DISABLE KEYS */;
/*!40000 ALTER TABLE `shift_adjusts` ENABLE KEYS */;


-- Dumping structure for table sas_master.shift_collects
CREATE TABLE IF NOT EXISTS `shift_collects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `total_sales` decimal(15,3) DEFAULT NULL,
  `total_sales_other` decimal(15,3) DEFAULT NULL,
  `total_register` decimal(15,3) DEFAULT NULL,
  `total_register_other` decimal(15,3) DEFAULT NULL,
  `total_adj` decimal(15,3) DEFAULT NULL,
  `total_adj_other` decimal(15,3) DEFAULT NULL,
  `total_cash_collect` decimal(15,3) unsigned DEFAULT NULL,
  `total_cash_collect_other` decimal(15,3) DEFAULT NULL,
  `total_spread` decimal(15,3) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters` (`code`,`date`),
  KEY `searchs` (`company_id`,`branch_id`,`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.shift_collects: ~0 rows (approximately)
/*!40000 ALTER TABLE `shift_collects` DISABLE KEYS */;
/*!40000 ALTER TABLE `shift_collects` ENABLE KEYS */;


-- Dumping structure for table sas_master.shipments
CREATE TABLE IF NOT EXISTS `shipments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.shipments: ~0 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;


-- Dumping structure for table sas_master.stock_orders
CREATE TABLE IF NOT EXISTS `stock_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sales_order_id` int(11) DEFAULT NULL,
  `sales_order_detail_id` int(11) DEFAULT NULL,
  `transfer_order_id` int(11) DEFAULT NULL,
  `purchase_return_id` int(11) DEFAULT NULL,
  `purchase_return_detail_id` int(11) DEFAULT NULL,
  `consignment_id` int(11) DEFAULT NULL,
  `consignment_detail_id` int(11) DEFAULT NULL,
  `consignment_return_id` int(11) DEFAULT NULL,
  `vendor_consignment_return_id` int(11) DEFAULT NULL,
  `cycle_product_id` int(11) DEFAULT NULL,
  `inventory_physical_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  `qty` decimal(15,3) DEFAULT NULL COMMENT 'Qty As Small Uom',
  PRIMARY KEY (`id`),
  KEY `key_search` (`product_id`,`location_group_id`,`location_id`,`lots_number`,`expired_date`,`date`),
  KEY `sys_code` (`sys_code`),
  KEY `key_filter` (`sales_order_id`,`transfer_order_id`,`purchase_return_id`,`sales_order_detail_id`,`consignment_id`,`consignment_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.stock_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `stock_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_orders` ENABLE KEYS */;


-- Dumping structure for table sas_master.streets
CREATE TABLE IF NOT EXISTS `streets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `sys_code` (`sys_code`),
  KEY `search` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.streets: ~0 rows (approximately)
/*!40000 ALTER TABLE `streets` DISABLE KEYS */;
/*!40000 ALTER TABLE `streets` ENABLE KEYS */;


-- Dumping structure for table sas_master.system_activities
CREATE TABLE IF NOT EXISTS `system_activities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `act` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bug` mediumtext COLLATE utf8_unicode_ci,
  `browser` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operating_system` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '1: Complete; 2: Bug',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `key_search` (`module`,`act`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.system_activities: ~0 rows (approximately)
/*!40000 ALTER TABLE `system_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `system_activities` ENABLE KEYS */;


-- Dumping structure for table sas_master.s_module_detail_settings
CREATE TABLE IF NOT EXISTS `s_module_detail_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_module_setting_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `module_description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_controller` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_view` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  `is_has_module` tinyint(4) DEFAULT '0',
  `is_use_date` tinyint(4) DEFAULT '0',
  `is_use_chart_account` tinyint(4) DEFAULT '0',
  `is_use_integer` tinyint(4) DEFAULT '0',
  `is_use_currency` tinyint(4) DEFAULT '0',
  `is_use_checked` tinyint(4) DEFAULT '1',
  `is_import` tinyint(4) DEFAULT '0',
  `is_checked` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `module_setting_id` (`s_module_setting_id`),
  KEY `is_checked` (`is_checked`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.s_module_detail_settings: ~54 rows (approximately)
/*!40000 ALTER TABLE `s_module_detail_settings` DISABLE KEYS */;
INSERT INTO `s_module_detail_settings` (`id`, `s_module_setting_id`, `name`, `date_value`, `value`, `module_description`, `module_controller`, `module_view`, `template`, `ordering`, `is_has_module`, `is_use_date`, `is_use_chart_account`, `is_use_integer`, `is_use_currency`, `is_use_checked`, `is_import`, `is_checked`, `is_active`) VALUES
	(1, 1, 'Barcode Scaner', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1),
	(2, 1, 'Reservation', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(3, 1, 'Attribute & varriants', NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(4, 1, 'Product Packages', NULL, NULL, NULL, NULL, NULL, NULL, 4, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(5, 1, 'Units of Measure', NULL, NULL, 'UoM List', 'uoms', 'index', NULL, 5, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(6, 1, 'Lots & Serial Numbers', NULL, NULL, NULL, NULL, NULL, NULL, 6, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(7, 1, 'Expiration Dates', NULL, NULL, NULL, NULL, NULL, NULL, 7, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(8, 2, 'Landed Costs', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(9, 2, 'Using FIFO starting on ', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, 0, 0, 1, 0, 0, 2),
	(10, 3, 'Multi-warehouse', NULL, NULL, 'Warehouse', 'location_groups', 'index', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(11, 3, 'Storage Locations', NULL, NULL, 'Location', 'locations', 'index', NULL, 2, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(12, 4, 'Purchase orders are active', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1),
	(13, 4, 'Warn about duplicate purchase order numbers', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(14, 5, 'Delivery slip is used', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(15, 6, 'Warn about duplicate bill from same vendors', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(16, 7, 'Automatically use credits', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(17, 7, 'Automatically use discounts', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(18, 8, 'Sale orders are active', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1),
	(19, 8, 'Warn about duplicate sale order number', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0),
	(20, 9, 'Delivery slip is used', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(21, 10, 'Warn about duplicate sale invoice number', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0),
	(22, 10, 'Warn if not enough inventory to sell', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(23, 10, 'Allow credit terms', NULL, NULL, 'Term Setting', 'payment_terms', 'index', NULL, 3, 1, 0, 0, 0, 0, 1, 0, 0, 1),
	(24, 11, 'Allow payment discount', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(25, 12, 'Use price level', NULL, NULL, 'Price Setting', 'price_types', 'index', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(26, 13, 'Allow cash & bank mangement', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(27, 12, 'Allow manual discount', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(28, 14, 'Allow VAT', NULL, NULL, 'VAT Setting', 'vat_settings', 'index', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(31, 15, 'Base currency', NULL, 1, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1),
	(32, 15, 'Multi-currency', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 0, 0, 1, 0, 0, 2),
	(33, 16, 'Last day', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 1, 0, 0, 2),
	(34, 16, 'Lock Transaction', '2017-01-31', NULL, NULL, NULL, NULL, NULL, 2, 0, 1, 0, 0, 0, 0, 0, 0, 1),
	(35, 17, '', NULL, NULL, 'User Setting', 'users', 'index', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(36, 17, '', NULL, NULL, 'User Group Setting', 'groups', 'index', NULL, 2, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(37, 18, '', NULL, NULL, 'Reporting Setting', 'chart_accounts', 'index', NULL, 2, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(38, 18, '', NULL, NULL, 'Default Accounts Setting', 'settings', 'ics', NULL, 3, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(39, 19, 'Purchases', NULL, 2, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1),
	(40, 19, 'Sales', NULL, 2, NULL, NULL, NULL, NULL, 2, 0, 0, 0, 1, 0, 0, 0, 0, 1),
	(41, 19, 'Reports', NULL, 2, NULL, NULL, NULL, NULL, 3, 0, 0, 0, 1, 0, 0, 0, 0, 0),
	(42, 20, 'Allow multi branches', NULL, NULL, 'Branch Setting', 'branches', 'index', NULL, 2, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(43, 21, 'Print Templates', NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, 0, 0, 0, 0, 0, 0, 0, 2),
	(44, 20, '', NULL, NULL, 'Company Setting', 'companies', 'index', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(45, 18, 'Disable account code', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(46, 22, 'Enable sales rep', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1),
	(47, 23, 'Product List', NULL, NULL, NULL, NULL, NULL, 'product.csv', 1, 0, 0, 0, 0, 0, 0, 1, 0, 1),
	(48, 23, 'Customer List', NULL, NULL, NULL, NULL, NULL, 'customer.csv', 2, 0, 0, 0, 0, 0, 0, 1, 0, 1),
	(49, 23, 'Vendor List', NULL, NULL, NULL, NULL, NULL, 'vendor.csv', 3, 0, 0, 0, 0, 0, 0, 1, 0, 1),
	(50, 23, 'Chart of Account', NULL, NULL, NULL, NULL, NULL, 'coa.csv', 3, 0, 0, 0, 0, 0, 0, 1, 0, 1),
	(51, 1, '', NULL, NULL, 'Category List', 'pgroups', 'index', NULL, 8, 1, 0, 0, 0, 0, 0, 0, 0, 1),
	(52, 19, 'AVG Cost', NULL, 6, NULL, NULL, NULL, NULL, 3, 0, 0, 0, 1, 0, 0, 0, 0, 1),
	(53, 24, 'Allow Shift', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1),
	(54, 25, 'Multi-Payment Method', NULL, NULL, 'Payment Method', 'pos_pay_methods', 'index', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(55, 25, 'Multi-Payment Currency', NULL, NULL, 'POS Currency', 'branch_currencies', 'index', NULL, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1),
	(56, 26, '', NULL, NULL, 'Exchange List', 'exchange_rates', 'index', NULL, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1);
/*!40000 ALTER TABLE `s_module_detail_settings` ENABLE KEYS */;


-- Dumping structure for table sas_master.s_module_settings
CREATE TABLE IF NOT EXISTS `s_module_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `s_module_type_setting_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `module_type_id` (`s_module_type_setting_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.s_module_settings: ~26 rows (approximately)
/*!40000 ALTER TABLE `s_module_settings` DISABLE KEYS */;
INSERT INTO `s_module_settings` (`id`, `s_module_type_setting_id`, `name`, `ordering`, `created`, `is_active`) VALUES
	(1, 1, 'Products', 1, '2018-07-18 14:44:14', 1),
	(2, 1, 'Costing', 2, '2018-07-18 14:44:14', 1),
	(3, 1, 'Warehose', 3, '2018-07-18 14:44:14', 1),
	(4, 2, 'Ordering', 1, '2018-07-18 14:44:14', 1),
	(5, 2, 'Delivery', 2, '2018-07-18 14:44:14', 1),
	(6, 2, 'Entering bills', 3, '2018-07-18 14:44:14', 1),
	(7, 2, 'Paying bills', 4, '2018-07-18 14:44:14', 1),
	(8, 3, 'Ordering', 1, '2018-07-18 14:44:14', 1),
	(9, 3, 'Delivery', 2, '2018-07-18 14:44:14', 1),
	(10, 3, 'Billing', 3, '2018-07-18 14:44:14', 1),
	(11, 3, 'Payment', 4, '2018-07-18 14:44:14', 1),
	(12, 3, 'Pricing & discount', 5, '2018-07-18 14:44:14', 1),
	(13, 4, 'Cash & bank', 4, '2018-07-18 14:44:14', 1),
	(14, 4, 'Assign tax code', 2, '2018-07-18 14:44:14', 1),
	(15, 4, 'Currency', 5, '2018-07-18 14:44:14', 1),
	(16, 4, 'Fiscal period', 6, '2018-07-18 14:44:14', 1),
	(17, 5, 'Users setup', 2, '2018-07-18 14:44:14', 1),
	(18, 4, 'Simplify accounts', 1, '2018-07-18 14:44:14', 1),
	(19, 4, 'Decimal setting', 3, '2018-07-18 14:44:14', 1),
	(20, 5, 'Company setup', 1, '2018-07-18 14:44:14', 1),
	(21, 5, 'Templates', 3, '2018-07-18 14:44:14', 1),
	(22, 3, 'Sales rep', 6, '2018-07-18 14:44:14', 1),
	(23, 5, 'Import', 4, '2018-07-18 14:44:14', 1),
	(24, 6, 'Cashier', 1, '2018-07-18 14:44:14', 1),
	(25, 6, 'Payment', 2, '2018-07-18 14:44:14', 1),
	(26, 6, 'Exchange Rate', 3, '2018-07-18 14:44:14', 1);
/*!40000 ALTER TABLE `s_module_settings` ENABLE KEYS */;


-- Dumping structure for table sas_master.s_module_type_settings
CREATE TABLE IF NOT EXISTS `s_module_type_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.s_module_type_settings: ~6 rows (approximately)
/*!40000 ALTER TABLE `s_module_type_settings` DISABLE KEYS */;
INSERT INTO `s_module_type_settings` (`id`, `name`, `module_id`, `ordering`, `created`, `is_active`) VALUES
	(1, 'Inventory', 597, 1, '2018-07-18 14:28:55', 1),
	(2, 'Purchase', 503, 2, '2018-07-18 14:28:55', 1),
	(3, 'Sales', 282, 3, '2018-07-18 14:28:55', 1),
	(4, 'Accounting', 626, 5, '2018-07-18 14:28:55', 1),
	(5, 'General Setting', 41, 6, '2018-07-18 14:28:55', 1),
	(6, 'POS', 504, 4, '2018-07-18 14:28:55', 1);
/*!40000 ALTER TABLE `s_module_type_settings` ENABLE KEYS */;


-- Dumping structure for table sas_master.term_conditions
CREATE TABLE IF NOT EXISTS `term_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `name` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `term_condition_type_id` (`term_condition_type_id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`(255),`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.term_conditions: ~0 rows (approximately)
/*!40000 ALTER TABLE `term_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `term_conditions` ENABLE KEYS */;


-- Dumping structure for table sas_master.term_condition_applies
CREATE TABLE IF NOT EXISTS `term_condition_applies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_type_id` int(11) DEFAULT NULL,
  `term_condition_type_id` int(11) DEFAULT NULL,
  `term_condition_default_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modules` (`module_type_id`,`term_condition_type_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.term_condition_applies: ~0 rows (approximately)
/*!40000 ALTER TABLE `term_condition_applies` DISABLE KEYS */;
/*!40000 ALTER TABLE `term_condition_applies` ENABLE KEYS */;


-- Dumping structure for table sas_master.term_condition_types
CREATE TABLE IF NOT EXISTS `term_condition_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.term_condition_types: ~0 rows (approximately)
/*!40000 ALTER TABLE `term_condition_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `term_condition_types` ENABLE KEYS */;


-- Dumping structure for table sas_master.test
CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.test: ~1 rows (approximately)
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` (`id`, `name`, `date`) VALUES
	(1, 'Test', '2018-05-24 21:57:47');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;


-- Dumping structure for table sas_master.tmp_ponit_of_sales
CREATE TABLE IF NOT EXISTS `tmp_ponit_of_sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_group_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.tmp_ponit_of_sales: ~0 rows (approximately)
/*!40000 ALTER TABLE `tmp_ponit_of_sales` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_ponit_of_sales` ENABLE KEYS */;


-- Dumping structure for table sas_master.tmp_ponit_of_sale_details
CREATE TABLE IF NOT EXISTS `tmp_ponit_of_sale_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tmp_point_of_sale_id` int(11) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty_uom_id` bigint(20) DEFAULT NULL,
  `total_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.tmp_ponit_of_sale_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `tmp_ponit_of_sale_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmp_ponit_of_sale_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.tracks
CREATE TABLE IF NOT EXISTS `tracks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `description` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `val` date DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `is_recalculate` tinyint(4) NOT NULL DEFAULT '0',
  `is_recalculate_process` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.tracks: ~1 rows (approximately)
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
INSERT INTO `tracks` (`id`, `pid`, `description`, `val`, `date_start`, `date_end`, `is_recalculate`, `is_recalculate_process`) VALUES
	(1, 3844, 'Recalculate Average Cost', '2018-09-12', '2018-09-15 15:59:59', '2018-09-15 15:59:59', 0, 0);
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;


-- Dumping structure for table sas_master.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `file` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accounting` int(11) DEFAULT '0',
  `save_acct` int(11) DEFAULT '0',
  `products` int(11) DEFAULT '0',
  `service` int(11) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `action` tinyint(4) DEFAULT '1' COMMENT '1: Add; 2: Void',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`type`,`module_id`,`action`),
  KEY `created` (`created`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;


-- Dumping structure for table sas_master.transaction_details
CREATE TABLE IF NOT EXISTS `transaction_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1: Product; 2: Service',
  `module_id` int(11) DEFAULT NULL,
  `accounting` int(11) DEFAULT '0',
  `save_acct` int(11) DEFAULT '0',
  `inventory_valutaion` tinyint(4) DEFAULT '0',
  `inventory` tinyint(4) DEFAULT '0',
  `inventory_total` tinyint(4) DEFAULT '0',
  `loc_inventory` tinyint(4) DEFAULT '0',
  `loc_inventory_total` tinyint(4) DEFAULT '0',
  `loc_inventory_detail` tinyint(4) DEFAULT '0',
  `g_inventory` tinyint(4) DEFAULT '0',
  `g_inventory_detail` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transaction_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.transfer_orders
CREATE TABLE IF NOT EXISTS `transfer_orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `request_stock_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `from_location_group_id` int(11) DEFAULT NULL,
  `to_location_group_id` int(11) DEFAULT NULL,
  `to_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `fulfillment_date` date DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Transfer; 2: Consignment',
  `approved` datetime DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `is_approve` tinyint(4) DEFAULT '1' COMMENT '0: Confirm Approval; 1: Approved; 2: Reject',
  `is_process` tinyint(4) NOT NULL DEFAULT '0',
  `error` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `from_location_id` (`from_location_group_id`),
  KEY `to_location_id` (`to_location_group_id`),
  KEY `to_code` (`to_code`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transfer_orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `transfer_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `transfer_orders` ENABLE KEYS */;


-- Dumping structure for table sas_master.transfer_order_details
CREATE TABLE IF NOT EXISTS `transfer_order_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_order_id` bigint(20) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_from_id` int(11) DEFAULT NULL,
  `location_to_id` int(11) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT '0',
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transfer_order_id` (`transfer_order_id`),
  KEY `product_id` (`product_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transfer_order_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `transfer_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `transfer_order_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.transfer_receives
CREATE TABLE IF NOT EXISTS `transfer_receives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `transfer_receive_result_id` bigint(20) DEFAULT NULL,
  `transfer_order_id` bigint(20) DEFAULT NULL,
  `transfer_order_detail_id` bigint(20) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transfer_order_id` (`transfer_order_id`),
  KEY `transfer_order_detail_id` (`transfer_order_detail_id`),
  KEY `product_id` (`product_id`),
  KEY `transfer_receive_result_id` (`transfer_receive_result_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transfer_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `transfer_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `transfer_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.transfer_receive_results
CREATE TABLE IF NOT EXISTS `transfer_receive_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transfer_order_id` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transfer_order_id` (`transfer_order_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.transfer_receive_results: ~0 rows (approximately)
/*!40000 ALTER TABLE `transfer_receive_results` DISABLE KEYS */;
/*!40000 ALTER TABLE `transfer_receive_results` ENABLE KEYS */;


-- Dumping structure for table sas_master.unearned_recognitions
CREATE TABLE IF NOT EXISTS `unearned_recognitions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `module_detail_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `debit_account_id` int(11) DEFAULT NULL,
  `credit_account_id` int(11) DEFAULT NULL,
  `debit` decimal(20,9) DEFAULT NULL,
  `credit` decimal(20,9) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Sales; 2: CM',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modules` (`module_id`,`module_detail_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.unearned_recognitions: ~0 rows (approximately)
/*!40000 ALTER TABLE `unearned_recognitions` DISABLE KEYS */;
/*!40000 ALTER TABLE `unearned_recognitions` ENABLE KEYS */;


-- Dumping structure for table sas_master.unearned_schedules
CREATE TABLE IF NOT EXISTS `unearned_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `module_detail_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `debit_account_id` int(11) DEFAULT NULL,
  `credit_account_id` int(11) DEFAULT NULL,
  `debit` decimal(20,9) DEFAULT '0.000000000',
  `credit` decimal(20,9) DEFAULT '0.000000000',
  `type` tinyint(4) DEFAULT '1' COMMENT '1: Sales; 2: CM',
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modules` (`module_id`,`module_detail_id`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.unearned_schedules: ~0 rows (approximately)
/*!40000 ALTER TABLE `unearned_schedules` DISABLE KEYS */;
/*!40000 ALTER TABLE `unearned_schedules` ENABLE KEYS */;


-- Dumping structure for table sas_master.unearned_tacks
CREATE TABLE IF NOT EXISTS `unearned_tacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_processing` tinyint(4) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_processing` (`is_processing`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.unearned_tacks: ~1 rows (approximately)
/*!40000 ALTER TABLE `unearned_tacks` DISABLE KEYS */;
INSERT INTO `unearned_tacks` (`id`, `is_processing`, `start`, `end`) VALUES
	(1, 0, '2018-02-24 15:28:45', '2018-02-24 15:28:45');
/*!40000 ALTER TABLE `unearned_tacks` ENABLE KEYS */;


-- Dumping structure for table sas_master.uoms
CREATE TABLE IF NOT EXISTS `uoms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abbr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.uoms: ~2 rows (approximately)
/*!40000 ALTER TABLE `uoms` DISABLE KEYS */;
INSERT INTO `uoms` (`id`, `sys_code`, `type`, `name`, `abbr`, `description`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '4230a9bf25116858815bcf69af1a95ff', 'Time', 'Month', 'Month', '', '2018-02-16 09:47:52', 1, '2018-02-16 09:47:52', NULL, 2),
	(2, '1fcf633f114ae6b9ef272d75a47eaf85', 'Count', 'Unit', 'Unit', '', '2018-02-16 10:14:40', 1, '2018-02-16 10:14:40', NULL, 1);
/*!40000 ALTER TABLE `uoms` ENABLE KEYS */;


-- Dumping structure for table sas_master.uom_conversions
CREATE TABLE IF NOT EXISTS `uom_conversions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_uom_id` int(11) DEFAULT NULL,
  `to_uom_id` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_small_uom` tinyint(4) DEFAULT '0',
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `from_uom_id` (`from_uom_id`),
  KEY `to_uom_id` (`to_uom_id`),
  KEY `is_small_uom` (`is_small_uom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.uom_conversions: ~0 rows (approximately)
/*!40000 ALTER TABLE `uom_conversions` DISABLE KEYS */;
/*!40000 ALTER TABLE `uom_conversions` ENABLE KEYS */;


-- Dumping structure for table sas_master.upload_slides
CREATE TABLE IF NOT EXISTS `upload_slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caption` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.upload_slides: ~0 rows (approximately)
/*!40000 ALTER TABLE `upload_slides` DISABLE KEYS */;
/*!40000 ALTER TABLE `upload_slides` ENABLE KEYS */;


-- Dumping structure for table sas_master.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pin` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_project_id` bigint(20) DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `user_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_start` datetime DEFAULT NULL,
  `session_active` datetime DEFAULT NULL,
  `session_lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `session_accuracy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_attempt` datetime DEFAULT NULL,
  `login_attempt_remote_ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_attempt_http_user_agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_lat` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_long` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_accuracy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expired` date DEFAULT NULL,
  `duration` bigint(20) DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `telephone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationality` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `first_name` (`first_name`),
  KEY `last_name` (`last_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.users: ~5 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `sys_code`, `pin`, `main_project_id`, `project_id`, `user_code`, `session_id`, `session_start`, `session_active`, `session_lat`, `session_long`, `session_accuracy`, `login_attempt`, `login_attempt_remote_ip`, `login_attempt_http_user_agent`, `login_lat`, `login_long`, `login_accuracy`, `expired`, `duration`, `username`, `password`, `first_name`, `last_name`, `sex`, `dob`, `address`, `telephone`, `email`, `nationality`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '123', NULL, NULL, NULL, NULL, 'utartsogfhg67roghfvg2vcag5', '2018-10-05 14:30:13', '2018-10-05 14:39:10', '', '', '', NULL, '', 'OS: Windows 8.1 Browser: Mozilla Firefox 41.0', NULL, NULL, NULL, '2019-02-17', 0, 'admin', '273ad0d33586ddbdb7536761cc3daad0', 'Admin', 'Mr.', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 09:33:11', 1, '2018-10-05 14:30:13', NULL, 1),
	(2, 'ee3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-17', 0, 'user1', '273ad0d33586ddbdb7536761cc3daad0', 'User', '1', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 09:33:11', 1, '2018-08-07 14:39:50', NULL, 1),
	(3, 'wed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-17', 0, 'user2', '273ad0d33586ddbdb7536761cc3daad0', 'User', '2', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 09:33:11', 1, '2018-08-07 14:39:50', NULL, 1),
	(4, 'ews', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-17', 0, 'user3', '273ad0d33586ddbdb7536761cc3daad0', 'User', '3', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 09:33:11', 1, '2018-08-07 14:39:50', NULL, 1),
	(5, 'axd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-17', 0, 'user4', '273ad0d33586ddbdb7536761cc3daad0', 'User', '4', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 09:33:11', 1, '2018-08-07 14:39:50', NULL, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_activity_logs
CREATE TABLE IF NOT EXISTS `user_activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tbl_from_id` bigint(20) DEFAULT NULL,
  `tbl_to_id` bigint(20) DEFAULT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `browser` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operating_system` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `key_search` (`type`,`tbl_from_id`,`tbl_to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_activity_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_activity_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_activity_logs` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_branches
CREATE TABLE IF NOT EXISTS `user_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_branch_id` (`user_id`,`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_branches: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_branches` DISABLE KEYS */;
INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1);
/*!40000 ALTER TABLE `user_branches` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_cgroups
CREATE TABLE IF NOT EXISTS `user_cgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cgroup_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cgroup_id_user_id` (`cgroup_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_cgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_cgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_cgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_companies
CREATE TABLE IF NOT EXISTS `user_companies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_company_id` (`user_id`,`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_companies: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_companies` DISABLE KEYS */;
INSERT INTO `user_companies` (`id`, `user_id`, `company_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1);
/*!40000 ALTER TABLE `user_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_dashboards
CREATE TABLE IF NOT EXISTS `user_dashboards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `display` tinyint(4) DEFAULT NULL COMMENT '1: Show; 2: Hide',
  `auto_refresh` tinyint(4) DEFAULT NULL COMMENT '1: Off; 2: Auto',
  `time_refresh` int(11) DEFAULT NULL COMMENT 'Calculate As Second',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_module_id` (`user_id`,`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_dashboards: ~9 rows (approximately)
/*!40000 ALTER TABLE `user_dashboards` DISABLE KEYS */;
INSERT INTO `user_dashboards` (`id`, `user_id`, `module_id`, `display`, `auto_refresh`, `time_refresh`, `created`, `modified`) VALUES
	(1, 1, 499, 1, 1, 5, '2018-09-22 09:06:53', '2018-09-22 09:06:53'),
	(2, 1, 602, 1, 1, 30, '2018-09-22 09:06:53', '2018-09-22 09:06:53'),
	(3, 1, 611, 1, 1, 30, '2018-09-22 09:06:54', '2018-09-22 09:06:54'),
	(4, 1, 612, 1, 1, 30, '2018-09-22 09:06:54', '2018-09-22 09:06:54'),
	(5, 1, 613, 1, 1, 30, '2018-09-22 09:06:54', '2018-09-22 09:06:54'),
	(6, 1, 614, 1, 1, 30, '2018-09-22 09:06:54', '2018-09-22 09:06:54'),
	(7, 1, 615, 1, 1, 30, '2018-09-22 09:06:55', '2018-09-22 09:06:55'),
	(8, 1, 630, 1, 1, 30, '2018-09-22 09:06:55', '2018-09-22 09:06:55'),
	(9, 1, 631, 1, 1, 30, '2018-10-04 16:39:49', '2018-10-04 16:39:49');
/*!40000 ALTER TABLE `user_dashboards` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_groups
CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_group_id` (`user_id`,`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_groups: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` (`id`, `user_id`, `group_id`) VALUES
	(26, 1, 1),
	(27, 2, 1),
	(28, 3, 1),
	(29, 4, 1),
	(30, 5, 1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_introduces
CREATE TABLE IF NOT EXISTS `user_introduces` (
  `user_id` int(11) NOT NULL DEFAULT '0',
  `introduce_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`introduce_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_introduces: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_introduces` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_introduces` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_locations
CREATE TABLE IF NOT EXISTS `user_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_location_id` (`user_id`,`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_locations: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_locations` DISABLE KEYS */;
INSERT INTO `user_locations` (`id`, `user_id`, `location_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1);
/*!40000 ALTER TABLE `user_locations` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_location_groups
CREATE TABLE IF NOT EXISTS `user_location_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `location_group_id` (`location_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_location_groups: ~5 rows (approximately)
/*!40000 ALTER TABLE `user_location_groups` DISABLE KEYS */;
INSERT INTO `user_location_groups` (`id`, `user_id`, `location_group_id`) VALUES
	(1, 1, 1),
	(2, 2, 1),
	(3, 3, 1),
	(4, 4, 1),
	(5, 5, 1);
/*!40000 ALTER TABLE `user_location_groups` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_logs
CREATE TABLE IF NOT EXISTS `user_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `http_user_agent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remote_addr` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_logs: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_logs` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_pgroups
CREATE TABLE IF NOT EXISTS `user_pgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `pgroup_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_pgroup_id` (`user_id`,`pgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_pgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_pgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_pgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_print_product
CREATE TABLE IF NOT EXISTS `user_print_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters` (`user_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_print_product: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_print_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_print_product` ENABLE KEYS */;


-- Dumping structure for table sas_master.user_shares
CREATE TABLE IF NOT EXISTS `user_shares` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `module_type_id` int(11) DEFAULT NULL,
  `share_option` tinyint(4) DEFAULT NULL,
  `share_users` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_except_users` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_searchs` (`module_type_id`,`share_option`,`share_users`(255),`share_except_users`(255)),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.user_shares: ~0 rows (approximately)
/*!40000 ALTER TABLE `user_shares` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_shares` ENABLE KEYS */;


-- Dumping structure for table sas_master.ut_api_auth_tokens
CREATE TABLE IF NOT EXISTS `ut_api_auth_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `auth_token` varchar(250) DEFAULT NULL,
  `auth_signature` varchar(250) DEFAULT NULL,
  `last_logged` datetime DEFAULT NULL,
  `expired_in` datetime DEFAULT NULL,
  `is_logged` char(1) DEFAULT '0',
  `is_expired` char(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `auth` (`auth_token`,`auth_signature`),
  KEY `status` (`is_logged`,`is_expired`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table sas_master.ut_api_auth_tokens: ~0 rows (approximately)
/*!40000 ALTER TABLE `ut_api_auth_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `ut_api_auth_tokens` ENABLE KEYS */;


-- Dumping structure for table sas_master.vat_modules
CREATE TABLE IF NOT EXISTS `vat_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vat_setting_id` int(11) DEFAULT NULL,
  `apply_to` tinyint(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `key_search` (`vat_setting_id`,`apply_to`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vat_modules: ~8 rows (approximately)
/*!40000 ALTER TABLE `vat_modules` DISABLE KEYS */;
INSERT INTO `vat_modules` (`id`, `vat_setting_id`, `apply_to`, `created`, `created_by`, `is_active`) VALUES
	(1, 1, 34, '2018-04-19 10:00:36', 1, 1),
	(2, 1, 68, '2018-04-19 10:00:36', 1, 1),
	(3, 1, 69, '2018-04-19 10:00:36', 1, 1),
	(4, 1, 25, '2018-04-19 10:00:36', 1, 1),
	(5, 1, 40, '2018-04-19 10:00:36', 1, 1),
	(6, 2, 48, '2018-04-19 10:01:51', 1, 1),
	(7, 2, 21, '2018-04-19 10:01:51', 1, 1),
	(8, 2, 41, '2018-04-19 10:01:51', 1, 1);
/*!40000 ALTER TABLE `vat_modules` ENABLE KEYS */;


-- Dumping structure for table sas_master.vat_settings
CREATE TABLE IF NOT EXISTS `vat_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1: Sales; 2: Purchase',
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vat_percent` decimal(6,3) DEFAULT NULL,
  `chart_account_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1' COMMENT '1: Active; 2: Deactive',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`),
  KEY `searchs` (`name`,`chart_account_id`,`is_active`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vat_settings: ~2 rows (approximately)
/*!40000 ALTER TABLE `vat_settings` DISABLE KEYS */;
INSERT INTO `vat_settings` (`id`, `sys_code`, `company_id`, `type`, `name`, `vat_percent`, `chart_account_id`, `created`, `created_by`, `modified`, `modified_by`, `is_active`) VALUES
	(1, '45df3021379594cf6b1d95b19e61d463', 1, 1, 'None', 0.000, 102, '2017-07-21 13:41:05', 1, '2018-04-19 10:00:36', 1, 1),
	(2, '7836a7a5b933b31a04eda437428ada3f', 1, 2, 'None', 0.000, 104, '2017-07-21 13:41:33', 1, '2018-04-19 10:01:51', 1, 1);
/*!40000 ALTER TABLE `vat_settings` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendors
CREATE TABLE IF NOT EXISTS `vendors` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `village_id` int(11) DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `work_telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `vendor_code` (`vendor_code`),
  KEY `sys_code` (`sys_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendors: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_companies
CREATE TABLE IF NOT EXISTS `vendor_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_consignments
CREATE TABLE IF NOT EXISTS `vendor_consignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `currency_center_id` int(11) DEFAULT NULL,
  `total_amount` decimal(15,3) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued; 2: Fullfiled',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filters` (`date`,`code`,`status`),
  KEY `filter2` (`location_group_id`,`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_consignments: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_consignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_consignments` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_consignment_details
CREATE TABLE IF NOT EXISTS `vendor_consignment_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_consignment_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `date_expired` date DEFAULT NULL,
  `qty` decimal(15,2) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `unit_cost` decimal(15,3) DEFAULT '0.000',
  `total_cost` decimal(15,3) DEFAULT '0.000',
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `vendor_consignment_id` (`vendor_consignment_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_consignment_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_consignment_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_consignment_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_consignment_returns
CREATE TABLE IF NOT EXISTS `vendor_consignment_returns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor_consignment_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `location_group_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `edited` datetime DEFAULT NULL,
  `edited_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '-1: Edit; 0: Void; 1: Issued; 2: Fullfiled',
  PRIMARY KEY (`id`),
  KEY `company` (`company_id`,`branch_id`),
  KEY `filters` (`date`,`code`,`status`),
  KEY `filter2` (`location_group_id`,`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_consignment_returns: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_consignment_returns` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_consignment_returns` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_consignment_return_details
CREATE TABLE IF NOT EXISTS `vendor_consignment_return_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_consignment_return_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `default_order` decimal(15,2) DEFAULT NULL,
  `lots_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '0',
  `date_expired` date DEFAULT NULL,
  `qty` decimal(15,2) DEFAULT NULL,
  `qty_uom_id` int(11) DEFAULT NULL,
  `conversion` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `vendor_consignment_return_id` (`vendor_consignment_return_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_consignment_return_details: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_consignment_return_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_consignment_return_details` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_contacts
CREATE TABLE IF NOT EXISTS `vendor_contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `title` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_telephone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `modules` (`company_id`,`vendor_id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`contact_name`,`is_active`,`contact_telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_contacts` ENABLE KEYS */;


-- Dumping structure for table sas_master.vendor_vgroups
CREATE TABLE IF NOT EXISTS `vendor_vgroups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendor_id` bigint(20) DEFAULT NULL,
  `vgroup_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `vgroup_id` (`vgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vendor_vgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `vendor_vgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_vgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.vgroups
CREATE TABLE IF NOT EXISTS `vgroups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` int(10) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` int(10) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vgroups: ~0 rows (approximately)
/*!40000 ALTER TABLE `vgroups` DISABLE KEYS */;
/*!40000 ALTER TABLE `vgroups` ENABLE KEYS */;


-- Dumping structure for table sas_master.vgroup_companies
CREATE TABLE IF NOT EXISTS `vgroup_companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vgroup_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vgroup_id` (`vgroup_id`),
  KEY `company_id` (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vgroup_companies: ~0 rows (approximately)
/*!40000 ALTER TABLE `vgroup_companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `vgroup_companies` ENABLE KEYS */;


-- Dumping structure for table sas_master.villages
CREATE TABLE IF NOT EXISTS `villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sys_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commune_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` bigint(20) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `sys_code` (`sys_code`),
  KEY `commune_id` (`commune_id`),
  KEY `searchs` (`name`,`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.villages: ~0 rows (approximately)
/*!40000 ALTER TABLE `villages` DISABLE KEYS */;
/*!40000 ALTER TABLE `villages` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_processes
CREATE TABLE IF NOT EXISTS `vs_processes` (
  `sync_script` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `is_processing` tinyint(4) DEFAULT '0',
  `total_will_receive` int(11) DEFAULT '0',
  `status` tinyint(4) DEFAULT '0' COMMENT '0: OFF, 1: ON',
  PRIMARY KEY (`sync_script`),
  KEY `key_filters` (`is_processing`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_processes: ~1 rows (approximately)
/*!40000 ALTER TABLE `vs_processes` DISABLE KEYS */;
INSERT INTO `vs_processes` (`sync_script`, `pid`, `start`, `end`, `is_processing`, `total_will_receive`, `status`) VALUES
	('sync', NULL, NULL, NULL, 0, 0, 1);
/*!40000 ALTER TABLE `vs_processes` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_process_histories
CREATE TABLE IF NOT EXISTS `vs_process_histories` (
  `pid` bigint(20) NOT NULL COMMENT 'PHP Process ID',
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`pid`),
  KEY `created` (`created`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_process_histories: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_process_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_process_histories` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_receives
CREATE TABLE IF NOT EXISTS `vs_receives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `s_t` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `syn_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contents` longtext COLLATE utf8_unicode_ci,
  `total_query` int(11) DEFAULT NULL,
  `received_time` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `filters` (`s_t`,`syn_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_receives: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_receives` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_receives` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_request_codes
CREATE TABLE IF NOT EXISTS `vs_request_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1' COMMENT '1: Issue; 2: Completed',
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_request_codes: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_request_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_request_codes` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_sends
CREATE TABLE IF NOT EXISTS `vs_sends` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL COMMENT 'Null: All Branch',
  `syn_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contents` longtext COLLATE utf8_unicode_ci,
  `content_size` int(11) DEFAULT NULL,
  `sent_time` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT '1: Send By Branch; 2: Send All Branch in Company',
  `status` tinyint(4) DEFAULT '1' COMMENT '1: Waiting Send; 2: Complete',
  PRIMARY KEY (`id`),
  KEY `created` (`created`),
  KEY `type_status` (`type`,`status`),
  KEY `filters` (`company_id`,`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_sends: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_sends` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_sends` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_syn_opts
CREATE TABLE IF NOT EXISTS `vs_syn_opts` (
  `s_t` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Rest Code of Project',
  `sct_c` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Security Code of Project',
  `type` tinyint(4) NOT NULL COMMENT '2: Cloud; 3: Master; 4: Company; 5: Slave',
  `p_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Project Code',
  PRIMARY KEY (`s_t`,`sct_c`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_syn_opts: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_syn_opts` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_syn_opts` ENABLE KEYS */;


-- Dumping structure for table sas_master.vs_s_t_ps
CREATE TABLE IF NOT EXISTS `vs_s_t_ps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_id` int(11) DEFAULT NULL,
  `slave_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `s_t` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Rest Code of Project',
  `sct_c` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Security Code of Project',
  `type` tinyint(4) DEFAULT NULL COMMENT '2: Cloud; 3: Master; 4: Company; 5: Slave',
  `total_sent` int(11) DEFAULT NULL,
  `total_received` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table sas_master.vs_s_t_ps: ~0 rows (approximately)
/*!40000 ALTER TABLE `vs_s_t_ps` DISABLE KEYS */;
/*!40000 ALTER TABLE `vs_s_t_ps` ENABLE KEYS */;


-- Dumping structure for event sas_master.zyDecreaseUnearn
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `zyDecreaseUnearn` ON SCHEDULE EVERY 25 SECOND STARTS '2018-02-16 17:25:25' ON COMPLETION PRESERVE DISABLE DO BEGIN
	DECLARE isProcess TINYINT(4) DEFAULT 0;
	SELECT is_processing INTO isProcess FROM unearned_tacks WHERE id = 1;
	IF isProcess = 0 THEN
		## Update track Status
		UPDATE unearned_tacks SET is_processing = 1, `start` = NOW() WHERE id = 1;
		## Add Unearn to Move
		INSERT INTO `unearned_recognitions` (`sys_code`, `module_id`, `module_detail_id`, `customer_id`, `date`, `debit_account_id`, `credit_account_id`, `debit`, `credit`, `type`, `created`, `created_by`) 
		SELECT `sys_code`, `module_id`, `module_detail_id`, `customer_id`, `date`, `debit_account_id`, `credit_account_id`, `debit`, `credit`, `type`, `created`, `created_by` FROM unearned_schedules WHERE `date` <= DATE(NOW());
		## Call Function Move
		CALL zyUnearnedRecognition();
	END IF;
END//
DELIMITER ;


-- Dumping structure for event sas_master.zyRemoveAppToken
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `zyRemoveAppToken` ON SCHEDULE EVERY 15 MINUTE STARTS '2018-02-20 14:52:02' ON COMPLETION PRESERVE DISABLE DO BEGIN
	DELETE FROM ut_api_auth_tokens WHERE expired_in < NOW() OR is_expired = 1;
END//
DELIMITER ;


-- Dumping structure for event sas_master.zyRemoveSync
DELIMITER //
CREATE DEFINER=`root`@`localhost` EVENT `zyRemoveSync` ON SCHEDULE EVERY 1 DAY STARTS '2018-01-29 17:11:53' ON COMPLETION PRESERVE DISABLE DO BEGIN
	DELETE FROM vs_sends WHERE status = 2 AND DATE_ADD(created, INTERVAL 7 DAY) < now();
	DELETE FROM vs_receives WHERE DATE_ADD(created, INTERVAL 7 DAY) < now();
	DELETE FROM vs_request_codes WHERE status = 3;
END//
DELIMITER ;


-- Dumping structure for procedure sas_master.zyUnearnedRecognition
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `zyUnearnedRecognition`()
BEGIN
	DECLARE done TINYINT(4) DEFAULT 0;
	DECLARE scheduleId VARCHAR(50);
   DECLARE moduleId INT(11) DEFAULT 0;
   DECLARE moduleDetailId INT(11) DEFAULT 0;
   DECLARE customerId INT(11) DEFAULT 0;
   DECLARE startDate DATE;
   DECLARE amtDebit DECIMAL(20,9) DEFAULT 0;
   DECLARE amtCredit DECIMAL(20,9) DEFAULT 0;
   DECLARE accDebit INT(11);
   DECLARE accCredit INT(11);
   DECLARE createdBy INT(11);
   DECLARE sysCode VARCHAR(50);
   DECLARE getGLId INT(11);
   DECLARE InvoiceCode VARCHAR(50);
	DECLARE CompanyId INT(11);
	DECLARE BranchId INT(11);
	DECLARE WarehouseId INT(11);
	DECLARE ServiceId INT(11);
	DECLARE ServiceName VARCHAR(50);
	DECLARE glMemo VARCHAR(250);
   DEClARE unearnedRecog CURSOR FOR SELECT sys_code, module_id, module_detail_id, customer_id, `date`, `debit_account_id`, credit_account_id, debit, `credit`, `created_by` FROM unearned_recognitions WHERE `status` = 1;
   DEClARE CONTINUE HANDLER FOR 1329 
	BEGIN 
	  SET done = 1; 
	END;
   ## OPEN CURSOR
   OPEN unearnedRecog;
   	## LOOP
   	get_unearn: LOOP FETCH unearnedRecog INTO scheduleId, moduleId, moduleDetailId, customerId, startDate, accDebit, accCredit, amtDebit, amtCredit, createdBy;
	 	   ## Check EMPTY OR NOT
			IF done = 1 THEN 
				UPDATE unearned_tacks SET is_processing = 0, `end` = NOW() WHERE id = 1;
				LEAVE get_unearn;
			END IF;
			## GET Invoice and Service Info
			SELECT so_code, company_id, branch_id, location_group_id INTO InvoiceCode, CompanyId, BranchId, WarehouseId FROM sales_orders WHERE id = moduleId;
			SELECT srv.id, CONCAT_WS(' - ', srv.code, srv.name) INTO ServiceId, ServiceName FROM sales_order_services AS srs INNER JOIN services AS srv ON srv.id = srs.service_id WHERE srs.id = moduleDetailId;
			SET glMemo = CONCAT('ICS: INV # ', InvoiceCode, ' Service # ', ServiceName);
			SELECT MD5(CONCAT(NOW(),moduleId,startDate)) INTO sysCode;
			## INSERT GENERAL LEDGER
			INSERT INTO `general_ledgers` (`sys_code`, `sales_order_id`, `date`, `reference`, `created`, `created_by`, `modified`, `is_sys`) 
			VALUES (sysCode, moduleId, startDate, InvoiceCode, NOW(), createdBy, NOW(), createdBy);
			SET getGLId = LAST_INSERT_ID();
			## Unearned Revenue
			INSERT INTO `general_ledger_details` (`general_ledger_id`, `chart_account_id`, `company_id`, `branch_id`, `location_group_id`, `service_id`, `type`, `debit`, `memo`, `customer_id`) 
			VALUES (getGLId, accDebit, CompanyId, BranchId, WarehouseId, ServiceId, 'Unearned Recognition', amtDebit, glMemo, customerId);
			## Service Income
			INSERT INTO `general_ledger_details` (`general_ledger_id`, `chart_account_id`, `company_id`, `branch_id`, `location_group_id`, `service_id`, `type`, `credit`, `memo`, `customer_id`) 
			VALUES (getGLId, accCredit, CompanyId, BranchId, WarehouseId, ServiceId, 'Unearned Recognition', amtCredit, glMemo, customerId);
			## Delete Unearned Schedule
			DELETE FROM unearned_schedules WHERE `sys_code` = scheduleId;
		## END LOOP
		END LOOP get_unearn;
	## Close CURSOR
	CLOSE unearnedRecog;
	UPDATE unearned_recognitions SET `status` = 2 WHERE `date` <= DATE(NOW());
END//
DELIMITER ;


-- Dumping structure for trigger sas_master.z1GroupDetailAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1GroupDetailAfInsert` AFTER INSERT ON `1_group_total_details` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `g_inventory_detail` =  (`g_inventory_detail` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1GroupDetailAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1GroupDetailAfUpdate` AFTER UPDATE ON `1_group_total_details` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `g_inventory_detail` =  (`g_inventory_detail` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1GroupTotalAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1GroupTotalAfInsert` AFTER INSERT ON `1_group_totals` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `g_inventory`=(`g_inventory` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1GroupTotalAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1GroupTotalAfUpdate` AFTER UPDATE ON `1_group_totals` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `g_inventory`=(`g_inventory` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1InventoryAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1InventoryAfInsert` AFTER INSERT ON `1_inventories` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `loc_inventory`=(`loc_inventory` + 1) WHERE  `id`=NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1InventoryDetailAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1InventoryDetailAfInsert` AFTER INSERT ON `1_inventory_total_details` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `loc_inventory_detail`=(`loc_inventory_detail` + 1) WHERE  `id`=NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1InventoryDetailAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1InventoryDetailAfUpdate` AFTER UPDATE ON `1_inventory_total_details` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `loc_inventory_detail`=(`loc_inventory_detail` + 1) WHERE  `id`=NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1InventoryTotalAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1InventoryTotalAfInsert` AFTER INSERT ON `1_inventory_totals` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `loc_inventory_total`=(`loc_inventory_total` + 1) WHERE  `id`=NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.z1InventoryTotalAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `z1InventoryTotalAfUpdate` AFTER UPDATE ON `1_inventory_totals` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `loc_inventory_total`=(`loc_inventory_total` + 1) WHERE  `id`=NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zAccountBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zAccountBfInsert` BEFORE INSERT ON `accounts` FOR EACH ROW BEGIN
	IF NEW.product_id = "" OR NEW.product_id = NULL OR NEW.account_type_id = "" OR NEW.account_type_id = NULL OR NEW.chart_account_id = "" OR NEW.chart_account_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zAccountClosingAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zAccountClosingAfUpdate` AFTER UPDATE ON `account_closing_dates` FOR EACH ROW BEGIN
	INSERT INTO account_closing_date_histories (`date`, `created`, `created_by`) VALUES (NEW.date, NEW.created, NEW.created_by);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchAfInsert` AFTER INSERT ON `branches` FOR EACH ROW BEGIN
	INSERT INTO product_branches (branch_id, product_id) SELECT NEW.id, products.id FROM products WHERE company_id = NEW.company_id;
	IF NEW.id = 1 THEN
		INSERT INTO user_branches (user_id, branch_id) SELECT users.id, NEW.id FROM users;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchBfInsert` BEFORE INSERT ON `branches` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.branch_type_id = "" OR NEW.branch_type_id = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_other = "" OR NEW.name_other = NULL OR NEW.work_start = "" OR NEW.work_start = NULL OR NEW.work_end = "" OR NEW.work_end = NULL OR NEW.address = "" OR NEW.address = NULL OR NEW.address_other = "" OR NEW.address_other = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
   END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchBfUpdate` BEFORE UPDATE ON `branches` FOR EACH ROW BEGIN
	DECLARE isSale TINYINT(4);
	DECLARE isOrder TINYINT(4);
	DECLARE isQuote TINYINT(4);
	DECLARE isCM TINYINT(4);
	DECLARE isPO TINYINT(4);
	DECLARE isPB TINYINT(4);
	DECLARE isBR TINYINT(4);
	DECLARE isAdj TINYINT(4);
	DECLARE isRS TINYINT(4);
	DECLARE isTO TINYINT(4);
	DECLARE isCusCsm TINYINT(4);
	DECLARE isCusRCsm TINYINT(4);
	DECLARE isLandCost TINYINT(4);
	DECLARE isVenCsm TINYINT(4);
	DECLARE isVenRCsm TINYINT(4);
	DECLARE isJournal TINYINT(4);
	IF NEW.act = 2 THEN
		SELECT COUNT(id) INTO isSale FROM sales_orders WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isOrder FROM orders WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isQuote FROM quotations WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isCM FROM credit_memos WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isPO FROM purchase_requests WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isPB FROM purchase_orders WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isBR FROM purchase_returns WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isAdj FROM cycle_products WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isRS FROM request_stocks WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isTO FROM transfer_orders WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isCusCsm FROM consignments WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isCusRCsm FROM consignment_returns WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isLandCost FROM landing_costs WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isVenCsm FROM vendor_consignments WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(id) INTO isVenRCsm FROM vendor_consignment_returns WHERE branch_id = OLD.id AND status > 0 LIMIT 1;
		SELECT COUNT(general_ledger_details.id) INTO isJournal FROM general_ledger_details INNER JOIN general_ledgers ON general_ledgers.id = general_ledger_details.general_ledger_id WHERE general_ledger_details.branch_id = OLD.id AND general_ledgers.is_active = 1 LIMIT 1;
		IF isSale > 0 OR isOrder > 0 OR isQuote > 0 OR isCM > 0 OR isPO > 0 OR isPB > 0 OR isBR > 0 OR isAdj > 0 OR isRS > 0 OR isTO > 0 OR isCusCsm > 0 OR isCusRCsm > 0 OR isLandCost > 0 OR isVenCsm > 0 OR isVenRCsm > 0 OR isJournal > 0 THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data could not been delete';
		END IF;
	ELSE
		IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.branch_type_id = "" OR NEW.branch_type_id = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_other = "" OR NEW.name_other = NULL OR NEW.work_start = "" OR NEW.work_start = NULL OR NEW.work_end = "" OR NEW.work_end = NULL OR NEW.address = "" OR NEW.address = NULL OR NEW.address_other = "" OR NEW.address_other = NULL THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchCurrencyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchCurrencyBfInsert` BEFORE INSERT ON `branch_currencies` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.branch_id = "" OR NEW.branch_id = NULL OR NEW.currency_center_id = "" OR NEW.currency_center_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchTypeBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchTypeBeforeDelete` BEFORE DELETE ON `branch_types` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete this branch type';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchTypeBeforeUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchTypeBeforeUpdate` BEFORE UPDATE ON `branch_types` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete/update this branch type';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zBranchTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zBranchTypeBfInsert` BEFORE INSERT ON `branch_types` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCgroupBfInsert` BEFORE INSERT ON `cgroups` FOR EACH ROW BEGIN
	IF NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCgroupCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCgroupCompanyBfInsert` BEFORE INSERT ON `cgroup_companies` FOR EACH ROW BEGIN
	IF NEW.cgroup_id = "" OR NEW.cgroup_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCgroupPriceTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCgroupPriceTypeBfInsert` BEFORE INSERT ON `cgroup_price_types` FOR EACH ROW BEGIN
	IF NEW.cgroup_id = "" OR NEW.cgroup_id = NULL OR NEW.price_type_id = "" OR NEW.price_type_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zChartAccountBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zChartAccountBfInsert` BEFORE INSERT ON `chart_accounts` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.chart_account_type_id = "" OR NEW.chart_account_type_id = NULL OR NEW.chart_account_group_id = "" OR NEW.chart_account_group_id = NULL OR NEW.account_codes = "" OR NEW.account_codes = NULL OR NEW.account_description = "" OR NEW.account_description = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zChartAccountCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zChartAccountCompanyBfInsert` BEFORE INSERT ON `chart_account_companies` FOR EACH ROW BEGIN
	IF NEW.chart_account_id = "" OR NEW.chart_account_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zChartAccountGroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zChartAccountGroupBfInsert` BEFORE INSERT ON `chart_account_groups` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.chart_account_type_id = "" OR NEW.chart_account_type_id = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zClassBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zClassBfInsert` BEFORE INSERT ON `classes` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zClassCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zClassCompanyBfInsert` BEFORE INSERT ON `class_companies` FOR EACH ROW BEGIN
	IF NEW.class_id = "" OR NEW.class_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCommunesBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCommunesBfInsert` BEFORE INSERT ON `communes` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.district_id = "" OR NEW.district_id = NULL THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCompanyAfterInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCompanyAfterInsert` AFTER INSERT ON `companies` FOR EACH ROW BEGIN
	INSERT INTO customer_companies (customer_id, company_id) VALUES (1, NEW.id);
	INSERT INTO chart_account_companies (chart_account_id, company_id) SELECT id, NEW.id FROM chart_accounts WHERE id >= 1 AND id <= 108;
	INSERT INTO price_type_companies (price_type_id, company_id) SELECT id, NEW.id FROM price_types WHERE 1;
	INSERT INTO user_companies (user_id, company_id) SELECT id, NEW.id FROM users;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCompanyBeforeInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCompanyBeforeInsert` BEFORE INSERT ON `companies` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.photo = "" OR NEW.photo = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_other = "" OR NEW.name_other = NULL OR NEW.currency_center_id = "" OR NEW.currency_center_id = NULL OR NEW.vat_calculate = "" OR NEW.vat_calculate = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCompanyBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCompanyBfUpdate` BEFORE UPDATE ON `companies` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.photo = "" OR NEW.photo = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_other = "" OR NEW.name_other = NULL OR NEW.currency_center_id = "" OR NEW.currency_center_id = NULL OR NEW.vat_calculate = "" OR NEW.vat_calculate = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCompanyCategoryBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCompanyCategoryBfInsert` BEFORE INSERT ON `company_with_categories` FOR EACH ROW BEGIN
	IF NEW.company_id = "" OR NEW.company_id = NULL OR NEW.company_category_id = "" OR NEW.company_category_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCreditMemoAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCreditMemoAfInsert` AFTER INSERT ON `credit_memos` FOR EACH ROW BEGIN
	DECLARE salesMonth int(11);
	DECLARE salesYear int(11);
	SET salesMonth = MONTH(NEW.order_date);
	SET salesYear  = YEAR(NEW.order_date);
	IF NEW.status = 2 THEN
		INSERT INTO report_sales_by_days (`date`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `c_total_amount`, `c_total_discount`, `c_total_mark_up`, `c_total_vat`) 
		VALUES (NEW.order_date, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.mark_up, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE c_total_amount = c_total_amount + NEW.total_amount, c_total_discount = c_total_discount + NEW.discount, c_total_mark_up = c_total_mark_up + NEW.mark_up, c_total_vat = c_total_vat + NEW.total_vat;
		INSERT INTO report_sales_by_months (`month`, `year`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `c_total_amount`, `c_total_discount`, `c_total_mark_up`, `c_total_vat`) 
		VALUES (salesMonth, salesYear, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.mark_up, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE c_total_amount = c_total_amount + NEW.total_amount, c_total_discount = c_total_discount + NEW.discount, c_total_mark_up = c_total_mark_up + NEW.mark_up, c_total_vat = c_total_vat + NEW.total_vat;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCreditMemoAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCreditMemoAfUpdate` AFTER UPDATE ON `credit_memos` FOR EACH ROW BEGIN
	DECLARE salesMonth int(11);
	DECLARE salesYear int(11);
	SET salesMonth = MONTH(OLD.order_date);
	SET salesYear  = YEAR(OLD.order_date);
	IF (OLD.status = 2 AND NEW.status = -1) OR (OLD.status = 2 AND NEW.status = 0) THEN
		UPDATE report_sales_by_days SET c_total_amount = c_total_amount - OLD.total_amount, c_total_discount = c_total_discount - OLD.discount, c_total_mark_up = c_total_mark_up - OLD.mark_up, c_total_vat = c_total_vat - OLD.total_vat WHERE date = OLD.order_date AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = NEW.sales_rep_id;
		UPDATE report_sales_by_months SET c_total_amount = c_total_amount - OLD.total_amount, c_total_discount = c_total_discount - OLD.discount, c_total_mark_up = c_total_mark_up - OLD.mark_up, c_total_vat = c_total_vat - OLD.total_vat WHERE month = salesMonth AND year = salesYear AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = NEW.sales_rep_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerBeforeDelete` BEFORE DELETE ON `customers` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete general customer';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerBeforeUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerBeforeUpdate` BEFORE UPDATE ON `customers` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot edit general customer';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerBfInsert` BEFORE INSERT ON `customers` FOR EACH ROW BEGIN
	IF NEW.customer_code = "" OR NEW.customer_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_kh = "" OR NEW.name_kh = NULL OR NEW.main_number = "" OR NEW.main_number = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerCgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerCgroupBfInsert` BEFORE INSERT ON `customer_cgroups` FOR EACH ROW BEGIN
	IF NEW.customer_id = "" OR NEW.customer_id = NULL OR NEW.cgroup_id = "" OR NEW.cgroup_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerCompanyBfInsert` BEFORE INSERT ON `customer_companies` FOR EACH ROW BEGIN
	IF NEW.customer_id = "" OR NEW.customer_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zCustomerContactBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zCustomerContactBfInsert` BEFORE INSERT ON `customer_contacts` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.customer_id = "" OR NEW.customer_id = NULL OR NEW.contact_name = "" OR NEW.contact_name = NULL OR NEW.sex = "" OR NEW.sex = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zDiscountBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zDiscountBfInsert` BEFORE INSERT ON `discounts` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.name = "" OR NEW.name = NULL OR (NEW.percent = "" AND NEW.amount = "") THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zDistrictBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zDistrictBfInsert` BEFORE INSERT ON `districts` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.province_id = "" OR NEW.province_id = NULL THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zEgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zEgroupBfInsert` BEFORE INSERT ON `egroups` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zEgroupCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zEgroupCompanyBfInsert` BEFORE INSERT ON `egroup_companies` FOR EACH ROW BEGIN
	IF NEW.egroup_id = "" OR NEW.egroup_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zEmployeeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zEmployeeBfInsert` BEFORE INSERT ON `employees` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.employee_code = "" OR NEW.employee_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.name_kh = "" OR NEW.name_kh = NULL OR NEW.sex = "" OR NEW.sex = NULL OR NEW.dob = "" OR NEW.dob = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zEmployeeCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zEmployeeCompanyBfInsert` BEFORE INSERT ON `employee_companies` FOR EACH ROW BEGIN
	IF NEW.employee_id = "" OR NEW.employee_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zEProductPriceBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zEProductPriceBeforeDelete` BEFORE DELETE ON `e_product_prices` FOR EACH ROW BEGIN
	INSERT INTO e_product_price_histories VALUES (NULL, OLD.product_id, OLD.uom_id, OLD.before_price, OLD.sell_price, OLD.created, OLD.created_by);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zExchangeRateBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zExchangeRateBfInsert` BEFORE INSERT ON `exchange_rates` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.branch_id = "" OR NEW.branch_id = NULL OR NEW.currency_center_id = "" OR NEW.currency_center_id = NULL OR NEW.rate_to_sell = "" OR NEW.rate_to_sell = NULL OR NEW.rate_to_change = "" OR NEW.rate_to_change = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zInventoriesAfterInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zInventoriesAfterInsert` AFTER INSERT ON `inventories` FOR EACH ROW BEGIN
	DECLARE qty decimal(15,3);
	DECLARE totalQty decimal(15,3);
	DECLARE totalAmtSales decimal(15,3);
	DECLARE totalAmtPurchase decimal(15,3);
   SELECT SUM((total_pb + total_cm + total_cycle) - (total_so + total_pos + total_pbc)) INTO totalQty FROM inventory_total_by_dates WHERE product_id = NEW.product_id AND lots_number = NEW.lots_number AND expired_date = NEW.date_expired AND date <= NEW.date;
	IF totalQty IS NULL THEN SET totalQty = 0; END IF;
	IF NEW.type = 'Inv Adj' THEN 
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_cycle`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_cycle = (total_cycle + qty), total_ending = totalQty;
	ELSEIF NEW.type = 'Purchase' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtPurchase = ROUND(NEW.qty * NEW.unit_cost, 2);
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_purchase`, `total_pb`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtPurchase, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pb = (total_pb + qty), total_amount_purchase = (total_amount_purchase + totalAmtPurchase), total_ending = totalQty;
   ELSEIF NEW.type = 'Void Purchase' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtPurchase = ROUND(NEW.qty * NEW.unit_cost, 2) * -1;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_purchase`, `total_pb`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtPurchase, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pb = (total_pb + qty), total_amount_purchase = (total_amount_purchase + totalAmtPurchase), total_ending = totalQty;
	ELSEIF NEW.type = 'Purchase Return' THEN
		SET qty  = (NEW.qty * -1);
		SET totalQty =  totalQty - qty;
		SET totalAmtPurchase = ROUND((NEW.qty * NEW.unit_cost), 2);
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_purchase`, `total_pbc`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtPurchase, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pbc = (total_pbc + qty), total_amount_purchase = (total_amount_purchase + totalAmtPurchase), total_ending = totalQty;
	ELSEIF NEW.type = 'Void Purchase Return' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtPurchase = ROUND((NEW.qty * NEW.unit_cost), 2);
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_purchase`, `total_pbc`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtPurchase, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pbc = (total_pbc - qty), total_amount_purchase = (total_amount_purchase + totalAmtPurchase), total_ending = totalQty;
	ELSEIF NEW.type = 'POS' THEN
		SET qty  = (NEW.qty * -1);
		SET totalQty =  totalQty - qty;
		SET totalAmtSales = NEW.unit_price;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_pos`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pos = (total_pos + qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
   ELSEIF NEW.type = 'Void POS' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtSales = NEW.unit_price * -1;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_pos`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_pos = (total_pos - qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
	ELSEIF NEW.type = 'Sale' THEN
		SET qty  = (NEW.qty * -1);
		SET totalQty =  totalQty - qty;
		SET totalAmtSales = NEW.unit_price;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_so`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_so = (total_so + qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
	ELSEIF NEW.type = 'Void Sale' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtSales = NEW.unit_price * -1;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_so`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_so = (total_so - qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
	ELSEIF NEW.type = 'Sales Return' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtSales = NEW.unit_price * -1;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_cm`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_cm = (total_cm + qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
   ELSEIF NEW.type = 'Void Sales Return' THEN
		SET qty  = NEW.qty;
		SET totalQty =  totalQty + qty;
		SET totalAmtSales = NEW.unit_price;
		INSERT INTO inventory_total_by_dates (`product_id`, `lots_number`, `expired_date`, `date`, `total_amount_sales`, `total_cm`, `total_ending`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.date, totalAmtSales, qty, totalQty) 
		ON DUPLICATE KEY UPDATE total_cm = (total_cm + qty), total_amount_sales = (total_amount_sales + totalAmtSales), total_ending = totalQty;
	END IF;
	
	INSERT INTO product_inventories (`product_id`, `lots_number`, `expired_date`, `location_group_id`, `location_id`, `total_qty`) VALUES (NEW.product_id, NEW.lots_number, NEW.date_expired, NEW.location_group_id, NEW.location_id, NEW.qty) 
	ON DUPLICATE KEY UPDATE total_qty = (total_qty + NEW.qty);
	## Transaction
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `inventory` =  (`inventory` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zInventoryTotalAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zInventoryTotalAfInsert` AFTER INSERT ON `inventory_totals` FOR EACH ROW BEGIN
	## Transaction
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `inventory_total` =  (`inventory_total` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zInventoryTotalAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zInventoryTotalAfUpdate` AFTER UPDATE ON `inventory_totals` FOR EACH ROW BEGIN
	## Transaction
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != '' THEN
		UPDATE `transaction_details` SET `inventory_total` =  (`inventory_total` + 1) WHERE  `id`= NEW.transaction_detail_id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zInventoryValutaionAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zInventoryValutaionAfInsert` AFTER INSERT ON `inventory_valuations` FOR EACH ROW BEGIN
	IF NEW.transaction_detail_id IS NOT NULL OR NEW.transaction_detail_id != ""  THEN
		UPDATE transaction_details SET inventory_valutaion = 1 WHERE id = NEW.transaction_detail_id;
	END IF;
	## Backgroup Calculate COGS
	INSERT INTO `inventory_valuation_cals` (`date`, `product_id`, `is_lock`, `created`) 
   VALUES (NEW.date, NEW.pid, 0, NEW.created) 
   ON DUPLICATE KEY UPDATE created = NEW.created;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLandCostTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLandCostTypeBfInsert` BEFORE INSERT ON `landed_cost_types` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationAfInsert` AFTER INSERT ON `locations` FOR EACH ROW BEGIN
	INSERT INTO user_locations (user_id, location_id) SELECT id, NEW.id FROM users;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationBfInsert` BEFORE INSERT ON `locations` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code IS NULL OR NEW.location_group_id IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationBfUpdate` BEFORE UPDATE ON `locations` FOR EACH ROW BEGIN
	DECLARE isCheck TINYINT(4);
	SELECT SUM(IFNULL(qty, 0)) INTO isCheck FROM inventories WHERE location_id = OLD.id;
	IF OLD.is_active = 1 AND NEW.is_active =2 THEN
		IF isCheck > 0 THEN 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data cloud not been delete';
		END IF;
   ELSE
   	IF OLD.location_group_id != NEW.location_group_id THEN
   		IF isCheck > 0 THEN
   			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot change warehouse';
   		END IF;
      ELSE 
      	IF NEW.location_group_id IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
				SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
			END IF;
   	END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupBfInsert` BEFORE INSERT ON `location_groups` FOR EACH ROW BEGIN
   DECLARE isAllowNeg TINYINT(4);
   DECLARE isComTrans TINYINT(4);
	IF NEW.sys_code = "" OR NEW.sys_code IS NULL OR NEW.location_group_type_id IS NULL OR NEW.code = "" OR NEW.code IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
   ELSE 
   	SELECT allow_negative_stock, stock_tranfer_confirm  INTO isAllowNeg, isComTrans FROM location_group_types WHERE id = NEW.location_group_type_id;
	   SET NEW.allow_negative_stock = isAllowNeg;
	   SET NEW.stock_tranfer_confirm = isComTrans;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupBfUpdate` BEFORE UPDATE ON `location_groups` FOR EACH ROW BEGIN
	DECLARE isCheck TINYINT(4);
	DECLARE isAllowNeg TINYINT(4);
	DECLARE isComTrans TINYINT(4);
	IF OLD.is_active = 1 AND NEW.is_active =2 THEN
		SELECT COUNT(id) INTO isCheck FROM locations WHERE location_group_id = OLD.id AND is_active = 1;
		IF isCheck > 0 THEN 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data cloud not been delete';
		END IF;
   ELSE 
   	IF NEW.location_group_type_id IS NULL OR NEW.code = "" OR NEW.code IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	   ELSE 
	   	SELECT allow_negative_stock, stock_tranfer_confirm  INTO isAllowNeg, isComTrans FROM location_group_types WHERE id = NEW.location_group_type_id;
	   	SET NEW.allow_negative_stock = isAllowNeg;
	   	SET NEW.stock_tranfer_confirm = isComTrans;
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupTypeAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupTypeAfInsert` AFTER INSERT ON `location_group_types` FOR EACH ROW BEGIN
	UPDATE location_groups SET allow_negative_stock = NEW.allow_negative_stock, stock_tranfer_confirm = NEW.stock_tranfer_confirm WHERE location_group_type_id = NEW.id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupTypeAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupTypeAfUpdate` AFTER UPDATE ON `location_group_types` FOR EACH ROW BEGIN
	IF NEW.is_active = 1 THEN 
		UPDATE location_groups SET allow_negative_stock = NEW.allow_negative_stock, stock_tranfer_confirm = NEW.stock_tranfer_confirm WHERE location_group_type_id = NEW.id;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupTypeBfInsert` BEFORE INSERT ON `location_group_types` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationGroupTypeBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationGroupTypeBfUpdate` BEFORE UPDATE ON `location_group_types` FOR EACH ROW BEGIN
	DECLARE isCheck TINYINT(4);
	IF OLD.is_active = 1 AND NEW.is_active =2 THEN
		SELECT COUNT(id) INTO isCheck FROM location_groups WHERE location_group_type_id = OLD.id;
		IF isCheck > 0 THEN 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data cloud not been delete';
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zLocationSettingBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zLocationSettingBeforeDelete` BEFORE DELETE ON `location_settings` FOR EACH ROW BEGIN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default records';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zModuleCodeBranchBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zModuleCodeBranchBfInsert` BEFORE INSERT ON `module_code_branches` FOR EACH ROW BEGIN
	IF NEW.branch_id = "" OR NEW.branch_id = NULL OR NEW.adj_code = "" OR NEW.adj_code = NULL OR NEW.request_code = "" OR NEW.request_code = NULL OR NEW.to_code = "" OR NEW.to_code = NULL OR NEW.tr_code = "" OR NEW.tr_code = NULL OR NEW.pos_code = "" OR NEW.pos_code = NULL OR NEW.pos_rep_code = "" OR NEW.pos_rep_code = NULL OR NEW.quote_code = "" OR NEW.quote_code = NULL OR NEW.so_code = "" OR NEW.so_code = NULL OR NEW.inv_code = "" OR NEW.inv_code = NULL OR NEW.inv_rep_code = "" OR NEW.inv_rep_code = NULL OR NEW.dn_code = "" OR NEW.dn_code = NULL OR NEW.receive_pay_code = "" OR NEW.receive_pay_code = NULL OR NEW.cm_code = "" OR NEW.cm_code = NULL OR NEW.cm_rep_code = "" OR NEW.cm_rep_code = NULL OR NEW.po_code = "" OR NEW.po_code = NULL OR NEW.pb_code = "" OR NEW.pb_code = NULL OR NEW.pb_rep_code = "" OR NEW.pb_rep_code = NULL OR NEW.br_code = "" OR NEW.br_code = NULL OR NEW.br_rep_code = "" OR NEW.br_rep_code = NULL OR NEW.pay_bill_code = "" OR NEW.pay_bill_code = NULL OR NEW.cus_consign_code = "" OR NEW.cus_consign_code = NULL OR NEW.cus_consign_return_code = "" OR NEW.cus_consign_return_code = NULL OR NEW.ven_consign_code = "" OR NEW.ven_consign_code = NULL OR NEW.ven_consign_return_code = "" OR NEW.ven_consign_return_code = NULL OR NEW.landed_cost_code = "" OR NEW.landed_cost_code = NULL OR NEW.landed_cost_receipt_code = "" OR NEW.landed_cost_receipt_code = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zOtherBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zOtherBfInsert` BEFORE INSERT ON `others` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zOtherCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zOtherCompanyBfInsert` BEFORE INSERT ON `other_companies` FOR EACH ROW BEGIN
	IF NEW.other_id = "" OR NEW.other_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPaymentTermBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPaymentTermBfInsert` BEFORE INSERT ON `payment_terms` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.net_days = "" OR NEW.net_days = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPgroupAccountBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPgroupAccountBfInsert` BEFORE INSERT ON `pgroup_accounts` FOR EACH ROW BEGIN
	IF NEW.pgroup_id = "" OR NEW.pgroup_id = NULL OR NEW.account_type_id = "" OR NEW.account_type_id = NULL OR NEW.chart_account_id = "" OR NEW.chart_account_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPgroupBfInsert` BEFORE INSERT ON `pgroups` FOR EACH ROW BEGIN
	IF NEW.name = "" OR NEW.name = "" THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPgroupCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPgroupCompanyBfInsert` BEFORE INSERT ON `pgroup_companies` FOR EACH ROW BEGIN
	IF NEW.pgroup_id = "" OR NEW.pgroup_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPlaceBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPlaceBfInsert` BEFORE INSERT ON `places` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPositionBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPositionBfInsert` BEFORE INSERT ON `positions` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPriceTypeBfDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPriceTypeBfDelete` BEFORE DELETE ON `price_types` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete this price type';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPriceTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPriceTypeBfInsert` BEFORE INSERT ON `price_types` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.ordering = NULL OR NEW.is_set = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProcessBeforeUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProcessBeforeUpdate` BEFORE UPDATE ON `vs_processes` FOR EACH ROW BEGIN
	IF OLD.is_processing = 0 AND NEW.is_processing = 1 THEN
		INSERT INTO vs_process_histories (`pid`, `start`, `created`) VALUES (NEW.pid, NEW.start, now());
	ELSEIF OLD.is_processing = 1 AND NEW.is_processing = 0 THEN
		UPDATE vs_process_histories SET `end` = NEW.end WHERE `pid` = OLD.pid;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductAfInsert` AFTER INSERT ON `products` FOR EACH ROW BEGIN
	UPDATE cache_datas SET modified = now() WHERE `type` = 'Products';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductAfUpdate` AFTER UPDATE ON `products` FOR EACH ROW BEGIN
	UPDATE cache_datas SET modified = now() WHERE `type` = 'Products';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductBfInsert` BEFORE INSERT ON `products` FOR EACH ROW BEGIN
	IF NEW.barcode = "" OR NEW.barcode IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
	IF NEW.barcode = "P" THEN
		SET NEW.barcode = (SELECT CONCAT(NEW.barcode, (SELECT LPAD(((SELECT count(tmp.id) FROM `products` as tmp WHERE tmp.barcode LIKE CONCAT(NEW.barcode, '%')) + 1),6,'0'))));
	END IF;
	SET NEW.code = NEW.barcode;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductBfUpdate` BEFORE UPDATE ON `products` FOR EACH ROW BEGIN
	DECLARE isCheck TINYINT(4);
	IF OLD.is_active = 1 AND NEW.is_active =2 THEN
		SELECT SUM(total_qty) INTO isCheck FROM inventory_totals WHERE product_id = OLD.id GROUP BY product_id;
		IF isCheck > 0 THEN 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Data cloud not been delete';
		END IF;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductBranchBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductBranchBfInsert` BEFORE INSERT ON `product_branches` FOR EACH ROW BEGIN
	IF NEW.product_id = "" OR NEW.product_id = NULL OR NEW.branch_id = "" OR NEW.branch_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductPgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductPgroupBfInsert` BEFORE INSERT ON `product_pgroups` FOR EACH ROW BEGIN
	IF NEW.product_id = "" OR NEW.product_id = NULL OR NEW.pgroup_id = "" OR NEW.pgroup_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductPriceAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductPriceAfUpdate` AFTER UPDATE ON `product_prices` FOR EACH ROW BEGIN
	INSERT INTO product_price_histories (branch_id, price_type_id, product_id, uom_id, unit_cost, old_amount_before, old_amount, old_percent, old_add_on, old_set_type, new_amount_before, new_amount, new_percent, new_add_on, new_set_type, created, created_by) VALUES
	(NEW.branch_id, OLD.price_type_id, OLD.product_id, OLD.uom_id, OLD.old_unit_cost, OLD.amount_before, OLD.amount, OLD.percent, OLD.add_on, OLD.set_type, NEW.amount_before, NEW.amount, NEW.percent, NEW.add_on, NEW.set_type, NEW.created, NEW.created_by);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductPriceBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductPriceBfInsert` BEFORE INSERT ON `product_prices` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.branch_id = "" OR NEW.branch_id = NULL OR NEW.product_id = "" OR NEW.product_id = NULL OR NEW.price_type_id = "" OR NEW.price_type_id = NULL OR NEW.uom_id = "" OR NEW.uom_id = NULL OR NEW.set_type = "" OR NEW.set_type = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductWithPacketBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductWithPacketBfInsert` BEFORE INSERT ON `product_with_packets` FOR EACH ROW BEGIN
	IF NEW.main_product_id = "" OR NEW.main_product_id = NULL OR NEW.packet_product_id = "" OR NEW.packet_product_id = NULL OR NEW.qty = "" OR NEW.qty = NULL OR NEW.qty_uom_id = "" OR NEW.qty_uom_id = NULL OR NEW.conversion = "" OR NEW.conversion = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProductWithSkuBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProductWithSkuBfInsert` BEFORE INSERT ON `product_with_skus` FOR EACH ROW BEGIN
	IF NEW.product_id = "" OR NEW.product_id = NULL OR NEW.sku = "" OR NEW.sku = NULL OR NEW.uom_id = "" OR NEW.uom_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zProvinceBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zProvinceBfInsert` BEFORE INSERT ON `provinces` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.abbr = "" OR NEW.abbr = NULL THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zPurchaseRequestBeforeInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zPurchaseRequestBeforeInsert` BEFORE INSERT ON `purchase_requests` FOR EACH ROW BEGIN
	IF NEW.company_id IS NULL OR NEW.branch_id IS NULL OR NEW.vendor_id IS NULL OR NEW.order_date IS NULL OR NEW.order_date = '' OR NEW.order_date = '0000-00-00' OR NEW.currency_center_id IS NULL OR NEW.vat_percent IS NULL OR NEW.vat_setting_id IS NULL OR NEW.vat_calculate IS NULL OR NEW.total_vat IS NULL OR NEW.total_amount IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zReasonBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zReasonBfInsert` BEFORE INSERT ON `reasons` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSalesOrderAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSalesOrderAfInsert` AFTER INSERT ON `sales_orders` FOR EACH ROW BEGIN
	DECLARE salesMonth int(11);
	DECLARE salesYear int(11);
	SET salesMonth = MONTH(NEW.order_date);
	SET salesYear  = YEAR(NEW.order_date);
	IF NEW.is_pos = 0 AND NEW.status > 0 THEN
		INSERT INTO report_sales_by_days (`date`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `s_total_amount`, `s_total_discount`, `s_total_vat`) 
		VALUES (NEW.order_date, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE s_total_amount = s_total_amount + NEW.total_amount, s_total_discount = s_total_discount + NEW.discount, s_total_vat = s_total_vat + NEW.total_vat;
		INSERT INTO report_sales_by_months (`month`, `year`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `s_total_amount`, `s_total_discount`, `s_total_vat`) 
		VALUES (salesMonth, salesYear, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE s_total_amount = s_total_amount + NEW.total_amount, s_total_discount = s_total_discount + NEW.discount, s_total_vat = s_total_vat + NEW.total_vat;
	ELSEIF NEW.is_pos = 1 AND NEW.status = 2 THEN
		INSERT INTO report_sales_by_days (`date`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `p_total_amount`, `p_total_discount`, `p_total_vat`) 
		VALUES (NEW.order_date, NEW.company_id, NEW.branch_id, NEW.customer_id, 0, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE p_total_amount = p_total_amount + NEW.total_amount, p_total_discount = p_total_discount + NEW.discount, p_total_vat = p_total_vat + NEW.total_vat;
		INSERT INTO report_sales_by_months (`month`, `year`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `p_total_amount`, `p_total_discount`, `p_total_vat`) 
		VALUES (salesMonth, salesYear, NEW.company_id, NEW.branch_id, NEW.customer_id, 0, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE p_total_amount = p_total_amount + NEW.total_amount, p_total_discount = p_total_discount + NEW.discount, p_total_vat = p_total_vat + NEW.total_vat;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSalesOrderAfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSalesOrderAfUpdate` AFTER UPDATE ON `sales_orders` FOR EACH ROW BEGIN
	DECLARE salesMonth int(11);
	DECLARE salesYear int(11);
	SET salesMonth = MONTH(OLD.order_date);
	SET salesYear  = YEAR(OLD.order_date);
	IF (OLD.status > 0 AND NEW.status = -1 AND NEW.is_pos = 0) OR (OLD.status > 0 AND NEW.status = 0 AND NEW.is_pos = 0) THEN
		UPDATE report_sales_by_days SET s_total_amount = s_total_amount - OLD.total_amount, s_total_discount = s_total_discount - OLD.discount, s_total_vat = s_total_vat - OLD.total_vat WHERE date = OLD.order_date AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = OLD.sales_rep_id;
		UPDATE report_sales_by_months SET s_total_amount = s_total_amount - OLD.total_amount, s_total_discount = s_total_discount - OLD.discount, s_total_vat = s_total_vat - OLD.total_vat WHERE month = salesMonth AND year = salesYear AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = OLD.sales_rep_id;
	ELSEIF OLD.status = 2 AND NEW.status = 0 AND NEW.is_pos = 1 THEN
		UPDATE report_sales_by_days SET p_total_amount = p_total_amount - OLD.total_amount, p_total_discount = p_total_discount - OLD.discount, p_total_vat = p_total_vat - OLD.total_vat WHERE date = OLD.order_date AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = 0;
		UPDATE report_sales_by_months SET p_total_amount = p_total_amount - OLD.total_amount, p_total_discount = p_total_discount - OLD.discount, p_total_vat = p_total_vat - OLD.total_vat WHERE month = salesMonth AND year = salesYear AND company_id = OLD.company_id AND branch_id = OLD.branch_id AND customer_id = OLD.customer_id AND sales_rep_id = 0;
   ELSEIF OLD.status = -2 AND NEW.status > 0 AND NEW.is_pos = 0 THEN
		INSERT INTO report_sales_by_days (`date`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `s_total_amount`, `s_total_discount`, `s_total_vat`) 
		VALUES (NEW.order_date, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE s_total_amount = s_total_amount + NEW.total_amount, s_total_discount = s_total_discount + NEW.discount, s_total_vat = s_total_vat + NEW.total_vat;
		INSERT INTO report_sales_by_months (`month`, `year`, `company_id`, `branch_id`, `customer_id`, `sales_rep_id`, `s_total_amount`, `s_total_discount`, `s_total_vat`) 
		VALUES (salesMonth, salesYear, NEW.company_id, NEW.branch_id, NEW.customer_id, NEW.sales_rep_id, NEW.total_amount, NEW.discount, NEW.total_vat) 
		ON DUPLICATE KEY UPDATE s_total_amount = s_total_amount + NEW.total_amount, s_total_discount = s_total_discount + NEW.discount, s_total_vat = s_total_vat + NEW.total_vat;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSectionBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSectionBfInsert` BEFORE INSERT ON `sections` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSectionCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSectionCompanyBfInsert` BEFORE INSERT ON `section_companies` FOR EACH ROW BEGIN
	IF NEW.section_id = "" OR NEW.section_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zServiceBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zServiceBfInsert` BEFORE INSERT ON `services` FOR EACH ROW BEGIN
	IF NEW.section_id = "" OR NEW.section_id IS NULL OR NEW.code = "" OR NEW.code IS NULL OR NEW.name = "" OR NEW.name IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	ELSE
	   IF NEW.type = 2 AND (NEW.uom_id IS NULL OR NEW.uom_id = 0) THEN
	   	SET NEW.uom_id = 1;
	   END IF;
	END IF;
	IF NEW.code = "S" THEN
		SET NEW.code = (SELECT CONCAT(NEW.code, (SELECT LPAD(((SELECT count(tmp.id) FROM `services` as tmp WHERE tmp.code LIKE CONCAT(NEW.code, '%')) + 1),6,'0'))));
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSettingOptBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSettingOptBeforeDelete` BEFORE DELETE ON `setting_options` FOR EACH ROW BEGIN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default setting';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zSettingOptionBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zSettingOptionBfUpdate` BEFORE UPDATE ON `setting_options` FOR EACH ROW BEGIN
	DECLARE posShift TINYINT(4);
	SELECT COUNT(id) INTO posShift FROM shifts WHERE status = 1 OR status = 2;
	IF posShift > 0 THEN
		SET NEW.shift = 1;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zShipmentBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zShipmentBfInsert` BEFORE INSERT ON `shipments` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zStreetBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zStreetBfInsert` BEFORE INSERT ON `streets` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTermBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTermBeforeDelete` BEFORE DELETE ON `payment_terms` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default term';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTermBeforeUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTermBeforeUpdate` BEFORE UPDATE ON `payment_terms` FOR EACH ROW BEGIN
	IF OLD.id = 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot update default term';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTermConditionApplyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTermConditionApplyBfInsert` BEFORE INSERT ON `term_condition_applies` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.module_type_id = "" OR NEW.module_type_id = NULL OR NEW.term_condition_type_id = "" OR NEW.term_condition_type_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTermConditionBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTermConditionBfInsert` BEFORE INSERT ON `term_conditions` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.term_condition_type_id = "" OR NEW.term_condition_type_id = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTermConditionTypeBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTermConditionTypeBfInsert` BEFORE INSERT ON `term_condition_types` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTrackBeforeDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTrackBeforeDelete` BEFORE DELETE ON `tracks` FOR EACH ROW BEGIN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default setting';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zTrackBeforeInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zTrackBeforeInsert` BEFORE INSERT ON `tracks` FOR EACH ROW BEGIN
	IF NEW.id != 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert default setting';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUnearnBfDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUnearnBfDelete` BEFORE DELETE ON `unearned_tacks` FOR EACH ROW BEGIN
	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot delete default setting';
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUnearnTrackBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUnearnTrackBfInsert` BEFORE INSERT ON `unearned_tacks` FOR EACH ROW BEGIN
	IF NEW.id != 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot insert default setting';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUoMBfDelete
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUoMBfDelete` BEFORE DELETE ON `uoms` FOR EACH ROW BEGIN
	IF OLD.id = 1 OR OLD.id = 2 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'This UoM cannot delete';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUomBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUomBfInsert` BEFORE INSERT ON `uoms` FOR EACH ROW BEGIN
	IF NEW.name = "" OR NEW.name = NULL OR NEW.abbr = "" OR NEW.abbr = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUomBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUomBfUpdate` BEFORE UPDATE ON `uoms` FOR EACH ROW BEGIN
	IF NEW.type = "" OR NEW.type IS NULL OR NEW.name = "" OR NEW.name IS NULL OR NEW.abbr = "" OR NEW.abbr IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUomConversionBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUomConversionBfInsert` BEFORE INSERT ON `uom_conversions` FOR EACH ROW BEGIN
	IF NEW.from_uom_id = "" OR NEW.from_uom_id IS NULL OR NEW.to_uom_id = "" OR NEW.to_uom_id IS NULL OR NEW.value = "" OR NEW.value IS NULL OR NEW.value < 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUomConversionBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUomConversionBfUpdate` BEFORE UPDATE ON `uom_conversions` FOR EACH ROW BEGIN
	DECLARE isCheck INT(11);
	SELECT COUNT(id) INTO isCheck FROM products WHERE id IN (SELECT product_id FROM inventory_totals) AND price_uom_id = OLD.from_uom_id AND is_active = 1;
	IF isCheck > 0 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Cannot update or delete this data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUserBfUpdate
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUserBfUpdate` BEFORE INSERT ON `users` FOR EACH ROW BEGIN
	IF NEW.first_name = "" OR NEW.first_name = NULL OR NEW.last_name = "" OR NEW.last_name = NULL OR NEW.username = "" OR NEW.username = NULL OR NEW.password = "" OR NEW.password = NULL OR NEW.expired = "" OR NEW.expired = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUserCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUserCompanyBfInsert` BEFORE INSERT ON `user_companies` FOR EACH ROW BEGIN
	IF NEW.user_id = "" OR NEW.user_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zUsersAfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zUsersAfInsert` AFTER INSERT ON `users` FOR EACH ROW BEGIN
	INSERT INTO user_companies (user_id, company_id) SELECT NEW.id, companies.id FROM companies;
	INSERT INTO user_locations (user_id, location_id) SELECT NEW.id, id FROM locations;
	IF NEW.id = 1 THEN
		INSERT INTO user_location_groups (user_id, location_group_id) SELECT NEW.id, id FROM location_groups;
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVatSettingBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVatSettingBfInsert` BEFORE INSERT ON `vat_settings` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code IS NULL OR NEW.company_id = "" OR NEW.company_id IS NULL OR NEW.`type` = "" OR NEW.`type` IS NULL OR NEW.name = "" OR NEW.name IS NULL OR NEW.vat_percent IS NULL OR NEW.chart_account_id = "" OR NEW.chart_account_id IS NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVendorBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVendorBfInsert` BEFORE INSERT ON `vendors` FOR EACH ROW BEGIN
	IF NEW.vendor_code = "" OR NEW.vendor_code = NULL OR NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVendorCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVendorCompanyBfInsert` BEFORE INSERT ON `vendor_companies` FOR EACH ROW BEGIN
	IF NEW.vendor_id = "" OR NEW.vendor_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVendorContactBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVendorContactBfInsert` BEFORE INSERT ON `vendor_contacts` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.company_id = "" OR NEW.company_id = NULL OR NEW.vendor_id = "" OR NEW.vendor_id = NULL OR NEW.contact_name = "" OR NEW.contact_name = NULL OR NEW.sex = "" OR NEW.sex = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVendorVgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVendorVgroupBfInsert` BEFORE INSERT ON `vendor_vgroups` FOR EACH ROW BEGIN
	IF NEW.vendor_id = "" OR NEW.vendor_id = NULL OR NEW.vgroup_id = "" OR NEW.vgroup_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVgroupBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVgroupBfInsert` BEFORE INSERT ON `vgroups` FOR EACH ROW BEGIN
	IF NEW.name = "" OR NEW.name = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVgroupCompanyBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVgroupCompanyBfInsert` BEFORE INSERT ON `vgroup_companies` FOR EACH ROW BEGIN
	IF NEW.vgroup_id = "" OR NEW.vgroup_id = NULL OR NEW.company_id = "" OR NEW.company_id = NULL THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sas_master.zVillagesBfInsert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `zVillagesBfInsert` BEFORE INSERT ON `villages` FOR EACH ROW BEGIN
	IF NEW.sys_code = "" OR NEW.sys_code = NULL OR NEW.name = "" OR NEW.name = NULL OR NEW.commune_id = "" OR NEW.commune_id = NULL THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Invalid Data';
	END IF;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
