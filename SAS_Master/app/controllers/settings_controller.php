<?php

class SettingsController extends AppController {

    var $uses = 'User';
    var $components = array('Helper', 'Import');

    function index() {
        $this->layout = 'ajax';
    }

    function ics($productId = null) {
        $this->layout = 'ajax';
        $user = $this->getCurrentUser();
        if (!empty($_POST)) {
            $queryAccountType = mysql_query("SELECT id,name FROM account_types WHERE status = 1 ORDER BY ordering");
            while ($dataAccountType = mysql_fetch_array($queryAccountType)) {
                $name = "t" . $dataAccountType['id'];
                mysql_query("UPDATE account_types SET chart_account_id=" . $_POST[$name] . " WHERE id=" . $dataAccountType['id']);
            }
            // Save User Activity
            $this->Helper->saveUserActivity($user['User']['id'], 'ICS', 'Save Edit');
            echo MESSAGE_DATA_HAS_BEEN_SAVED;
            exit();
        }
        // User Activity
        $this->Helper->saveUserActivity($user['User']['id'], 'ICS', 'Edit');
        $this->set('productId', $productId);
    }

    function accountClosingDate(){
        $this->layout = 'ajax';
        if (!empty($this->data)) {
            $user = $this->getCurrentUser();
            mysql_query("INSERT INTO account_closing_dates (date,created,created_by) VALUES ('" . $this->data['Setting']['date'] . "', now(), " . $user['User']['id'] . ")");
            // Save User Activity
            $this->Helper->saveUserActivity($user['User']['id'], 'Account Closing Date', 'Save Change');
            echo MESSAGE_DATA_HAS_BEEN_SAVED;
            exit();
        }
    }
    
    function config() {
        $this->layout = 'ajax';
    }
    
    function configSetting($id) {
        $this->layout = 'ajax';
        if (!$id) {
            echo MESSAGE_DATA_INVALID;
            exit;
        }
        $this->set(compact('id'));
    }
    
    function downloadTemplate($fileName = null){
        $this->layout = 'ajax';
        if (!$fileName) {
            echo MESSAGE_DATA_INVALID;
            exit;
        }
        // Get parameters
        $file = urldecode($fileName); // Decode URL-encoded string
        $filepath = "public/template_import/" . $file;
        if(file_exists($filepath)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            flush(); // Flush system output buffer
            readfile($filepath);
        }
        exit;
    }
    
    function import($type = null){
        $this->layout = 'ajax';
        if ($type == 'product' || $type == 'customer' || $type == 'vendor' || $type == 'coa') {
            if ($_FILES['file_import']['name'] != '') {
                $targetFolder = 'public/import/';
                $allowed  = array('csv');
                $filename = $_FILES['file_import']['name'];
                $ext      = pathinfo($filename, PATHINFO_EXTENSION);
                if( in_array( $ext, $allowed ) ) {
                    $extDot     = explode(".", $_FILES['file_import']['name']);
                    $targetName = rand() . '.' . $extDot[sizeof($extDot) - 1];
                    move_uploaded_file($_FILES['file_import']['tmp_name'], $targetFolder . $targetName);
                    $fileImport = $targetFolder.$targetName;
                    $check      = true;
                    $csv        = $this->Import->ImportCSV2Array($fileImport);
                    if(!empty($csv)){
                        $match = $this->Import->matchField($type."s");
                        $csvRequired = array();
                        // Check Field Match
                        foreach($csv[0] AS $k => $field){
                            $fCsv  = str_replace(" ", "_", trim($field));
                            if (!array_key_exists($fCsv, $match)){
                                $check = false;
                                break;
                            } else {
                                $csvRequired[$k] = $match[$fCsv];
                            }
                        }
                        // Check Field Required
                        foreach($match['required'] AS $required){
                            if (!in_array($required, $csvRequired)){
                                $check = false;
                                break;
                            }
                        }
                    } else {
                        $check = false;
                    }
                    if($check == false){
                        echo MESSAGE_DATA_INVALID;
                        exit();
                    } else {
                        $this->set(compact('csv', 'targetName', 'match', 'csvRequired'));
                    }
                } else {
                    echo MESSAGE_DATA_INVALID;
                    exit();
                }
            }
        } else {
            echo MESSAGE_DATA_INVALID;
            exit();
        }
    }
    
    function convertImportToDb($fileName, $type = null){
        $this->layout = 'ajax';
        if ($type == 'product' || $type == 'customer' || $type == 'vendor' || $type == 'coa') {
            $filepath = "public/import/" . $fileName;
            $allowed  = array('csv');
            $ext      = pathinfo($filepath, PATHINFO_EXTENSION);
            if( in_array( $ext, $allowed ) ) {
                $user = $this->getCurrentUser();
                $csv  = $this->Import->ImportCSV2Array($filepath);
                if(!empty($csv)){
                    $insert = $this->Import->insertImportToDB($type."s", $csv, $user);
                    if($insert == true){
                        @unlink($filepath);
                        echo MESSAGE_DATA_HAS_BEEN_SAVED;
                        exit();
                    } else {
                        @unlink($filepath);
                        echo MESSAGE_DATA_INVALID;
                        exit();
                    }
                } else {
                    @unlink($filepath);
                    echo MESSAGE_DATA_INVALID;
                    exit();
                }
            } else {
                echo MESSAGE_DATA_INVALID;
                exit();
            }
        } else {
            echo MESSAGE_DATA_INVALID;
            exit;
        }
    }
    
    function saveCheckBox($id = null, $value = null){
        $this->layout = 'ajax';
        $result = array();
        if(empty($id) || $value == null){
            $result['msg'] = 0;
            echo json_encode($result);
            exit;
        }
        mysql_query("UPDATE s_module_detail_settings SET is_checked = ".$value." WHERE id = ".$id);
        if($id == 12){ // Purchase Order
            mysql_query("UPDATE modules SET status = ".$value." WHERE module_type_id = 48");
            mysql_query("UPDATE module_types SET status = ".$value." WHERE id = 48");
        } else if($id == 14){ // Purchase Receive
            mysql_query("UPDATE modules SET status = ".$value." WHERE module_type_id = 116");
            mysql_query("UPDATE module_types SET status = ".$value." WHERE id = 116");
        } else if($id == 18){ // Sales Order
            mysql_query("UPDATE modules SET status = ".$value." WHERE module_type_id = 69");
            mysql_query("UPDATE module_types SET status = ".$value." WHERE id = 69");
        } else if($id == 20){ // Delivery Note
            mysql_query("UPDATE modules SET status = ".$value." WHERE module_type_id = 115");
            mysql_query("UPDATE module_types SET status = ".$value." WHERE id = 115");
        } else if($id == 22){ // Allow Negative Stock
            mysql_query("UPDATE location_groups SET allow_negative_stock = ".$value." WHERE 1");
        }
        $result['msg'] = 1;
        $result['val'] = $value;
        echo json_encode($result);
        exit;
    }
    
    function saveBaseCurrencySetting($id = null, $value = null){
        $this->layout = 'ajax';
        $result = array();
        if(empty($id) || $value == null){
            $result['msg'] = 0;
            echo json_encode($result);
            exit;
        }
        $checkT = mysql_query("SELECT id FROM system_activities WHERE module IN ('Purchase Bill', 'Sales Invoice', 'Sales Return', 'Bill Return', 'Inventory Adjustment', 'Point Of Sales') LIMIT 1");
        if(!mysql_num_rows($checkT)){
            mysql_query("UPDATE s_module_detail_settings SET value = ".$value." WHERE id = ".$id);
            mysql_query("UPDATE companies SET currency_center_id = ".$value." WHERE id = 1");
        }
        $result['msg'] = 1;
        echo json_encode($result);
        exit;
    }
    
    function saveDecimalSetting($id = null, $value = null){
        $this->layout = 'ajax';
        $result = array();
        if(empty($id) || $value == null){
            $result['msg'] = 0;
            echo json_encode($result);
            exit;
        }
        mysql_query("UPDATE s_module_detail_settings SET value = ".$value." WHERE id = ".$id);
        $result['msg'] = 1;
        echo json_encode($result);
        exit;
    }
    
    function saveLockTransaction($id, $date = null){
        $this->layout = 'ajax';
        if(!empty($date)){
            $user = $this->getCurrentUser();
            mysql_query("UPDATE s_module_detail_settings SET date_value = '".$date."' WHERE id = ".$id);
            mysql_query("UPDATE `account_closing_dates` SET `date`='".$date."', `created`='".date("Y-m-d H:i:s")."', `created_by` = ".$user['User']['id']." WHERE  `id`=1;");
            $result['msg'] = 1;
            echo json_encode($result);
            exit;
        } else {
            $result['msg'] = 0;
            echo json_encode($result);
            exit;
        }
    }

}

?>