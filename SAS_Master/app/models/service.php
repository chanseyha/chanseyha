<?php

class Service extends AppModel {
    var $name = 'Service';
    var $belongsTo = array(
        'Pgroup' => array(
            'className' => 'Pgroup',
            'foreignKey' => 'section_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}
?>