<input type="hidden" id="fileImportName" value="<?php echo $targetName; ?>" />
<table class="table" style="max-width: 500px; margin: 0px auto;">
    <tr>
        <th class="first"><?php echo TABLE_NO; ?></th>
        <?php
        foreach($csv[0] AS $i => $field){
            echo '<th>'.$field.'</th>';
        }
        ?>
    </tr>
    <?php
    $index = 0;
    foreach($csv AS $c => $col){
        if($c == 0){
            continue;
        }
        $checkValRequired = true;
        $tdColor = '';
        foreach($col AS $i => $val){ 
            if (in_array($csvRequired[$i], $match['required']) && $val == ''){
                $checkValRequired = false;
                $tdColor = ' style="color: #fff;"';
            }
        }
    ?>
    <tr<?php if($checkValRequired == false){ ?> style="background-color: red;"<?php } ?>>
        <td class="first"><?php echo  ++$index; ?></td>
        <?php
        foreach($col AS $i => $val){ 
            echo '<td'.$tdColor.'>'.$val.'</td>';
        }
        ?>
    </tr>
    <?php
    }
    ?>
</table>


