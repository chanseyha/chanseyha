<?php 
header("Expires: Mon, 26 Jul 1990 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$this->element('check_access');
include("includes/function.php");
// Authentication
$allowAdd      = checkAccess($user['User']['id'], $this->params['controller'], 'add');
$allowViewCost = checkAccess($user['User']['id'], $this->params['controller'], 'viewCost');
$allowExport   = checkAccess($user['User']['id'], $this->params['controller'], 'exportExcel');
$allowSetPrice = checkAccess($user['User']['id'], $this->params['controller'], 'productPrice');
$tblName = "tbl" . rand(); 

$isSetPriceType = 0;
$queryPOSpriceType = mysql_query("SELECT price_type_id FROM pos_price_types WHERE is_active = 1");
if(mysql_num_rows($queryPOSpriceType)){
    $isSetPriceType = 1;
}
?>
<style>
    .text_colsan{
        display: table-cell;
    }
</style>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/pipeline.js"></script>
<script type="text/javascript">
    var oTableProductDashBoard;
    var data = "all";
    var tabProductId  = $(".ui-tabs-selected a").attr("href");
    function calcDataTableHeight() {
        var tableHeight = $(window).height() - ($(".ui-layout-north").height() + $(".ui-layout-south").height() + $(".ui-tabs-nav").height() + $("#divHeader").height() + 37 + 22 + 56 + 110.3);
        return tableHeight;
    }
    $(document).ready(function(){
        // Prevent Key Enter
        preventKeyEnter();
        $("#<?php echo $tblName; ?> td:first-child").addClass('first');      
        oTableProductDashBoard = $("#<?php echo $tblName; ?>").dataTable({
            "aaSorting": [[0, 'DESC']],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->base.'/'.$this->params['controller']; ?>/ajax/"+$("#changeCategoryProductView").val()+"/"+$("#displayProduct").val()+"/"+$("#showProductQty").val(),
            "fnServerData": fnDataTablesPipeline,
            "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
                // Create ID for Action
                setIdActionProduct();
                $("#<?php echo $tblName; ?> td:first-child").addClass('first');                
                $("#<?php echo $tblName; ?> td:last-child").css("white-space", "nowrap");
                $("#<?php echo $tblName; ?> td:nth-child(1)").css("width", "4%");
                $("#<?php echo $tblName; ?> td:nth-child(2)").css("width", "7%");
                $("#<?php echo $tblName; ?> td:nth-child(3)").css("width", "20%");
                $("#<?php echo $tblName; ?> tr").click(function(){
                    changeBackgroupProduct();
                    $(this).closest("tr").css('background','#eeeca9');
                });
                // Double Click View
                $("#<?php echo $tblName; ?> tr").unbind("dblclick").dblclick(function(){
                    var id   = $(this).find(".btnViewProductView").attr('rel');
                    var type = $(this).closest("tr").find("td:eq(1)").text();
                    if(type == 'Service' || type == 'Product'){
                        var url  = "view";
                        if(type == 'Service'){
                            url  = "viewService";
                        }
                        var leftPanel  = $("#dashboardProduct");
                        var rightPanel = leftPanel.parent().find(".rightPanel");
                        leftPanel.hide("slide", { direction: "left" }, 500, function() {
                            rightPanel.show();
                        });
                        rightPanel.html("<?php echo ACTION_LOADING; ?>");
                        rightPanel.load("<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/"+url+"/" + id);
                    }
                });
                
                $(".btnViewProductView").unbind("click").click(function(event){
                    event.preventDefault();
                    var id   = $(this).attr('rel');
                    var type = $(this).closest("tr").find("td:eq(1)").text();
                    var url  = "view";
                    if(type == 'Service'){
                        url  = "viewService";
                    }
                    var leftPanel  = $("#dashboardProduct");
                    var rightPanel = leftPanel.parent().find(".rightPanel");
                    leftPanel.hide("slide", { direction: "left" }, 500, function() {
                        rightPanel.show();
                    });
                    rightPanel.html("<?php echo ACTION_LOADING; ?>");
                    rightPanel.load("<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/"+url+"/" + id);
                });
                
                $(".btnEditProductView").unbind("click").click(function(event){
                    event.preventDefault();
                    var id   = $(this).attr('rel');
                    var type = $(this).closest("tr").find("td:eq(1)").text();
                    var url  = "edit";
                    if(type == 'Service'){
                        url  = "editService";
                    }
                    var leftPanel  = $("#dashboardProduct");
                    var rightPanel = leftPanel.parent().find(".rightPanel");
                    leftPanel.hide("slide", { direction: "left" }, 500, function() {
                        rightPanel.show();
                    });
                    rightPanel.html("<?php echo ACTION_LOADING; ?>");
                    rightPanel.load("<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/"+url+"/" + id);
                });
                
                $(".btnDeleteProductView").unbind("click").click(function(event){
                    event.preventDefault();
                    var id   = $(this).attr('rel');
                    var name = $(this).attr('name');
                    var type = $(this).closest("tr").find("td:eq(1)").text();
                    var url  = "delete";
                    if(type == 'Service'){
                        url  = "deleteService";
                    }
                    $("#dialog").html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><?php echo MESSAGE_CONFIRM_DELETE; ?> <b>' + name + '</b>?</p>');
                    $("#dialog").dialog({
                        title: '<?php echo DIALOG_CONFIRMATION; ?>',
			resizable: false,
			modal: true,
                        width: 'auto',
                        height: 'auto',
                        position: 'center',
                        open: function(event, ui){
                            $(".ui-dialog-buttonpane").show();
                        },
			buttons: {
                            '<?php echo ACTION_DELETE; ?>': function() {
                                $.ajax({
                                    type: "GET",
                                    url: "<?php echo $this->base.'/'.$this->params['controller']; ?>/"+url+"/" + id,
                                    data: "",
                                    beforeSend: function(){
                                        $("#dialog").dialog("close");
                                        $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner.gif");
                                    },
                                    success: function(result){
                                        $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner-placeholder.gif");
                                        oCache.iCacheLower = -1;
                                        oTableProductDashBoard.fnDraw(false);
                                        if(result != '<?php echo MESSAGE_DATA_HAVE_CHILD; ?>' && result != '<?php echo MESSAGE_DATA_HAS_BEEN_DELETED; ?>' && result != '<?php echo MESSAGE_DATA_COULD_NOT_BE_SAVED; ?>'){
                                            createSysAct(type, 'Delete', 2, result);
                                            $("#dialog").html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?php echo MESSAGE_PROBLEM; ?></p>');
                                        }else {
                                            createSysAct(type, 'Delete', 1, '');
                                            // alert message
                                            $("#dialog").html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>'+result+'</p>');
                                        }
                                        // alert message
                                        $("#dialog").dialog({
                                            title: '<?php echo DIALOG_INFORMATION; ?>',
                                            resizable: false,
                                            modal: true,
                                            width: 'auto',
                                            height: 'auto',
                                            position: 'center',
                                            buttons: {
                                                '<?php echo ACTION_CLOSE; ?>': function() {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    }
                                });
                            },
                            '<?php echo ACTION_CANCEL; ?>': function() {
                                $(this).dialog("close");
                            }
			}
                    });
                });
                
                $(".viewInventoryProduct").unbind("click").click(function(event){
                    event.preventDefault();
                    var id = $(this).attr('data');
                    var code = $(this).closest("tr").find("td:eq(3)").text();
                    var name = $(this).closest("tr").find("td:eq(1)").text();
                    if(id != ''){
                        $.ajax({
                            type: "GET",
                            url: "<?php echo $this->base.'/'.$this->params['controller']; ?>/viewProductInventory/" + id,
                            data: "",
                            beforeSend: function(){
                                $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner.gif");
                            },
                            success: function(result){
                                $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner-placeholder.gif");
                                // alert message
                                $("#dialog").html(result);
                                $("#dialog").dialog({
                                    title: code+" - "+name,
                                    resizable: false,
                                    modal: true,
                                    width: 'auto',
                                    height: 'auto',
                                    position: 'center',
                                    buttons: {
                                        '<?php echo ACTION_CLOSE; ?>': function() {
                                            $(this).dialog("close");
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
                <?php
                if($allowSetPrice){
                ?>
                $(".setProductPrice").unbind("click").click(function(){
                    var id = $(this).attr('data');
                    $.ajax({
                        type:   "POST",
                        url:    "<?php echo $this->base . "/products/productPrice/"; ?>"+id,
                        beforeSend: function(){
                            $(".loader").attr('src','<?php echo $this->webroot; ?>img/layout/spinner.gif');
                        },
                        success: function(msg){
                            $(".loader").attr('src', '<?php echo $this->webroot; ?>img/layout/spinner-placeholder.gif');
                            $("#dialog").html(msg);
                            $("#dialog").dialog({
                                title: '<?php echo ACTION_SET_PRICE; ?>',
                                resizable: false,
                                modal: true,
                                width: '95%',
                                height: '570',
                                position:'center',
                                open: function(event, ui){
                                    $(".ui-dialog-buttonpane").show();
                                },
                                buttons: {
                                    '<?php echo ACTION_SAVE; ?>': function() {
                                        var formName = "#ProductPrice";
                                        var validateBack =$(formName).validationEngine("validate");
                                        if(!validateBack){
                                            return false;
                                        }else{
                                            $(this).dialog("close");
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo $this->base . '/' . $this->params['controller']; ?>/productPrice",
                                                data: $(":input").serialize(),
                                                beforeSend: function(){
                                                    $(".loader").attr('src','<?php echo $this->webroot; ?>img/layout/spinner.gif');
                                                },
                                                success: function(result){
                                                    $(".loader").attr('src', '<?php echo $this->webroot; ?>img/layout/spinner-placeholder.gif');
                                                    oCache.iCacheLower = -1;
                                                    oTableProductDashBoard.fnDraw(false);
                                                    if(result != '<?php echo MESSAGE_DATA_HAS_BEEN_SAVED; ?>'){
                                                        createSysAct('Products', 'Set Price', 2, result);
                                                        $("#dialog").html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span><?php echo MESSAGE_PROBLEM; ?></p>');
                                                    }else {
                                                        createSysAct('Products', 'Set Price', 1, '');
                                                        // alert message
                                                        $("#dialog").html('<p><span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>'+result+'</p>');
                                                    }
                                                    $("#dialog").dialog({
                                                        title: '<?php echo DIALOG_INFORMATION; ?>',
                                                        resizable: false,
                                                        modal: true,
                                                        width: 'auto',
                                                        height: 'auto',
                                                        position: 'center',
                                                        open: function(event, ui){
                                                            $(".ui-dialog-buttonpane").show();
                                                        },
                                                        buttons: {
                                                            '<?php echo ACTION_CLOSE; ?>': function() {
                                                                $(this).dialog("close");
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                        }  
                                    }
                                }
                            });
                        }
                    });
                });
                
                $(".setServicePrice").unbind("focus").focus(function(){
                    if(replaceNum($(this).val()) == 0){
                        $(this).val('');
                    }
                });
                
                $(".setServicePrice").unbind("blur").blur(function(){
                    if($(this).val() == ""){
                        $(this).val('0');
                    }
                    var id   = $(this).attr('data');
                    var code = $(this).closest("tr").find("td:eq(3)").text();
                    var name = $(this).closest("tr").find("td:eq(1)").text();
                    var obj  = $(this);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo $this->base.'/'.$this->params['controller']; ?>/saveServicePrice/" + id,
                        data: "price="+$(this).val(),
                        beforeSend: function(){
                            obj.attr("disabled", true);
                            $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner.gif");
                        },
                        success: function(result){
                            $(".loader").attr("src", "<?php echo $this->webroot; ?>img/layout/spinner-placeholder.gif");
                            obj.attr("disabled", false);
                            // Alert message
                            $("#dialog").html(result);
                            $("#dialog").dialog({
                                title: code+" - "+name,
                                resizable: false,
                                modal: true,
                                width: 'auto',
                                height: 'auto',
                                position: 'center',
                                buttons: {
                                    '<?php echo ACTION_CLOSE; ?>': function() {
                                        $(this).dialog("close");
                                    }
                                }
                            });
                        }
                    });
                });
                <?php
                }
                ?>
                return sPre;
            },
            "fnDrawCallback": function(oSettings, json) {
                $("#<?php echo $tblName; ?> .colspanParent").parent().attr("colspan", 11);
                $("#<?php echo $tblName; ?> .colspanParentHidden").parent().css("display", "none");
            },
            "aoColumnDefs": [{
                "sType": "numeric", "aTargets": [ 0 ],
                "bSortable": false, "aTargets": [ -1 ]
            }]
        });
        
        $(".btnAddProduct").unbind("click").click(function(event){
            event.preventDefault();
            $("#dialog").html('<div class="buttons"><button type="submit" class="positive addNewProduct" ><img src="<?php echo $this->webroot; ?>img/button/product.png" style="width: 20px; height: 20px;" /> <span class="txtaddNewProduct"><?php echo MENU_PRODUCT_MANAGEMENT_ADD; ?></span></button><button type="submit" class="positive addNewService" ><img src="<?php echo $this->webroot; ?>img/button/service.png" style="width: 20px; height: 20px;" /> <span class="txtaddNewService"><?php echo MENU_ADD_SERVICE; ?></span></button></div>');
            $(".addNewProduct").click(function(){
                $("#dialog").dialog("close");
                var leftPanel  = $("#dashboardProduct");
                var rightPanel = leftPanel.parent().find(".rightPanel");
                leftPanel.hide("slide", { direction: "left" }, 500, function() {
                    rightPanel.show();
                });
                rightPanel.html("<?php echo ACTION_LOADING; ?>");
                rightPanel.load("<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/add/");
            });
            $(".addNewService").click(function(){
                $("#dialog").dialog("close");
                var leftPanel  = $("#dashboardProduct");
                var rightPanel = leftPanel.parent().find(".rightPanel");
                leftPanel.hide("slide", { direction: "left" }, 500, function() {
                    rightPanel.show();
                });
                rightPanel.html("<?php echo ACTION_LOADING; ?>");
                rightPanel.load("<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/addService/");
            });
            $("#dialog").dialog({
                title: '<?php echo DIALOG_SELECT_TYPE; ?>',
                resizable: false,
                modal: true,
                width: 'auto',
                height: 'auto',
                position:'center',
                open: function(event, ui){
                    $(".ui-dialog-buttonpane").show();
                },
                buttons: {
                    '<?php echo ACTION_CLOSE; ?>': function() {
                        $(this).dialog("close");
                    }
                }
            });
        });
        
        $(".btnRefreshProduct").unbind("click").click(function(){
            $("#<?php echo $tblName; ?>").find("tbody").html('<tr><td colspan="9" class="dataTables_empty first"><?php echo TABLE_LOADING; ?></td></tr>');
            var Tablesetting = oTableProductDashBoard.fnSettings();
            Tablesetting.sAjaxSource = "<?php echo $this->base . '/' . $this->params['controller']; ?>/ajax/"+$("#changeCategoryProductView").val()+"/"+$("#displayProduct").val()+"/"+$("#showProductQty").val();
            oCache.iCacheLower = -1;
            oTableProductDashBoard.fnDraw(false);
        });
        
        $("#changeCategoryProductView, #displayProduct, #showProductQty").unbind("change").change(function(){
            $("#<?php echo $tblName; ?>").find("tbody").html('<tr><td colspan="9" class="dataTables_empty first"><?php echo TABLE_LOADING; ?></td></tr>');
            var Tablesetting = oTableProductDashBoard.fnSettings();
            Tablesetting.sAjaxSource = "<?php echo $this->base . '/' . $this->params['controller']; ?>/ajax/"+$("#changeCategoryProductView").val()+"/"+$("#displayProduct").val()+"/"+$("#showProductQty").val();
            oCache.iCacheLower = -1;
            oTableProductDashBoard.fnDraw(false);
        });
        
        $(".btnExportProduct").unbind("click").click(function(){
            $.ajax({
                type: "POST",
                url: "<?php echo $this->base; ?>/<?php echo $this->params['controller']; ?>/exportExcel",
                data: "action=export",
                beforeSend: function(){
                    $(".btnExportProduct").attr('disabled','disabled');
                    $(".btnExportProduct").find('img').attr("src", "<?php echo $this->webroot; ?>img/layout/spinner.gif");
                },
                success: function(){
                    $(".btnExportProduct").removeAttr('disabled');
                    $(".btnExportProduct").find('img').attr("src", "<?php echo $this->webroot; ?>img/button/csv.png");
                    window.open("<?php echo $this->webroot; ?>public/report/product_export.csv", "_blank");
                }
            });
        });
    });
    
    function changeBackgroupProduct(){
        $("#<?php echo $tblName; ?> tbody tr").each(function(){
                $(this).removeAttr('style');
        });
    }
    
    function setIdActionProduct(){
        var action = new Array('btnViewProductView', 'btnEditProductView', 'btnDeleteProductView', 'setProductPrice', 'viewInventoryProduct');
        $.each( action, function( key, value ) {
            $("#<?php echo $tblName; ?> tr:eq(0)").find('.'+value).attr('id', value);
        });
    }
</script>
<div class="leftPanel" id="dashboardProduct">
    <div style="padding-top: 3px; padding-bottom: 3px; padding-left: 5px; padding-right: 5px; border: 1px dashed #bbbbbb; margin-bottom: 5px;" id="divHeader">
        <?php 
            if($allowAdd){
        ?>
        <div class="buttons">
            <a href="" class="positive btnAddProduct">
                <img src="<?php echo $this->webroot; ?>img/button/plus.png" alt=""/>
                <?php echo MENU_PRODUCT_SERVICE_ADD; ?>
            </a>
        </div>
        <?php 
            } 
            if($allowExport){ 
        ?>
        <div class="buttons">
            <button type="button" class="positive btnExportProduct">
                <img src="<?php echo $this->webroot; ?>img/button/csv.png" alt=""/>
                <?php echo ACTION_EXPORT_TO_EXCEL; ?>
            </button>
        </div>
        <?php } ?>
        <div class="buttons" style="float: right; margin-left: 10px;">
            <button type="button" class="positive btnRefreshProduct">
                <img src="<?php echo $this->webroot; ?>img/button/refresh-active.png" alt=""/>
                <?php echo ACTION_REFRESH; ?>
            </button>
        </div>
        <div style="float:right;">
            <label for="displayProduct"><?php echo TABLE_SHOW; ?> </label>: 
            <select id="displayProduct" style="width: 100px; height: 30px;">
                <option value="all" selected="selected"><?php echo TABLE_ALL; ?></option>
                <option value="1"><?php echo TABLE_PRODUCT; ?></option>
                <option value="2"><?php echo TABLE_SERVICE; ?></option>
            </select>	
            &nbsp;&nbsp;&nbsp;
            <label for="changeCategoryProductView"><?php echo MENU_PRODUCT_GROUP_MANAGEMENT; ?></label>:
            <select id="changeCategoryProductView" style="width: 250px; height: 30px;">
                <option value="all"><?php echo TABLE_ALL; ?></option>
                <?php
                $queryPgroup = mysql_query("SELECT * FROM `pgroups` WHERE is_active = 1 AND id IN (SELECT pgroup_id FROM pgroup_companies WHERE company_id IN (SELECT company_id FROM user_companies WHERE user_id = ".$user['User']['id'].")) AND is_active = 1 ORDER BY name");
                while($dataPgroup=mysql_fetch_array($queryPgroup)){
                ?>
                <option value="<?php echo $dataPgroup['id']; ?>"><?php echo $dataPgroup['name']; ?></option>
                <?php
                }
                ?>
            </select>
            &nbsp;&nbsp;&nbsp;
            <label for="showProductQty"><?php echo TABLE_QTY ?></label>: 
            <select id="showProductQty" style="width: 50px; height: 30px;">
                <option value="all"><?php echo TABLE_ALL; ?></option>
                <option value="1">>0</option>
                <option value="2">=0</option>
            </select>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div id="dynamic" style="height: 100%">
        <table id="<?php echo $tblName; ?>" class="table" cellspacing="0" style="width: 100%;">
            <thead>
                <tr>
                    <th class="first" style="width: 4%;"><?php echo TABLE_NO; ?></th>
                    <th style="width: 7%;"><?php echo TABLE_TYPE; ?></th>
                    <th style="width: 20%;"><?php echo TABLE_NAME; ?></th>
                    <th><?php echo TABLE_BARCODE; ?></th>
                    <th><?php echo TABLE_UOM; ?></th>
                    <th><?php echo TABLE_QTY; ?></th>
                    <?php
                    if($allowViewCost){
                    ?>
                    <th><?php echo TABLE_UNIT_COST; ?></th>
                    <?php
                    }
                    ?>
                    <th><?php echo TABLE_PRICE; ?></th>
                    <th><?php echo ACTION_ACTION; ?></th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="9" class="dataTables_empty first"><?php echo TABLE_LOADING; ?></td></tr>
            </tbody>
        </table>
    </div>
    <br />
    <br />
</div>
<div class="rightPanel"></div>