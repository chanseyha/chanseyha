<style type="text/css" media="screen">
    .titleHeader{
        vertical-align: top; 
        padding-bottom:3px !important; 
        padding-top:3px !important;
        padding-right: 2px !important;
        font-size: 12px;
    }
    .titleContent{
        font-weight: bold;
        text-align: right;
    }
    .contentHeight{
        height: 25px !important;
    }
    .marginTop10{
        padding-top: 11px !important;
    }
    .titleHeaderTable{
        padding-bottom:3px !important; 
        padding-top:3px !important;
        font-size: 12px;
        color: #000;
    }
    .titleHeaderHeight{
        height: 25px !important;
    }
    div.footerTablePrint {display: block; width: 100%; position: fixed; bottom: 2px; font-size: 12px; text-align: center;} 
    @font-face {   
        font-family: 'Moul'; 
        src: local('Moul'), url(../fonts/khmer-Moul.woff2) format('woff2'), url(../fonts/khmer-Moul.woff2) format('woff2');
    }
</style>
<style type="text/css" media="print">
    #footerTablePrint { width: 100%; position: fixed; bottom: 0px; }
    table.table_print {
        border-collapse: separate;
        border :  0px solid #000000;
        border-spacing: 0;
        width: 100%;
        border-color:  #000000 ;
    }
    .titleHeader{
        vertical-align: top; 
        padding-bottom:3px !important; 
        padding-top:3px !important;
        padding-right: 2px !important;
        font-size: 12px;
    }
    .titleContent{
        font-weight: bold;
        text-align: right;
    }
    .contentHeight{
        height: 25px !important;
    }
    .marginTop10{
        padding-top: 11px !important;
    }
    .titleHeaderTable{
        padding-bottom:3px !important; 
        padding-top:3px !important;
        font-size: 12px;
        color: #000;
    }
    .titleHeaderHeight{
        height: 25px !important;
    }
    div.print_doc { width:100%;}
    #btnDisappearPrint { display: none;}
    #footerTablePrint { width: 100%; position: fixed; bottom: 0px; }
    @font-face {   
        font-family: 'Moul'; 
        src: local('Moul'), url(../fonts/khmer-Moul.woff2) format('woff2'), url(../fonts/khmer-Moul.woff2) format('woff2');
    }
    @page
    {
        /*this affects the margin in the printer settings*/  
        margin: 20px 20px 0 20px;
    }
</style>
<div class="print_doc">
    <?php
    //debug($salesOrder);
    include("includes/function.php");
    $vatInvoice = '';
    if ($salesOrder['SalesOrder']['vat_percent'] > 0) {
        $vatInvoice = $salesOrder['Company']['vat_number'];
    }
    $display = "";
    $resultVAT = "VAT Included";
    if ($salesOrder['SalesOrder']['vat_percent'] <= 0) {
        $display = "display:none;";
        $resultVAT = "VAT Excluded";
    }
    ?>
    <table style="width:100%;">
        <tr style="">
            <td style="width:20%; vertical-align: top;"><img alt="" src="<?php echo $this->webroot;?>public/company_photo/<?php echo $salesOrder['Company']['photo'];?>" style="max-width: 130px;"/></td>
            <td style="border-width:3px;  width: 80%; vertical-align: top;">
                <div style="width:100%; text-align: center;">
                    <table style="width:100%; text-align: center;margin-left: -30px;">
                        <tr>
                            <td style="width:85%;vertical-align: top;">
                                <div style="text-align: center;font-family: 'Moul'; font-size: 17px; line-height: 25px;">
                                    <?php echo $salesOrder['Branch']['name_other']; ?><br />
                                    <span style="font-weight: bold; font-size: 17px; font-weight: bold; font-family: cambria;"><?php echo $salesOrder['Branch']['name']; ?></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: left; margin-left: 3px; margin-top: 5px; line-height: 15px;">
                    <span style="font-size:11px; text-align: left;">
                        <?php echo TABLE_VATTIN;?> <span style="font-size:11px; text-transform: uppercase;">(<?php echo TABLE_VATTIN_EN;?>)</span>
                        <?php 
                            if($salesOrder['Company']['vat_number'] != ""){
                                $vatCompanyConvert = str_split($salesOrder['Company']['vat_number']);
                                $countVarCompany   = count($vatCompanyConvert);
                                for($i = 0; $i< $countVarCompany; $i++){
                                    if($i == 0){
                                        echo "<span style='border: 1px solid #00afc1; padding: 1px 2px;margin-left: 2px;'>".$vatCompanyConvert[$i]."</span>";
                                    }
                                    else if($i == 4){
                                        echo "<span> - </span>";
                                        echo "<span style='border: 1px solid #00afc1; padding: 1px 2px; margin-left: 2px;'>".$vatCompanyConvert[$i]."</span>";
                                    }
                                    else{
                                        echo "<span style='border: 1px solid #00afc1; padding: 1px 2px; margin-left: 2px;'>".$vatCompanyConvert[$i]."</span>";
                                    }
                                }
                            }
                        ?>
                    </span>
                </div>
                <div style="width:100%;">
                    <table style="width:100%;">
                        <tr>
                            <td style="font-size: 11px; width: 121px;">
                                <?php
                                $house = '';
                                $street = '';
                                $commune = '';
                                $district = '';
                                $province = '';
                                $country  = '';
                                if(!empty($salesOrder['Branch']['address_other'])){
                                    $addressConvert = explode(",", $salesOrder['Branch']['address_other']);
                                    $house = @$addressConvert[0];
                                    $street = @$addressConvert[1];
                                    $commune = @$addressConvert[2];
                                    $district = @$addressConvert[3];
                                    $province = @$addressConvert[4];
                                    $country  = @$addressConvert[5];
                                }
                                ?>
                                អាសយដ្ឋាន៖ ផ្ទះលេខ <br />
                                Address <?php echo TABLE_NO; ?>
                            </td>
                            <td style="font-size: 11px; vertical-align: top;">
                                <?php echo $house; ?>
                            </td>
                            <td style="font-size: 11px; width: 82px;">
                                ផ្លូវ <br />
                                Street
                            </td>
                            <td style="font-size: 11px; vertical-align: top;">
                                <?php echo $street; ?>
                            </td>
                            <td style="font-size: 11px; width: 100px;">
                                សង្កាត់ <br />
                                Commune/Sangkat
                            </td>
                            <td style="font-size: 11px; vertical-align: top;">
                                <?php echo $commune; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 11px; font-size: 11px;">
                                ខណ្ឌ <br />
                                Town/District/Khan
                            </td>
                            <td style="font-size: 11px; vertical-align: top;">
                                <?php echo $district; ?>
                            </td>
                            <td style="font-size: 11px;">
                                រាជធានី <br />
                                Province/City
                            </td>
                            <td style="font-size: 11px; vertical-align: top;">
                                <?php echo $province; ?>
                            </td>
                            <td style="font-size: 11px;">
                                ទូរស័ព្ទលេខ <br />
                                Telephone <?php echo TABLE_NO; ?>
                            </td>
                            <td style="font-size: 11px; vertical-align: top; padding-top: 5px;">
                                <?php echo $salesOrder['Branch']['telephone']; ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2"><div style="clear: both;"></div></td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" style="width: 100%; <?php if($salesOrder['Branch']['name_other'] == ''){?> margin-top: -40px; <?php }?>">
        <tbody>
            <tr>
                <td style="">
                    <table cellpadding="0" cellspacing="0" style="width: 100%; margin-top: 0px;">
                        <tr>
                            <td style="font-family: cambria; line-height: 25px; font-size: 17px; text-align: center; vertical-align: bottom;">
                                <span style="font-size:17px;font-family: 'Moul';">
                                    <?php echo TABLE_TAX_INVOICE_KH;?>
                                </span> <br>
                                <span style="font-size: 18px; font-weight: bold;font-family: cambria; text-transform: uppercase;">
                                    <?php echo TABLE_TAX_INVOICE_EN;?>
                                </span>
                            </td>
                        </tr>
                    </table>
                    <table cellpadding="0" cellspacing="0" style="margin-top: 5px; height: 100px; width: 100%; vertical-align: top;">
                        <tr>
                            <td style="border-width:3px;  width:55%;font-size: 12px; padding-top: 9px; padding-bottom: 8px; vertical-align: top;">
                                <table width="100%" cellpadding="0" cellspacing="0" style="border:none; line-height: 15px; vertical-align: top;">
                                    <tr>
                                        <td style="font-family: 'Moul'; font-size: 12px; width: 38%;">
                                            <?php echo TABLE_CUSTOMER_KH;?> / 
                                            <span style="">
                                            <?php echo TABLE_CUSTOMER_EN;?>:
                                            </span>
                                        </td>
                                        <td style="width: 38%; vertical-align: top;">
                                            <table style="margin-top:-1px;">
                                                <tr>
                                                    <td style="font-size:12px;"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:40%; font-size: 12px; padding-top: 5px;">
                                            <span style="font-size:11px;"><?php echo TABLE_CUSTOMER_NAME_CUSTOMER_KH;?> : <?php echo $salesOrder['Customer']['name_kh']; ?></span><br />
                                            <?php echo TABLE_CUSTOMER_NAME_CUSTOMER_EN;?> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%; font-size: 12px; padding-top: 5px;">
                                            <span style="font-size:11px;"><?php echo TABLE_ADDRESS_KH;?>  :  <?php echo $salesOrder['Customer']['address_other']; ?></span><br />
                                            <?php echo 'Address';?> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:40%; font-size: 12px; padding-top: 5px;">
                                            <span style="font-size:11px;"><?php echo TABLE_PHONE_NUMBER_KH;?>  :  <?php echo $salesOrder['Customer']['main_number']; ?></span><br />
                                            <?php echo TABLE_PHONE_NUMBER_EN;?> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%; font-size: 12px; padding-top: 4px;">
                                            <span style="font-size:11px;"><?php echo TABLE_VATTIN_KH;?></span><span style="font-size:11px; padding-left: 5px; text-transform: uppercase;">(<?php echo TABLE_VATTIN_EN;?>)</span>  : 
                                            <span style="font-size:10px; padding-top: 8px;">
                                            <?php 
                                                if($salesOrder['Customer']['vat'] != ""){
                                                    $vatCustomerConvert = str_split($salesOrder['Customer']['vat']);
                                                    $countVarCustmoer   = count($vatCustomerConvert);
                                                    for($i = 0; $i< $countVarCustmoer; $i++){
                                                        if($i == 0){
                                                            echo "<span style='border: 1px solid #00afc1; padding: 1px 2px; margin-top:1px !mportant;'>".$vatCustomerConvert[$i]."</span>";
                                                        }else if($i == 4){
                                                            echo "<span> - </span>";
                                                            echo "<span style='border: 1px solid #00afc1; padding: 1px 2px; margin-top:1px !mportant; margin-left: 2px;'>".$vatCustomerConvert[$i]."</span>";
                                                        }else{
                                                            echo "<span style='border: 1px solid #00afc1; padding: 1px 2px;  margin-top:1px !mportant; margin-left: 2px;'>".$vatCustomerConvert[$i]."</span>";
                                                        }
                                                    }
                                                }
                                            ?>  
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width:1%;"></td>
                            <td style="border-width:3px;  width:40%; font-size: 12px; vertical-align: top;padding-left: 5px; padding-top: 0px;">
                                <table style="width: 100%; padding: 0;border:none; line-height: 15px; vertical-align: top;">
                                    <tr>
                                        <td style="width:40%; font-size: 12px;">
                                            <span style="font-size:11px;"><?php echo TABLE_INVOICE_VAT_KH;?>  : </span><br />
                                            <?php echo TABLE_INVOICE_VAT_EN;?> <?php echo TABLE_NO_EN; ?>
                                        </td>
                                        <td style="width: 60%; vertical-align: top; padding-top: 5px;">
                                            <div style="margin-top: -2px;  font-size: 12px; font-weight: bold;">
                                                <?php 
                                                echo $salesOrder['SalesOrder']['so_code']; 
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:40%;font-size: 12px; padding-top: 5px;">
                                            <span style="font-size:11px;"><?php echo TABLE_DATE_KH;?></span><br />
                                            <?php echo TABLE_DATE_EN;?> <?php echo TABLE_NO_EN; ?>
                                        </td>
                                        <td style="width:60%; font-size: 12px; vertical-align: top;">
                                            <div style="font-size: 12px; padding-top: 8px; vertical-align: top; margin-top: 3px;">
                                                <?php 
                                                if(dateShort($salesOrder['SalesOrder']['order_date'], "d m Y") != ""){
                                                    $vatCustomerConvert = str_split(dateShort($salesOrder['SalesOrder']['order_date'], "d m Y"));
                                                    $countVarCustmoer   = count($vatCustomerConvert);
                                                    for($i = 0; $i< $countVarCustmoer; $i++){
                                                        if($i == 0){
                                                            echo "<span style='border: 1px solid #00afc1; padding: 4px 3px; margin-top:3px !mportant;'>".$vatCustomerConvert[$i]."</span>";
                                                        }else if($i == 2 || $i == 5){
                                                            echo "<span>  </span>";
                                                        }else{
                                                            echo "<span style='border: 1px solid #00afc1; padding: 4px 3px;  margin-top:3px !mportant; margin-left: 2px;'>".$vatCustomerConvert[$i]."</span>";
                                                        }
                                                    }
                                                }
                                            ?>  
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div style="height: 5px"></div>
                    <table class="table_print" style="margin-top: 0px; border: none;">
                        <tr>
                            <th class="first titleHeaderTable" style="  border-bottom-width:3px; border-bottom-style:double;  line-height: 15px; width: 5%;"><?php echo TABLE_NO_VAT_SALE_INVOICE;?></th>
                            <th class="titleHeaderTable" style="border-bottom-width:3px; border-bottom-style:double;  line-height: 15px; "><?php echo TABLE_DESCRIPTION_VAT_SALE_INVOICE;?></th>
                            <th class="titleHeaderTable" style="border-bottom-width:3px; border-bottom-style:double;  width: 10%; line-height: 15px; "><?php echo TABLE_QTY_VAT_SALE_INVOICE;?></th>
                            <th class="titleHeaderTable" style="border-bottom-width:3px; border-bottom-style:double;  width: 19%; line-height: 15px; "><?php echo TABLE_UNIT_PRICE_VAT_SALE_INVOICE;?></th>
                            <th class="titleHeaderTable" style=" border-bottom-width:3px; border-bottom-style:double;    width: 15%;line-height: 15px; "><?php echo TABLE_AMOUNT_VAT_SALE_INVOICE;?></th>
                        </tr>
                        <?php
                        $index = 0;
                        $productNameKh = '';                  
                        if (!empty($salesOrderDetails)) {
                            foreach ($salesOrderDetails as $salesOrderDetail) {
                                // Check Name With Customer
                                $productName = $salesOrderDetail['Product']['name'];
                                $productNameKh = $salesOrderDetail['Product']['name_kh'];
                                $unitPriceAfterDis = $salesOrderDetail['SalesOrderDetail']['unit_price'];
                                if($salesOrderDetail['SalesOrderDetail']['discount_amount'] > 0){
                                    $unitPriceAfterDis = ($salesOrderDetail['SalesOrderDetail']['total_price'] - $salesOrderDetail['SalesOrderDetail']['discount_amount'])/$salesOrderDetail['SalesOrderDetail']['qty'];
                                }
                                ?>
                                <tr class="rowListDN">
                                    <td style="  text-align: center; font-size: 12px; height: 25px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php echo ++$index; ?>
                                    </td>      
<!--                                   <td style="font-size: 12px; padding-top: 0px; padding-bottom: 0px;"><?php echo $salesOrderDetail['Product']['barcode'];?></td>-->
                                   <td style="font-size: 12px; padding-top: 0px; padding-bottom: 0px; line-height: 15px;">
                                        <?php if($productNameKh != ''){echo $productNameKh.'<br>';}?><?php echo $productName;?>
                                    </td>
                                   <!--  <td style="text-align: center; font-size: 12px; padding:0px 0px 0px 0px;"><?php echo $salesOrderDetail['Uom']['name']; ?></td>-->
                                    <td style="text-align: center; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php
                                        echo number_format($salesOrderDetail['SalesOrderDetail']['qty'], 0);
                                        ?>
                                    </td>                                   
                                    <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span>
                                        <?php echo number_format($unitPriceAfterDis, 2);?>
                                    </td>
                                    <td style="  text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span>
                                        <?php echo number_format($salesOrderDetail['SalesOrderDetail']['total_price'] - $salesOrderDetail['SalesOrderDetail']['discount_amount'], 2); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        if (!empty($salesOrderServices)) {
                            foreach ($salesOrderServices AS $salesOrderService) {
                                $unitPriceAfterDis = $salesOrderService['SalesOrderService']['unit_price'];
                                if($salesOrderService['SalesOrderService']['discount_amount'] > 0){
                                    $unitPriceAfterDis = ($salesOrderService['SalesOrderService']['total_price'] - $salesOrderService['SalesOrderService']['discount_amount'])/$salesOrderService['SalesOrderService']['qty'];
                                }
                                ?>
                                <tr class="rowListDN">
                                    <td style="  text-align: center; font-size: 12px; height: 25px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php echo ++$index; ?>
                                    </td>
                                    <td style="font-size: 12px; padding-top: 0px; padding-bottom: 0px;">                                       
                                        <?php
                                        echo $salesOrderService['Service']['name'];
                                        if (trim($salesOrderService['Service']['description']) != "") {
                                            echo '<br/>';
                                            echo '<p style="padding-left:10px;font-size:10px;">' . nl2br($salesOrderService['Service']['description']) . '</p>';
                                        }                                       
                                        ?>
                                    </td>
                                    <td style="text-align: center; font-size: 12px; padding-top: 0px; padding-bottom: 0px;"></td>
                                    <td style="text-align: center; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php
                                        echo number_format($salesOrderService['SalesOrderService']['qty'], 0);
                                        ?>
                                    </td>
                                    <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span><?php echo number_format($unitPriceAfterDis, 2); ?>
                                    </td>
                                    <td style="  text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span>
                                        <?php echo number_format($salesOrderService['SalesOrderService']['total_price'] - $salesOrderService['SalesOrderService']['discount_amount'], 2); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        if (!empty($salesOrderMiscs)) {
                            foreach ($salesOrderMiscs AS $salesOrderMisc) {
                                $unitPriceAfterDis = $salesOrderMisc['SalesOrderMisc']['unit_price'];
                                if($salesOrderMisc['SalesOrderMisc']['discount_amount'] > 0){
                                    $unitPriceAfterDis = ($salesOrderMisc['SalesOrderMisc']['total_price'] - $salesOrderMisc['SalesOrderMisc']['discount_amount'])/$salesOrderMisc['SalesOrderMisc']['qty'];
                                }
                                ?>
                                <tr class="rowListDN">
                                    <td style="  text-align: center; font-size: 12px; height: 25px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php echo ++$index; ?>
                                    </td>
                                    <td style="font-size: 12px; padding-top: 0px; padding-bottom: 0px;">                                        
                                        <?php
                                        echo $salesOrderMisc['SalesOrderMisc']['description'];                                        
                                        ?>
                                    </td>
                                    <td style="text-align: center; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <?php
                                        echo number_format($salesOrderMisc['SalesOrderMisc']['qty'], 0);
                                        ?>
                                    </td>
                                    <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span><?php echo number_format($unitPriceAfterDis, 2);?>
                                    </td>
                                    <td style="  text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                        <span style="float: left; width: 12px; font-size: 12px;">$</span>
                                        <?php echo number_format($salesOrderMisc['SalesOrderMisc']['total_price'] - $salesOrderMisc['SalesOrderMisc']['discount_amount'], 2); ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        $i = 14;
                        $row = $i - $index;
                        for($z = 1;$z<$row;$z++){
                        ?>
                        <tr class="rowListDN">
                            <td style="text-align: center; height: 25px;font-size: 12px; border-bottom:1px solid !important;  padding-top:0px; padding-bottom:0px;border-bottom: none;"></td>                                  
                            <td style="font-size: 12px; height: 25px;  padding-top:0px; padding-bottom:0px;border-bottom: none; border:1px solid !important; "></td>
                            <td style="text-align: center; font-size: 12px;  height: 25px;  padding-top:0px; padding-bottom:0px;border-bottom: none; border:1px solid !important; "></td>                                   
                            <td style="text-align: right; font-size: 12px; padding-top:0px ;height: 25px;  padding-bottom:0px;border-bottom: none; border:1px solid !important; "></td>
                            <td style="text-align: right; font-size: 12px; border-bottom:1px solid !important; padding-top:0px; height: 25px;  padding-bottom:0px;border-bottom: none;"> </td>
                        </tr>        
                        <?php $i++;}
                        $rowspan = 3;
                        if($salesOrder['SalesOrder']['discount'] > 0){
                            $rowspan = 4;
                        }else{
                            $rowspan = 3;
                        }
                        ?>   
                        <tr class="rowListDN">
                            <td style="   text-align: center; height: 25px;padding-top: 0px; padding-bottom: 0px;"></td>                                  
                            <td style=" font-size: 12px; height: 25px;padding-top: 0px; padding-bottom: 0px;"></td>
                            <td style=" text-align: center; font-size: 12px;padding-top: 0px; padding-bottom: 0px;"></td>                                   
                            <td style=" text-align: right; font-size: 12px;padding-top: 0px; padding-bottom: 0px;"></td>
                            <td style="   text-align: right; font-size: 12px;padding-top: 0px; padding-bottom: 0px;"> </td>
                        </tr>   
                        <tr>
                            <td colspan="4" style=" line-height: 18px;  text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px;">
                                <span style="font-size: 12px;">សរុប</span> <br> Sub Total
                            </td>
                            <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px; font-weight: bold;">
                                <span style="float: left; width: 12px; font-size: 12px; font-weight: bold;">$</span><?php echo number_format(($salesOrder['SalesOrder']['total_amount']), 2);?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style=" line-height: 18px;  text-align: right; font-size: 12px;height: 15px; padding-top: 0px; padding-bottom: 0px;"><span style="font-size: 12px;">អាករលើតម្លៃបន្ថែម​ (<?php echo number_format($salesOrder['SalesOrder']['vat_percent'], 0) ?>%) </span> <br> <span style="font-size:11px;">VAT (<?php echo number_format($salesOrder['SalesOrder']['vat_percent'], 0) ?>%)</span></td>
                            <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px; font-weight: bold;"><span style="float: left; width: 12px; font-size: 12px;font-weight: bold;">$</span><?php echo number_format(($salesOrder['SalesOrder']['total_vat']), 2); ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="  line-height: 18px;  text-align: right; font-size: 12px; height: 15px; padding-top: 4px; padding-bottom: 0px;"><span style="font-size: 12px;"> សរុបរួម </span> <br> Grand Total</td>
                            <td style="text-align: right; font-size: 12px; padding-top: 0px; padding-bottom: 0px; font-weight: bold;"><span style="float: left; width: 12px; font-size: 12px; font-weight: bold;">$</span><?php echo number_format(($salesOrder['SalesOrder']['total_amount'] - $salesOrder['SalesOrder']['discount'] + $salesOrder['SalesOrder']['total_vat']), 2); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td style="height: 50px; font-size: 12px; text-align: right;">
                   <div style=" margin-top: 20px;"  id="footerTablePrint">
                        <table style="width: 100%;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="height: 130px; font-size: 10px; text-align: right;">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 33%; vertical-align: bottom; text-align: center; height: 130px;">
                                                <div style=" margin: 0px auto; width: 70%; border-top: 1px solid #000; text-align: center; font-size: 10px; font-weight: bold; font-family: 'Calibri'">
                                                    <span style='font-size: 12px; font-weight: bold;'>ហត្ថលេខា និងឈ្មោះអ្នកទិញ </span><br />
                                                    Customer's Signature & Name
                                                </div>
                                            </td>
                                            <td style="width: 34%; vertical-align: bottom; text-align: center;">
                                                <div style=" margin: 0px auto; width: 70%; border-top: 0px solid #000; text-align: center; font-size: 10px; font-weight: bold;">

                                                </div>
                                            </td>
                                            <td style="width: 33%; vertical-align: bottom; text-align: center;">
                                                <div style=" margin: 0px auto; width: 70%; border-top: 1px solid #000; text-align: center; font-size: 10px; font-weight: bold; font-family: 'Calibri'">
                                                    <span style='font-size: 12px; font-weight: bold;'>ហត្ថលេខា និងឈ្មោះអ្នកលក់</span> <br />
                                                    Seller's Signature & Name
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td>
                                    <table style="width:100%; text-align: left; margin-top: 10px; margin-bottom: 10px;">
                                        <tr>
                                            <td style="width:5%; font-size: 12px; font-weight: bold;">សម្គាល់</td>
                                            <td style="width:1%; font-size: 12px; font-weight: bold;">:</td>
                                            <td style="width:89%; font-size: 12px; font-weight: bold;">ច្បាប់ដើមសម្រាប់អ្នកទិញ ច្បាប់ចម្លងសម្រាប់អ្នកលក់</td>
                                        </tr>
                                        <tr>
                                            <td style="width:5%; font-size: 12px; font-weight: bold;">Note</td>
                                            <td style="width:1%; font-size: 12px; font-weight: bold;">:</td>
                                            <td style="width:89%; font-size: 12px; ">Original invoice for customer, copied invoice for seller</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </tfoot>
    </table>
    <br />
    <div style="width: 450px;">
        <div style="float:left;">
            <input type="button" value="<?php echo ACTION_PRINT; ?>" style="height: 75px;" id='btnDisappearPrint' class='noprint' />
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-1.4.4.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $(document).dblclick(function() {
            window.close();
        });
        $("#btnDisappearPrint").click(function() {
            $("#footerTablePrint").show();
            $("#footerTablePrint").css("width", "100%");
            //Default printing if jsPrintsetup is not available
            window.print();
            window.close();  
        });
        $("#btnExportExcelSalesInvoice").click(function(){
            window.open("<?php echo $this->webroot; ?>public/report/sales_invoice_vat_<?php echo $user['User']['id']; ?>.csv", "_self");
        });
    });
</script>
