<?php
$sqlSym = mysql_query("SELECT symbol FROM currency_centers WHERE id = (SELECT currency_center_id FROM companies WHERE is_active = 1 LIMIT 1)");
$rowSym = mysql_fetch_array($sqlSym);
?>
<script type="text/javascript">
    $(document).ready(function(){
        // Prevent Key Enter
        preventKeyEnter();
        $(".btnBackService").click(function(event){
            event.preventDefault();
            oCache.iCacheLower = -1;
            oTableService.fnDraw(false);
            var rightPanel=$(this).parent().parent().parent();
            var leftPanel=rightPanel.parent().find(".leftPanel");
            rightPanel.hide();rightPanel.html("");
            leftPanel.show("slide", { direction: "left" }, 500);
        });
    });
</script>
<div style="padding: 5px;border: 1px dashed #bbbbbb;">
    <div class="buttons">
        <a href="" class="positive btnBackService">
            <img src="<?php echo $this->webroot; ?>img/button/left.png" alt=""/>
            <?php echo ACTION_BACK; ?>
        </a>
    </div>
    <div style="clear: both;"></div>
</div>
<br />
<table width="100%" class="info">
    <tr>
        <th><?php __(TABLE_CODE); ?></th>
        <td><?php echo $service['Service']['code']; ?></td>
    </tr>
    <tr>
        <th><?php __(TABLE_NAME); ?></th>
        <td><?php echo $service['Service']['name']; ?></td>
    </tr>
    <tr>
        <th><?php __(MENU_PRODUCT_GROUP_MANAGEMENT); ?></th>
        <td><?php echo $service['Pgroup']['name']; ?></td>
    </tr>
    <tr>
        <th><?php __(TABLE_UOM); ?></th>
        <td>
            <?php 
            if(!empty($service['Service']['uom_id'])){
                $sqlUom = mysql_query("SELECT name FROM uoms WHERE id = ".$service['Service']['uom_id']);
                $rowUom = mysql_fetch_array($sqlUom);
                echo $rowUom[0];
            } 
            ?>
        </td>
    </tr>
    <tr>
        <th><?php __(SALES_ORDER_UNIT_PRICE); ?></th>
        <td><?php echo number_format($service['Service']['unit_price']); ?> <?php echo $rowSym[0]; ?></td>
    </tr>
    <tr>
        <th><?php __(GENERAL_DESCRIPTION); ?></th>
        <td><?php echo nl2br($service['Service']['description']); ?></td>
    </tr>
</table>